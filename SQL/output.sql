USE [Amanat]
GO

/****** Object:  StoredProcedure [dbo].[ApplyFunction]    Script Date: 29.05.2018 16:20:39 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[ApplyFunction]
	@FunctionName [nvarchar](4000),
	@InputValue [nvarchar](4000),
	@OutputValue [nvarchar](4000) OUTPUT
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [LEADEngine].[LEADEngine].[ApplyFunction]
GO


USE [Amanat]
GO

/****** Object:  StoredProcedure [dbo].[CLRGetVariable]    Script Date: 29.05.2018 16:18:13 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[CLRGetVariable]
	@GUID [nvarchar](4000),
	@ParameterType_id [int],
	@Value [nvarchar](4000) OUTPUT,
	@IsExist [bit] OUTPUT,
	@FilterCondition [nvarchar](4000) OUTPUT,
	@IsApplication [bit] OUTPUT,
	@Priority [int] OUTPUT
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [LEADEngine].[LEADEngine].[CLRGetVariable]
GO


USE [Amanat]
GO

/****** Object:  StoredProcedure [dbo].[CLR_SetVariable]    Script Date: 29.05.2018 16:23:16 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[CLR_SetVariable]
	@GUID [uniqueidentifier],
	@ParameterType_id [int],
	@Value [nvarchar](4000),
	@IsError [bit] OUTPUT,
	@FilterCondition [nvarchar](4000),
	@Priority [int],
	@IsApplication [int]
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [LEADEngine].[LEADEngine].[CLR_SetVariable]
GO


USE [Amanat]
GO

/****** Object:  UserDefinedAggregate [dbo].[ColumnSequence]    Script Date: 29.05.2018 16:09:45 ******/
CREATE AGGREGATE [dbo].[ColumnSequence]
(@value [nvarchar](4000))
RETURNS[nvarchar](4000)
EXTERNAL NAME [LEADEngine].[ColumnSequence]
GO


USE [Amanat]
GO

/****** Object:  UserDefinedFunction [dbo].[CompareFirstWords]    Script Date: 29.05.2018 16:39:21 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE FUNCTION [dbo].[CompareFirstWords](@FirstString [nvarchar](4000), @SecondString [nvarchar](4000))
RETURNS [bit] WITH EXECUTE AS CALLER
AS 
EXTERNAL NAME [LEADEngine].[LEADEngine].[CompareFirstWords]
GO


USE [Amanat]
GO

/****** Object:  UserDefinedAggregate [dbo].[Concatenate]    Script Date: 29.05.2018 16:12:07 ******/
CREATE AGGREGATE [dbo].[Concatenate]
(@value [nvarchar](4000))
RETURNS[nvarchar](4000)
EXTERNAL NAME [LEADEngine].[Concatenate]
GO


USE [Amanat]
GO

/****** Object:  StoredProcedure [dbo].[ConstructMiningCondition]    Script Date: 29.05.2018 16:32:06 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[ConstructMiningCondition]
	@UserQuery_id [int],
	@SQLStatement [nvarchar](4000),
	@SQLCondition [nvarchar](4000) OUTPUT
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [LEADEngine].[LEADEngine].[ConstructMiningCondition]
GO


USE [Amanat]
GO

/****** Object:  StoredProcedure [dbo].[CountRecords]    Script Date: 29.05.2018 16:19:04 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[CountRecords]
	@TableName [nvarchar](4000),
	@RowCount [int] OUTPUT
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [LEADEngine].[LEADEngine].[CountRecords]
GO


USE [Amanat]
GO

/****** Object:  StoredProcedure [dbo].[CountRowsForSQL]    Script Date: 29.05.2018 16:35:49 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[CountRowsForSQL]
	@SQLStatement [nvarchar](4000),
	@RowCount [int] OUTPUT
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [LEADEngine].[LEADEngine].[CountRowsForSQL]
GO


USE [Amanat]
GO

/****** Object:  StoredProcedure [dbo].[CreateQueryObject]    Script Date: 29.05.2018 16:27:40 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[CreateQueryObject]
	@TableGUID [nvarchar](4000),
	@ObjectName [nvarchar](4000),
	@SortType [nvarchar](4000),
	@SortPriority [int],
	@ReporterLayer_id [int] = 0
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [LEADEngine].[LEADEngine].[CreateQueryObject]
GO


USE [Amanat]
GO

/****** Object:  StoredProcedure [dbo].[CreateTransactionBySchema]    Script Date: 29.05.2018 16:30:04 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[CreateTransactionBySchema]
	@TransactionType_id [int],
	@User_id [int],
	@ParentTransaction_id [int],
	@ParentTable_id [int],
	@ParentRow [int],
	@ObjectSchema [xml],
	@LastError [nvarchar](4000) OUTPUT,
	@Transaction_id [int] OUTPUT
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [LEADEngine].[LEADEngine].[CreateTransactionBySchema]
GO


USE [Amanat]
GO

/****** Object:  StoredProcedure [dbo].[DropQuerySchema]    Script Date: 29.05.2018 16:28:59 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[DropQuerySchema]
	@SchemaGUID [nvarchar](4000)
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [LEADEngine].[LEADEngine].[DropQuerySchema]
GO


USE [Amanat]
GO

/****** Object:  StoredProcedure [dbo].[ExecCommand]    Script Date: 29.05.2018 16:25:16 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[ExecCommand]
	@ConnectionString [nvarchar](4000),
	@CommandString [nvarchar](4000),
	@ReturnResult [bit],
	@LastError [nvarchar](4000) OUTPUT
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [LEADEngine].[LEADEngine].[ExecCommand]
GO


USE [Amanat]
GO

/****** Object:  StoredProcedure [dbo].[ExecuteTransaction]    Script Date: 29.05.2018 16:21:17 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[ExecuteTransaction]
	@GUID [uniqueidentifier],
	@IsNormal [bit] OUTPUT,
	@ErrorNumber [int] OUTPUT,
	@ErrorMessage [nvarchar](4000) OUTPUT
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [LEADEngine].[LEADEngine].[ExecuteTransaction]
GO


USE [Amanat]
GO

/****** Object:  StoredProcedure [dbo].[FieldOfRecordset]    Script Date: 29.05.2018 16:31:26 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[FieldOfRecordset]
	@SQL [nvarchar](4000),
	@FieldName [nvarchar](4000)
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [LEADEngine].[LEADEngine].[FieldOfRecordset]
GO


USE [Amanat]
GO

/****** Object:  StoredProcedure [dbo].[ForceInsertBatch]    Script Date: 29.05.2018 16:14:44 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[ForceInsertBatch]
	@LocalCommand [nvarchar](4000),
	@ConnectionString [nvarchar](4000),
	@TargetTable [nvarchar](4000),
	@TargetPrepare [nvarchar](4000),
	@LastError [nvarchar](4000) OUTPUT,
	@FinishCommand [nvarchar](4000)
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [LEADEngine].[LEADEngine].[ForceInsertBatch]
GO


USE [Amanat]
GO

/****** Object:  StoredProcedure [dbo].[GetObjectsSQL]    Script Date: 29.05.2018 16:28:15 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[GetObjectsSQL]
	@ObjectsTable [nvarchar](4000),
	@Condition [nvarchar](4000),
	@GroupType [nvarchar](4000),
	@DirectWhere [nvarchar](4000),
	@ErrorMessage [nvarchar](4000) OUTPUT,
	@IsNormal [bit] OUTPUT,
	@SQLStatement [nvarchar](4000) OUTPUT,
	@IsIgnoreGrouping [bit],
	@ReporterLayer_id [int] = 0
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [LEADEngine].[LEADEngine].[GetObjectsSQL]
GO


USE [Amanat]
GO

/****** Object:  StoredProcedure [dbo].[GetTableColumn]    Script Date: 29.05.2018 16:20:02 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[GetTableColumn]
	@TableName [nvarchar](4000),
	@KeyColumn [nvarchar](4000),
	@GetColumn [nvarchar](4000),
	@KeyValue [nvarchar](4000),
	@Value [nvarchar](4000) OUTPUT
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [LEADEngine].[LEADEngine].[GetTableColumn]
GO


USE [Amanat]
GO

/****** Object:  StoredProcedure [dbo].[GetTransactionProperty]    Script Date: 29.05.2018 16:27:08 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[GetTransactionProperty]
	@TransactionGUID [uniqueidentifier],
	@PropertyName [nvarchar](4000),
	@PropertyValue [nvarchar](4000) OUTPUT
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [LEADEngine].[LEADEngine].[GetTransactionProperty]
GO


USE [Amanat]
GO

/****** Object:  StoredProcedure [dbo].[GetUserQueryAdvanced]    Script Date: 29.05.2018 16:34:02 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[GetUserQueryAdvanced]
	@UserQuery_id [int],
	@TopRows [int],
	@ReporterFilter_id [int],
	@TotalRows [int] OUTPUT,
	@SQLStatement [nvarchar](4000) OUTPUT,
	@DirectWhere [nvarchar](4000),
	@IsIgnoreGrouping [bit]
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [LEADEngine].[LEADEngine].[GetUserQueryAdvanced]
GO


USE [Amanat]
GO

/****** Object:  StoredProcedure [dbo].[GetUserQuerySQL]    Script Date: 29.05.2018 16:29:35 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[GetUserQuerySQL]
	@UserQuery_id [int],
	@SQLStatement [nvarchar](4000) OUTPUT,
	@ReporterLayer_id [int] = 0
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [LEADEngine].[LEADEngine].[GetUserQuerySQL]
GO


USE [Amanat]
GO

/****** Object:  StoredProcedure [dbo].[GetWaveCode]    Script Date: 29.05.2018 16:24:33 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[GetWaveCode]
	@WaveType_id [int],
	@SQLStatement [nvarchar](4000)
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [LEADEngine].[LEADEngine].[GetWaveCode]
GO


USE [Amanat]
GO

/****** Object:  StoredProcedure [dbo].[GetWaveCodeOnce]    Script Date: 29.05.2018 16:36:26 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[GetWaveCodeOnce]
	@WaveType_id [int],
	@SQLStatement [nvarchar](4000),
	@WaveCode [nvarchar](4000) OUTPUT
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [LEADEngine].[LEADEngine].[GetWaveCodeOnce]
GO


USE [Amanat]
GO

/****** Object:  StoredProcedure [dbo].[GetXMLCube]    Script Date: 29.05.2018 16:17:03 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[GetXMLCube]
	@ObjectXML [xml],
	@ReporterFilter_id [int],
	@DirectWhere [nvarchar](4000),
	@IsIgnoreGrouping [bit],
	@SQL [nvarchar](4000) OUTPUT,
	@ReporterLayer_id [int] = 0
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [LEADEngine].[LEADEngine].[GetXMLCube]
GO


USE [Amanat]
GO

/****** Object:  UserDefinedAggregate [dbo].[PutawayAggregate]    Script Date: 29.05.2018 16:12:49 ******/
CREATE AGGREGATE [dbo].[PutawayAggregate]
(@value [nvarchar](4000))
RETURNS[nvarchar](4000)
EXTERNAL NAME [LEADEngine].[PutawayAggregate]
GO


USE [Amanat]
GO

/****** Object:  UserDefinedFunction [dbo].[ReplicateValue]    Script Date: 29.05.2018 16:38:34 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE FUNCTION [dbo].[ReplicateValue](@Value [nvarchar](4000), @Prefix [nvarchar](4000), @Length [int])
RETURNS [nvarchar](4000) WITH EXECUTE AS CALLER
AS 
EXTERNAL NAME [LEADEngine].[LEADEngine].[ReplicateValue]
GO


USE [Amanat]
GO

/****** Object:  StoredProcedure [dbo].[ReportingServicesRequest]    Script Date: 29.05.2018 16:33:26 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[ReportingServicesRequest]
	@URL [nvarchar](4000),
	@ReportName [nvarchar](4000),
	@PrinterName [nvarchar](4000),
	@ReportParams [nvarchar](4000),
	@PageLimit [int],
	@IsLandscape [bit],
	@IsFinished [bit] OUTPUT,
	@Copies [int]
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [LEADEngine].[LEADEngine].[ReportingServicesRequest]
GO


USE [Amanat]
GO

/****** Object:  StoredProcedure [dbo].[ResultToFile]    Script Date: 29.05.2018 16:25:54 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[ResultToFile]
	@FilePath [nvarchar](4000),
	@FileName [nvarchar](4000),
	@ColumnDivider [nvarchar](4000),
	@RowDivider [nvarchar](4000),
	@RecordsetDivider [nvarchar](4000),
	@IsIncludeNames [bit],
	@SQL [nvarchar](4000)
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [LEADEngine].[LEADEngine].[ResultToFile]
GO


USE [Amanat]
GO

/****** Object:  StoredProcedure [dbo].[RetrieveRecordset]    Script Date: 29.05.2018 16:26:25 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[RetrieveRecordset]
	@SQLStatement [nvarchar](4000),
	@ObjectGUID [nvarchar](4000)
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [LEADEngine].[LEADEngine].[RetrieveRecordset]
GO


USE [Amanat]
GO

/****** Object:  StoredProcedure [dbo].[RowAsRecordset]    Script Date: 29.05.2018 16:32:41 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[RowAsRecordset]
	@SQLStatement [nvarchar](4000)
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [LEADEngine].[LEADEngine].[RowAsRecordset]
GO


USE [Amanat]
GO

/****** Object:  StoredProcedure [dbo].[RunTransaction]    Script Date: 29.05.2018 16:22:28 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[RunTransaction]
	@GUID [uniqueidentifier],
	@ErrorMessage [nvarchar](4000) OUTPUT,
	@IsNormal [bit] OUTPUT
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [LEADEngine].[LEADEngine].[RunTransaction]
GO


USE [Amanat]
GO

/****** Object:  UserDefinedAggregate [dbo].[Sequence]    Script Date: 29.05.2018 16:11:20 ******/
CREATE AGGREGATE [dbo].[Sequence]
(@value [nvarchar](4000))
RETURNS[nvarchar](4000)
EXTERNAL NAME [LEADEngine].[Sequence]
GO


USE [Amanat]
GO

/****** Object:  StoredProcedure [dbo].[SetDebug]    Script Date: 29.05.2018 16:35:07 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[SetDebug]
	@IsDebug [bit]
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [LEADEngine].[LEADEngine].[SetDebug]
GO


USE [Amanat]
GO

/****** Object:  StoredProcedure [dbo].[SetTransactionProperty]    Script Date: 29.05.2018 16:23:50 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[SetTransactionProperty]
	@TransactionGUID [uniqueidentifier],
	@PropertyName [nvarchar](4000),
	@PropertyValue [nvarchar](4000)
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [LEADEngine].[LEADEngine].[SetTransactionProperty]
GO


USE [Amanat]
GO

/****** Object:  StoredProcedure [dbo].[ShowSessionGUID]    Script Date: 29.05.2018 16:36:58 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[ShowSessionGUID]
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [LEADEngine].[LEADEngine].[ShowSessionGUID]
GO


USE [AMANAT]
GO

/****** Object:  UserDefinedAggregate [dbo].[ValueSequence]    Script Date: 29.05.2018 16:06:17 ******/
CREATE AGGREGATE [dbo].[ValueSequence]
(@value [nvarchar](4000))
RETURNS[nvarchar](4000)
EXTERNAL NAME [LEADEngine].[ValueSequence]
GO


USE [Amanat]
GO

/****** Object:  StoredProcedure [dbo].[XMLFinishAction]    Script Date: 29.05.2018 16:37:24 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[XMLFinishAction]
	@ActionSystemCode [nvarchar](4000),
	@Action_id [int]
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [LEADEngine].[LEADEngine].[XMLFinishAction]
GO


USE [Amanat]
GO

/****** Object:  StoredProcedure [dbo].[XMLPostEvent]    Script Date: 29.05.2018 16:30:34 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[XMLPostEvent]
	@EventSystemCode [nvarchar](4000),
	@EventParameters [xml]
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [LEADEngine].[LEADEngine].[XMLPostEvent]
GO


USE [Amanat]
GO

/****** Object:  StoredProcedure [dbo].[XMLRetrieveRecordset]    Script Date: 29.05.2018 16:16:03 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[XMLRetrieveRecordset]
	@SQLStatement [nvarchar](4000),
	@ObjectXML [xml]
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [LEADEngine].[LEADEngine].[XMLRetrieveRecordset]
GO


USE [Amanat]
GO

/****** Object:  StoredProcedure [dbo].[XMLStartAction]    Script Date: 29.05.2018 16:34:41 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[XMLStartAction]
	@ActionSystemCode [nvarchar](4000),
	@ActionParameters [xml],
	@Action_id [int] OUTPUT
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [LEADEngine].[LEADEngine].[XMLStartAction]
GO


