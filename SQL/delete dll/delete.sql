USE [Amanat]
GO

DROP AGGREGATE ValueSequence;
DROP AGGREGATE [dbo].[ColumnSequence];
DROP AGGREGATE [dbo].[Sequence];
DROP AGGREGATE [dbo].[Concatenate];
DROP AGGREGATE [dbo].[PutawayAggregate];
DROP PROCEDURE [dbo].[ForceInsertBatch];
DROP PROCEDURE [dbo].[XMLRetrieveRecordset];
DROP PROCEDURE [dbo].[GetXMLCube];
DROP PROCEDURE [dbo].[CLRGetVariable];
DROP PROCEDURE [dbo].[CountRecords];
DROP PROCEDURE [dbo].[GetTableColumn];
DROP PROCEDURE [dbo].[ApplyFunction];
DROP PROCEDURE [dbo].[ExecuteTransaction];
DROP PROCEDURE [dbo].[RunTransaction];
DROP PROCEDURE [dbo].[CLR_SetVariable];
DROP PROCEDURE SetTransactionProperty;
DROP PROCEDURE GetWaveCode;
DROP PROCEDURE ExecCommand;
DROP PROCEDURE ResultToFile;
DROP PROCEDURE RetrieveRecordset;
DROP PROCEDURE GetTransactionProperty;
DROP PROCEDURE CreateQueryObject;
DROP PROCEDURE [dbo].[GetObjectsSQL];
DROP PROCEDURE [dbo].[DropQuerySchema];
DROP PROCEDURE GetUserQuerySQL;
DROP PROCEDURE CreateTransactionBySchema;
DROP PROCEDURE XMLPostEvent;
DROP PROCEDURE FieldOfRecordset;
DROP PROCEDURE ConstructMiningCondition;
DROP PROCEDURE RowAsRecordset;
DROP PROCEDURE ReportingServicesRequest;
DROP PROCEDURE GetUserQueryAdvanced;
DROP PROCEDURE XMLStartAction;
DROP PROCEDURE [dbo].[SetDebug];
DROP PROCEDURE CountRowsForSQL;
DROP PROCEDURE GetWaveCodeOnce;
DROP PROCEDURE ShowSessionGUID;
DROP PROCEDURE XMLFinishAction;
DROP FUNCTION ReplicateValue;
DROP FUNCTION CompareFirstWords;

DROP ASSEMBLY [LEADEngine] ;  