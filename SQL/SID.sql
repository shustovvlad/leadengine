DECLARE @Command VARCHAR(MAX) = 'ALTER AUTHORIZATION ON DATABASE::[Amanat] TO 
[sa]' 

SELECT @Command = REPLACE(REPLACE(@Command 
            , 'Amanat', SD.Name)
            , 'sa', SL.Name)
FROM master..sysdatabases SD 
JOIN master..syslogins SL ON  SD.SID = SL.SID
WHERE  SD.Name = DB_NAME()

PRINT @Command
EXEC(@Command)