USE [Amanat]
GO

/****** Object:  StoredProcedure [dbo].[FieldOfRecordset]    Script Date: 29.05.2018 16:31:26 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[FieldOfRecordset]
	@SQL [nvarchar](4000),
	@FieldName [nvarchar](4000)
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [LEADEngine].[LEADEngine].[FieldOfRecordset]
GO


