USE [Amanat]
GO

/****** Object:  StoredProcedure [dbo].[GetObjectsSQL]    Script Date: 29.05.2018 16:28:15 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[GetObjectsSQL]
	@ObjectsTable [nvarchar](4000),
	@Condition [nvarchar](4000),
	@GroupType [nvarchar](4000),
	@DirectWhere [nvarchar](4000),
	@ErrorMessage [nvarchar](4000) OUTPUT,
	@IsNormal [bit] OUTPUT,
	@SQLStatement [nvarchar](4000) OUTPUT,
	@IsIgnoreGrouping [bit],
	@ReporterLayer_id [int] = 0
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [LEADEngine].[LEADEngine].[GetObjectsSQL]
GO


