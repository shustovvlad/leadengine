USE [Amanat]
GO

/****** Object:  UserDefinedFunction [dbo].[ReplicateValue]    Script Date: 29.05.2018 16:38:34 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE FUNCTION [dbo].[ReplicateValue](@Value [nvarchar](4000), @Prefix [nvarchar](4000), @Length [int])
RETURNS [nvarchar](4000) WITH EXECUTE AS CALLER
AS 
EXTERNAL NAME [LEADEngine].[LEADEngine].[ReplicateValue]
GO


