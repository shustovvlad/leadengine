USE [Amanat]
GO

/****** Object:  StoredProcedure [dbo].[GetUserQuerySQL]    Script Date: 29.05.2018 16:29:35 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[GetUserQuerySQL]
	@UserQuery_id [int],
	@SQLStatement [nvarchar](4000) OUTPUT,
	@ReporterLayer_id [int] = 0
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [LEADEngine].[LEADEngine].[GetUserQuerySQL]
GO


