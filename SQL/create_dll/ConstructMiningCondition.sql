USE [Amanat]
GO

/****** Object:  StoredProcedure [dbo].[ConstructMiningCondition]    Script Date: 29.05.2018 16:32:06 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[ConstructMiningCondition]
	@UserQuery_id [int],
	@SQLStatement [nvarchar](4000),
	@SQLCondition [nvarchar](4000) OUTPUT
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [LEADEngine].[LEADEngine].[ConstructMiningCondition]
GO


