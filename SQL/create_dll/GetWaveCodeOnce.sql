USE [Amanat]
GO

/****** Object:  StoredProcedure [dbo].[GetWaveCodeOnce]    Script Date: 29.05.2018 16:36:26 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[GetWaveCodeOnce]
	@WaveType_id [int],
	@SQLStatement [nvarchar](4000),
	@WaveCode [nvarchar](4000) OUTPUT
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [LEADEngine].[LEADEngine].[GetWaveCodeOnce]
GO


