USE [Amanat]
GO

/****** Object:  StoredProcedure [dbo].[ForceInsertBatch]    Script Date: 29.05.2018 16:14:44 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[ForceInsertBatch]
	@LocalCommand [nvarchar](4000),
	@ConnectionString [nvarchar](4000),
	@TargetTable [nvarchar](4000),
	@TargetPrepare [nvarchar](4000),
	@LastError [nvarchar](4000) OUTPUT,
	@FinishCommand [nvarchar](4000)
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [LEADEngine].[LEADEngine].[ForceInsertBatch]
GO


