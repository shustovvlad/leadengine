USE [Amanat]
GO

/****** Object:  StoredProcedure [dbo].[ShowSessionGUID]    Script Date: 29.05.2018 16:36:58 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[ShowSessionGUID]
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [LEADEngine].[LEADEngine].[ShowSessionGUID]
GO


