USE [Amanat]
GO

/****** Object:  StoredProcedure [dbo].[RetrieveRecordset]    Script Date: 29.05.2018 16:26:25 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[RetrieveRecordset]
	@SQLStatement [nvarchar](4000),
	@ObjectGUID [nvarchar](4000)
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [LEADEngine].[LEADEngine].[RetrieveRecordset]
GO


