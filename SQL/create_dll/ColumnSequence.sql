USE [Amanat]
GO

/****** Object:  UserDefinedAggregate [dbo].[ColumnSequence]    Script Date: 29.05.2018 16:09:45 ******/
CREATE AGGREGATE [dbo].[ColumnSequence]
(@value [nvarchar](4000))
RETURNS[nvarchar](4000)
EXTERNAL NAME [LEADEngine].[ColumnSequence]
GO


