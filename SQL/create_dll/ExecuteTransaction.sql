USE [Amanat]
GO

/****** Object:  StoredProcedure [dbo].[ExecuteTransaction]    Script Date: 29.05.2018 16:21:17 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[ExecuteTransaction]
	@GUID [uniqueidentifier],
	@IsNormal [bit] OUTPUT,
	@ErrorNumber [int] OUTPUT,
	@ErrorMessage [nvarchar](4000) OUTPUT
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [LEADEngine].[LEADEngine].[ExecuteTransaction]
GO


