USE [Amanat]
GO

/****** Object:  StoredProcedure [dbo].[XMLPostEvent]    Script Date: 29.05.2018 16:30:34 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[XMLPostEvent]
	@EventSystemCode [nvarchar](4000),
	@EventParameters [xml]
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [LEADEngine].[LEADEngine].[XMLPostEvent]
GO


