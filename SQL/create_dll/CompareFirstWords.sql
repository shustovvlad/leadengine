USE [Amanat]
GO

/****** Object:  UserDefinedFunction [dbo].[CompareFirstWords]    Script Date: 29.05.2018 16:39:21 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE FUNCTION [dbo].[CompareFirstWords](@FirstString [nvarchar](4000), @SecondString [nvarchar](4000))
RETURNS [bit] WITH EXECUTE AS CALLER
AS 
EXTERNAL NAME [LEADEngine].[LEADEngine].[CompareFirstWords]
GO


