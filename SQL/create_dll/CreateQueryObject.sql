USE [Amanat]
GO

/****** Object:  StoredProcedure [dbo].[CreateQueryObject]    Script Date: 29.05.2018 16:27:40 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[CreateQueryObject]
	@TableGUID [nvarchar](4000),
	@ObjectName [nvarchar](4000),
	@SortType [nvarchar](4000),
	@SortPriority [int],
	@ReporterLayer_id [int] = 0
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [LEADEngine].[LEADEngine].[CreateQueryObject]
GO


