USE [Amanat]
GO

/****** Object:  StoredProcedure [dbo].[GetWaveCode]    Script Date: 29.05.2018 16:24:33 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[GetWaveCode]
	@WaveType_id [int],
	@SQLStatement [nvarchar](4000)
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [LEADEngine].[LEADEngine].[GetWaveCode]
GO


