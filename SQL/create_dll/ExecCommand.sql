USE [Amanat]
GO

/****** Object:  StoredProcedure [dbo].[ExecCommand]    Script Date: 29.05.2018 16:25:16 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[ExecCommand]
	@ConnectionString [nvarchar](4000),
	@CommandString [nvarchar](4000),
	@ReturnResult [bit],
	@LastError [nvarchar](4000) OUTPUT
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [LEADEngine].[LEADEngine].[ExecCommand]
GO


