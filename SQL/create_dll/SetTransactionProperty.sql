USE [Amanat]
GO

/****** Object:  StoredProcedure [dbo].[SetTransactionProperty]    Script Date: 29.05.2018 16:23:50 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[SetTransactionProperty]
	@TransactionGUID [uniqueidentifier],
	@PropertyName [nvarchar](4000),
	@PropertyValue [nvarchar](4000)
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [LEADEngine].[LEADEngine].[SetTransactionProperty]
GO


