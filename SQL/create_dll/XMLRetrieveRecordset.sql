USE [Amanat]
GO

/****** Object:  StoredProcedure [dbo].[XMLRetrieveRecordset]    Script Date: 29.05.2018 16:16:03 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[XMLRetrieveRecordset]
	@SQLStatement [nvarchar](4000),
	@ObjectXML [xml]
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [LEADEngine].[LEADEngine].[XMLRetrieveRecordset]
GO


