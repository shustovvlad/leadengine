USE [Amanat]
GO

/****** Object:  StoredProcedure [dbo].[RowAsRecordset]    Script Date: 29.05.2018 16:32:41 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[RowAsRecordset]
	@SQLStatement [nvarchar](4000)
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [LEADEngine].[LEADEngine].[RowAsRecordset]
GO


