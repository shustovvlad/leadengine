USE [Amanat]
GO

/****** Object:  StoredProcedure [dbo].[RunTransaction]    Script Date: 29.05.2018 16:22:28 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[RunTransaction]
	@GUID [uniqueidentifier],
	@ErrorMessage [nvarchar](4000) OUTPUT,
	@IsNormal [bit] OUTPUT
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [LEADEngine].[LEADEngine].[RunTransaction]
GO


