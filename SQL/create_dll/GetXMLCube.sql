USE [Amanat]
GO

/****** Object:  StoredProcedure [dbo].[GetXMLCube]    Script Date: 29.05.2018 16:17:03 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[GetXMLCube]
	@ObjectXML [xml],
	@ReporterFilter_id [int],
	@DirectWhere [nvarchar](4000),
	@IsIgnoreGrouping [bit],
	@SQL [nvarchar](4000) OUTPUT,
	@ReporterLayer_id [int] = 0
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [LEADEngine].[LEADEngine].[GetXMLCube]
GO


