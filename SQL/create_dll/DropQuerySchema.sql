USE [Amanat]
GO

/****** Object:  StoredProcedure [dbo].[DropQuerySchema]    Script Date: 29.05.2018 16:28:59 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[DropQuerySchema]
	@SchemaGUID [nvarchar](4000)
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [LEADEngine].[LEADEngine].[DropQuerySchema]
GO


