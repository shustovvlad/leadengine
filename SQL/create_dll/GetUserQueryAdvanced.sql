USE [Amanat]
GO

/****** Object:  StoredProcedure [dbo].[GetUserQueryAdvanced]    Script Date: 29.05.2018 16:34:02 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[GetUserQueryAdvanced]
	@UserQuery_id [int],
	@TopRows [int],
	@ReporterFilter_id [int],
	@TotalRows [int] OUTPUT,
	@SQLStatement [nvarchar](4000) OUTPUT,
	@DirectWhere [nvarchar](4000),
	@IsIgnoreGrouping [bit]
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [LEADEngine].[LEADEngine].[GetUserQueryAdvanced]
GO


