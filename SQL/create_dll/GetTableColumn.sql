USE [Amanat]
GO

/****** Object:  StoredProcedure [dbo].[GetTableColumn]    Script Date: 29.05.2018 16:20:02 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[GetTableColumn]
	@TableName [nvarchar](4000),
	@KeyColumn [nvarchar](4000),
	@GetColumn [nvarchar](4000),
	@KeyValue [nvarchar](4000),
	@Value [nvarchar](4000) OUTPUT
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [LEADEngine].[LEADEngine].[GetTableColumn]
GO


