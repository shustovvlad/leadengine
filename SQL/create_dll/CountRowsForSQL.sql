USE [Amanat]
GO

/****** Object:  StoredProcedure [dbo].[CountRowsForSQL]    Script Date: 29.05.2018 16:35:49 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[CountRowsForSQL]
	@SQLStatement [nvarchar](4000),
	@RowCount [int] OUTPUT
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [LEADEngine].[LEADEngine].[CountRowsForSQL]
GO


