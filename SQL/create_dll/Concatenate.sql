USE [Amanat]
GO

/****** Object:  UserDefinedAggregate [dbo].[Concatenate]    Script Date: 29.05.2018 16:12:07 ******/
CREATE AGGREGATE [dbo].[Concatenate]
(@value [nvarchar](4000))
RETURNS[nvarchar](4000)
EXTERNAL NAME [LEADEngine].[Concatenate]
GO


