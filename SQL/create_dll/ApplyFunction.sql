USE [Amanat]
GO

/****** Object:  StoredProcedure [dbo].[ApplyFunction]    Script Date: 29.05.2018 16:20:39 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[ApplyFunction]
	@FunctionName [nvarchar](4000),
	@InputValue [nvarchar](4000),
	@OutputValue [nvarchar](4000) OUTPUT
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [LEADEngine].[LEADEngine].[ApplyFunction]
GO


