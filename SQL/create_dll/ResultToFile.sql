USE [Amanat]
GO

/****** Object:  StoredProcedure [dbo].[ResultToFile]    Script Date: 29.05.2018 16:25:54 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[ResultToFile]
	@FilePath [nvarchar](4000),
	@FileName [nvarchar](4000),
	@ColumnDivider [nvarchar](4000),
	@RowDivider [nvarchar](4000),
	@RecordsetDivider [nvarchar](4000),
	@IsIncludeNames [bit],
	@SQL [nvarchar](4000)
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [LEADEngine].[LEADEngine].[ResultToFile]
GO


