USE [Amanat]
GO

/****** Object:  StoredProcedure [dbo].[XMLFinishAction]    Script Date: 29.05.2018 16:37:24 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[XMLFinishAction]
	@ActionSystemCode [nvarchar](4000),
	@Action_id [int]
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [LEADEngine].[LEADEngine].[XMLFinishAction]
GO


