USE [Amanat]
GO

/****** Object:  StoredProcedure [dbo].[GetTransactionProperty]    Script Date: 29.05.2018 16:27:08 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[GetTransactionProperty]
	@TransactionGUID [uniqueidentifier],
	@PropertyName [nvarchar](4000),
	@PropertyValue [nvarchar](4000) OUTPUT
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [LEADEngine].[LEADEngine].[GetTransactionProperty]
GO


