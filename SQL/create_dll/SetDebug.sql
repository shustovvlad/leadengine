USE [Amanat]
GO

/****** Object:  StoredProcedure [dbo].[SetDebug]    Script Date: 29.05.2018 16:35:07 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[SetDebug]
	@IsDebug [bit]
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [LEADEngine].[LEADEngine].[SetDebug]
GO


