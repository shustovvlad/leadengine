USE [Amanat]
GO

/****** Object:  UserDefinedAggregate [dbo].[PutawayAggregate]    Script Date: 29.05.2018 16:12:49 ******/
CREATE AGGREGATE [dbo].[PutawayAggregate]
(@value [nvarchar](4000))
RETURNS[nvarchar](4000)
EXTERNAL NAME [LEADEngine].[PutawayAggregate]
GO


