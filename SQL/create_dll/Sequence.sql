USE [Amanat]
GO

/****** Object:  UserDefinedAggregate [dbo].[Sequence]    Script Date: 29.05.2018 16:11:20 ******/
CREATE AGGREGATE [dbo].[Sequence]
(@value [nvarchar](4000))
RETURNS[nvarchar](4000)
EXTERNAL NAME [LEADEngine].[Sequence]
GO


