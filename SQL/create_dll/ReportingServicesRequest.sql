USE [Amanat]
GO

/****** Object:  StoredProcedure [dbo].[ReportingServicesRequest]    Script Date: 29.05.2018 16:33:26 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[ReportingServicesRequest]
	@URL [nvarchar](4000),
	@ReportName [nvarchar](4000),
	@PrinterName [nvarchar](4000),
	@ReportParams [nvarchar](4000),
	@PageLimit [int],
	@IsLandscape [bit],
	@IsFinished [bit] OUTPUT,
	@Copies [int]
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [LEADEngine].[LEADEngine].[ReportingServicesRequest]
GO


