USE [AMANAT]
GO

/****** Object:  UserDefinedAggregate [dbo].[ValueSequence]    Script Date: 29.05.2018 16:06:17 ******/
CREATE AGGREGATE [dbo].[ValueSequence]
(@value [nvarchar](4000))
RETURNS[nvarchar](4000)
EXTERNAL NAME [LEADEngine].[ValueSequence]
GO


