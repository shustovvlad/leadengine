USE [Amanat]
GO

/****** Object:  StoredProcedure [dbo].[CLRGetVariable]    Script Date: 29.05.2018 16:18:13 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[CLRGetVariable]
	@GUID [nvarchar](4000),
	@ParameterType_id [int],
	@Value [nvarchar](4000) OUTPUT,
	@IsExist [bit] OUTPUT,
	@FilterCondition [nvarchar](4000) OUTPUT,
	@IsApplication [bit] OUTPUT,
	@Priority [int] OUTPUT
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [LEADEngine].[LEADEngine].[CLRGetVariable]
GO


