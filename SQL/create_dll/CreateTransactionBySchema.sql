USE [Amanat]
GO

/****** Object:  StoredProcedure [dbo].[CreateTransactionBySchema]    Script Date: 29.05.2018 16:30:04 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[CreateTransactionBySchema]
	@TransactionType_id [int],
	@User_id [int],
	@ParentTransaction_id [int],
	@ParentTable_id [int],
	@ParentRow [int],
	@ObjectSchema [xml],
	@LastError [nvarchar](4000) OUTPUT,
	@Transaction_id [int] OUTPUT
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [LEADEngine].[LEADEngine].[CreateTransactionBySchema]
GO


