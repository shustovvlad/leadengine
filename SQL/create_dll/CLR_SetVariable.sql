USE [Amanat]
GO

/****** Object:  StoredProcedure [dbo].[CLR_SetVariable]    Script Date: 29.05.2018 16:23:16 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[CLR_SetVariable]
	@GUID [uniqueidentifier],
	@ParameterType_id [int],
	@Value [nvarchar](4000),
	@IsError [bit] OUTPUT,
	@FilterCondition [nvarchar](4000),
	@Priority [int],
	@IsApplication [int]
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [LEADEngine].[LEADEngine].[CLR_SetVariable]
GO


