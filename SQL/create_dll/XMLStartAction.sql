USE [Amanat]
GO

/****** Object:  StoredProcedure [dbo].[XMLStartAction]    Script Date: 29.05.2018 16:34:41 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[XMLStartAction]
	@ActionSystemCode [nvarchar](4000),
	@ActionParameters [xml],
	@Action_id [int] OUTPUT
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [LEADEngine].[LEADEngine].[XMLStartAction]
GO


