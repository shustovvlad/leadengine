USE [Amanat]
GO

/****** Object:  StoredProcedure [dbo].[CountRecords]    Script Date: 29.05.2018 16:19:04 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[CountRecords]
	@TableName [nvarchar](4000),
	@RowCount [int] OUTPUT
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [LEADEngine].[LEADEngine].[CountRecords]
GO


