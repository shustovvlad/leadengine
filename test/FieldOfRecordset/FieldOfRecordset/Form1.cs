﻿using Microsoft.SqlServer.Server;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FieldOfRecordset
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string sql_string = "SELECT [ID_ТранзакцияИсходящейПоставки] = hdr_Delivery.Transaction_id, [ID_ЗаданиеНаПикинг] = hdr_MaterialPicking.tid, [ID_НаборВидУпаковки] = PickingWarehouseReserve.PackType_id, [Набор.ID_ГруппаХранения] = PickingMaterials.StorageGroup_id, [Набор.ID_СтанцияНабора] = isnull(PickingLocations.PickingStation_id, -1) FROM hdr_MaterialPicking as hdr_MaterialPicking with (nolock)  INNER JOIN WarehouseReserve as PickingWarehouseReserve with (nolock) ON (hdr_MaterialPicking.Transaction_id = PickingWarehouseReserve.Transaction_id) INNER JOIN Materials as PickingMaterials with (nolock) ON (hdr_MaterialPicking.Material_id = PickingMaterials.tid) INNER JOIN Locations as PickingLocations with (nolock) ON (hdr_MaterialPicking.SourceLocation_id = PickingLocations.tid) INNER JOIN Transactions as Transactions with (nolock) ON (hdr_MaterialPicking.Transaction_id = Transactions.tid) INNER JOIN hdr_Delivery as hdr_Delivery with (nolock) ON (Transactions.ParentTransaction_id = hdr_Delivery.Transaction_id)  WHERE hdr_Delivery.Transaction_id = 37024415 AND PickingWarehouseReserve.PackType_id =\'3\' AND PickingMaterials.StorageGroup_id =\'8\' AND isnull(PickingLocations.PickingStation_id, -1) >=\'0\'";
            sql_string = "select 1 as temp";

            //FieldOfRecordset(sql_string,
            //    "ID_ЗаданиеНаПикинг");

            FieldOfRecordset(sql_string,
                "temp");
        }

        public static void FieldOfRecordset(SqlString SQL, SqlString FieldName)
        {
            ArrayList array = DivideToArray(FieldName.ToString(),
                ",");

            string connections = "Server=192.168.1.207;Database=Amanat;User Id=sa;Password =123456Qaz; ";

            SqlConnection connection = new SqlConnection(connections);
            connection.Open();
            SqlMetaData[] sqlMetaDataArray = new SqlMetaData[array.Count];
            SqlDataRecord record = (SqlDataRecord)null;
            SqlDataReader sqlDataReader = new SqlCommand(SQL.ToString(), connection).ExecuteReader();

            bool flag = false;
            while (sqlDataReader.Read())
            {
                if (!flag)
                {
                    DataTableReader dataReader = sqlDataReader.GetSchemaTable().CreateDataReader();
                    while (dataReader.Read())
                    {
                        for (int index = 0; index < array.Count; ++index)
                        {
                            if (array[index].ToString().ToUpper() == dataReader["ColumnName"].ToString().ToUpper())
                                sqlMetaDataArray[index] = smethod_9(dataReader);
                            else if (array[index].ToString() == "NULL")
                                sqlMetaDataArray[index] = new SqlMetaData("Column" + index.ToString(), SqlDbType.VarChar, 200L, 1033L, SqlCompareOptions.None);
                        }
                    }
                    record = new SqlDataRecord(sqlMetaDataArray);
                    //SqlContext.Pipe.SendResultsStart(record);
                    flag = true;
                }

                if (flag)
                {
                    for (int ordinal1 = 0; ordinal1 < sqlDataReader.FieldCount; ++ordinal1)
                    {
                        int ordinal2 = array.IndexOf((object)sqlDataReader.GetName(ordinal1).ToUpper());
                        if (ordinal2 >= 0)
                        {
                            if (sqlDataReader.IsDBNull(ordinal1))
                                record.SetDBNull(ordinal2);
                            else
                                record.SetValue(ordinal2, sqlDataReader.GetValue(ordinal1));
                        }
                    }
                    //SqlContext.Pipe.SendResultsRow(record);
                }

                
                //if (flag)
                    //SqlContext.Pipe.SendResultsEnd();
                
            }
            sqlDataReader.Close();
            connection.Close();
        }

        public static ArrayList DivideToArray(string SourceString, string Divider)
        {
            ArrayList arrayList = new ArrayList();
            string str1 = "";
            for (int index = 0; index < SourceString.Length; ++index)
            {
                char ch = SourceString[index];
                if (ch.ToString() != Divider)
                {
                    string str2 = str1;
                    ch = SourceString[index];
                    string str3 = ch.ToString();
                    str1 = str2 + str3;
                }
                else
                {
                    arrayList.Add((object)str1.Trim().ToUpper());
                    str1 = "";
                }
            }
            if (str1.Trim().Length > 0)
                arrayList.Add((object)str1.Trim().ToUpper());
            return arrayList;
        }

        private static SqlMetaData smethod_9(DataTableReader MetaReader)
        {
            SqlMetaData sqlMetaData = (SqlMetaData)null;
            string upper = ((SqlDbType)MetaReader["ProviderType"]).ToString().ToUpper();
            if (upper == "BIGINT")
                sqlMetaData = new SqlMetaData(MetaReader["ColumnName"].ToString(), SqlDbType.BigInt);
            else if (upper == "VARCHAR")
                sqlMetaData = new SqlMetaData(MetaReader["ColumnName"].ToString(), SqlDbType.VarChar,
                    (long)Convert.ToInt32(
                        MetaReader["ColumnSize"]), 1033L, SqlCompareOptions.None);
            else if (upper == "BIT")
                sqlMetaData = new SqlMetaData(MetaReader["ColumnName"].ToString(), SqlDbType.Bit);
            else if (upper == "CHAR")
                sqlMetaData = new SqlMetaData(MetaReader["ColumnName"].ToString(), SqlDbType.Char,
                    (long)Convert.ToInt32(MetaReader["ColumnSize"]), 1033L, SqlCompareOptions.None);
            else if (upper == "DATE")
                sqlMetaData = new SqlMetaData(MetaReader["ColumnName"].ToString(), SqlDbType.Date);
            else if (upper == "DATETIME")
                sqlMetaData = new SqlMetaData(MetaReader["ColumnName"].ToString(), SqlDbType.DateTime);
            else if (upper == "DATETIME2")
                sqlMetaData = new SqlMetaData(MetaReader["ColumnName"].ToString(), SqlDbType.DateTime2);
            else if (upper == "DECIMAL")
                sqlMetaData = new SqlMetaData(MetaReader["ColumnName"].ToString(), SqlDbType.Decimal, Convert.ToByte(MetaReader["NumericPrecision"]), Convert.ToByte(MetaReader["NumericScale"]));
            else if (upper == "FLOAT")
                sqlMetaData = new SqlMetaData(MetaReader["ColumnName"].ToString(), SqlDbType.Float, Convert.ToByte(MetaReader["NumericPrecision"]), Convert.ToByte(MetaReader["NumericScale"]));
            else if (upper == "INT")
                sqlMetaData = new SqlMetaData(MetaReader["ColumnName"].ToString(), SqlDbType.Int);
            else if (upper == "MONEY")
                sqlMetaData = new SqlMetaData(MetaReader["ColumnName"].ToString(), SqlDbType.Money);
            else if (upper == "NCHAR")
                sqlMetaData = new SqlMetaData(MetaReader["ColumnName"].ToString(), SqlDbType.NChar, (long)Convert.ToInt32(MetaReader["ColumnSize"]), 1033L, SqlCompareOptions.None);
            else if (upper == "NUMERIC")
                sqlMetaData = new SqlMetaData(MetaReader["ColumnName"].ToString(), SqlDbType.Decimal, Convert.ToByte(MetaReader["NumericPrecision"]), Convert.ToByte(MetaReader["NumericScale"]));
            else if (upper == "NVARCHAR")
                sqlMetaData = new SqlMetaData(MetaReader["ColumnName"].ToString(), SqlDbType.NVarChar, (long)Convert.ToInt32(MetaReader["ColumnSize"]), 1033L, SqlCompareOptions.None);
            else if (upper == "REAL")
                sqlMetaData = new SqlMetaData(MetaReader["ColumnName"].ToString(), SqlDbType.Real, Convert.ToByte(MetaReader["NumericPrecision"]), Convert.ToByte(MetaReader["NumericScale"]));
            else if (upper == "SMALLINT")
                sqlMetaData = new SqlMetaData(MetaReader["ColumnName"].ToString(), SqlDbType.SmallInt);
            else if (upper == "SMALLMONEY")
                sqlMetaData = new SqlMetaData(MetaReader["ColumnName"].ToString(), SqlDbType.SmallMoney);
            else if (upper == "TINYINT")
                sqlMetaData = new SqlMetaData(MetaReader["ColumnName"].ToString(), SqlDbType.TinyInt);
            else if (upper == "UNIQUEIDENTIFIER")
                sqlMetaData = new SqlMetaData(MetaReader["ColumnName"].ToString(), SqlDbType.UniqueIdentifier);
            return sqlMetaData;
        }

    }
}
