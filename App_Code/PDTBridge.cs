using System;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;

public static class PDTBridge
{
    public static string Execute(NameValueCollection Query)
    {
        if (Query.Count == 0)
        {
            return "swd c# wms dll 0.2 (��������� �� 1.207 ������)";
        }

        string var_terminal     = PDTBridge.Unquote(Query["Terminal"]);
        string var_marker       = PDTBridge.Unquote(Query["Marker"]);
        string var_method       = PDTBridge.Unquote(Query["Method"]);
        string var_name         = PDTBridge.Unquote(Query["Name"]);
        string var_value        = PDTBridge.Unquote(Query["Value"]);
        string var_wait         = PDTBridge.Unquote(Query["Wait"]);

        int num = 0; //���� ������ ���

        if (var_method.Trim().Equals("")) {
            num = -1;
            return string.Format("MARKER\t{0}\vERROR\t{1}\f{2}", var_marker, num, string.Empty);
        }
        
        if (var_method == "SET")
        {
            if (var_name.Length == 0)
            {
                num = -1;
                return string.Format("MARKER\t{0}\vERROR\t{1}\f{2}", var_marker, num, string.Empty);
            }
        }

        SqlConnection sqlConnection = null;
        try
        {
                //sqlConnection = new SqlConnection(
                //"Data Source=192.168.1.207;" +
                //"Initial Catalog=Amanat;" +
                //"User id=sa;" +
                //"Password=123456Qaz;");

                sqlConnection = new SqlConnection(
                "Data Source=192.168.1.10;" +
                "Initial Catalog=Amanat;" +
                "User id=sa;" +
                "Password=Wms$erver;");

            //sqlConnection = new SqlConnection(
            //"Data Source=192.168.4.11;" +
            //"Initial Catalog=Amanat;" +
            //"User id=sa;" +
            //"Password=Wms$erver;");

            sqlConnection.Open();
        }
        catch
        {
            num = -2;
            sqlConnection.Close();
            return string.Format("MARKER\t{0}\vERROR\t{1}\f{2}", var_marker, num, string.Empty);
        }

        float num2 = 60f;

        if (var_wait.Length > 0)
        {
           try
              {
                num2 = Convert.ToSingle(var_wait);
              }
           catch
                {
                num = -2;
                sqlConnection.Close();
                return string.Format("MARKER\t{0}\vERROR\t{1}\f{2}", var_marker, num, string.Empty);
            }
        }

        int timeout = (int)Math.Round((double)(0.166666672f * num2));
        int timeout2 = (int)Math.Round((double)(0.6666667f * num2));

        string arg = string.Empty;

        if (var_method == "SET")
        {
            PDTBridge.SetVariable(sqlConnection, var_terminal, var_name, var_value, out num, timeout);
            sqlConnection.Close();
            return string.Format("MARKER\t{0}\vERROR\t{1}\f{2}", var_marker, 0, arg);
        }

        if (var_method == "TIMER")
        {
            arg = PDTBridge.NextProcedure(sqlConnection, var_terminal, var_marker, true, out num, timeout, timeout2);
            sqlConnection.Close();
            return string.Format("MARKER\t{0}\vERROR\t{1}\f{2}", var_marker, 0, arg);
        }

        if (var_method == "NEXT")
        {
            arg = PDTBridge.NextProcedure(sqlConnection, var_terminal, var_marker, false, out num, timeout, timeout2);
            sqlConnection.Close();
            return string.Format("MARKER\t{0}\vERROR\t{1}\f{2}", var_marker, 0, arg);
        }

        //���� �� ���� �� ������� �� ���������
        num = -2;
        sqlConnection.Close();
        return string.Format("MARKER\t{0}\vERROR\t{1}\f{2}", var_marker, num, string.Empty);
    }


    private static string Unquote(string s)
    {
        if (s == null)
        {
            s = string.Empty;
        }
        string result;
        if (s.Length == 0)
        {
            result = s;
        }
        else
        {
            int num = 0;
            if (s[0] == '"')
            {
                num = 1;
            }
            int num2 = s.Length - num;
            if (s[s.Length - 1] == '"')
            {
                num2--;
            }
            result = s.Substring(num, num2);
        }
        return result;
    }

    private static string NextProcedure(SqlConnection conn, string Terminal, string Marker, bool Timer, out int ErrorCode, int Timeout1, int Timeout2)
    {
        ErrorCode = 0;

        SqlCommand sqlCommand = new SqlCommand("pdt_PDTClient_Check", conn);
        sqlCommand.CommandType = CommandType.StoredProcedure;

        sqlCommand.Parameters.Add("@TerminalAddress", SqlDbType.VarChar, 200).Value = Terminal;
        sqlCommand.Parameters.Add("@Marker", SqlDbType.VarChar, 200).Value = Marker;

        SqlParameter param_Session_id = sqlCommand.Parameters.Add("@Session_id", SqlDbType.Int);
        param_Session_id.Direction = ParameterDirection.Output;

        SqlParameter param_CurrProcedure = sqlCommand.Parameters.Add("@CurrProcedure", SqlDbType.VarChar, 200);
        param_CurrProcedure.Direction = ParameterDirection.Output;

        SqlParameter param_NextProcedure = sqlCommand.Parameters.Add("@NextProcedure", SqlDbType.VarChar, 200);
        param_NextProcedure.Direction = ParameterDirection.Output;

        SqlParameter param_TimerProcedure = sqlCommand.Parameters.Add("@TimerProcedure", SqlDbType.VarChar, 200);
        param_TimerProcedure.Direction = ParameterDirection.Output;

        SqlParameter param_Response = sqlCommand.Parameters.Add("@Response", SqlDbType.VarChar, -1);
        param_Response.Direction = ParameterDirection.Output;

        SqlParameter param_WatchdogCounter = sqlCommand.Parameters.Add("@WatchdogCounter", SqlDbType.Int);
        param_WatchdogCounter.Direction = ParameterDirection.Output;

        int var_Session_id = -1;

        string var_CurrProcedure    = "";
        string var_NextProcedure    = "pdt_Main";
        string var_TimerProcedure   = "";
        int var_WatchdogCounter     = 0;
        string string_Response      = ""; 

        try
        {
            sqlCommand.CommandTimeout = Timeout1;
            sqlCommand.ExecuteNonQuery();

            if (!Convert.IsDBNull(param_Session_id.Value))
            {
                var_Session_id = (int)param_Session_id.Value;
            }

            if (!Convert.IsDBNull(param_CurrProcedure.Value))
            {
                var_CurrProcedure = (string)param_CurrProcedure.Value;
            }

            if (!Convert.IsDBNull(param_NextProcedure.Value))
            {
                var_NextProcedure = (string)param_NextProcedure.Value;
            }

            if (!Convert.IsDBNull(param_TimerProcedure.Value))
            {
                var_TimerProcedure = (string)param_TimerProcedure.Value;
            }

            if (!Convert.IsDBNull(param_WatchdogCounter.Value))
            {
                var_WatchdogCounter = (int)param_WatchdogCounter.Value;
            }

            string_Response = string_Response + param_Response.Value;

            if (var_NextProcedure.Length == 0)
            {
                var_Session_id = -1;
                var_NextProcedure = "pdt_Main";
            }

            if (var_TimerProcedure.Length == 0)
            {
                var_TimerProcedure = var_NextProcedure;
            }
        }
        catch (SqlException ex)
        {
            ErrorCode = ((ex.Number == -2) ? -4 : -3);
            return "";
        }
        catch
        {
            ErrorCode = -3;
            return "";
        }

        //���� ������ ��������� �����
        if (string_Response.Length > 0)
        {
            return string_Response.ToString();
        }
        
        var_CurrProcedure = (Timer ? var_TimerProcedure : var_NextProcedure);
        sqlCommand = new SqlCommand("pdt_PDTClient_Next", conn);
        sqlCommand.CommandType = CommandType.StoredProcedure;
        sqlCommand.Parameters.Add("@TerminalAddress", SqlDbType.VarChar, 200).Value = Terminal;

        if (var_Session_id == -1)
        {
            sqlCommand.Parameters.Add("@Session_id", SqlDbType.Int).Value = DBNull.Value;
        }
        else
        {
            sqlCommand.Parameters.Add("@Session_id", SqlDbType.Int).Value = var_Session_id;
        }

        sqlCommand.Parameters.Add("@ProcedureName", SqlDbType.VarChar, 200).Value = var_CurrProcedure;

        SqlDataReader sqlDataReader = null;

        try
        {
            sqlCommand.CommandTimeout = Timeout2;
            sqlDataReader = sqlCommand.ExecuteReader();
            var_NextProcedure = "pdt_Main";

            var_TimerProcedure = "";

            while (true)
            {
                if (!sqlDataReader.Read())
                {
                    if (!sqlDataReader.NextResult())
                    {
                        break;
                    }
                }
                else
                {
                    for (int i = 0; i < sqlDataReader.FieldCount; i++)
                    {
                        string str_key      = sqlDataReader.GetName(i).Replace("\t", " ").Replace("\v", "").Replace("\f", "");
                        string str_value    = sqlDataReader[i].ToString().Replace("\t", " ").Replace("\v", "").Replace("\f", "");

                        if (str_key.Length == 0 || str_key == null)
                        {
                            continue;
                        }

                        if (str_value.Length > 0 && sqlDataReader.GetFieldType(i).IsPrimitive)
                        {
                            str_value = str_value.Replace(',', '.');
                        }
                            
                        if (str_key == "ERRORCODE")
                        {
                            ErrorCode = (int)sqlDataReader[i];
                            continue;
                        }

                        if (str_key == "SESSION")
                        {
                            var_Session_id = -1;
                            if (str_value.Length > 0)
                            {
                                var_Session_id = (int)sqlDataReader[i];
                                continue;
                            }
                            continue;
                        }

                        if (str_key == "NEXTPROC")
                        {
                            if (str_value.Length > 0)
                            {
                                var_NextProcedure = str_value;
                                continue;
                            }
                            continue;
                        }

                        if (str_key == "TIMER")
                        {
                            if (str_value.Length > 0)
                            {
                                var_TimerProcedure = str_value;
                                continue;
                            }
                            continue;
                        }

                        if (str_key == "COMMAND")
                        {
                            if (str_value != null && str_value == "CLEARSESSION")
                            {
                                var_Session_id = -1;
                                continue;
                            }
                            continue;
                        }

                        if (string_Response.Length > 0)
                            {
                                string_Response = string_Response + '\v';
                            }
                            string_Response = string_Response + String.Format("{0}\t{1}", str_key, str_value);
                        }
                        
                    }
                }
            }
            catch (SqlException ex2)
            {
                ErrorCode = ((ex2.Number == -2) ? -4 : -3);
                return "";
            }
            catch
            {
                ErrorCode = -3;
                return "";
            }
            finally
            {
                if (sqlDataReader != null)
                {
                    sqlDataReader.Close();
                }
            }
            if (string_Response.Length > 0)
            {
                var_WatchdogCounter = 0;
            }
            else
            {
                var_WatchdogCounter++;
            }
            if (var_WatchdogCounter > 100)
            {
                ErrorCode = -5;
                return "";
            }
            
            if (var_NextProcedure == "pdt_Main")
            {
                    if (var_Session_id != -1)
                    {
                        sqlCommand = new SqlCommand("lxa_CloseSession", conn);
                        sqlCommand.CommandType = CommandType.StoredProcedure;
                        sqlCommand.Parameters.Add("@Session_id", SqlDbType.Int).Value = var_Session_id;
                        try
                        {
                            sqlCommand.CommandTimeout = Timeout1;
                            sqlCommand.ExecuteNonQuery();
                        }
                        catch
                        {
                        }
                    }
                    var_Session_id = -1;
                }

                sqlCommand = new SqlCommand("pdt_PDTClient_Adjust", conn);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add("@TerminalAddress", SqlDbType.VarChar, 200).Value = Terminal;
                sqlCommand.Parameters.Add("@Marker", SqlDbType.VarChar, 200).Value = Marker;

                if (var_CurrProcedure == "pdt_CheckPersonalBarcode" && var_Session_id == -1 && string_Response.Length == 0)
                {
                    sqlCommand.Parameters.Add("@Session_id", SqlDbType.Int).Value = DBNull.Value;
                    sqlCommand.Parameters.Add("@CurrProcedure", SqlDbType.VarChar, 200).Value = DBNull.Value;
                    sqlCommand.Parameters.Add("@NextProcedure", SqlDbType.VarChar, 200).Value = DBNull.Value;
                    sqlCommand.Parameters.Add("@TimerProcedure", SqlDbType.VarChar, 200).Value = DBNull.Value;
                    sqlCommand.Parameters.Add("@Response", SqlDbType.VarChar, -1).Value = string.Empty;
                    sqlCommand.Parameters.Add("@WatchdogCounter", SqlDbType.Int).Value = 0;
                    string_Response = "COMMAND\tEXIT";
                }
                else
                {
                    if (var_Session_id == -1)
                    {
                        sqlCommand.Parameters.Add("@Session_id", SqlDbType.Int).Value = DBNull.Value;
                    }
                    else
                    {
                        sqlCommand.Parameters.Add("@Session_id", SqlDbType.Int).Value = var_Session_id;
                    }
                    sqlCommand.Parameters.Add("@CurrProcedure", SqlDbType.VarChar, 200).Value = var_CurrProcedure;
                    sqlCommand.Parameters.Add("@NextProcedure", SqlDbType.VarChar, 200).Value = var_NextProcedure;
                    if (var_TimerProcedure.Length == 0)
                    {
                        sqlCommand.Parameters.Add("@TimerProcedure", SqlDbType.VarChar, 200).Value = DBNull.Value;
                    }
                    else
                    {
                        sqlCommand.Parameters.Add("@TimerProcedure", SqlDbType.VarChar, 200).Value = var_TimerProcedure;
                    }
                    sqlCommand.Parameters.Add("@Response", SqlDbType.VarChar, -1).Value = string_Response;
                    sqlCommand.Parameters.Add("@WatchdogCounter", SqlDbType.Int).Value = var_WatchdogCounter;
                }
                try
                {
                    sqlCommand.CommandTimeout = Timeout1;
                    sqlCommand.ExecuteNonQuery();
                }
                catch (SqlException ex3)
                {
                    ErrorCode = ((ex3.Number == -2) ? -4 : -3);
                    return "";
                }
                catch
                {
                    ErrorCode = -3;
                    return "";
                }
            
        return string_Response;
    }


    private static void SetVariable(SqlConnection conn, string Terminal, string Name, string Value, out int ErrorCode, int Timeout1)
    {
        ErrorCode = 0;
        SqlCommand sqlCommand = new SqlCommand("pdt_SetTerminalVariable", conn);
        sqlCommand.CommandType = CommandType.StoredProcedure;
        sqlCommand.Parameters.Add("@TerminalAddress", SqlDbType.VarChar, 200).Value = Terminal;
        sqlCommand.Parameters.Add("@VariableName", SqlDbType.VarChar, 200).Value = Name;
        if (Value == "\0")
        {
            sqlCommand.Parameters.Add("@VariableValue", SqlDbType.VarChar, 200).Value = DBNull.Value;
        }
        else
        {
            sqlCommand.Parameters.Add("@VariableValue", SqlDbType.VarChar, 200).Value = Value;
        }
        try
        {
            sqlCommand.CommandTimeout = Timeout1;
            sqlCommand.ExecuteNonQuery();
        }
        catch (SqlException ex)
        {
            ErrorCode = ((ex.Number == -2) ? -4 : -3);
        }
        catch
        {
            ErrorCode = -3;
        }
    }
    

}
