﻿using Microsoft.SqlServer.Server;
using System;
using System.Data.SqlTypes;
using System.IO;
using System.Text;

[SqlUserDefinedAggregate(Format.UserDefined, IsInvariantToDuplicates = false, IsInvariantToNulls = true, IsInvariantToOrder = false, MaxByteSize = 8000)]
[Serializable]
public class PutawayAggregate : IBinarySerialize
{
  private StringBuilder stringBuilder_0;

  public void Init()
  {
    this.stringBuilder_0 = new StringBuilder();
  }

  public void Accumulate(SqlString value)
  {
    if (value.IsNull)
      value = (SqlString) "";
    string str1 = value.ToString();
    string str2 = this.stringBuilder_0.ToString();
    string str3 = "";
    if (str1.Length == str2.Length)
    {
      for (int index = 0; index < str1.Length; ++index)
      {
        char ch1 = str1[index];
        char ch2 = str2[index];
        str3 = (ch1 == 'R' ? 0 : (ch1 != 'P' ? 1 : 0)) != 0 ? str3 + (object) ch2 : str3 + (object) ch1;
      }
    }
    else
      str3 = str1;
    this.stringBuilder_0.Remove(0, str2.Length);
    this.stringBuilder_0.Insert(0, str3);
  }

  public void Merge(PutawayAggregate other)
  {
    string str1 = this.stringBuilder_0.ToString();
    string str2 = other.stringBuilder_0.ToString();
    string str3 = "";
    if (str1.Length == str2.Length)
    {
      for (int index = 0; index < str1.Length; ++index)
      {
        char ch1 = str1[index];
        char ch2 = str2[index];
        str3 = (ch1 == 'P' ? 0 : (ch2 != 'P' ? 1 : 0)) != 0 ? ((ch1 == 'R' ? 0 : (ch2 != 'R' ? 1 : 0)) != 0 ? ((ch1 == 'B' ? 0 : (ch2 != 'B' ? 1 : 0)) != 0 ? str3 + (object) ch1 : str3 + "B") : str3 + "R") : str3 + "P";
      }
    }
    else
      str3 = str1;
    this.stringBuilder_0.Insert(0, str3);
  }

  public SqlString Terminate()
  {
    string empty = string.Empty;
    if ((this.stringBuilder_0 == null ? 1 : (this.stringBuilder_0.Length <= 0 ? 1 : 0)) == 0)
      empty = this.stringBuilder_0.ToString(0, this.stringBuilder_0.Length);
    return new SqlString(empty);
  }

  public void Read(BinaryReader r)
  {
    this.stringBuilder_0 = new StringBuilder(r.ReadString());
  }

  public void Write(BinaryWriter w)
  {
    string str = this.stringBuilder_0.ToString();
    w.Write(str);
  }
}
