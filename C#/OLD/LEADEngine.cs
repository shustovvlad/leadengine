﻿// Decompiled with JetBrains decompiler
// Type: LEADEngine
// Assembly: LEADEngine, Version=3.5.4571.20772, Culture=neutral, PublicKeyToken=null
// MVID: 827379C1-423B-4109-A89B-ED084C02FF99
// Assembly location: C:\2\leadengine-cleaned.dll

using Guardant;
using Microsoft.SqlServer.Server;
using ns1;
using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Xml;

public class LEADEngine
{
  private static bool bool_0 = false;
  private static Guid guid_0 = Guid.NewGuid();
  private static object object_0 = new object();
  private static string string_0 = "Период эксплуатации программного эмулятора ИИК завершен";
  private static string string_1 = "NO_AUTH";
  private static string string_2 = "cyRqc97eZxrKfNsmJN3t5dI5Axi8mNw16EunGNa1oAU=";
  private static string string_3 = "EC4LXT66BZNX3Z45cHq8xer70jqa64/yQXEhLeWcP1Y=";
  private static uint uint_0 = 741957090;
  private static uint uint_1 = 92;
  private static uint uint_2 = 20;
  private static uint uint_3 = 16;
  private static bool bool_1 = false;
  private static Handle handle_0;
  private static GrdE grdE_0;

  private static void smethod_0(string MessageText)
  {
    EventLog eventLog = new EventLog();
    eventLog.Source = Class2.smethod_0("ᚩᚣᚠᚤᚁᚹᚰᚷ");
    eventLog.WriteEntry(MessageText, EventLogEntryType.Error);
    eventLog.Close();
  }

  private static bool smethod_1()
  {
    lock (LEADEngine.object_0)
    {
      LEADEngine.grdE_0 = GrdApi.GrdStartup((GrdFMR) 1);
      if (LEADEngine.grdE_0 != 0)
      {
        if (LEADEngine.grdE_0 == GrdE.AlreadyInitialized)
        {
          GrdApi.GrdCloseHandle(LEADEngine.handle_0);
          GrdApi.GrdCleanup();
          LEADEngine.grdE_0 = GrdApi.GrdStartup((GrdFMR) 1);
          if (LEADEngine.grdE_0 != 0)
          {
            GrdApi.GrdCleanup();
            if (LEADEngine.bool_0)
              SqlContext.Pipe.Send(Class2.smethod_0("᩻᪦᪗᪑\x1A9B᪒ᚃ\x1A9C᪢\x1A9E᪭᪠᪙᪥᪣᪣\x1A9D᪴ᪧ᪨ᚑ᪦᪳᪤\x1AAE᪨᪬᪸᪷᪬᚛ᛃᛒᚿᛑᛄᛂᛐᛗᚤᚭ") + ((object) LEADEngine.grdE_0).ToString() + Class2.smethod_0("ᚆ"));
            LEADEngine.smethod_0(Class2.smethod_0("᩻᪦᪗᪑\x1A9B᪒ᚃ\x1A9C᪢\x1A9E᪭᪠᪙᪥᪣᪣\x1A9D᪴ᪧ᪨ᚑ᪦᪳᪤\x1AAE᪨᪬᪸᪷᪬᚛ᛃᛒᚿᛑᛄᛂᛐᛗ"));
            LEADEngine.bool_1 = false;
            return LEADEngine.bool_1;
          }
        }
        else
        {
          GrdApi.GrdCleanup();
          if (LEADEngine.bool_0)
            SqlContext.Pipe.Send(Class2.smethod_0("᩻᪦᪗᪑\x1A9B᪒ᚃ\x1A9C᪢\x1A9E᪭᪠᪙᪥᪣᪣\x1A9D᪴ᪧ᪨ᚑ᪦᪳᪤\x1AAE᪨᪬᪸᪷᪬᚛ᛃᛒᚿᛑᛄᛂᛐᛗᚤᚭ") + ((object) LEADEngine.grdE_0).ToString() + Class2.smethod_0("ᚆ"));
          LEADEngine.smethod_0(Class2.smethod_0("᩻᪦᪗᪑\x1A9B᪒ᚃ\x1A9C᪢\x1A9E᪭᪠᪙᪥᪣᪣\x1A9D᪴ᪧ᪨ᚑ᪦᪳᪤\x1AAE᪨᪬᪸᪷᪬᚛ᛃᛒᚿᛑᛄᛂᛐᛗ"));
          LEADEngine.bool_1 = false;
          return LEADEngine.bool_1;
        }
      }
      switch (LEADEngine.uint_0)
      {
        case 741957090:
          LEADEngine.string_2 = Class2.smethod_0("ᛀᛗᚱᛑᛄ᚛ᚚᛉᚿᛞᛙᚳᛏᚸᛞᛙᚷᚼᚢᛤᚦᛖᚼᚩᚶᛮᛠᚰᛦᛈᛲᚭᚳᛃᛴᛮᛈᛐᛤᚵᛴᛇᛜᛅ");
          LEADEngine.string_3 = Class2.smethod_0("ᚢᚡᚓᚬᚹᚶᚙᚚᚧᛀᚵᛀ᚜ᛄ\x169Fᚡᛐᚶᛠᚨᛩᛗᛥᚫᚥᛠᛨᛙᚯᚮᚪᛵᛎᛖᛄᛨᛍᛧᛚᛧᛕᚷᛠᛅ");
          break;
        case 741957894:
          LEADEngine.string_2 = Class2.smethod_0("ᚈᛂᛏᛉᚪᚧᛛ᚜ᚬᛈᚿ\x169Dᚶ\x169Dᛐᛖᛡᚡᛩᚣᚤᚥᛠᚥᛚᚻᛌᛑᚼᛄᛴᚴᛯᛋᛧᚹᛌᛈᛧᚹᛦᚹᛈᛅ");
          LEADEngine.string_3 = Class2.smethod_0("ᚪᚯᚖᛐᚙᚰᛝᚏᚨᛍᚩᛁᛢᚠᛡᚴᛚᛕᛜᛙᛈᛢᛕᛗᛌᛪᛩᛀᛱᛱᛮᛠᛑᛍᚵ᛭ᛐᚹᛴᛩᛌᛲᛜᛅ");
          break;
      }
      //LEADEngine.handle_0 = (Handle) null;
      LEADEngine.handle_0 = GrdApi.GrdCreateHandle(LEADEngine.handle_0, (GrdCHM) 0);
      // ISSUE: explicit reference operation
      //if (((Handle) @LEADEngine.handle_0).get_Address() == 0)
      //{
      //  GrdApi.GrdCleanup();
      //  if (LEADEngine.bool_0)
      //    SqlContext.Pipe.Send(Class2.smethod_0("᩻᪦᪗᪑\x1A9B᪒ᚃ᪥᪣\x1A9D\x1A9B᪘᪦᪢᪺ᚌ᪲᪻᪬᪤᪬ᪧ᪳᪤ᚕ\x169E᪺᪸᪷᪬᪹᪰\x1AC9\x169E᪳\x1AC0᪵᪹᪱᪻\x1AC5\x1AC4᪹ᚱ"));
      //  LEADEngine.smethod_0(Class2.smethod_0("᩻᪦᪗᪑\x1A9B᪒ᚃ᪥᪣\x1A9D\x1A9B᪘᪦᪢᪺ᚌ᪲᪻᪬᪤᪬ᪧ᪳᪤ᚕ\x169E᪺᪸᪷᪬᪹᪰\x1AC9\x169E᪳\x1AC0᪵᪹᪱᪻\x1AC5᪶ᚰ"));
      //  LEADEngine.bool_1 = false;
      //  return LEADEngine.bool_1;
      //}
      LEADEngine.grdE_0 = GrdApi.GrdSetAccessCodes(LEADEngine.handle_0, Convert.ToUInt32(LEADEngine.Decrypt(LEADEngine.string_2, LEADEngine.uint_0.ToString()).ToString()), Convert.ToUInt32(LEADEngine.Decrypt(LEADEngine.string_3, LEADEngine.uint_0.ToString()).ToString()));
      if (LEADEngine.grdE_0 != 0)
      {
        if (LEADEngine.bool_0)
          SqlContext.Pipe.Send(Class2.smethod_0("᩻᪦᪗᪑\x1A9B᪒ᚃᪧ᪦᪨᪗᪥ᪧ\x1A9C᪥᪤ᚍ᪭\x1A9F᪰᪡\x1AAE᪨᪶᪵᪴᪩ᚘ᪸᪸᪽᪷᪳\x1AAE\x169F᪺᪼\x1AD0\x1ACA᪴ᚥᚮ\x1ACA\x1AC8\x1AC7᪼\x1AC0\x1AC9\x1AD9ᚮ\x1ACE\x1AD0\x1AC9\x1ACD\x1AD1\x1ACA\x1ACA\x1AD3\x1ACF\x1AE7ᛂᚺᛃ") + ((object) LEADEngine.grdE_0).ToString() + Class2.smethod_0("ᚆ"));
        LEADEngine.smethod_0(Class2.smethod_0("᩻᪦᪗᪑\x1A9B᪒ᚃᪧ᪦᪨᪗᪥ᪧ\x1A9C᪥᪤ᚍ᪭\x1A9F᪰᪡\x1AAE᪨᪶᪵᪴᪩ᚘ᪸᪸᪽᪷᪳\x1AAE\x169F᪺᪼\x1AD0\x1ACA᪴ᚥᚮ\x1ACA\x1AC8\x1AC7᪼\x1AC0\x1AC9\x1AD9ᚮ\x1ACE\x1AD0\x1AC9\x1ACD\x1AD1\x1ACA\x1ACA\x1AD3\x1ACF\x1AE7ᛂ"));
        GrdApi.GrdCloseHandle(LEADEngine.handle_0);
        GrdApi.GrdCleanup();
        LEADEngine.bool_1 = false;
        return LEADEngine.bool_1;
      }
      LEADEngine.grdE_0 = GrdApi.GrdSetFindMode(LEADEngine.handle_0, (GrdFMR) 1, (GrdFM) 43, LEADEngine.uint_1, LEADEngine.uint_0, LEADEngine.uint_2, LEADEngine.uint_3, 0U, (GrdDT) 8, (GrdFMM) 128, (GrdFMI) 2);
      if (LEADEngine.grdE_0 != 0)
      {
        if (LEADEngine.bool_0)
          SqlContext.Pipe.Send(Class2.smethod_0("᩿\x1A9E᪔᪑᪤᪗\x1A9F\x1AAF\x1A9Eᚆ᩿᪀᪃ᚊ᪨᪡ᚍ᪬᪠᪭᪡᪶᪲᪪᪪᪳ᚗᚠ") + ((object) LEADEngine.grdE_0).ToString() + Class2.smethod_0("ᚆ"));
        LEADEngine.smethod_0(Class2.smethod_0("᩿\x1A9E᪔᪑᪤᪗\x1A9F\x1AAF\x1A9Eᚆ᩿᪀᪃ᚊ᪨᪡ᚍ᪬᪠᪭᪡᪶᪲᪪᪪᪳"));
        GrdApi.GrdCloseHandle(LEADEngine.handle_0);
        GrdApi.GrdCleanup();
        LEADEngine.bool_1 = false;
        return LEADEngine.bool_1;
      }
      //FindInfo findInfo = (FindInfo) null;
      //uint num1;
      //LEADEngine.grdE_0 = GrdApi.GrdFind(LEADEngine.handle_0, (GrdF) 1, ref num1, ref findInfo);
      //if (LEADEngine.grdE_0 != 0)
      //{
      //  if (LEADEngine.bool_0)
      //    SqlContext.Pipe.Send(Class2.smethod_0("᩻᪦᪗᪑\x1A9B᪒ᚃ᪫ᪧ\x1A9B᪤᪠᪸ᚊ᪪\x1A9C᪭\x1A9E᪫᪥᪳᪲᪱᪦ᚕ\x1A8E\x1A8F᪒ᚙᚢ") + ((object) LEADEngine.grdE_0).ToString() + Class2.smethod_0("ᚆ"));
      //  LEADEngine.smethod_0(Class2.smethod_0("᩻᪦᪗᪑\x1A9B᪒ᚃ᪫ᪧ\x1A9B᪤᪠᪸ᚊ᪪\x1A9C᪭\x1A9E᪫᪥᪳᪲᪱᪦ᚕ\x1A8E\x1A8F᪒"));
      //  GrdApi.GrdCloseHandle(LEADEngine.handle_0);
      //  GrdApi.GrdCleanup();
      //  LEADEngine.bool_1 = false;
      //  return LEADEngine.bool_1;
      //}
      LEADEngine.grdE_0 = GrdApi.GrdLogin(LEADEngine.handle_0, 0U, (GrdLM) 0);
      if (LEADEngine.grdE_0 != 0)
      {
        if (LEADEngine.bool_0)
          SqlContext.Pipe.Send(Class2.smethod_0("᩻᪦᪗᪑\x1A9B᪒ᚃ\x1A9B᪕᪥ᪧ᪦᪪\x1A9Aᚋ᪦ᚍ᪆᪇\x1A8Aᚑᚚ") + ((object) LEADEngine.grdE_0).ToString() + Class2.smethod_0("ᚆ"));
        LEADEngine.smethod_0(Class2.smethod_0("᩻᪦᪗᪑\x1A9B᪒ᚃ\x1A9B᪕᪥ᪧ᪦᪪\x1A9Aᚋ᪦ᚍ᪆᪇\x1A8A"));
        GrdApi.GrdCloseHandle(LEADEngine.handle_0);
        GrdApi.GrdCleanup();
        LEADEngine.bool_1 = false;
        return LEADEngine.bool_1;
      }
      LEADEngine.bool_1 = true;
      if (LEADEngine.smethod_7(19U, 1, 0U) == Class2.smethod_0("ᚎ"))
      {
        DateTime now;
        if (LEADEngine.smethod_7(0U, 8, 0U) == Class2.smethod_0("ᚫᚳᚫᚬᚥᚣᚷᚩ"))
        {
          int num2 = 0;
          now = DateTime.Now;
          string str = now.ToString(Class2.smethod_0("ᛖᛗᛘᛙᚮᚯᛇᛈ"));
          int Length = 8;
          LEADEngine.smethod_3((uint) num2, str, Length);
        }
        DateTime exact = DateTime.ParseExact(LEADEngine.smethod_7(0U, 8, 0U), Class2.smethod_0("ᛖᛗᛘᛙᚮᚯᛇᛈ"), (IFormatProvider) null);
        int num3;
        if (!(exact > DateTime.Now))
        {
          DateTime dateTime1 = exact;
          now = DateTime.Now;
          DateTime dateTime2 = now.AddDays(-31.0);
          num3 = !(dateTime1 <= dateTime2) ? 1 : 0;
        }
        else
          num3 = 0;
        if (num3 == 0)
        {
          LEADEngine.smethod_4();
          LEADEngine.bool_1 = false;
        }
      }
      return LEADEngine.bool_1;
    }
  }

  private static void smethod_2()
  {
    if (LEADEngine.uint_0 == 741957090U)
      LEADEngine.uint_0 = 741957894U;
    else
      LEADEngine.uint_0 = 741957090U;
  }

  private static bool smethod_3(uint FieldNumber, string Value, int Length)
  {
    bool flag = false;
    int num = 0;
    if (!LEADEngine.bool_1)
      LEADEngine.smethod_1();
    for (; (num >= 100 ? 0 : (!flag ? 1 : 0)) != 0; ++num)
    {
      flag = LEADEngine.smethod_5(FieldNumber, Value, Length);
      Thread.Sleep(500);
      LEADEngine.smethod_2();
      LEADEngine.smethod_1();
    }
    return flag;
  }

  private static void smethod_4()
  {
    for (uint FieldNumber = 0; FieldNumber <= 9U; ++FieldNumber)
      LEADEngine.smethod_3(FieldNumber, Class2.smethod_0("ᚍᚎᚏᚐᚑᚒᚓᚔ"), 8);
    LEADEngine.smethod_0(Class2.smethod_0("᩼\x1A9E\x1A9D᪘᪘᪔᪘᪘\x1A9A᪣᪥ᚈ᪬ᪧ᪣᪳\x1AAF᪬᪥᪥\x1AAE᪪᪨ᚔ᪩᪦᪵᪴\x1AC4\x1ABF᚛᪔᪕᪘"));
  }

  private static bool smethod_5(uint FieldNumber, string Value, int Length)
  {
    bool flag = true;
    lock (LEADEngine.object_0)
    {
      byte[] bytes = Encoding.Default.GetBytes(Value);
      if (Length > Value.Length)
        Length = Value.Length;
      try
      {
        LEADEngine.grdE_0 = GrdApi.GrdPI_Update(LEADEngine.handle_0, FieldNumber, 0U, Length, bytes, 0U, (GrdUM) 0);
      }
      catch (Exception ex)
      {
        if (LEADEngine.bool_0)
          SqlContext.Pipe.Send(Class2.smethod_0("᩻᪦᪗᪑\x1A9B᪒ᚃ\x1A9B᪕᪥\x1A9F᪩᪡ᚊ\x1A9Dᚌᪧ᪩᪽᪷ᚑᚚ") + ex.Message + Class2.smethod_0("ᚆ"));
        LEADEngine.smethod_0(Class2.smethod_0("᩻᪦᪗᪑\x1A9B᪒ᚃ\x1A9B᪕᪥\x1A9F᪩᪡ᚊ\x1A9Dᚌᪧ᪩᪽᪷ᚑᚚ") + ex.Message + Class2.smethod_0("ᚆ"));
        flag = false;
      }
      if (LEADEngine.grdE_0 != 0)
      {
        flag = false;
        if (LEADEngine.bool_0)
          SqlContext.Pipe.Send(Class2.smethod_0("᩻᪦᪗᪑\x1A9B᪒ᚃ᪕\x1A9D᪗᪢᪠ᪧ᪬᪠᪦᪥ᚎᚶᛂᚵᚒ᚛") + ((object) LEADEngine.grdE_0).ToString() + Class2.smethod_0("ᚆ"));
        LEADEngine.smethod_0(Class2.smethod_0("᩻᪦᪗᪑\x1A9B᪒ᚃ᪕\x1A9D᪗᪢᪠ᪧ᪬᪠᪦᪥ᚎᚶᛂᚵᚒ᚛") + ((object) LEADEngine.grdE_0).ToString() + Class2.smethod_0("ᚆ"));
      }
    }
    return flag;
  }

  private static LEADEngine.Struct5 smethod_6(uint FieldNumber, int BufferSize, uint ReadPassword)
  {
    lock (LEADEngine.object_0)
    {
      LEADEngine.Struct5 struct5 = new LEADEngine.Struct5();
      struct5.bool_0 = false;
      byte[] bytes = new byte[BufferSize];
      try
      {
        LEADEngine.grdE_0 = GrdApi.GrdPI_Read(LEADEngine.handle_0, FieldNumber, 0U, BufferSize, out bytes, ReadPassword);
      }
      catch (Exception ex)
      {
        if (LEADEngine.bool_0)
          SqlContext.Pipe.Send(ex.Message);
        struct5.string_0 = "";
        struct5.bool_0 = true;
        return struct5;
      }
      if (LEADEngine.grdE_0 != 0)
      {
        if (LEADEngine.grdE_0 == GrdE.DongleNotFound)
          LEADEngine.bool_1 = false;
        if (LEADEngine.bool_0)
          SqlContext.Pipe.Send(Class2.smethod_0("᩻᪦᪗᪑\x1A9B᪒ᚃ᪫ᪧ\x1A9B᪤᪠᪸ᚊ᪃᪄᪇ᚎᚗ") + ((object) LEADEngine.grdE_0).ToString() + Class2.smethod_0("ᚆ"));
        LEADEngine.smethod_0(Class2.smethod_0("᩻᪦᪗᪑\x1A9B᪒ᚃ᪫ᪧ\x1A9B᪤᪠᪸ᚊ\x1A9F\x1A9C᪪᪫᪺᪵ᚑ᪪᪪ᚔ\x1A8D\x1A8E᪑"));
        struct5.string_0 = "";
        struct5.bool_0 = true;
        return struct5;
      }
      struct5.string_0 = Encoding.UTF8.GetString(bytes);
      struct5.bool_0 = false;
      return struct5;
    }
  }

  private static string smethod_7(uint FieldNumber, int BufferSize, uint ReadPassword)
  {
    if (!LEADEngine.bool_1)
      LEADEngine.smethod_1();
    bool flag = false;
    string str = "";
    for (int index = 1; (flag ? 0 : (index <= 300 ? 1 : 0)) != 0; ++index)
    {
      LEADEngine.Struct5 struct5 = LEADEngine.smethod_6(FieldNumber, BufferSize, ReadPassword);
      flag = !struct5.bool_0;
      str = struct5.string_0;
      if (!flag)
      {
        Thread.Sleep(500);
        LEADEngine.smethod_2();
        LEADEngine.smethod_1();
      }
    }
    if (!flag)
    {
      if (LEADEngine.bool_0)
        SqlContext.Pipe.Send(Class2.smethod_0("᩺᪓ᙿ᪣᪕᪒\x1A9E᪢᪦᪲ᚇᪧ᪩᪨᪣᪣\x1A9F᪣᪰᪲᪩ᚒ᪺᪶᪪᪳\x1AAF᪭ᚙ᪲᪲᚜᪕᪖᪙"));
      LEADEngine.smethod_0(Class2.smethod_0("᩻᪦᪗᪑\x1A9B᪒ᚃ᪫ᪧ\x1A9B᪤᪠᪸ᚊ\x1A9F\x1A9C᪪᪫᪺᪵ᚑ᪪᪪ᚔ\x1A8D\x1A8E᪑"));
    }
    return str;
  }

  public static int CountRowsForSQL_Inner(string SQLStatement)
  {
    SqlConnection connection = new SqlConnection(LEADEngine.smethod_7(16U, 23, 0U));
    connection.Open();
    SqlCommand sqlCommand = new SqlCommand(SQLStatement.ToString(), connection);
    int num = 0;
    try
    {
      SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
      while (sqlDataReader.Read())
        ++num;
      sqlDataReader.Close();
    }
    catch (Exception ex)
    {
      num = -1;
      if (LEADEngine.bool_0)
      {
        SqlContext.Pipe.Send(Class2.smethod_0("ᚊᚋᚌᚍᚎᚏᚐᚑᚒᚓᚇ᪆᪱᪢\x1A9C᪦\x1A9Dᚎ\x1AAE᪰᪩ᚒ᪥\x1ABF᪵᪴᪴᪲\x1AAE᪷᪳᪴\x169Dᛁᛮᛵᛯᛶᛕᛳ\x16FC\x16F9ᛍᛷ\x16FBᛝᛜᛘ᛬ᛗ\x16FD\x16FEᛶᜄᚳᚼ\x1AD2\x1AC6\x1ADE\x1AC8\x1AD4\x1AD8ᛄᚼᛊᛋᛌᛍᛎᛏᛐᛑᛒᛓ"));
        SqlContext.Pipe.Send(ex.Message);
        SqlContext.Pipe.Send(Class2.smethod_0("ᚊᚋᚌᚍᚎᚏᚐᚑᚒᚓᚇ᪆᪱᪢\x1A9C᪦\x1A9Dᚎ\x1AAE᪰᪩ᚒ᪥\x1ABF᪵᪴᪴᪲\x1AAE᪷᪳᪴\x169Dᛁᛮᛵᛯᛶᛕᛳ\x16FC\x16F9ᛍᛷ\x16FBᛝᛜᛘ᛬ᛗ\x16FD\x16FEᛶᜄᚳᚼ\x1ACF\x1AD4\x1AD4\x1ACD\x1ADFᛃᚻᛉᛊᛋᛌᛍᛎᛏᛐᛑᛒ"));
      }
    }
    connection.Close();
    return num;
  }

  [SqlProcedure]
  public static void ShowSessionGUID()
  {
    SqlContext.Pipe.Send(LEADEngine.guid_0.ToString());
  }

  [SqlProcedure]
  public static void SetDebug(SqlBoolean IsDebug)
  {
    LEADEngine.bool_0 = IsDebug.Value;
  }

  private static int smethod_8(string ProcedureName)
  {
    return ProcedureName.Length;
  }

  [SqlProcedure]
  public static void CountRowsForSQL(SqlString SQLStatement, out SqlInt32 RowCount)
  {
    if (LEADEngine.smethod_7(1U, 8, 0U) == Class2.smethod_0("ᚱᚣᚱᚭᚰᚵᚷᚥ"))
      RowCount = (SqlInt32) LEADEngine.CountRowsForSQL_Inner(SQLStatement.ToString());
    else
      RowCount = (SqlInt32) (-1);
  }

  [SqlProcedure]
  public static void GetXMLCube(SqlXml ObjectXML, SqlInt32 ReporterFilter_id, SqlString DirectWhere, SqlBoolean IsIgnoreGrouping, out SqlString SQL, SqlInt32 ReporterLayer_id)
  {
    SQL = SqlString.Null;
    if (LEADEngine.smethod_8(Class2.smethod_0("ᚤᛃᛓᚸᚮᚮᚦᛙᛇᛋ")) % 5 != 0)
      return;
    using (SqlConnection sqlConnection = new SqlConnection(LEADEngine.smethod_7(16U, 23, 0U)))
    {
      sqlConnection.Open();
      DataPathConstructor dataPathConstructor = new DataPathConstructor();
      dataPathConstructor.ThisConnection = sqlConnection;
      if (LEADEngine.smethod_7(2U, 8, 0U) == Class2.smethod_0("ᚲᚲᚮᚰᚪᚣᚩᚺ"))
        SQL = (SqlString) dataPathConstructor.ConstructSQLFromXML(ObjectXML, ReporterFilter_id, DirectWhere.ToString(), IsIgnoreGrouping, ReporterLayer_id);
      else
        SQL = (SqlString) Class2.smethod_0("ᚰᚣᚫᚥᚤᚶᚃᚕ");
    }
  }

  [SqlProcedure]
  public static void CLRGetVariable(SqlString GUID, SqlInt32 ParameterType_id, out SqlString Value, out SqlBoolean IsExist, out SqlString FilterCondition, out SqlBoolean IsApplication, out SqlInt32 Priority)
  {
    Value = SqlString.Null;
    IsExist = SqlBoolean.False;
    FilterCondition = SqlString.Null;
    IsApplication = SqlBoolean.Null;
    Priority = SqlInt32.Null;
    if (LEADEngine.smethod_8(Class2.smethod_0("ᚠᚪᚱᚧᛆᛖᚹᛅᛗᛏᛈᛊᛕᛏ")) % 5 != 4)
      return;
    SqlConnection connection = new SqlConnection(LEADEngine.smethod_7(16U, 23, 0U));
    connection.Open();
    if (LEADEngine.smethod_7(3U, 8, 0U) != Class2.smethod_0("ᚥᚷᚣᚲᚰᚬᚲᚦ"))
      ParameterType_id = (SqlInt32) 0;
    SqlCommand sqlCommand = new SqlCommand(LEADEngine.smethod_7(11U, 6, 0U) + Class2.smethod_0("ᙽᚲᚮᚰᚁᚓᚃᚿᚻᛇᛓᛝᛎᛇᚗᚌᛈᚴᛘᛜᛥᛗᛥᚷᛤᛤᛛᛡ᛭ᛣᛪᛪᛚᚪ\x169Fᛛᛊᛵᛄᛴᛵᛲᛰ᛫ᛪ\x16FEᛴ\x16FB\x16FB᛫ᚯᛍᚱ\x16FBᜆᜂᜊᜂᜃᛀᛴᛣᜎᛝ\x170Dᜎᜋᜉᜄᜃ\x1717\x170D᜔᜔ᜄᛔᛉᛚᛔᛘᛍᜉ\x16FFᜢ\x171Aᜡᜥ\x171Dᜩᜯ᜔ᛘᛶᛚᜤᜯᜫᜳᜫᜬᛩ\x171Dᜓ᜶ᜮ᜵\x1739ᜱ\x173Dᝃᜨᛸ᛭\x16FEᛸᛰ\x1717ᜤᜢᜡᛵᜱ") + LEADEngine.smethod_7(12U, 23, 0U) + GUID.ToString() + Class2.smethod_0("ᚺᙾᛖᛉᛕᛊᚃᚌᛓᛕᛓᛗᛌᛕᚔᚌᛄᚶᚴᛂᚶᚒᛃᛕᛧᛗᛤᛝ᛭ᛟ᛭ᛐᛶᛮᛤᛟᛪᛦᚣᛁᚥ") + ParameterType_id.ToString(), connection);
    try
    {
      SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
      while (sqlDataReader.Read())
      {
        Value = (SqlString) sqlDataReader.GetValue(0).ToString();
        IsExist = SqlBoolean.True;
        FilterCondition = !sqlDataReader.IsDBNull(1) ? (SqlString) sqlDataReader.GetValue(1).ToString() : (SqlString) "";
        IsApplication = !(sqlDataReader.GetValue(2).ToString() == Class2.smethod_0("ᚎ")) ? SqlBoolean.False : SqlBoolean.True;
        Priority = (SqlInt32) Convert.ToInt32(sqlDataReader.GetValue(3));
      }
      sqlDataReader.Close();
    }
    catch (Exception ex)
    {
      Value = SqlString.Null;
      SqlContext.Pipe.Send(ex.Message);
    }
  }

  [SqlProcedure]
  public static void CountRecords(SqlString TableName, out SqlInt32 RowCount)
  {
    RowCount = (SqlInt32) (-1);
    if (LEADEngine.smethod_8(Class2.smethod_0("ᚠᛍᛔᛎᛕᚴᛈᛇᛔᛘᛋᛛ")) % 5 != 2)
      return;
    RowCount = (SqlInt32) 0;
    SqlConnection connection = new SqlConnection(LEADEngine.smethod_7(16U, 23, 0U));
    connection.Open();
    SqlCommand sqlCommand = new SqlCommand(LEADEngine.smethod_7(11U, 6, 0U) + Class2.smethod_0("ᙽᛁᛎᛕᛏᛖᚋᚎᚎᚆᚭᚺᚸᚷᚋᛇ") + TableName.ToString() + Class2.smethod_0("ᚺᙾᛖᛉᛕᛊᚃᚌᛓᛕᛓᛗᛌᛕᚔ"), connection);
    try
    {
      SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
      while (sqlDataReader.Read())
        RowCount = (SqlInt32) sqlDataReader.GetInt32(0);
      sqlDataReader.Close();
    }
    catch
    {
      RowCount = (SqlInt32) (-1);
    }
    if (LEADEngine.smethod_7(4U, 8, 0U) != Class2.smethod_0("ᚫᚭᚳᚡᚭᚱᚱᚩ"))
      RowCount = (SqlInt32) (-1);
    connection.Close();
  }

  [SqlProcedure]
  public static void GetTableColumn(SqlString TableName, SqlString KeyColumn, SqlString GetColumn, SqlString KeyValue, out SqlString Value)
  {
    Value = SqlString.Null;
    if (LEADEngine.smethod_8(Class2.smethod_0("ᚤᛃᛓᚴᛂᛄᛏᛉᚨᛕᛓᛝᛖᛘ")) % 6 == 2)
    {
      SqlConnection connection = new SqlConnection(LEADEngine.smethod_7(16U, 23, 0U));
      connection.Open();
      string cmdText;
      if (LEADEngine.smethod_7(5U, 8, 0U) == Class2.smethod_0("ᚶᚭᚴᚡᚳᚧᚶᚳ"))
        cmdText = LEADEngine.smethod_7(11U, 6, 0U) + Class2.smethod_0("ᙽᚹ") + GetColumn.ToString() + Class2.smethod_0("ᚺᙾᚥᚲᚰᚯᚃᚿ") + TableName.ToString() + Class2.smethod_0("ᚺᙾᛖᛉᛕᛊᚃᚌᛓᛕᛓᛗᛌᛕᚔᚌᛄᚶᚴᛂᚶᚒᛎ") + KeyColumn.ToString() + Class2.smethod_0("ᚺᙾ᚜ ᚈ") + KeyValue.ToString() + Class2.smethod_0("ᚄ");
      else
        cmdText = LEADEngine.smethod_7(11U, 6, 0U) + Class2.smethod_0("ᙽᚹ") + TableName.ToString() + Class2.smethod_0("ᚺᙾᚥᚲᚰᚯᚃᚿ") + GetColumn.ToString() + Class2.smethod_0("ᚺᙾᛖᛉᛕᛊᚃᚌᛓᛕᛓᛗᛌᛕᚔᚌᛄᚶᚴᛂᚶᚒᛎ") + KeyColumn.ToString() + Class2.smethod_0("ᚺᙾ᚜ ᚈ") + KeyValue.ToString() + Class2.smethod_0("ᚄ");
      SqlCommand sqlCommand = new SqlCommand(cmdText, connection);
      try
      {
        SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
        while (sqlDataReader.Read())
          Value = (SqlString) sqlDataReader.GetValue(0).ToString();
        sqlDataReader.Close();
      }
      catch
      {
        Value = SqlString.Null;
      }
    }
    else
      Value = (SqlString) LEADEngine.string_1;
  }

  [SqlProcedure]
  public static void ApplyFunction(SqlString FunctionName, SqlString InputValue, out SqlString OutputValue)
  {
    OutputValue = SqlString.Null;
    if (LEADEngine.smethod_8(Class2.smethod_0("\x169Eᛎᛏᛌᛚᚨᛘᛒᛈᛚᛐᛗᛗ")) % 4 != 1)
      return;
    string str = !(InputValue == SqlString.Null ? true : false) ? Class2.smethod_0("ᚄ") + InputValue.ToString().Replace(Class2.smethod_0("ᚄ"), Class2.smethod_0("ᚄᚅ")) + Class2.smethod_0("ᚄ") : Class2.smethod_0("ᚫᚳᚫᚬ");
    SqlConnection connection = new SqlConnection(LEADEngine.smethod_7(16U, 23, 0U));
    connection.Open();
    SqlCommand sqlCommand = new SqlCommand(LEADEngine.smethod_7(11U, 6, 0U) + Class2.smethod_0("ᙽᚹ") + LEADEngine.smethod_7(10U, 6, 0U) + Class2.smethod_0("ᚺᙾ᚜ ") + FunctionName.ToString() + Class2.smethod_0("ᚅ") + str + Class2.smethod_0("ᚆ"), connection);
    try
    {
      OutputValue = (SqlString) sqlCommand.ExecuteScalar().ToString();
    }
    catch
    {
      OutputValue = SqlString.Null;
    }
    if (!(LEADEngine.smethod_7(6U, 8, 0U) != Class2.smethod_0("ᚱᚭᚦᚥᚵᚪᚨᚶ")))
      return;
    OutputValue = SqlString.Null;
  }

  [SqlProcedure]
  public static void ExecuteTransaction(SqlGuid GUID, out SqlBoolean IsNormal, out SqlInt32 ErrorNumber, out SqlString ErrorMessage)
  {
    IsNormal = SqlBoolean.True;
    ErrorNumber = SqlInt32.Null;
    ErrorMessage = SqlString.Null;
    if (LEADEngine.smethod_8(Class2.smethod_0("ᚢᛖᛄᛃᛖᛖᛈᚸᛗᛇᛕᛛᛊᛍᛟᛕᛜᛜ")) % 12 != 6)
      return;
    SqlConnection connection = new SqlConnection(LEADEngine.smethod_7(16U, 23, 0U));
    connection.Open();
    SqlString sqlString = (SqlString) (Class2.smethod_0("ᛑᛃᛌᛐᛀᚶᛕᛅᛓᛙᛈᛋᛝᛓᛚᛚᛌ") + GUID.ToString());
    string str1 = Class2.smethod_0("ᛑᛃᛌᛐᛀᚶᛕᛅᛓᚶᛙᛗᛙᛏᛝᛠᛖᛓᛢᛏ") + GUID.ToString();
    SqlCommand sqlCommand = new SqlCommand(Class2.smethod_0("ᚰᚣᚫᚥᚤᚶᚃᛅᚓᚹᚸᚴᚉᚰᚽᚻᚺᚎᛊ") + sqlString.ToString() + Class2.smethod_0("ᚺᙾᛀᛓᚁᛃᚃᛛᛎᛚᛏᚈᚑᛘᛚᛘᛜᛑᛚᚙᚑᚾᚸᚺᛉᚖᛁᛇᛂᛈ᚛ᛎᛢᛥᛨᛳᛵᛧᛵᛷᚥᛧ\x16FAᚨ᛫ᚪᜂᛵᜁᛶᚯᚸ\x16FFᜁ\x16FFᜃᛸᜁᛀᚸᛨᛨᚻ\x16FDᛋᛰᜄᜇᜊ\x1715\x1717ᜉ\x1717ᜅᜐᜌᛉᛧᛋᜎᛛᜢ\x1718᜔ᛑᜁᜅᛸ\x16FAᜈᛗ\x16FAᜒᛚ\x171Dᛪᜂ᜶ᜤᜣ᜶᜶ᜬᜳᜳ\x1716\x1739ᜱ\x1738\x173C᜴ᝀᝆᛮᜐᜣ᜔\x16FEᛳ᜵ᜃᝊᝀ\x173C\x16F9\x171Bᜮ\x171F"), connection);
    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
    string str2 = Class2.smethod_0("ᛁᛃᛂᛌᛂᛔᛈᚄᚥᚯᛚᚶᛘᛜᛘᛍᛙᚎᛑᛙᛥ\x169Eᚓᚴᛇᛛ᛫ᛪᛲᚚᛤᛪᛱᚪ\x169Fᛀᛂᛵ\x16FCᛲᛨᛍᛜᛑᛍᚪᜁ᛭\x16FFᛱᛷᛱᜃᚺᛈᛄᚾᛂᚷᛘᛱᛧᜀᜏᜐ\x16FFᜆᜅᛁ\x1718ᜄ\x1716ᜈᜎᜈ\x171Aᛑᛟᛛᛜᛖᛚᛏᛰᜉᜀᜢᜦᜢ\x1717ᜣᛘ\x171Bᜣᜯᛷᛝᜱᜤ᜴ᛡ᜶᜵ᜥᜳ\x1739ᜨᜫ\x173Dᜳ\x173A\x173A᛭\x1737ᝂ\x173F\x173Dᜳᝇ\x173Dᝄᝄᛷᝄ\x173Eᝐᝀᝈ\x16FDᝐᝄᝁᝅᜂᝆᝓᝒᝓᝐ\x175C\x175Dᝏᝏᜧ\x170Dᝡ\x1754ᝤᜑ\x1756\x1758\x1755\x1759ᝢᝦ\x175Bᝤ\x1759ᝫᝮᝦ\x176D\x1771ᝩ\x1775\x177Bᜣᝬᝮ\x176Dᝯᝃᜩ\x177Dᝰកᜭᝎᝡ\x1775ចងឌ᜴ᝒ᜶ᝌᝓ\x1739ឍកថ\x173D\x175Eᝨនᝯទផទឆធᝇᝥᝉ\x175Bᝦᝌឤពមវពᝒ\x175B\x1774ជលឫឪឲ\x175A\x1779\x175C\x176Dᝧ\x175Fអឦឩឬឲᝥᝦᝧᝨᝩឬឰឳាូᝯោៃ់ᝳ\x1774\x1775\x1776\x1777\x1778\x1779\x177Aួេោះ៍ក៕។ោ្៘ះ៊ៜ្៙៙ឧឍ");
    while (sqlDataReader.Read())
      str2 = str2 + sqlDataReader.GetString(0) + Class2.smethod_0("ᙽ");
    sqlDataReader.Close();
    string str3 = str2 + Class2.smethod_0("ᙽᙾᙿ ᚁᚂᚃᛇᛔᛓᛔᛑᛝᚊᛟᛞᛎᛜᛢᛑᛔᛦᛜᛣᛣᚱᚗᚘᚙᚚ᚛᚜\x169D\x169Eᛤᛸᛦᛥᚣᛋᛪ\x16FAᛛ\x16FAᛪᛸ\x16FE᛭ᛰᜂᛸ\x16FF\x16FFᛢᜅᜃᜅ\x16FBᜉᜌᜒᚺᚻᚼᚽᚾᚿᛀᛁᛂᛃᛄᛅᛆᛇᛈᛉᛊᛋᛌᛍᛎᛏᛐᛑᛲᜇᜦ\x1716ᜤᜪ\x1719\x171Cᜮᜤᜫᜫᜅ᜔ᜉᜅᛢᜀᛤ᛬") + GUID.ToString() + Class2.smethod_0("ᚄᚊᙿ ᚁᚂᚃᚄᚅᚆᚇᚈᚉᚊᚋᚌᚍᚎᚏᚐᚑᚒᚓᚔᚕᚖᚷᛈ᛫ᛩ᛫ᛡᛯᛲᛸᛎᛢᛯᛨᚤᛂᚦᚮᛉ\x16FCᜃ\x16F9ᛯᛔᛣᛘᛔᚸᚾᚳᚴᚵᚶᚷᚸᚹᚺᚻᚼᚽᚾᚿᛀᛁᛂᛃᛄᛅᛆᛇᛈᛉᛊ᛫\x16FC\x171F\x171D\x171F\x1715ᜣᜦᜬᜊ\x1716ᜢᜬ\x171Dᛙᛷᛛ\x16FC\x16FEᜱ\x1738ᜮᜤᜉ\x1718\x170Dᜉᛦ᜶\x173D\x173D\x173Aᝀᝀ᛭ᛮᛯᛰᛱᛲᛳᛴ\x173E\x173Cᛷ\x1718\x171Aᝍ\x1754ᝊᝀᜥ᜴ᜩᜥᜂᝌ\x1757ᜅ᜴᜶\x173Cᜉ\x1738ᝀ\x1738\x1739ᜎᝐ\x175E\x1755ᜒ\x175F\x1759ᝣ\x171E\x1737\x1739ᝬᝳᝩ\x175Fᝄᝓᝈᝄᜪᜢᝁᜤ᜵ᜦᝩ\x176Dᝰᝳ\x1779ᜬᜭᜮᜯᜰᜱᜲᜳ᜴᜵᜶\x1737\x177Dទ\x177F\x177E\x173Cᝯនឍ\x1774នឃទភឆញលទមមᝋᝌᝍᝎᝏᝐᝑᝒᝓ\x1754\x1755\x1756\x1757\x1758\x1759\x175A\x175B\x175C\x175D\x175E\x175Fᝠᝡᝢឃឋរតឋᝨឆᝪឋឍៀះួឳមឧវមខ\x1776\x1777\x1778\x1779\x177A\x177B\x177C\x177D\x177E\x177Fកខគឃងចឆជឈញដឋឌឍឮ឴២៣១៥េ៚៩\x17EA៙០\x17DFលឹឝើៗ៍៦\x17F5\x17F6៥\x17EC\x17EBឧ\x17F7\x17FE\x17FE\x17FB᠁᠁ឺឯឰឱឲឳ឴឵ាិីឹឺុូួើឿៀេែៃោៅំ៧\x17F1\x181C\x17F8\x181A\x181E\x181A\x180F\x181B័\x17EE្\x17F3᠌᠃ᠥᠩᠥ\x181Aᠦ៛ᠫᠲᠲᠯᠵᠵ២៣៤៥៦៧៨៩\x17EA\x17EB\x17EC\x17EDᠷᠵ\x17F0ᠺᡅᡁᡉᡁᡂ\x17FF᠘ᠱᠨᡊᡎᡊᠿᡋ᠌᠁᠒᠌᠄ᠢ᠆᠗᠈ᡋᡏᡒᡕᡛ\x180E\x180F᠐᠑᠒᠓᠔᠕᠖᠗᠘᠙\x181A\x181B\x181C\x181Dᡧᡭᡳᡦᡴᡷᠤᡮᡴ\x187Bᡷᠩᡋ\x187Eᢅ\x187Bᡱᡔᢂᢃᢁᢅᢇᠵᠾᡜᢊᢋᢉᢍᡩᢂᢑᢒᢁᢈᢇᡏᡄᡵᢘᢖᢋᢎᢎᢠᢞᢒ\x187Cᢐᢝᢖᡞᡓᢆᢚᢙᢦᢪᢝ\x187Eᢜᢰᢢᡪᡟᢄᢺᢰᢤᢱ\x18AEᢩᢚᢙᢕᡳᡫᣂ\x18AEᢺᣄᢵᣄᡲ\x187Bᢔ\x18ADᢣᢼᣋᣌᢻᣂᣁᢉ\x187Eᢆᢴᣓᣃᣑᣗᣆᣉᣛᣑᣘᣘᢒᢘᢍᣕᣔᣤᣕᣓᣧᣙᢝᢟᢣᢘᢹᢻᣮᣵᣫᣡᣆᣕᣊᣆ\x18ACᢤᢥᢦᢧᢨᢩᢪ\x18AB\x18AC\x18AD\x18AE\x18AFᣵ\x18FF\x18F6ᢳᢴᢵᢶᢷᢸᢹᢺᤀᤊᤁᢾᢿᣀᣁᣂᣃᣄᣅᤙᤌᤜᣉᣪ\x18FDᤑᤡᤠᤨᣐᣮᣒᣣᣯᣕᣖᣗᣘᣙᣚᣛᣜᤤ\x192Dᤳ\x192Fᣡᤨ\x192Cᤲ\x192E᤹\x192F\x192D\x192Dᣪ") + Class2.smethod_0("ᛏᛍᛋᛌᛃᛃᛆᛏᛄᛇᛓᛔᚣᚊᚋᚌᚍᚎᚏᚐᚑᛤᛢᛠᛡᛘᛘᛛᛤᚚᛯᛮᛞ᛬ᛲᛡᛤᛶ᛬ᛳᛳᚦᚧᚨᚩᚪᚫᚬᚭᜁᛴ\x16FCᛶᛵᜇᚴᛰᛛᛩᛪᛨ᛬\x16FAᛪᛲ᛫ᛡᛥᛳ\x16FFᛃᛡᛅᛓᛘᛔᛉᜅᛰ\x16FE\x16FF\x16FDᜁᜏ\x16FEᛷᜆᜇᛶ\x16FD\x16FC\x1715ᛙᛷᛛᛣ\x1ADF\x1AFE\x1AEF\x1AFD\x1AF8\x1AF2\x1AFDᬊ\x1AFDᬕᛧᬅ\x1AFEᛪᬇᬊᬃᬃᬑᛰᬂᬝᬕᬠᛵᬕᬗᬖᬋᬏᬏᬑᬚᬎᜀᜇ\x170Dᜂ\x173Eᜭ\x1738ᝅ᜵\x1737\x173B\x1737ᜬ\x1738ᝊᜎᜬᜐᜡᜒᜓ᜔\x1715\x1716\x1717\x1718\x1719\x176Dᝠᝰ\x171D\x173Eᝈᝳᝏ\x1771\x1775\x1771ᝦᝲᜧᝅᜩ\x173A") + Class2.smethod_0("ᛃᛇᛍᛉᛔᛊᛈᛈ\x169Fᚆᚇᚈᚉᚊᚋᚌᚍᛡᛔᛤᚑᚲᛅᛙᛩᛨᛰᚘᚶᚚᚫᚷ\x169D\x169E\x169Fᚠᛦᛰᛧᚤ\x16F9ᛸᜀᚨᚩᚪᚫᛮᛲᛵᛸ\x16FEᚱᛵᛴᜈᛸ\x16FEᚷᚸᚹᚺᚻᚼᚽᚾᜈᜆᛁᛊᛨᛶᛷᛵ\x16F9ᜇᛷ\x16FFᛸᛮᛲᜀᛗᛙᛑᛯᛓᛥᛧᛦ᛬ᛡᛙ\x171Cᜠᜣᜦᜬᛟᛠᛡᛢᛣᛤᛥᛦᛧᛨᛩᛪᝂᜭ᜶ᝂ᜵\x173Fᝃᛲ\x1737\x1739ᝁ\x1737ᝐᛸᜀᜊᜋ\x1716\x170Dᜎ\x1719ᜐᜒᜉ\x171Eᜄᜅᜆᜇᜈᜉᜊᜋᜌ\x170Dᜎᜏᝣ\x1756ᝦᜓ᜴ᝇ\x175Bᝫᝪᝲ\x171A\x1738\x171C\x173Dᝐᝤ\x1774ᝳ\x177Bᜣᜱᜥ\x1737ᝂᜨᜩᜪᜫᜬᜭᜮᜯ\x1775\x177F\x1776ᜳ\x1779ខញ\x177C\x1738\x177B\x177Fគចឋ\x173E\x173Fᝀᝁᝂᝃᝄᝅᝆᝇᝈᝉឝថហᝍᝮខផឥឤឬ\x1754ᝲ\x1756ᝧᝳ\x1759\x175A\x175B\x175C\x175D\x175E\x175Fᝠឦឰឧ\x177Fᝥᝦᝧᝨᝩᝪᝫᝬា឴ᝯឨធផឧឳឨឪមឬឞគង\x177Cយវ\x177Fថខ។្័៑ៈៈ់។ដ\x17DF\x17DE៎ៜ២៑។៦ៜ៣៣ឱភមយរលវឝឞ៨៦ឡែ៕៩\x17F9\x17F8᠀ឨំឪុឬ\x17EF\x17F3\x17F6\x17F9\x17FFឲឳ឴឵ាិីឹឺុូួ᠑᠄᠌᠆᠅᠗ោ᠀\x17EB\x17F9\x17FA\x17F8\x17FC᠊\x17FA᠂\x17FB\x17F1\x17F5᠃\x180F៓\x17F1៕\x17FB᠉᠊᠈᠌\x181A᠊᠒᠋᠁᠅᠓\x17EA\x17EC\x17F0៥ᠡ᠌\x181A\x181B᠙\x181Dᠫ\x181A᠓ᠢᠣ᠒᠙᠘ᠱ\x17F5᠓\x17F7\x181Dᠫᠬᠪᠮᠼᠫᠤᠳᠴᠣᠪᠩ᠍\x180F᠓᠈ᡄᠳᠾᡋᠻᠽᡁᠽᠲᠾᡐ᠔ᠲ᠖ᠧ᠘᠙\x181A\x181B\x181C\x181D\x181E\x181Fᠠᠡᠢᡶᡩ\x1879ᠦᡇᡑ\x187Cᡘ\x187A\x187E\x187Aᡯ\x187Bᠰᡎᠲᡃᠴᠵᠶᠷᠸᠹᠺᠻᢁᢋᢂᡚᡀᡁᡂᡃᢉᢓᢊᡇᢋᢊᢞᢎᢔᡨᡎᢔᢞᢕᡭ") + Class2.smethod_0("ᛆᛄᙿᚠᚪᛕᚱᛓᛗᛓᛈᛔᚉᚧᚋ\x169Dᚍᛏᛝᛔᚑᛡᛕᛞᛚᛙ᛫ᛗᛢᛞᚣᚣ") + sqlString.ToString() + Class2.smethod_0("ᚄᚇᙿᛉᛔᚂᚱᚳᚹᚆᚵᚽᚵᚶᚋᛑᛥᛓᛒᚐᛄᛦᛔᛦᛩᛀᛦᛚᚙᚺᚿᛵ᛫ᛟ᛬ᛩᛤᛕᛔᛐᚥᛃᚧᚯ᛭\x16FC\x16FA\x16FCᚭᜂᛰᛲ\x16FDᛷᚳᛯ") + sqlString.ToString() + Class2.smethod_0("ᚺᚅᚋ ᚡᚬᛒᛆᚹᛟᛗᛍᚉᚧᚋᚓᛀᛇᛂᛄᚶᚿᚚᚔᛞᛜᚗᚸᛂ᛭ᛉ᛫ᛯ᛫ᛠ᛬ᚡᚿᚣᚵᚥᛧᛵ᛬ᚩ\x16F9᛭ᛶᛲᛱᜃᛯ\x16FAᛶᚻᚻ") + str1 + Class2.smethod_0("ᚄᚇᙿᛉᛔᚂᚱᚳᚹᚆᚵᚽᚵᚶᚋᛑᛥᛓᛒᚐᛄᛦᛔᛦᛩᛀᛦᛚᚙᚺᚿᛵ᛫ᛟ᛬ᛩᛤᛕᛔᛐᚥᛃᚧᚯ᛭\x16FC\x16FA\x16FCᚭᜂᛰᛲ\x16FDᛷᚳᛯ") + str1 + Class2.smethod_0("ᚺᚅᚋ ᚡᚬᛒᛆᚹᛟᛗᛍᚉᚧᚋᚓᛀᛇᛂᛄᚶᚿᚚᚔ");
    sqlCommand.CommandText = str3;
    try
    {
      if (LEADEngine.smethod_7(7U, 8, 0U) == Class2.smethod_0("ᚠᚰᚸᚰᚵᚱᚽᚾ"))
        sqlDataReader = sqlCommand.ExecuteReader();
    }
    catch (Exception ex)
    {
      IsNormal = SqlBoolean.False;
      ErrorMessage = (SqlString) (Class2.smethod_0("᩼\x1A8E᪙᪕᪣ᚂ᪥᪤᪕᪣\x1A9E᪘᪣᪰᪣᪥ᚍ\x1AAF᪭᪤᪦᪲᪩᪬᪷ᚖ᪵\x1AC0᪱᪫᪵᪴\x169D᪶\x169F᪽᪶ᚢ\x1ABF\x1AC2᪻᪻\x1AC9ᚨ᪺\x1AD5\x1ACD\x1AD8ᚭ\x1ACD\x1ACF\x1ACE\x1AC3\x1AC7\x1AC7\x1AC9\x1AD2ᛐᚷ") + ex.Message.ToString());
      ErrorNumber = (SqlInt32) (-1);
    }
    if (IsNormal == SqlBoolean.True)
    {
      while (sqlDataReader.Read())
      {
        for (int ordinal = 0; ordinal < sqlDataReader.FieldCount; ++ordinal)
        {
          if (sqlDataReader.GetName(ordinal) == Class2.smethod_0("ᚢᚰᚱᚯᚳᛁᚱᚹᚲᚨᚬᚺ"))
            ErrorNumber = (SqlInt32) Convert.ToInt32(sqlDataReader.GetValue(ordinal));
          if (sqlDataReader.GetName(ordinal) == Class2.smethod_0("ᚢᚰᚱᚯᚳᛁᚰᚩᚸᚹᚨᚯᚮ"))
            ErrorMessage = (SqlString) sqlDataReader.GetValue(ordinal).ToString();
          if (sqlDataReader.GetName(ordinal) == Class2.smethod_0("ᚦᚱᚾᚮᚰᚴᚰᚥᚱ"))
            IsNormal = !(sqlDataReader.GetValue(ordinal).ToString() == Class2.smethod_0("ᚍ")) ? SqlBoolean.True : SqlBoolean.False;
        }
      }
      sqlDataReader.Close();
    }
  }

  [SqlProcedure]
  public static void RunTransaction(SqlGuid GUID, out SqlString ErrorMessage, out SqlBoolean IsNormal)
  {
    IsNormal = SqlBoolean.True;
    ErrorMessage = SqlString.Null;
    if (LEADEngine.smethod_8(Class2.smethod_0("ᚯᛓᛍᚴᛓᛃᛑᛗᛆᛉᛛᛑᛘᛘ")) % 9 != 5)
      return;
    SqlConnection connection = new SqlConnection(LEADEngine.smethod_7(16U, 23, 0U));
    connection.Open();
    SqlCommand sqlCommand = new SqlCommand(LEADEngine.smethod_7(11U, 6, 0U) + Class2.smethod_0("ᙽᚿᚍᚳᚲᚮᚃᚪᚷᚵᚴᚈᛄ") + (Class2.smethod_0("ᛑᛃᛌᛐᛀᚶᛕᛅᛓᛙᛈᛋᛝᛓᛚᛚᛌ") + GUID.ToString()).ToString() + Class2.smethod_0("ᚺᙾᛀᛓᚁᛃᚃᛛᛎᛚᛏᚈᚑᛘᛚᛘᛜᛑᛚᚙᚑᚾᚸᚺᛉᚖᛁᛇᛂᛈ᚛ᛎᛢᛥᛨᛳᛵᛧᛵᛷᚥᛧ\x16FAᚨ᛫ᚪᜂᛵᜁᛶᚯᚸ\x16FFᜁ\x16FFᜃᛸᜁᛀᚸᛨᛨᚻ\x16FDᛋᛰᜄᜇᜊ\x1715\x1717ᜉ\x1717ᜅᜐᜌᛉᛧᛋᜎᛛᜢ\x1718᜔ᛑᜁᜅᛸ\x16FAᜈᛗ\x16FAᜒᛚ\x171Dᛪᜂ᜶ᜤᜣ᜶᜶ᜬᜳᜳ\x1716\x1739ᜱ\x1738\x173C᜴ᝀᝆᛮᜐᜣ᜔\x16FEᛳ᜵ᜃᝊᝀ\x173C\x16F9\x171Bᜮ\x171F"), connection);
    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
    string str = "";
    while (sqlDataReader.Read())
      str = str + sqlDataReader.GetString(0) + Class2.smethod_0("ᙽ");
    sqlDataReader.Close();
    if (str.Length > 0)
    {
      sqlCommand.CommandText = str;
      try
      {
        if (!(LEADEngine.smethod_7(8U, 8, 0U) == Class2.smethod_0("ᚡᚣᚲᚣᚢᚮᚨᚶ")))
          return;
        sqlCommand.ExecuteNonQuery();
      }
      catch (Exception ex)
      {
        IsNormal = SqlBoolean.False;
        ErrorMessage = (SqlString) (Class2.smethod_0("ᚯᛓᛍᚴᛓᛃᛑᛗᛆᛉᛛᛑᛘᛘᚋᚔ") + GUID.ToString() + Class2.smethod_0("ᚆᚌᙿ᩿᪑\x1A9C᪘᪦ᚅ᪨ᪧ᪘᪦᪡\x1A9B᪦᪳᪦᪨ᚐ᪲᪰ᪧ᪩᪵᪬\x1AAF᪺ᚙ᪸\x1AC3᪴\x1AAE᪸᪷ᚺᚡ") + ex.Message.ToString());
      }
    }
    else
      IsNormal = SqlBoolean.True;
  }

  [SqlProcedure]
  public static void CLR_SetVariable(SqlGuid GUID, SqlInt32 ParameterType_id, SqlString Value, out SqlBoolean IsError, SqlString FilterCondition, SqlInt32 Priority, SqlInt32 IsApplication)
  {
    IsError = SqlBoolean.Null;
    if (LEADEngine.smethod_8(Class2.smethod_0("ᚠᚪᚱᚿᚴᛇᛗᚺᛆᛘᛐᛉᛋᛖᛐ")) % 5 != 0)
      return;
    SqlConnection connection = new SqlConnection(LEADEngine.smethod_7(16U, 23, 0U));
    connection.Open();
    SqlCommand sqlCommand = new SqlCommand(Class2.smethod_0("ᚰᚣᚫᚥᚤᚶᚃᚶᛊᛌᚻᛉᛋᛖᛐᚺᛎᛛᛔ᚜ᚑᚵᛢᛠᛪᛣᛥᛆᛚᛧᛠ᚜ᚺ\x169Eᛨᛳᛯᛷᛯᛰᚭᛘ᛬ᛮᛌ\x16F9ᛷᜁ\x16FA\x16FCᛜᛵ\x16FEᜁᛡᛵᜂ\x16FBᛃᚸ᛫\x16FFᜁᛟᜌᜊ᜔\x170Dᜏᛵᜋᜓ\x171Cᛴᜈ\x1715ᜎᛓᛗᛌ\x16FDᜣᜣᛶᜦᜠ\x1716ᜨ\x171Eᜥᜥᛤᛙᜀᜪᜮᜪᜒ\x1738ᜰᜦᛢᜉ\x1716᜔ᜓᛧ") + LEADEngine.smethod_7(13U, 14, 0U) + Class2.smethod_0("ᙽᚵᚧᚥᚳᚧᚃᛘᛎᛊᚇᚥᚉ") + ParameterType_id.ToString(), connection);
    SqlDataReader sqlDataReader1 = sqlCommand.ExecuteReader();
    SqlString sqlString1 = SqlString.Null;
    SqlString sqlString2 = SqlString.Null;
    SqlString sqlString3 = SqlString.Null;
    while (sqlDataReader1.Read())
    {
      sqlString1 = (SqlString) sqlDataReader1.GetValue(0).ToString();
      sqlString3 = (SqlString) sqlDataReader1.GetValue(1).ToString();
      sqlString2 = (SqlString) sqlDataReader1.GetValue(2).ToString();
      SqlString sqlString4 = (SqlString) sqlDataReader1.GetValue(3).ToString();
    }
    sqlDataReader1.Close();
    SqlInt32 sqlInt32_1 = (SqlInt32) 0;
    sqlCommand.CommandText = Class2.smethod_0("ᚰᚣᚫᚥᚤᚶᚃᛇᛓᛚᚇᚥᚉᛍᛚᛡᛛᛢᚗᚚᚚᚒᚹᛆᛄᛃᚗᛓ") + sqlString1.ToString() + Class2.smethod_0("ᚺᙾᛖᛉᛕᛊᚃᚌᛓᛕᛓᛗᛌᛕᚔᚌᛄᚶᚴᛂᚶᚒᛎ") + sqlString3.ToString() + Class2.smethod_0("ᚺᙾ᚜ ᚈ") + Value.ToString().Replace(Class2.smethod_0("ᚄ"), Class2.smethod_0("ᚄᚅ")) + Class2.smethod_0("ᚄ");
    SqlDataReader sqlDataReader2 = sqlCommand.ExecuteReader();
    while (sqlDataReader2.Read())
      sqlInt32_1 = (SqlInt32) sqlDataReader2.GetInt32(0);
    sqlDataReader2.Close();
    if (sqlInt32_1 == (SqlInt32) 0)
    {
      IsError = SqlBoolean.True;
    }
    else
    {
      IsError = SqlBoolean.False;
      if (sqlString2.ToString().Length > 0)
      {
        sqlCommand.CommandText = Class2.smethod_0("ᚰᚣᚫᚥᚤᚶᚃᚸᛍᛏᛚᚺᛎᛝᛠᛘᛡᚎᚬᚐ") + sqlString2.ToString() + Class2.smethod_0("ᚅᚅ") + Value.ToString().Replace(Class2.smethod_0("ᚄ"), Class2.smethod_0("ᚄᚅ")) + Class2.smethod_0("ᚄᚇ");
        SqlDataReader sqlDataReader3 = sqlCommand.ExecuteReader();
        while (sqlDataReader3.Read())
          Value = (SqlString) sqlDataReader3.GetString(0);
        sqlDataReader3.Close();
      }
      SqlInt32 sqlInt32_2 = (SqlInt32) 0;
      string str1 = Class2.smethod_0("ᛑᛃᛌᛐᛀᚨᛘᛒᛈᛚᛐᛗᛗᛀᛌᛞᛖᛏᛑᛜᛖᛥᛒ") + GUID.ToString();
      sqlCommand.CommandText = Class2.smethod_0("ᚰᚣᚫᚥᚤᚶᚃᛇᛓᛚᚇᚥᚉᛍᛚᛡᛛᛢᚗᚚᚚᚒᚹᛆᛄᛃᚗ᛫ᛲ᛭ᛪᛞᛧᛣᛢᛴᛴᚢ\x16FA᛭\x16F9ᛮᚧᚰᛷ\x16F9ᛷ\x16FBᛰ\x16F9ᚸᚰᛨᛚᛘᛦᛚᚶᛲᜆ\x16FAᜇᜀ\x16F9ᚽᛛᚿᛇ") + str1 + Class2.smethod_0("ᚄ");
      SqlDataReader sqlDataReader4 = sqlCommand.ExecuteReader();
      while (sqlDataReader4.Read())
        sqlInt32_2 = (SqlInt32) sqlDataReader4.GetInt32(0);
      sqlDataReader4.Close();
      if (sqlInt32_2 == (SqlInt32) 0)
      {
        sqlCommand.CommandText = Class2.smethod_0("ᚠᚰᚤᚡᚵᚧᚃᚸᚦᚨᚳᚭᚉᛅ") + str1 + Class2.smethod_0("ᚺᙾᚇᚻᚱᛃᛕᛅᛒᛋᛛᛍᛛᚾᛤᛜᛒᛍᛘᛔᛎᚒᛜᛢᛩᚖᛥᛧ᛭ᚚᛩᛱᛩᛪᚫᚠᛜᛘᛤᛰ\x16FA᛫ᛤᚨ\x16FF᛫\x16FDᛯᛵᛯᜁᚸᛂᛂᛃᛄᚾᚶᜅ\x170Dᜅᜆᛇᚼᛸᛤᜈᜌ\x1715ᜇ\x1715ᛧ᜔᜔ᜋᜑ\x171Dᜓ\x171A\x171Aᜊᛎᜥᜑᜣ\x1715\x171B\x1715ᜧᛞᛨᛨᛢᛚᜩᜱᜩᜪ᛫ᛠ\x171Cᜒ᜵ᜭ᜴\x1738ᜰ\x173Cᝂᜧ᛫᜵\x173Bᝂᛯ\x173Eᝆ\x173E\x173Fᜀᛵᜱᜠᝋ\x171Aᝊᝋᝈᝆᝁᝀ\x1754ᝊᝑᝑᝁᜅᝏ\x1755\x175Cᜉ\x1758ᝠ\x1758\x1759\x1717");
        sqlCommand.ExecuteNonQuery();
      }
      Value = !(Value == SqlString.Null ? true : false) ? (SqlString) (Class2.smethod_0("ᚄ") + Value.ToString().Replace(Class2.smethod_0("ᚄ"), Class2.smethod_0("ᚄᚅ")) + Class2.smethod_0("ᚄ")) : (SqlString) Class2.smethod_0("ᚫᚳᚫᚬ");
      FilterCondition = !(FilterCondition == SqlString.Null ? true : false) ? (SqlString) (Class2.smethod_0("ᚄ") + FilterCondition.ToString() + Class2.smethod_0("ᚄ")) : (SqlString) Class2.smethod_0("ᚫᚳᚫᚬ");
      string str2 = !(IsApplication == SqlInt32.Null ? true : false) ? IsApplication.ToString() : Class2.smethod_0("ᚫᚳᚫᚬ");
      string str3 = !(Priority == SqlInt32.Null ? true : false) ? Priority.ToString() : Class2.smethod_0("ᚫᚳᚫᚬ");
      sqlCommand.CommandText = Class2.smethod_0("ᚿᛃᛆᛉᛏᚂᛗᛖᛆᛔᛚᛉᛌᛞᛔᛛᛛᚎᛤᛠᛕᛓᛧᛙᚕᛑ") + str1.ToString() + Class2.smethod_0("ᚺᙾᛒᛅᛕᚂᚾᚺᛆᛒᛜᛍᛆᚊᚨᚌ") + Value.ToString() + Class2.smethod_0("ᚉᙾᚺᚦᛊᛎᛗᛉᛗᚩᛖᛖᛍᛓᛟᛕᛜᛜᛌᚐᚮᚒ") + FilterCondition.ToString() + Class2.smethod_0("ᚉᙾᚺᚰᛓᛋᛒᛖᛎᛚᛠᛅᚉᚧᚋ") + str3 + Class2.smethod_0("ᚉᙾᚺᚩᛔᚣᛓᛔᛑᛏᛊᛉᛝᛓᛚᛚᛊᚎᚬᚐ") + str2 + Class2.smethod_0("ᙽᛕᛇᛅᛓᛇᚃᚴᛆᛘᛈᛕᛎᛞᛐᛞᛁᛧᛟᛕᛐᛛᛗᚔᚲᚖ") + ParameterType_id.ToString() + Class2.smethod_0("ᛆᛄᙿᚠᚡᛔᛒᛛᛈᛕᛜᛖᛝᚊᚨᚌ\x169Dᚎᛘᛞᛤᛗᛥᛨᚕᛟᛥ᛬ᛨᚚᛖ") + str1.ToString() + Class2.smethod_0("ᚺᙾᚇᚰᛂᛔᛄᛑᛊᛚᛌᛚᚽᛣᛛᛑᛌᛗᛓ᚜ᚑᛍᛉᛕᛡ᛫ᛜᛕᚥᚚᛁᛥᛩᛲᛤᛲᛄᛱᛱᛨᛮ\x16FAᛰᛷᛷᚶᚫᛜ\x16FFᛷ\x16FEᜂ\x16FAᜆᜌᛀᚵᛟᜊᛙᜉᜊᜇᜅᜀ\x16FFᜓᜉᜐᜐᛌᛄ\x16FBᛧᛳ\x16FDᛮ\x16FDᛋᛔ") + ParameterType_id.ToString() + Class2.smethod_0("ᚉᙾ") + Value.ToString() + Class2.smethod_0("ᚉᙾ") + FilterCondition.ToString() + Class2.smethod_0("ᚉᙾ") + str3 + Class2.smethod_0("ᚉᙾ") + str2 + Class2.smethod_0("ᚆᚙᙿᛃᛐᛏᛐᛍᛙᚆᛛᛚᛊᛘᛞᛍᛐᛢᛘᛟᛟ");
      if (!(LEADEngine.smethod_7(9U, 8, 0U) == Class2.smethod_0("ᚡᚭᚭᚴᚭᚱᚲᚯ")))
        return;
      sqlCommand.ExecuteNonQuery();
    }
  }

  [SqlProcedure]
  public static void SetTransactionProperty(SqlGuid TransactionGUID, SqlString PropertyName, SqlString PropertyValue)
  {
    if (LEADEngine.smethod_8(Class2.smethod_0("ᚰᛃᛓᚴᛓᛃᛑᛗᛆᛉᛛᛑᛘᛘᚻᛞᛜᛞᛔᛢᛥ᛫")) % 11 != 0)
      return;
    SqlConnection connection = new SqlConnection(LEADEngine.smethod_7(16U, 23, 0U));
    connection.Open();
    PropertyValue = !PropertyValue.IsNull ? (SqlString) (Class2.smethod_0("ᚄ") + PropertyValue.ToString().Replace(Class2.smethod_0("ᚄ"), Class2.smethod_0("ᚄᚅ")) + Class2.smethod_0("ᚄ")) : (SqlString) Class2.smethod_0("ᚫᚳᚫᚬ");
    SqlCommand sqlCommand = new SqlCommand(Class2.smethod_0("ᚿᛃᛆᛉᛏᚂᛗᛖᛆᛔᛚᛉᛌᛞᛔᛛᛛᚎᛤᛠᛕᛓᛧᛙᚕᛑ᛫ᛝᛦᛪᛚᛐᛯᛟ᛭ᛐᛳᛱᛳᛩᛷ\x16FAᛰ᛭\x16FCᛩ") + TransactionGUID.ToString() + Class2.smethod_0("ᚺᙾᛖᛉᛕᛊᚃᚌᛙᛇᛉᛔᛘᛍᛖᛤᚖᚎᛢᛕᛥᚒᛃᛦᛤᛦᛜᛪ᛭ᛳᛑᛝᛩᛳᛤᚠᚾᚢ") + PropertyValue.ToString() + Class2.smethod_0("ᙽᛕᛇᛅᛓᛇᚃᚴᛗᛕᛗᛍᛛᛞᛤᚺᛎᛛᛔᚐᚮᚒᚚ") + PropertyName.ToString() + Class2.smethod_0("ᚄᙾᛈᛆᚁᚢᚣᛖᛔᛝᛊᛗᛞᛘᛟᚌᚪᚎ\x169Fᚐᛓᛗᛚᛝᛣᚖᛠᛦ᛬ᛟ᛭ᛰ\x169Dᛧ᛭ᛴᛰᚢᛞᛸᛪᛳᛷᛧᛝ\x16FC᛬\x16FAᛝᜀ\x16FEᜀᛶᜄᜇ\x16FD\x16FAᜉᛶ") + TransactionGUID.ToString() + Class2.smethod_0("ᚺᙾᚇᚰᛓᛑᛓᛉᛗᛚᛠᚶᛊᛗᛐᚘᚍᚾᛡᛟᛡᛗᛥᛨᛮᛌᛘᛤᛮᛟᚤ᚜ᛓᚿᛋᛕᛆᛕᚣᚬᚬ") + PropertyName.ToString() + Class2.smethod_0("ᚄᚊᙿ") + PropertyValue.ToString() + Class2.smethod_0("ᚆᙾᛄᛎᛅᚂᛆᛓᛒᛓᛐᛜᚉᛞᛝᛍᛛᛡᛐᛓᛥᛛᛢᛢ"), connection);
    try
    {
      if (!(LEADEngine.smethod_7(5U, 8, 0U) == Class2.smethod_0("ᚶᚭᚴᚡᚳᚧᚶᚳ")))
        return;
      sqlCommand.ExecuteNonQuery();
    }
    catch
    {
      SqlContext.Pipe.Send(Class2.smethod_0("ᚢᛐᛑᛏᛓᚂᛒᛒᚅᛚᛙᛉᛗᛝᛌᛏᛡᛗᛞᛞᚑᛢᛥᛣᛥᛛᛩ᛬ᛲᚚᛮᛡᛱ"));
    }
  }

  [SqlProcedure]
  public static void GetWaveCodeOnce(SqlInt32 WaveType_id, SqlString SQLStatement, out SqlString WaveCode)
  {
    WaveCode = SqlString.Null;
    if (LEADEngine.smethod_8(Class2.smethod_0("ᚤᛃᛓᚷᛂᛘᛈᚧᛔᛊᛌᚷᛗᛍᛐ")) % 7 != 1)
      return;
    WaveCode = SqlString.Null;
    SqlConnection connection = new SqlConnection(LEADEngine.smethod_7(16U, 23, 0U));
    connection.Open();
    SqlCommand sqlCommand = new SqlCommand(Class2.smethod_0("ᚰᚣᚫᚥᚤᚶᚃᛆᚓᚵᛉᛒᛎᛍᛟᚺᛎᛛᛔ᚜ᚑᛈᛔᛠᛪᛛᛃᛝᛧᛡᛯᛤ\x169Dᚻ\x169Fᛩᛴᛰᛸᛰᛱᚮᛨᚶᛟ᛫ᛷᜁᛲᛚᛴ\x16FEᛸᜆ\x16FBᛀᚵᛈᛇᛈᛂᚺ") + LEADEngine.smethod_7(14U, 113, 0U) + Class2.smethod_0("ᙽᚵᚧᚥᚳᚧᚃᛅᚓᚽᛈᛞᛎᚾᛤᛜᛒᛍᛘᛔᚑᚯᚓ") + WaveType_id.ToString(), connection);
    SqlDataReader sqlDataReader1 = sqlCommand.ExecuteReader();
    ArrayList arrayList = new ArrayList();
    while (sqlDataReader1.Read())
    {
      string str = sqlDataReader1.GetValue(0).ToString();
      int int32 = sqlDataReader1.GetInt32(1);
      arrayList.Add((object) new LEADEngine.Struct4()
      {
        string_0 = str,
        int_0 = int32
      });
    }
    sqlDataReader1.Close();
    sqlCommand.CommandText = SQLStatement.ToString();
    SqlDataReader sqlDataReader2 = sqlCommand.ExecuteReader();
    if (sqlDataReader2.Read())
    {
      string str1 = "";
      for (int index1 = 0; index1 < arrayList.Count; ++index1)
      {
        LEADEngine.Struct4 struct4 = (LEADEngine.Struct4) arrayList[index1];
        string str2 = sqlDataReader2[struct4.string_0].ToString();
        int int0 = struct4.int_0;
        if (str2.Length > int0)
          str2 = str2.Substring(0, int0);
        else if (str2.Length < int0)
        {
          string str3 = "";
          for (int index2 = 0; index2 < int0 - str2.Length; ++index2)
            str3 += Class2.smethod_0("ᚍ");
          str2 = str3 + str2;
        }
        str1 += str2;
      }
      WaveCode = !(LEADEngine.smethod_7(4U, 8, 0U) == Class2.smethod_0("ᚫᚭᚳᚡᚭᚱᚱᚩ")) ? (SqlString) str1.Length.ToString() : (SqlString) str1;
    }
    sqlDataReader2.Close();
    connection.Close();
  }

  [SqlProcedure]
  public static void GetWaveCode(SqlInt32 WaveType_id, SqlString SQLStatement)
  {
    if (LEADEngine.smethod_8(Class2.smethod_0("ᚤᛃᛓᚷᛂᛘᛈᚧᛔᛊᛌ")) % 2 != 1)
      return;
    SqlConnection connection = new SqlConnection(LEADEngine.smethod_7(16U, 23, 0U));
    connection.Open();
    string str1 = connection.Database;
    if (str1.Length < 3)
      str1 = Class2.smethod_0("ᛀᛍᛍᛔᛆᛚᛗ");
    SqlCommand sqlCommand = new SqlCommand(Class2.smethod_0("ᚰᚣᚫᚥᚤᚶᚃᛆᚓᚵᛉᛒᛎᛍᛟᚺᛎᛛᛔ᚜ᚑᛈᛔᛠᛪᛛᛃᛝᛧᛡᛯᛤ\x169Dᚻ\x169Fᛩᛴᛰᛸᛰᛱᚮᛨᚶᛟ᛫ᛷᜁᛲᛚᛴ\x16FEᛸᜆ\x16FBᛀᚵᛈᛇᛈᛂᚺ") + LEADEngine.smethod_7(14U, 113, 0U) + Class2.smethod_0("ᙽᚵᚧᚥᚳᚧᚃᛅᚓᚽᛈᛞᛎᚾᛤᛜᛒᛍᛘᛔᚑᚯᚓ") + WaveType_id.ToString(), connection);
    bool flag1 = true;
    flag1 = str1[2] != 'D';
    SqlDataReader sqlDataReader1 = sqlCommand.ExecuteReader();
    flag1 = str1[0] != 'C';
    ArrayList arrayList = new ArrayList();
    while (sqlDataReader1.Read())
    {
      string str2 = sqlDataReader1.GetValue(0).ToString();
      int int32 = sqlDataReader1.GetInt32(1);
      arrayList.Add((object) new LEADEngine.Struct4()
      {
        string_0 = str2,
        int_0 = int32
      });
    }
    sqlDataReader1.Close();
    bool flag2 = str1[1] != 'P';
    SqlDataRecord record = new SqlDataRecord(new SqlMetaData[2]
    {
      new SqlMetaData(Class2.smethod_0("ᚤᛐᛎᛕᛑᚥᛒᛈᛊ"), SqlDbType.NVarChar, 3000L, 1033L, SqlCompareOptions.None),
      new SqlMetaData(Class2.smethod_0("ᚤᛐᛎᛕᛑᚥᛒᛒᛉᛏᛛᛑᛘᛘ"), SqlDbType.NVarChar, 3000L, 1033L, SqlCompareOptions.None)
    });
    SqlContext.Pipe.SendResultsStart(record);
    sqlCommand.CommandText = SQLStatement.ToString();
    SqlDataReader sqlDataReader2 = sqlCommand.ExecuteReader();
    while (sqlDataReader2.Read())
    {
      string str2 = "";
      string str3 = "";
      for (int index1 = 0; index1 < arrayList.Count; ++index1)
      {
        LEADEngine.Struct4 struct4 = (LEADEngine.Struct4) arrayList[index1];
        string str4 = sqlDataReader2[struct4.string_0].ToString();
        int int0 = struct4.int_0;
        if (str4.Length > int0)
          str4 = str4.Substring(0, int0);
        else if (str4.Length < int0)
        {
          string str5 = "";
          for (int index2 = 0; index2 < int0 - str4.Length; ++index2)
            str5 += Class2.smethod_0("ᚍ");
          str4 = str5 + str4;
        }
        str2 += str4;
      }
      for (int ordinal = 0; ordinal < sqlDataReader2.FieldCount; ++ordinal)
      {
        if (str3.Length > 0)
          str3 += Class2.smethod_0("ᙽ\x169Fᚭᚤᚁ");
        str3 = str3 + Class2.smethod_0("ᛘ") + sqlDataReader2.GetName(ordinal) + Class2.smethod_0("ᛚᙾ᚜ ᚈ") + sqlDataReader2.GetValue(ordinal).ToString() + Class2.smethod_0("ᚄ");
      }
      record.SetString(0, str2);
      record.SetString(1, str3);
      SqlContext.Pipe.SendResultsRow(record);
    }
    sqlDataReader2.Close();
    if (!flag2)
      SqlContext.Pipe.SendResultsEnd();
    connection.Close();
  }

  [SqlProcedure]
  public static void ForceInsert(SqlString LocalCommand, SqlString ConnectionString, SqlString TargetTable, SqlString TargetPrepare, out SqlString LastError, SqlString FinishCommand)
  {
    LastError = SqlString.Null;
    bool flag1 = true;
    ArrayList arrayList = new ArrayList();
    if (LEADEngine.smethod_8(Class2.smethod_0("ᚣᛍᛑᛃᛆᚫᛑᛗᛊᛘᛛ")) % 3 != 2)
      return;
    SqlConnection sqlConnection = new SqlConnection();
    sqlConnection.ConnectionString = ConnectionString.ToString();
    SqlConnection connection = new SqlConnection(LEADEngine.smethod_7(16U, 23, 0U));
    try
    {
      connection.Open();
      sqlConnection.Open();
    }
    catch (Exception ex)
    {
      LastError = (SqlString) ex.Message;
      flag1 = false;
    }
    string str1 = connection.Database;
    if (str1.Length < 3)
      str1 = connection.ConnectionString;
    bool flag2 = false;
    string str2 = "";
    flag2 = str1[1] == 'P';
    bool flag3 = true;
    if (!flag1)
      return;
    flag2 = str1[0] == 'C';
    SqlCommand sqlCommand1 = new SqlCommand(LocalCommand.ToString(), connection);
    SqlCommand sqlCommand2 = new SqlCommand();
    bool flag4 = str1[2] == 'D';
    try
    {
      SqlDataReader sqlDataReader = sqlCommand1.ExecuteReader();
      while (sqlDataReader.Read())
      {
        string str3 = "";
        for (int ordinal = 0; ordinal < sqlDataReader.FieldCount; ++ordinal)
        {
          if (flag3)
          {
            if (str2.Length > 0)
              str2 += Class2.smethod_0("ᚉᙾ");
            str2 = str2 + Class2.smethod_0("ᚸ") + sqlDataReader.GetName(ordinal) + Class2.smethod_0("ᚺ");
          }
          if (str3.Length > 0)
            str3 += Class2.smethod_0("ᚉᙾ");
          str3 = !sqlDataReader.IsDBNull(ordinal) ? str3 + Class2.smethod_0("ᚄ") + sqlDataReader.GetValue(ordinal).ToString().Replace(Class2.smethod_0("ᚄ"), "") + Class2.smethod_0("ᚄ") : str3 + Class2.smethod_0("ᚫᚳᚫᚬ");
        }
        string str4;
        if (!flag4)
          str4 = Class2.smethod_0("ᚦᚬᚲᚥᚳᚶᚃᚭᚳᚺᚶᚈ") + TargetTable.ToString() + Class2.smethod_0("ᙽᚆ") + str2 + Class2.smethod_0("ᚆᙾᚵᚡᚭᚷᚨᚷᚅᚎ") + str3 + Class2.smethod_0("ᚆ");
        else
          str4 = Class2.smethod_0("ᚡᚣᚢᚬᚢᚴᚨᚄᚥᛇᚇᚬᚪᚾᚰᛀᚶᚻᚴᚫᚑᛅᚸᛈᚕᚶᛘᚘᚶᚚᛢᛡᛱᛢᛠᛴᛦᚪᚬᚿᚥᛖᛙᛑᛗᛞᚫᛌᛮ");
        arrayList.Add((object) str4);
        flag3 = false;
      }
      sqlDataReader.Close();
      sqlCommand2.Connection = sqlConnection;
      for (int index = 0; index < arrayList.Count; ++index)
      {
        if (TargetPrepare.IsNull)
          sqlCommand2.CommandText = arrayList[index].ToString();
        else
          sqlCommand2.CommandText = TargetPrepare.ToString() + Class2.smethod_0("ᙽ") + arrayList[index].ToString();
        if (LEADEngine.smethod_7(3U, 8, 0U) == Class2.smethod_0("ᚥᚷᚣᚲᚰᚬᚲᚦ"))
          sqlCommand2.ExecuteNonQuery();
      }
    }
    catch (Exception ex)
    {
      LastError = (SqlString) ex.Message;
      flag1 = false;
    }
    if (flag1 && !FinishCommand.IsNull)
    {
      sqlCommand1.Connection = connection;
      sqlCommand1.CommandText = FinishCommand.ToString();
      try
      {
        sqlCommand1.ExecuteNonQuery();
      }
      catch (Exception ex)
      {
        LastError = (SqlString) ex.Message;
      }
    }
    sqlConnection.Close();
    connection.Close();
  }

  [SqlProcedure]
  public static void ForceInsertBatch(SqlString LocalCommand, SqlString ConnectionString, SqlString TargetTable, SqlString TargetPrepare, out SqlString LastError, SqlString FinishCommand)
  {
    LastError = SqlString.Null;
    bool flag1 = true;
    ArrayList arrayList = new ArrayList();
    if (LEADEngine.smethod_8(Class2.smethod_0("ᚣᛍᛑᛃᛆᚫᛑᛗᛊᛘᛛᚪᛊᛞᛎᛔ")) % 8 != 0)
      return;
    SqlConnection sqlConnection = new SqlConnection();
    sqlConnection.ConnectionString = ConnectionString.ToString();
    SqlTransaction sqlTransaction = (SqlTransaction) null;
    SqlConnection connection = new SqlConnection(LEADEngine.smethod_7(16U, 23, 0U));
    try
    {
      connection.Open();
      sqlConnection.Open();
    }
    catch (Exception ex)
    {
      LastError = (SqlString) ex.Message;
      flag1 = false;
    }
    string str1 = connection.Database;
    if (str1.Length < 3)
      str1 = Class2.smethod_0("ᚠᛍᛍᛎᛆᛅᛗᛍᛔᛔ");
    bool flag2 = false;
    string str2 = "";
    bool flag3 = true;
    flag2 = str1[2] == 'D';
    if (!flag1)
      return;
    SqlCommand sqlCommand1 = new SqlCommand(LocalCommand.ToString(), connection);
    SqlCommand sqlCommand2 = new SqlCommand();
    try
    {
      flag2 = str1[0] == 'C';
      SqlDataReader sqlDataReader = sqlCommand1.ExecuteReader();
      while (sqlDataReader.Read())
      {
        string str3 = "";
        for (int ordinal = 0; ordinal < sqlDataReader.FieldCount; ++ordinal)
        {
          if (flag3)
          {
            if (str2.Length > 0)
              str2 += Class2.smethod_0("ᚉᙾ");
            str2 = str2 + Class2.smethod_0("ᚸ") + sqlDataReader.GetName(ordinal) + Class2.smethod_0("ᚺ");
          }
          if (str3.Length > 0)
            str3 += Class2.smethod_0("ᚉᙾ");
          str3 = !sqlDataReader.IsDBNull(ordinal) ? str3 + Class2.smethod_0("ᚄ") + sqlDataReader.GetValue(ordinal).ToString().Replace(Class2.smethod_0("ᚄ"), "") + Class2.smethod_0("ᚄ") : str3 + Class2.smethod_0("ᚫᚳᚫᚬ");
        }
        string str4 = LEADEngine.smethod_7(15U, 11, 0U) + Class2.smethod_0("ᙽ") + TargetTable.ToString() + Class2.smethod_0("ᙽᚆ") + str2 + Class2.smethod_0("ᚆᙾᚵᚡᚭᚷᚨᚷᚅᚎ") + str3 + Class2.smethod_0("ᚆ");
        arrayList.Add((object) str4);
        flag3 = false;
      }
      sqlDataReader.Close();
      bool flag4 = str1[1] == 'P';
      sqlCommand2.Connection = sqlConnection;
      sqlTransaction = sqlConnection.BeginTransaction();
      for (int index = 0; index < arrayList.Count; ++index)
      {
        sqlCommand2.Transaction = sqlTransaction;
        if (TargetPrepare.IsNull)
          sqlCommand2.CommandText = arrayList[index].ToString();
        else
          sqlCommand2.CommandText = TargetPrepare.ToString() + Class2.smethod_0("ᙽ") + arrayList[index].ToString();
        if (!flag4)
          sqlCommand2.ExecuteNonQuery();
      }
      if (LEADEngine.smethod_7(7U, 8, 0U) == Class2.smethod_0("ᚠᚰᚸᚰᚵᚱᚽᚾ"))
        sqlTransaction.Commit();
    }
    catch (Exception ex)
    {
      if (sqlTransaction != null)
        sqlTransaction.Rollback();
      LastError = (SqlString) ex.Message;
      flag1 = false;
    }
    if (flag1 && !FinishCommand.IsNull)
    {
      sqlCommand1.Connection = connection;
      sqlCommand1.CommandText = FinishCommand.ToString();
      try
      {
        sqlCommand1.ExecuteNonQuery();
      }
      catch (Exception ex)
      {
        LastError = (SqlString) ex.Message;
      }
    }
    sqlConnection.Close();
    connection.Close();
  }

  [SqlProcedure]
  public static void ExecCommand(SqlString ConnectionString, SqlString CommandString, SqlBoolean ReturnResult, out SqlString LastError)
  {
    LastError = SqlString.Null;
    bool flag = true;
    if (LEADEngine.smethod_8(Class2.smethod_0("ᚢᛖᛄᛃᚤᛑᛐᛑᛆᛔᛋ")) % 3 != 2)
      return;
    SqlConnection connection = new SqlConnection();
    try
    {
      connection.ConnectionString = ConnectionString.ToString();
      connection.Open();
    }
    catch (Exception ex)
    {
      LastError = (SqlString) ex.Message;
      flag = false;
    }
    if (!flag)
      return;
    SqlCommand sqlCommand = new SqlCommand(CommandString.ToString(), connection);
    try
    {
      if (!ReturnResult)
      {
        sqlCommand.ExecuteNonQuery();
      }
      else
      {
        SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
        do
        {
          SqlMetaData[] sqlMetaDataArray = new SqlMetaData[sqlDataReader.FieldCount];
          for (int ordinal = 0; ordinal < sqlDataReader.FieldCount; ++ordinal)
            sqlMetaDataArray[ordinal] = new SqlMetaData(sqlDataReader.GetName(ordinal), SqlDbType.NVarChar, 4000L, 1033L, SqlCompareOptions.None);
          SqlDataRecord record = new SqlDataRecord(sqlMetaDataArray);
          SqlContext.Pipe.SendResultsStart(record);
          while (sqlDataReader.Read())
          {
            for (int ordinal = 0; ordinal < sqlDataReader.FieldCount; ++ordinal)
            {
              if (!sqlDataReader.IsDBNull(ordinal))
                record.SetString(ordinal, sqlDataReader.GetValue(ordinal).ToString());
              else
                record.SetDBNull(ordinal);
            }
            SqlContext.Pipe.SendResultsRow(record);
          }
          SqlContext.Pipe.SendResultsEnd();
        }
        while (sqlDataReader.NextResult());
        sqlDataReader.Close();
      }
    }
    catch (Exception ex)
    {
      LastError = (SqlString) ex.Message;
    }
    connection.Close();
  }

  [SqlProcedure]
  public static void ResultToFile(SqlString FilePath, SqlString FileName, SqlString ColumnDivider, SqlString RowDivider, SqlString RecordsetDivider, SqlBoolean IsIncludeNames, SqlString SQL)
  {
    if (LEADEngine.smethod_8(Class2.smethod_0("ᚯᛃᛒᛕᛍᛖᚷᛓᚫᛏᛓᛍ")) % 6 != 0)
      return;
    string str = Guid.NewGuid().ToString() + Class2.smethod_0("ᚋᛊᛄᛁᛅ");
    FileStream fileStream = System.IO.File.Create(FilePath.ToString() + Class2.smethod_0("ᚹ") + str);
    SqlConnection connection = new SqlConnection(LEADEngine.smethod_7(16U, 23, 0U));
    connection.Open();
    SqlDataReader sqlDataReader = new SqlCommand(SQL.ToString(), connection).ExecuteReader();
    byte[] bytes1 = new UTF8Encoding(true).GetBytes(RowDivider.ToString());
    byte[] bytes2 = new UTF8Encoding(true).GetBytes(RecordsetDivider.ToString());
    int num = 0;
    do
    {
      bool flag = true;
      if (num > 0)
        goto label_19;
label_16:
      while (sqlDataReader.Read())
      {
        if ((SqlBoolean) flag && IsIncludeNames)
        {
          for (int ordinal = 0; ordinal < sqlDataReader.FieldCount; ++ordinal)
          {
            string s = sqlDataReader.GetName(ordinal).Replace(ColumnDivider.ToString(), "").Replace(RowDivider.ToString(), "").Replace(RecordsetDivider.ToString(), "");
            if (ordinal > 0)
              s = ColumnDivider.ToString() + s;
            byte[] bytes3 = new UTF8Encoding(true).GetBytes(s);
            fileStream.Write(bytes3, 0, bytes3.Length);
          }
          fileStream.Write(bytes1, 0, bytes1.Length);
          flag = false;
        }
        for (int ordinal = 0; ordinal < sqlDataReader.FieldCount; ++ordinal)
        {
          string s = !sqlDataReader.IsDBNull(ordinal) ? sqlDataReader.GetValue(ordinal).ToString().Replace(ColumnDivider.ToString(), "").Replace(RowDivider.ToString(), "").Replace(RecordsetDivider.ToString(), "") : "";
          if (ordinal > 0)
            s = ColumnDivider.ToString() + s;
          byte[] bytes3 = new UTF8Encoding(true).GetBytes(s);
          fileStream.Write(bytes3, 0, bytes3.Length);
        }
        if (LEADEngine.smethod_7(2U, 8, 0U) == Class2.smethod_0("ᚲᚲᚮᚰᚪᚣᚩᚺ"))
          fileStream.Write(bytes1, 0, bytes1.Length);
      }
      ++num;
      continue;
label_19:
      fileStream.Write(bytes2, 0, bytes2.Length);
      goto label_16;
    }
    while (sqlDataReader.NextResult());
    fileStream.Flush();
    fileStream.Close();
    connection.Close();
    if (!(LEADEngine.smethod_7(1U, 8, 0U) == Class2.smethod_0("ᚱᚣᚱᚭᚰᚵᚷᚥ")))
      return;
    if (System.IO.File.Exists(FilePath.ToString() + Class2.smethod_0("ᚹ") + FileName.ToString()))
      System.IO.File.Delete(FilePath.ToString() + Class2.smethod_0("ᚹ") + FileName.ToString());
    System.IO.File.Move(FilePath.ToString() + Class2.smethod_0("ᚹ") + str, FilePath.ToString() + Class2.smethod_0("ᚹ") + FileName.ToString());
  }

  private static SqlMetaData smethod_9(DataTableReader MetaReader)
  {
    SqlMetaData sqlMetaData = (SqlMetaData) null;
    string upper = ((SqlDbType) MetaReader[Class2.smethod_0("ᚭᛐᛎᛖᛊᛆᛈᛖᚹᛟᛗᛍ")]).ToString().ToUpper();
    if (upper == Class2.smethod_0("\x169Fᚧᚦᚩᚯᚶ"))
      sqlMetaData = new SqlMetaData(MetaReader[Class2.smethod_0("ᚠᛍᛋᛕᛎᛐᚱᛅᛒᛋ")].ToString(), SqlDbType.BigInt);
    else if (upper == Class2.smethod_0("ᚳ\x169Fᚱᚣᚩᚣᚵ"))
      sqlMetaData = new SqlMetaData(MetaReader[Class2.smethod_0("ᚠᛍᛋᛕᛎᛐᚱᛅᛒᛋ")].ToString(), SqlDbType.VarChar, (long) Convert.ToInt32(MetaReader[Class2.smethod_0("ᚠᛍᛋᛕᛎᛐᚶᛍᛟᛋ")]), 1033L, SqlCompareOptions.None);
    else if (upper == Class2.smethod_0("\x169Fᚧᚳ"))
      sqlMetaData = new SqlMetaData(MetaReader[Class2.smethod_0("ᚠᛍᛋᛕᛎᛐᚱᛅᛒᛋ")].ToString(), SqlDbType.Bit);
    else if (upper == Class2.smethod_0("ᚠᚦᚠᚲ"))
      sqlMetaData = new SqlMetaData(MetaReader[Class2.smethod_0("ᚠᛍᛋᛕᛎᛐᚱᛅᛒᛋ")].ToString(), SqlDbType.Char, (long) Convert.ToInt32(MetaReader[Class2.smethod_0("ᚠᛍᛋᛕᛎᛐᚶᛍᛟᛋ")]), 1033L, SqlCompareOptions.None);
    else if (upper == Class2.smethod_0("ᚡ\x169Fᚳᚥ"))
      sqlMetaData = new SqlMetaData(MetaReader[Class2.smethod_0("ᚠᛍᛋᛕᛎᛐᚱᛅᛒᛋ")].ToString(), SqlDbType.Date);
    else if (upper == Class2.smethod_0("ᚡ\x169Fᚳᚥᚵᚫᚰᚩ"))
      sqlMetaData = new SqlMetaData(MetaReader[Class2.smethod_0("ᚠᛍᛋᛕᛎᛐᚱᛅᛒᛋ")].ToString(), SqlDbType.DateTime);
    else if (upper == Class2.smethod_0("ᚡ\x169Fᚳᚥᚵᚫᚰᚩᚗ"))
      sqlMetaData = new SqlMetaData(MetaReader[Class2.smethod_0("ᚠᛍᛋᛕᛎᛐᚱᛅᛒᛋ")].ToString(), SqlDbType.DateTime2);
    else if (upper == Class2.smethod_0("ᚡᚣᚢᚩᚮᚣᚯ"))
      sqlMetaData = new SqlMetaData(MetaReader[Class2.smethod_0("ᚠᛍᛋᛕᛎᛐᚱᛅᛒᛋ")].ToString(), SqlDbType.Decimal, Convert.ToByte(MetaReader[Class2.smethod_0("ᚫᛓᛌᛅᛓᛋᛆᚴᛗᛋᛊᛑᛜᛓᛚᛚ")]), Convert.ToByte(MetaReader[Class2.smethod_0("ᚫᛓᛌᛅᛓᛋᛆᚷᛈᛇᛓᛍ")]));
    else if (upper == Class2.smethod_0("ᚣᚪᚮᚡᚵ"))
      sqlMetaData = new SqlMetaData(MetaReader[Class2.smethod_0("ᚠᛍᛋᛕᛎᛐᚱᛅᛒᛋ")].ToString(), SqlDbType.Float, Convert.ToByte(MetaReader[Class2.smethod_0("ᚫᛓᛌᛅᛓᛋᛆᚴᛗᛋᛊᛑᛜᛓᛚᛚ")]), Convert.ToByte(MetaReader[Class2.smethod_0("ᚫᛓᛌᛅᛓᛋᛆᚷᛈᛇᛓᛍ")]));
    else if (upper == Class2.smethod_0("ᚦᚬᚳ"))
      sqlMetaData = new SqlMetaData(MetaReader[Class2.smethod_0("ᚠᛍᛋᛕᛎᛐᚱᛅᛒᛋ")].ToString(), SqlDbType.Int);
    else if (upper == Class2.smethod_0("ᚪᚭᚭᚥᚺ"))
      sqlMetaData = new SqlMetaData(MetaReader[Class2.smethod_0("ᚠᛍᛋᛕᛎᛐᚱᛅᛒᛋ")].ToString(), SqlDbType.Money);
    else if (upper == Class2.smethod_0("ᚫᚡᚧᚡᚳ"))
      sqlMetaData = new SqlMetaData(MetaReader[Class2.smethod_0("ᚠᛍᛋᛕᛎᛐᚱᛅᛒᛋ")].ToString(), SqlDbType.NChar, (long) Convert.ToInt32(MetaReader[Class2.smethod_0("ᚠᛍᛋᛕᛎᛐᚶᛍᛟᛋ")]), 1033L, SqlCompareOptions.None);
    else if (upper == Class2.smethod_0("ᚫᚳᚬᚥᚳᚫᚦ"))
      sqlMetaData = new SqlMetaData(MetaReader[Class2.smethod_0("ᚠᛍᛋᛕᛎᛐᚱᛅᛒᛋ")].ToString(), SqlDbType.Decimal, Convert.ToByte(MetaReader[Class2.smethod_0("ᚫᛓᛌᛅᛓᛋᛆᚴᛗᛋᛊᛑᛜᛓᛚᛚ")]), Convert.ToByte(MetaReader[Class2.smethod_0("ᚫᛓᛌᛅᛓᛋᛆᚷᛈᛇᛓᛍ")]));
    else if (upper == Class2.smethod_0("ᚫᚴᚠᚲᚤᚪᚤᚶ"))
      sqlMetaData = new SqlMetaData(MetaReader[Class2.smethod_0("ᚠᛍᛋᛕᛎᛐᚱᛅᛒᛋ")].ToString(), SqlDbType.NVarChar, (long) Convert.ToInt32(MetaReader[Class2.smethod_0("ᚠᛍᛋᛕᛎᛐᚶᛍᛟᛋ")]), 1033L, SqlCompareOptions.None);
    else if (upper == Class2.smethod_0("ᚯᚣᚠᚬ"))
      sqlMetaData = new SqlMetaData(MetaReader[Class2.smethod_0("ᚠᛍᛋᛕᛎᛐᚱᛅᛒᛋ")].ToString(), SqlDbType.Real, Convert.ToByte(MetaReader[Class2.smethod_0("ᚫᛓᛌᛅᛓᛋᛆᚴᛗᛋᛊᛑᛜᛓᛚᛚ")]), Convert.ToByte(MetaReader[Class2.smethod_0("ᚫᛓᛌᛅᛓᛋᛆᚷᛈᛇᛓᛍ")]));
    else if (upper == Class2.smethod_0("ᚰᚫᚠᚬᚭᚫᚱᚸ"))
      sqlMetaData = new SqlMetaData(MetaReader[Class2.smethod_0("ᚠᛍᛋᛕᛎᛐᚱᛅᛒᛋ")].ToString(), SqlDbType.SmallInt);
    else if (upper == Class2.smethod_0("ᚰᚫᚠᚬᚭᚯᚲᚲᚪᚿ"))
      sqlMetaData = new SqlMetaData(MetaReader[Class2.smethod_0("ᚠᛍᛋᛕᛎᛐᚱᛅᛒᛋ")].ToString(), SqlDbType.SmallMoney);
    else if (upper == Class2.smethod_0("ᚱᚧᚭᚹᚪᚰᚷ"))
      sqlMetaData = new SqlMetaData(MetaReader[Class2.smethod_0("ᚠᛍᛋᛕᛎᛐᚱᛅᛒᛋ")].ToString(), SqlDbType.TinyInt);
    else if (upper == Class2.smethod_0("ᚲᚬᚨᚱᚶᚧᚬᚨᚪᚴᚻᚱᚯᚳᚰᚾ"))
      sqlMetaData = new SqlMetaData(MetaReader[Class2.smethod_0("ᚠᛍᛋᛕᛎᛐᚱᛅᛒᛋ")].ToString(), SqlDbType.UniqueIdentifier);
    return sqlMetaData;
  }

  [SqlProcedure]
  public static void RetrieveRecordset(SqlString SQLStatement, SqlString ObjectGUID)
  {
    if (LEADEngine.smethod_8(Class2.smethod_0("ᚯᛃᛓᛒᛊᛇᛙᛉᚷᛋᛊᛗᛛᛎᛞᛑᛡ")) % 12 != 5)
      return;
    SqlConnection connection = new SqlConnection(LEADEngine.smethod_7(16U, 23, 0U));
    connection.Open();
    SqlCommand sqlCommand = new SqlCommand(Class2.smethod_0("ᚰᚣᚫᚥᚤᚶᚃᛆᚓᚵᛉᛒᛎᛍᛟᚺᛎᛛᛔᚐᚷᛄᛂᛁᚕᛑ᛫ᛝᛦᛪᛚᛋᛟᛨᛤᛣᛵᛓᛸᛩᛷ\x16FFᛦ") + ObjectGUID.ToString() + Class2.smethod_0("ᚺᙾᛀᛓᚁᛃᚃᛛᛎᛚᛏᚈᚑᛘᛚᛘᛜᛑᛚᚙᚑ") + LEADEngine.smethod_7(17U, 89, 0U), connection);
    SqlDataReader sqlDataReader1 = sqlCommand.ExecuteReader();
    ArrayList arrayList = new ArrayList();
    while (sqlDataReader1.Read())
      arrayList.Add((object) sqlDataReader1.GetValue(0).ToString());
    sqlDataReader1.Close();
    string database = connection.Database;
    bool flag1 = false;
    SqlMetaData[] sqlMetaDataArray = new SqlMetaData[arrayList.Count];
    SqlDataRecord record = (SqlDataRecord) null;
    int num = -1;
    bool flag2 = false;
    sqlCommand.CommandText = SQLStatement.ToString();
    SqlDataReader sqlDataReader2 = sqlCommand.ExecuteReader();
    while (sqlDataReader2.Read())
    {
      if (!flag2)
      {
        DataTableReader dataReader = sqlDataReader2.GetSchemaTable().CreateDataReader();
        while (dataReader.Read())
        {
          int index = arrayList.IndexOf(dataReader[Class2.smethod_0("ᚠᛍᛋᛕᛎᛐᚱᛅᛒᛋ")]);
          if (index >= 0)
            sqlMetaDataArray[index] = LEADEngine.smethod_9(dataReader);
        }
        record = new SqlDataRecord(sqlMetaDataArray);
        SqlContext.Pipe.SendResultsStart(record);
        flag2 = true;
      }
      if (flag2)
      {
        for (int ordinal1 = 0; ordinal1 < sqlDataReader2.FieldCount; ++ordinal1)
        {
          num = -1;
          int ordinal2 = arrayList.IndexOf((object) sqlDataReader2.GetName(ordinal1));
          if ((ordinal2 <= -1 ? 1 : (sqlDataReader2.IsDBNull(ordinal1) ? 1 : 0)) == 0)
          {
            if (!flag1)
              record.SetValue(ordinal2, sqlDataReader2.GetValue(ordinal1));
            else
              record.SetDBNull(ordinal2);
          }
          else if ((ordinal2 <= -1 ? 1 : (!sqlDataReader2.IsDBNull(ordinal1) ? 1 : 0)) == 0)
            record.SetDBNull(ordinal2);
        }
        SqlContext.Pipe.SendResultsRow(record);
      }
    }
    if (flag2)
      SqlContext.Pipe.SendResultsEnd();
    sqlDataReader2.Close();
    connection.Close();
  }

  [SqlProcedure]
  public static void XMLRetrieveRecordset(SqlString SQLStatement, SqlXml ObjectXML)
  {
    if (LEADEngine.smethod_8(Class2.smethod_0("ᚵᚫᚫᚲᛆᛖᛕᛍᛊᛜᛌᚺᛎᛍᛚᛞᛑᛡᛔᛤ")) % 17 != 3)
      return;
    SqlConnection connection = new SqlConnection(LEADEngine.smethod_7(16U, 23, 0U));
    connection.Open();
    Class1 class1 = new Class1();
    class1.method_0(ObjectXML.ToString());
    ArrayList arrayList = class1.method_1();
    SqlMetaData[] sqlMetaDataArray = new SqlMetaData[arrayList.Count];
    SqlDataRecord record = (SqlDataRecord) null;
    int num = -1;
    bool flag = false;
    SqlDataReader sqlDataReader = new SqlCommand(SQLStatement.ToString(), connection).ExecuteReader();
    while (sqlDataReader.Read())
    {
      if (!flag)
      {
        DataTableReader dataReader = sqlDataReader.GetSchemaTable().CreateDataReader();
        while (dataReader.Read())
        {
          int index = arrayList.IndexOf(dataReader[Class2.smethod_0("ᚠᛍᛋᛕᛎᛐᚱᛅᛒᛋ")]);
          if (index >= 0)
            sqlMetaDataArray[index] = LEADEngine.smethod_9(dataReader);
        }
        record = new SqlDataRecord(sqlMetaDataArray);
        SqlContext.Pipe.SendResultsStart(record);
        flag = true;
      }
      if (flag)
      {
        for (int ordinal1 = 0; ordinal1 < sqlDataReader.FieldCount; ++ordinal1)
        {
          num = -1;
          int ordinal2 = arrayList.IndexOf((object) sqlDataReader.GetName(ordinal1));
          if ((ordinal2 <= -1 ? 1 : (sqlDataReader.IsDBNull(ordinal1) ? 1 : 0)) == 0)
            record.SetValue(ordinal2, sqlDataReader.GetValue(ordinal1));
          else if ((ordinal2 <= -1 ? 1 : (!sqlDataReader.IsDBNull(ordinal1) ? 1 : 0)) == 0)
            record.SetDBNull(ordinal2);
        }
        SqlContext.Pipe.SendResultsRow(record);
      }
    }
    if (flag)
      SqlContext.Pipe.SendResultsEnd();
    sqlDataReader.Close();
    connection.Close();
  }

  [SqlProcedure]
  public static void GetTransactionProperty(SqlGuid TransactionGUID, SqlString PropertyName, out SqlString PropertyValue)
  {
    PropertyValue = SqlString.Null;
    if (LEADEngine.smethod_8(Class2.smethod_0("ᚤᛃᛓᚴᛓᛃᛑᛗᛆᛉᛛᛑᛘᛘᚻᛞᛜᛞᛔᛢᛥ᛫")) % 20 != 2)
      return;
    SqlConnection connection = new SqlConnection(LEADEngine.smethod_7(16U, 23, 0U));
    connection.Open();
    SqlCommand sqlCommand = new SqlCommand(Class2.smethod_0("ᚰᚣᚫᚥᚤᚶᚃᚴᛗᛕᛗᛍᛛᛞᛤᛂᛎᛚᛤᛕᚑᚸᛅᛃᛂᚖᛒ᛬ᛞᛧ᛫ᛛᛑᛰᛠᛮᛑᛴᛲᛴᛪᛸ\x16FBᛱᛮ\x16FDᛪ") + TransactionGUID.ToString() + Class2.smethod_0("ᚺᙾᛖᛉᛕᛊᚃᚌᛓᛕᛓᛗᛌᛕᚔᚌᛄᚶᚴᛂᚶᚒᛃᛦᛤᛦᛜᛪ᛭ᛳᛉᛝᛪᛣ\x169Fᚽᚡᚩ") + PropertyName.ToString() + Class2.smethod_0("ᚄ"), connection);
    try
    {
      SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
      while (sqlDataReader.Read())
        PropertyValue = (SqlString) sqlDataReader.GetString(0);
      sqlDataReader.Close();
    }
    catch
    {
      PropertyValue = SqlString.Null;
    }
  }

  [SqlProcedure]
  public static void CreateQueryObject(SqlString TableGUID, SqlString ObjectName, SqlString SortType, SqlInt32 SortPriority, SqlInt32 ReporterLayer_id)
  {
    if (LEADEngine.smethod_8(Class2.smethod_0("ᚠᛐᛄᛁᛕᛇᚴᛙᛊᛘᛠᚷᛋᛔᛐᛏᛡ")) % 5 != 2)
      return;
    SqlConnection sqlConnection = new SqlConnection(LEADEngine.smethod_7(16U, 23, 0U));
    sqlConnection.Open();
    string database = sqlConnection.Database;
    new DataPathConstructor()
    {
      ThisConnection = sqlConnection
    }.CreateQueryObject(TableGUID, ObjectName, SortType, SortPriority, ReporterLayer_id);
    sqlConnection.Close();
  }

  [SqlProcedure]
  public static void GetObjectsSQL(SqlString ObjectsTable, SqlString Condition, SqlString GroupType, SqlString DirectWhere, out SqlString ErrorMessage, out SqlBoolean IsNormal, out SqlString SQLStatement, SqlBoolean IsIgnoreGrouping, SqlInt32 ReporterLayer_id)
  {
    ErrorMessage = SqlString.Null;
    IsNormal = SqlBoolean.Null;
    SQLStatement = SqlString.Null;
    if (LEADEngine.smethod_8(Class2.smethod_0("ᚤᛃᛓᚯᛃᛌᛈᛇᛙᛙᚺᚹᚵ")) % 12 != 1)
      return;
    SqlConnection sqlConnection = new SqlConnection(LEADEngine.smethod_7(16U, 23, 0U));
    sqlConnection.Open();
    DataPathConstructor dataPathConstructor = new DataPathConstructor();
    dataPathConstructor.ThisConnection = sqlConnection;
    if (LEADEngine.smethod_7(6U, 8, 0U) == Class2.smethod_0("ᚱᚭᚦᚥᚵᚪᚨᚶ"))
    {
      string ErrorMessage1;
      bool IsNormal1;
      SQLStatement = (SqlString) dataPathConstructor.ConstructSQLFromData(Class2.smethod_0("ᛑᛃᛌᛐᛀᚱᛅᛎᛊᛉᛛᚹᛞᛏᛝᛥᛌ") + ObjectsTable.ToString(), Condition.ToString(), GroupType.ToString(), DirectWhere.ToString(), out ErrorMessage1, out IsNormal1, (bool) IsIgnoreGrouping, (int) ReporterLayer_id);
      ErrorMessage = (SqlString) ErrorMessage1;
      IsNormal = (SqlBoolean) IsNormal1;
    }
    else
      SQLStatement = (SqlString) "";
    sqlConnection.Close();
  }

  [SqlProcedure]
  public static void DropQuerySchema(SqlString SchemaGUID)
  {
    if (LEADEngine.smethod_8(Class2.smethod_0("ᚡᛐᛎᛐᚲᛗᛈᛖᛞᚹᛊᛐᛎᛗᛌ")) % 9 != 6)
      return;
    SqlConnection connection = new SqlConnection(LEADEngine.smethod_7(16U, 23, 0U));
    SqlCommand sqlCommand = new SqlCommand(LEADEngine.smethod_7(18U, 29, 0U) + SchemaGUID.ToString() + Class2.smethod_0("ᚺ"), connection);
    try
    {
      sqlCommand.ExecuteNonQuery();
    }
    catch
    {
    }
    connection.Close();
  }

  [SqlProcedure]
  public static void GetUserQuerySQL(SqlInt32 UserQuery_id, out SqlString SQLStatement, SqlInt32 ReporterLayer_id)
  {
    SQLStatement = SqlString.Null;
    if (LEADEngine.smethod_8(Class2.smethod_0("ᚤᛃᛓᚵᛔᛇᛕᚵᛚᛋᛙᛡᚼᚻᚷ")) % 2 == 1)
    {
      SqlConnection sqlConnection = new SqlConnection(LEADEngine.smethod_7(16U, 23, 0U));
      sqlConnection.Open();
      SQLStatement = (SqlString) new DataPathConstructor()
      {
        ThisConnection = sqlConnection
      }.ConstructSQLFromQuery((int) UserQuery_id, -1, "", (SqlBoolean) false);
      sqlConnection.Close();
    }
    else
      SQLStatement = SqlString.Null;
  }

  public static ArrayList DivideToArray(string SourceString, string Divider)
  {
    ArrayList arrayList = new ArrayList();
    string str1 = "";
    for (int index = 0; index < SourceString.Length; ++index)
    {
      char ch = SourceString[index];
      if (ch.ToString() != Divider)
      {
        string str2 = str1;
        ch = SourceString[index];
        string str3 = ch.ToString();
        str1 = str2 + str3;
      }
      else
      {
        arrayList.Add((object) str1.Trim().ToUpper());
        str1 = "";
      }
    }
    if (str1.Trim().Length > 0)
      arrayList.Add((object) str1.Trim().ToUpper());
    return arrayList;
  }

  [SqlProcedure]
  public static void CreateTransactionBySchema(SqlInt32 TransactionType_id, SqlInt32 User_id, SqlInt32 ParentTransaction_id, SqlInt32 ParentTable_id, SqlInt32 ParentRow, SqlXml ObjectSchema, out SqlString LastError, out SqlInt32 Transaction_id)
  {
    LastError = SqlString.Null;
    Transaction_id = SqlInt32.Null;
    if (LEADEngine.smethod_8(Class2.smethod_0("ᚠᛐᛄᛁᛕᛇᚷᛖᛆᛔᛚᛉᛌᛞᛔᛛᛛᚰᛨᛃᛔᛚᛘᛡᛖ")) % 9 != 7)
      return;
    SqlConnection sqlConnection = new SqlConnection(LEADEngine.smethod_7(16U, 23, 0U));
    sqlConnection.Open();
    SqlCommand sqlCommand = new SqlCommand();
    sqlCommand.Connection = sqlConnection;
    sqlCommand.CommandType = CommandType.Text;
    ArrayList arrayList = new ArrayList();
    sqlCommand.CommandText = Class2.smethod_0("ᚰᚣᚫᚥᚤᚶᚃᚸᚴᚶᚇᚙᚉᚺᛌᛞᛎᛛᛔᛤᛖᛤᛦᛈᛖᛘᛣᛝᚙᛀᛍᛋᛊ\x169Eᛓᛲᛢᛰᛶᛥᛨ\x16FAᛰᛷᛷᛞᜄ\x16FCᛲᜁᚯᜇ\x16FAᜆ\x16FBᚴᚽᜄᜆᜄᜈ\x16FDᜆᛅᚽᛵᛧᛥᛳᛧᛃ\x1718ᜎᜊᛇᛥᛉ") + TransactionType_id.ToString();
    string str1;
    try
    {
      str1 = sqlCommand.ExecuteScalar().ToString();
    }
    catch (Exception ex)
    {
      LastError = (SqlString) (Class2.smethod_0("᩺᪓᪙\x1A9E᪡᪢᪘\x1A9Eᪧ᪣᪥ᚈ᪬᪤\x1A9B᪣\x1A9D᪫ᚏ᪲᪩᪱ᚓ᪶᪵᪦᪴\x1AAF᪩᪴\x1AC1᪵᪴\x169Eᛈᛄᚡᚿᚣ") + TransactionType_id.ToString() + Class2.smethod_0("ᙽᚆ") + ex.Message + Class2.smethod_0("ᚆ"));
      return;
    }
    sqlCommand.CommandText = Class2.smethod_0("ᚰᚣᚫᚥᚤᚶᚃᛅᚓᚶᛈᛚᛊᛗᛐᛠᛒᛠᚽᛑᛞᛗ\x169Fᚔᛖᚤᛇᛙ᛫ᛛᛨᛡᛱᛣᛱᛔ\x16FAᛲᛨᛣᛮᛪᚳᚨᛪᚸᛛ᛭\x16FFᛯ\x16FCᛵᜅᛷᜅᛗᜄᜂᜌᜅᜇᛆᚻ\x16FDᛋᛱᜓᜏᜓᜇᛦᜓᜑ\x171B᜔\x1716ᛕᛊᜌᛚᛶᜡ\x16FDᜥ\x171D\x171E᜔\x1716ᜡ\x171Bᛣᛘ\x171Bᛨᜄᜏ\x171Cᜌ᜔ᜌ\x170Dᜃᜅᜐᜊᛦ\x170D\x171A\x1718\x1717᛫ᜠ\x173Fᜯ\x173Dᝃᜲ᜵ᝇ\x173Dᝄᝄᜫᝑᝉ\x173Fᜫ\x173Dᝏ\x173Fᝌᝅ\x1755ᝇ\x1755\x1757ᜅᝇ\x175Aᜈᝊᜊᝢ\x1755ᝡ\x1756ᜏ\x1718\x175Fᝡ\x175Fᝣ\x1758ᝡᜠ\x1718ᝃᝉᝄᝊ\x171Dᝇᝍᝆᝐ\x1754ᝐᝅ\x1759ᝏ\x1756\x1756ᝨ\x175Dᝎ\x1754ᝒ\x175Bᝐ\x173E\x1754ᝡ\x175Fᝩᝢᝤᝪ\x1738\x177Aឍ\x173B\x177E\x173Dផឈបញᝂᝋធបធពឋបᝓᝋ\x177B\x177Bᝎថ\x175Eខនឥផអលឫឝឫ\x177Dឪឨឲឫឭᝠ\x177Eᝢឥᝲឈផនឝពមឪរណលបᝰធហភ\x1774ិងឫយលឦហុឫសឬឥខសឃឋ") + str1 + Class2.smethod_0("ᚄᙾᚶᚨᚦᚴᚨᚄᛆᚔᚻᛚᛊᛘᛞᛍᛐᛢᛘᛟᛟᛆ᛬ᛤᛚᛕᛠᛜᚙᚷ᚛") + TransactionType_id.ToString();
    LEADEngine.Struct2 struct2_1;
    try
    {
      SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
      while (sqlDataReader.Read())
      {
        struct2_1 = new LEADEngine.Struct2();
        struct2_1.sqlString_0 = (SqlString) sqlDataReader.GetString(0);
        struct2_1.sqlInt32_0 = (SqlInt32) sqlDataReader.GetInt32(1);
        struct2_1.sqlString_1 = (SqlString) sqlDataReader.GetString(2);
        if (!sqlDataReader.IsDBNull(3))
          struct2_1.sqlString_2 = (SqlString) sqlDataReader.GetString(3);
        struct2_1.sqlInt32_1 = sqlDataReader.IsDBNull(4) ? (SqlInt32) 0 : (SqlInt32) sqlDataReader.GetInt32(4);
        struct2_1.bool_1 = sqlDataReader.GetString(5) == Class2.smethod_0("ᚶᚣᚲ");
        arrayList.Add((object) struct2_1);
      }
      sqlDataReader.Close();
    }
    catch (Exception ex)
    {
      LastError = (SqlString) (Class2.smethod_0("᩺᪓᪙\x1A9E᪡᪢᪘\x1A9Eᪧ᪣᪥ᚈ᪬᪤\x1A9B᪣\x1A9D᪫ᚏ᪲᪩᪱ᚓ᪶᪵᪦᪴\x1AAF᪩᪴\x1AC1᪵᪴\x169Eᛈᛄᚡᚿᚣ") + TransactionType_id.ToString() + Class2.smethod_0("ᙽᚆ") + ex.Message + Class2.smethod_0("ᚆ"));
      return;
    }
    XmlDocument xmlDocument = new XmlDocument();
    try
    {
      xmlDocument.LoadXml(ObjectSchema.Value);
    }
    catch (Exception ex)
    {
      LastError = (SqlString) (Class2.smethod_0("᩺᪓᪙\x1A9E᪡᪢᪘\x1A9Eᪧ᪣᪲᪡ᚉᛂᚸᚸᚍ᪫\x1A9Fᚐ᪣᪷᪱᪨᪪ᚖ\x169F") + ex.Message + Class2.smethod_0("ᚆ"));
      return;
    }
    SqlInt32 sqlInt32_1 = SqlInt32.Null;
    SqlString sqlString1 = SqlString.Null;
    SqlString sqlString2 = SqlString.Null;
    string str2 = "";
    string str3 = "";
    foreach (XmlElement xmlElement in (XmlNode) xmlDocument[Class2.smethod_0("ᚬᛀᛉᛅᛄᛖᚶᛇᛍᛋᛔᛉ")])
    {
      if (xmlElement.Name == Class2.smethod_0("ᚬᛀᛉᛅᛄᛖ"))
      {
        sqlInt32_1 = SqlInt32.Null;
        SqlString sqlString3 = SqlString.Null;
        if (xmlElement.Attributes[Class2.smethod_0("ᚭᚿᛑᛁᛎᛇᛗᛉᛗᚺᛠᛘᛎᛉᛔᛐ")] != null)
        {
          SqlInt32 sqlInt32_2;
          try
          {
            sqlInt32_2 = SqlInt32.Parse(xmlElement.Attributes[Class2.smethod_0("ᚭᚿᛑᛁᛎᛇᛗᛉᛗᚺᛠᛘᛎᛉᛔᛐ")].Value);
          }
          catch
          {
            sqlInt32_1 = SqlInt32.Null;
            LastError = (SqlString) (Class2.smethod_0("᩺᪓᪙\x1A9E᪡᪢᪘\x1A9Eᪧ᪣᪥\x1A9Dᚉ᪡᪨\x1A9C᪴᪣᪬᪨᪦ᚒ᪣᪶᪵\x1AAE᪨᪻᪻᪪᚛ᛌᛞᛰᛠ᛭ᛦᛶᛨᛶᛙ\x16FFᛷ᛭ᛨᛳᛯᚬᛊᚮ") + xmlElement.Attributes[Class2.smethod_0("ᚭᚿᛑᛁᛎᛇᛗᛉᛗᚺᛠᛘᛎᛉᛔᛐ")].Value.ToString());
            return;
          }
          if (xmlElement.Attributes[Class2.smethod_0("ᚳᚿᛋᛕᛆ")] != null)
            sqlString3 = (SqlString) xmlElement.Attributes[Class2.smethod_0("ᚳᚿᛋᛕᛆ")].Value;
          sqlCommand.CommandType = CommandType.Text;
          sqlCommand.CommandText = Class2.smethod_0("ᚰᚣᚫᚥᚤᚶᚃᚶᛊᛌᚻᛉᛋᛖᛐᚺᛎᛛᛔ᚜ᚑᛄᛘᛚᚸᛥᛣ᛭ᛦᛨᛎᛤ᛬ᛵᛍᛡᛮᛧᚯᚤᛗ᛫᛭ᛋᛸᛶᜀ\x16F9\x16FBᛛᛴ\x16FDᜀᛠᛴᜁ\x16FAᛂᚷᛮᜂ\x170Dᜐ\x16FDᜉᛲ\x1718ᜐᜆᛎᛃᛲ\x171Aᜓᜉ\x170D\x171Bᛱᜐ\x171Aᜒᜠᜐᜤᜠᜤᜒ\x171D\x1719ᛖ\x16FDᜊᜈᜇᛛ") + LEADEngine.smethod_7(13U, 14, 0U) + Class2.smethod_0("ᙽᛕᛈᛔᛉᚂᚋᛒᛔᛒᛖᛋᛔᚓᚋᛃᚵᚳᛁᚵᚑᛦᛜᛘᚕᚳᚗ") + sqlInt32_2.ToString();
          SqlDataReader sqlDataReader1 = sqlCommand.ExecuteReader();
          string str4 = "";
          string str5 = "";
          string str6 = "";
          string str7 = "";
          string str8 = "";
          while (sqlDataReader1.Read())
          {
            if (!sqlDataReader1.IsDBNull(0))
              str4 = sqlDataReader1.GetValue(0).ToString();
            if (!sqlDataReader1.IsDBNull(1))
              str5 = sqlDataReader1.GetValue(1).ToString();
            if (!sqlDataReader1.IsDBNull(2))
              str6 = sqlDataReader1.GetValue(2).ToString();
            if (!sqlDataReader1.IsDBNull(3))
              str7 = sqlDataReader1.GetValue(3).ToString();
            if (!sqlDataReader1.IsDBNull(4))
              str8 = sqlDataReader1.GetValue(4).ToString();
          }
          sqlDataReader1.Close();
          SqlString sqlString4 = SqlString.Null;
          if (str7 == Class2.smethod_0("ᛓᛒᚱᛅᛇᛇᛕᛉᛓᛉᛌ"))
          {
            sqlCommand.CommandType = CommandType.Text;
            sqlCommand.CommandText = Class2.smethod_0("ᚰᚣᚫᚥᚤᚶᚃᚸᚴᚶᚇᚙᚉ") + str5 + Class2.smethod_0("ᙽᚤᚱᚯᚮᚂ") + str4 + Class2.smethod_0("ᙽᛕᛈᛔᛉᚂᚋᛒᛔᛒᛖᛋᛔᚓᚋᛃᚵᚳᛁᚵᚑ") + str6 + Class2.smethod_0("ᙽ᚛ᙿᚇ") + sqlString3.ToString().Replace(Class2.smethod_0("ᚄ"), Class2.smethod_0("ᚄᚅ")) + Class2.smethod_0("ᚄ");
            try
            {
              SqlDataReader sqlDataReader2 = sqlCommand.ExecuteReader();
              while (sqlDataReader2.Read())
              {
                if (!sqlDataReader2.IsDBNull(0))
                  sqlString4 = (SqlString) sqlDataReader2.GetValue(0).ToString();
              }
              sqlDataReader2.Close();
            }
            catch
            {
              LastError = (SqlString) (Class2.smethod_0("᩻\x1A9D\x1A9F᪕᪕᪗\x1A9E᪙᪢\x1A9E\x1A9Cᚈ᪫᪢᪪\x1A9Cᚍ᪭\x1AAF᪥᪥᪵᪳᪤ᪧ᪱᪬᪵᪱\x1AC9᚛᪰᪭᪻᪼\x1ACB\x1AC6ᚢ") + sqlInt32_2.ToString() + Class2.smethod_0("ᙽ᪐᪪\x1A9F\x1A9F\x1A9D᪠᪙᪢᪤ᚇ᪩ᚉ᪨᪳᪤\x1A9E᪨\x1A9F᪬᪩ᚠᚓ\x1A8C᪶᪵᪷᪨᪫\x1AC6᪽᪱\x169D᪾᪴\x1AC4᪶\x1AC2᪸\x1AC1\x1AC6\x1AC3\x1AD2᪽ᚩ\x1AC9᪻\x1ACC᪽\x1ACA\x1AC4\x1AD2\x1AD1\x1ADDᚴ"));
              return;
            }
          }
          else if ((!(str7 == Class2.smethod_0("ᛓᛒᚲᛔᛓᛋᛑᛋ")) || str8.Length <= 0 ? 1 : (!sqlString3.IsNull ? 1 : 0)) == 0)
          {
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandText = Class2.smethod_0("ᚤᛃᛍᛅᛓᛃᛗᛉᚳᛛᛔᛊᛎᛜ");
            SqlParameter sqlParameter1 = new SqlParameter();
            sqlParameter1.ParameterName = Class2.smethod_0("\x169Dᚬᛔᛍᛃᛇᛕᚫᛊᛔᛌᛚᛊᛞᛚᛞᛌᛗᛓ");
            sqlParameter1.SqlDbType = SqlDbType.Int;
            sqlParameter1.Direction = ParameterDirection.Input;
            sqlParameter1.Value = (object) Convert.ToInt32(str8);
            sqlCommand.Parameters.Add(sqlParameter1);
            SqlParameter sqlParameter2 = new SqlParameter();
            sqlParameter2.ParameterName = Class2.smethod_0("\x169Dᚥᛄᛎᛆᛔᛄᛘᛔᛘᚽᛉᛕᛟᛐ");
            sqlParameter2.SqlDbType = SqlDbType.VarChar;
            sqlParameter2.Size = 4000;
            sqlParameter2.Direction = ParameterDirection.Output;
            sqlCommand.Parameters.Add(sqlParameter2);
            try
            {
              sqlCommand.ExecuteNonQuery();
            }
            catch
            {
              LastError = (SqlString) (Class2.smethod_0("ᩯ᪩\x1AAE᪒\x1A9C᪗᪠᪔ᚅ᪣\x1A9C᪢ᪧ᪪᪫᪡ᪧ᪰᪬᪠\x1AC0ᚒ᪳᪤᪦᪹᪴᪨ᚙ᪭᪹᪰᪲᪾\x1AAF\x1AC2\x1ABF\x1AC2᪳ᚤ\x1AC2\x1AC4\x1AC3᪽\x1AC9\x1AC8᪽ᚬᛖᛒᚯᛍᚱ") + str8);
              return;
            }
            sqlString3 = (SqlString) sqlCommand.Parameters[Class2.smethod_0("\x169Dᚥᛄᛎᛆᛔᛄᛘᛔᛘᚽᛉᛕᛟᛐ")].Value.ToString();
            sqlCommand.Parameters.Clear();
          }
          for (int index = 0; index < arrayList.Count; ++index)
          {
            struct2_1 = (LEADEngine.Struct2) arrayList[index];
            if (struct2_1.sqlInt32_0 == sqlInt32_2)
            {
              if (str2.Length > 0)
                str2 += Class2.smethod_0("ᚉᙾ");
              if (str3.Length > 0)
                str3 += Class2.smethod_0("ᚉᙾ");
              str2 = str2 + Class2.smethod_0("ᚸ") + struct2_1.sqlString_1.ToString() + Class2.smethod_0("ᚺ");
              str3 = !sqlString3.IsNull ? str3 + Class2.smethod_0("ᚄ") + sqlString3.ToString().Replace(Class2.smethod_0("ᚄ"), Class2.smethod_0("ᚄᚅ")) + Class2.smethod_0("ᚄ") : str3 + Class2.smethod_0("ᚫᚳᚫᚬ");
              if (!struct2_1.sqlString_2.IsNull)
              {
                str2 = str2 + Class2.smethod_0("ᚉᙾᚺ") + struct2_1.sqlString_2.ToString() + Class2.smethod_0("ᚺ");
                str3 = !sqlString4.IsNull ? str3 + Class2.smethod_0("ᚉᙾᚆ") + sqlString4.ToString().Replace(Class2.smethod_0("ᚄ"), Class2.smethod_0("ᚄᚅ")) + Class2.smethod_0("ᚄ") : str3 + Class2.smethod_0("ᚉᙾᚭᚵᚭᚮ");
              }
              struct2_1.bool_0 = true;
              arrayList[index] = (object) struct2_1;
            }
          }
        }
      }
    }
    LastError = (SqlString) "";
    for (int index = 0; index < arrayList.Count; ++index)
    {
      LEADEngine.Struct2 struct2_2 = (LEADEngine.Struct2) arrayList[index];
      if ((!struct2_2.bool_1 || struct2_2.sqlInt32_1.ToString() == Class2.smethod_0("ᚍ") ? (struct2_2.bool_0 ? 1 : 0) : 1) == 0)
      {
        if (LastError.ToString().Length > 0)
          LastError += (SqlString) Class2.smethod_0("ᚉᙾ");
        LastError += (SqlString) (Class2.smethod_0("ᙿ") + struct2_2.sqlString_0.ToString() + Class2.smethod_0("ᙿ"));
      }
    }
    if (LastError.ToString().Length > 0)
    {
      LastError = (SqlString) (Class2.smethod_0("᩺᪓ᙿ᪗᪑᪡᪡\x1A9F᪢\x1A9B᪤᪳ᚉ᪨\x1A9C᪻᪤\x1A9E᪱᪥᪬᪾᪰\x1ABF᪪ᚖ᪶᪶᪴\x1AC9ᚵ᚜") + LastError.ToString());
    }
    else
    {
      sqlCommand.CommandType = CommandType.Text;
      string str4 = !ParentTransaction_id.IsNull ? ParentTransaction_id.ToString() : Class2.smethod_0("ᚫᚳᚫᚬ");
      string str5 = !ParentRow.IsNull ? ParentRow.ToString() : Class2.smethod_0("ᚫᚳᚫᚬ");
      string str6 = !ParentTable_id.IsNull ? ParentTable_id.ToString() : Class2.smethod_0("ᚫᚳᚫᚬ");
      sqlCommand.CommandText = Class2.smethod_0("ᚡᚣᚢᚬᚢᚴᚨᚄᚥᛚᛌᛕᛙᚊᚿᚭᚯᚺᚴᚐᚙᛄᛢ᛫ᛔᛟᛛᚘᛂᛈᛏ᚜ᛋᛍᛓᚠᛏᛗᛏᛐᚮᚦᛐᛖᛜᛏᛝᛠᚭᛗᛝᛤᛠᚲᛧᜆᛶᜄᜊ\x16F9\x16FCᜎᜄᜋᜋᜑᚿᛈᛵ᜔ᜄᜒ\x1718ᜇᜊ\x171Cᜒ\x1719\x1719ᜀᜦ\x171E᜔ᜏ\x171A\x1716ᛟᛔᜉᜨ\x1718ᜦᜬ\x171B\x171Eᜰᜦᜭᜭᜇ\x1716ᜋᜇᛰᛥ\x171A\x1739ᜩ\x1737\x173Dᜬᜯᝁ\x1737\x173E\x173Eᜤᝆ᜴ᝈᝊᝉᜃᛸ᜴ᜯᝎᝁᝏ\x173Dᝈᝄ\x173Eᜎᜃ᜴ᝆ\x1758ᝌ\x1756\x175D\x173E\x175Dᝍ\x175Bᝡᝐᝓᝥ\x175Bᝢᝢ\x1754\x175F\x175Bᜤ\x1719ᝊ\x175Cᝮᝢᝬᝳ\x1754ᝢᝤᝯᝩᝤᝯᝫ᜴ᜩ\x175Aᝬ\x177Eᝲ\x177Cឃᝢកញ\x173F᜴ᝩឈ\x1778ឆឌ\x177B\x177Eថឆឍឍᝣគធភឍបបᝓᝈᝬវថឍឡភឞឞ\x1775នឧយ\x175E\x1756ឆឍឍដថថ\x175Dឧឭឳឦ឴ិឩឩ\x1774ុឱឭᝪបរឡឝᝯថៅិៀោ\x1775\x177Eឩះ័ឹោៀឆ\x177E឵ឡឭិឨិចណ") + TransactionType_id.ToString() + Class2.smethod_0("ᚉᙾᚭᛅᛘᚫᛇᚌᚎᚒᚇᚘᚕᚊ") + User_id.ToString() + Class2.smethod_0("ᚉᙾ") + str4 + Class2.smethod_0("ᚉᙾ") + str6 + Class2.smethod_0("ᚉᙾ") + str5 + Class2.smethod_0("ᚉᙾᚭᚵᚭᚮᚏᚄᚬᛋᛛᚬᛊᛞᛐᚔᚖᚗᚏᛃᚶᚾᚸᚷᛉᚖᛋᛇᛉᚚᚬ᚜ᛏ᛭ᛶᛟᛪᛦᚣᛊᛗᛕᛔᚨᛉ\x16FEᛰ\x16F9\x16FD");
      try
      {
        Transaction_id = (SqlInt32) Convert.ToInt32(sqlCommand.ExecuteScalar());
      }
      catch (Exception ex)
      {
        LastError = (SqlString) (Class2.smethod_0("᩻᪦᪗᪑\x1A9B᪒ᚃ᪥᪣\x1A9D\x1A9B᪘᪦᪢᪺ᚌ\x1AAF\x1AAE\x1A9F᪭᪨᪢᪭᪺᪭\x1AAEᚗᚠ") + ex.Message + Class2.smethod_0("ᚆ"));
        return;
      }
      sqlCommand.CommandText = Class2.smethod_0("ᚦᚬᚲᚥᚳᚶᚃᚭᚳᚺᚶᚈ") + str1 + Class2.smethod_0("ᙽᚆᚳᛒᛂᛐᛖᛅᛈᛚᛐᛗᛗᛉᛔᛐᚙᚎ") + str2 + Class2.smethod_0("ᚆᙾᚵᚡᚭᚷᚨᚷᚅᚎ") + Transaction_id.ToString() + Class2.smethod_0("ᚉᙾ") + str3 + Class2.smethod_0("ᚆ");
      try
      {
        sqlCommand.ExecuteNonQuery();
      }
      catch (Exception ex)
      {
        LastError = (SqlString) (Class2.smethod_0("᩻᪦᪗᪑\x1A9B᪒ᚃ\x1A9B᪕᪥᪥᪣᪦\x1A9F᪨᪤᪼ᚎ᪱᪠᪢᪭᪫᪺\x1AC0ᚖ\x1AAE᪨᪬᪸᪶᪺\x1AAF᪸᪽᪲ᚡ\x1AC4\x1AC3᪴\x1AC2᪽᪷\x1AC2\x1ACF\x1AC2\x1AC3ᚬᚵ") + ex.Message + Class2.smethod_0("ᚆ"));
        return;
      }
      sqlConnection.Close();
    }
  }

  public static string ParameterShowValue(SqlConnection ThisConnection, int ParameterType_id, string ParameterValue)
  {
    string str1 = ParameterValue;
    string str2 = "";
    string str3 = "";
    string str4 = "";
    string str5 = "";
    string str6 = "";
    SqlCommand sqlCommand = (SqlCommand) null;
    try
    {
      sqlCommand = new SqlCommand(Class2.smethod_0("ᚰᚣᚫᚥᚤᚶᚃᚶᛊᛌᚻᛉᛋᛖᛐᚺᛎᛛᛔ᚜ᚑᛄᛘᛚᚸᛥᛣ᛭ᛦᛨᛈᛡᛪ᛭ᛍᛡᛮᛧᚯᚤᛗ᛫᛭ᛋᛸᛶᜀ\x16F9\x16FBᛡᛷ\x16FFᜈᛠᛴᜁ\x16FAᛂᚷᛮᜂ\x170Dᜐ\x16FDᜉᛲ\x1718ᜐᜆᛎᛃ᛫ᜊ\x171A᛭\x171D\x1717\x170D\x171F\x1715\x171C\x171Cᛏᛶᜃᜁᜀᛔ") + LEADEngine.smethod_7(13U, 14, 0U) + Class2.smethod_0("ᙽᛕᛈᛔᛉᚂᚋᛒᛔᛒᛖᛋᛔᚓᚋᛃᚵᚳᛁᚵᚑᛦᛜᛘᚕᚳᚗ") + ParameterType_id.ToString(), ThisConnection);
      SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
      while (sqlDataReader.Read())
      {
        str2 = sqlDataReader.GetString(0);
        str3 = sqlDataReader.GetString(1);
        str4 = sqlDataReader.GetString(2);
        str5 = sqlDataReader.GetString(3);
        str6 = sqlDataReader.GetString(4);
      }
      sqlDataReader.Close();
    }
    catch
    {
    }
    if (str5 == Class2.smethod_0("ᛓᛒᚱᛅᛇᛇᛕᛉᛓᛉᛌ"))
    {
      try
      {
        sqlCommand.CommandText = Class2.smethod_0("ᚰᚣᚫᚥᚤᚶᚃᚸᚴᚶᚇᚙᚉᛅ") + str4 + Class2.smethod_0("ᚺᙾᚥᚲᚰᚯᚃᚿ") + str2 + Class2.smethod_0("ᚺᙾᚶᚨᚦᚴᚨᚄᛀ") + str3 + Class2.smethod_0("ᚺᙾ᚜ ᚈ") + ParameterValue + Class2.smethod_0("ᚄ");
        str1 = sqlCommand.ExecuteScalar().ToString();
      }
      catch
      {
      }
    }
    if (str6.Length > 0)
    {
      try
      {
        sqlCommand.CommandText = Class2.smethod_0("ᚰᚣᚫᚥᚤᚶᚃᚿᚷᚫᚺᚽᚵᚾᛈᚌᚪᚎ") + str6 + Class2.smethod_0("ᚅᚅ") + str1 + Class2.smethod_0("ᚄᚇ");
        str1 = sqlCommand.ExecuteScalar().ToString();
      }
      catch
      {
      }
    }
    return str1;
  }

  [SqlProcedure]
  public static void XMLFinishAction(SqlString ActionSystemCode, SqlInt32 Action_id)
  {
    SqlConnection connection = new SqlConnection(LEADEngine.smethod_7(16U, 23, 0U));
    connection.Open();
    SqlCommand sqlCommand = new SqlCommand(Class2.smethod_0("ᚰᚣᚫᚥᚤᚶᚃᛘᛎᛊᚇᚮᚻᚹᚸᚌᚮᛑᛣᛙᛠᛠᛇ᛭ᛥᛛᛪᚘᛰᛣᛯᛤ\x169Dᚦ᛭ᛯ᛭ᛱᛦᛯᚮᚦᛞᛐᛎᛜᛐᚬᛠᜇᜂᜄᛶ\x16FFᛖᜃ\x16F9\x16FBᚷᛕᚹᛁ") + ActionSystemCode.ToString() + Class2.smethod_0("ᚄ"), connection);
    SqlInt32 sqlInt32;
    try
    {
      sqlInt32 = (SqlInt32) Convert.ToInt32(sqlCommand.ExecuteScalar());
    }
    catch
    {
      sqlInt32 = (SqlInt32) (-1);
    }
    if (sqlInt32 == (SqlInt32) (-1))
    {
      SqlContext.Pipe.Send(Class2.smethod_0("ᩯ᪖᪓ ᪢᪠᪔\x1AAFᪧ\x1A9E᪶ᚈ᪪ᚊ᪥᪪᪡᪬᪫ᚐᚓ") + ActionSystemCode.ToString() + Class2.smethod_0("ᙿᙾ\x1A9C᪕ᚁ᪠᪔᪡᪕᪦᪪\x1A9E\x1A9Eᪧᚋ\x1A9Eᚍ\x1AAF\x1AAE᪰᪡᪤᪱᪻᪲\x1AAE᪱᪭ᚚ"));
      connection.Close();
    }
    else
    {
      sqlCommand.CommandText = Class2.smethod_0("ᚰᚣᚫᚥᚤᚶᚃᛇᛓᛚᚇᚥᚉᛍᛚᛡᛛᛢᚗᚚᚚᚒᚹᛆᛄᛃᚗᛁᛇᛀᛊᛎᛊᚿᛓᛉᛐᛐᛢᛗᛈᛎᛌᛕᛊᚸᛟᛍᛏᛚᛔᛣᚱᛩᛛᛙᛧᛛᚷ᛬ᛚᛜᛧᛡ\x16FC᛬ᛠ᛭ᛦᛂᛠᛄᛌᜓ\x1716\x171Eᜈ᛫ᜎᜠ\x1716\x171D\x171Dᜣᜐ") + sqlInt32.ToString() + Class2.smethod_0("ᚄ");
      if (Convert.ToInt32(sqlCommand.ExecuteScalar()) == 0)
      {
        SqlContext.Pipe.Send(Class2.smethod_0("\x1A7E\x1A9C᪐᪫᪣\x1A9A᪘ᚄ᪦ᚆ\x1A9F\x1A9C\x1A9Eᪧ᪭᪤᪱᪦᪩᪠᪳᪰᪳᪲᪱ᚖ") + Action_id.ToString() + Class2.smethod_0("ᚉᙾ\x1A9D᪢\x1A9E᪠᪤᪳\x1AAE\x1A9B\x1A9C᪩᪸ᚊ᪥ᚌᪧ᪬᪣᪳ᚑᚔ") + ActionSystemCode.ToString() + Class2.smethod_0("ᙿᚊᙿ\x1A9D᪖ᚂ᪤ᪧ\x1AAE\x1A9B᪨᪪\x1A9B᪭᪠\x1AAE"));
        connection.Close();
      }
      else
      {
        sqlCommand.CommandText = Class2.smethod_0("ᚲᚮᚣᚡᚵᚧᚃᚿᛒᛕᛝᛇᚪᛍᛟᛕᛜᛜᛢᛏ") + sqlInt32.ToString() + Class2.smethod_0("ᚺᙾᚲᚥᚵᚂᚨᛒᛉᚧᛊᛜᛒᛙᛙᚌᚪᚎᛖᛕᛥᛖᛔᛨᛚ\x169Eᚠᚘᛐᛂᛀᛎᛂ\x169Eᛳᛩᛥᚢᛀᚤ") + Action_id.ToString();
        sqlCommand.ExecuteNonQuery();
        connection.Close();
      }
    }
  }

  [SqlProcedure]
  public static void XMLStartAction(SqlString ActionSystemCode, SqlXml ActionParameters, out SqlInt32 Action_id)
  {
    SqlConnection connection = new SqlConnection(LEADEngine.smethod_7(16U, 23, 0U));
    connection.Open();
    SqlCommand sqlCommand = new SqlCommand(Class2.smethod_0("ᚰᚣᚫᚥᚤᚶᚃᛘᛎᛊᚇᚮᚻᚹᚸᚌᚮᛑᛣᛙᛠᛠᛇ᛭ᛥᛛᛪᚘᛰᛣᛯᛤ\x169Dᚦ᛭ᛯ᛭ᛱᛦᛯᚮᚦᛞᛐᛎᛜᛐᚬᛠᜇᜂᜄᛶ\x16FFᛖᜃ\x16F9\x16FBᚷᛕᚹᛁ") + ActionSystemCode.ToString() + Class2.smethod_0("ᚄ"), connection);
    SqlInt32 sqlInt32_1;
    try
    {
      sqlInt32_1 = (SqlInt32) Convert.ToInt32(sqlCommand.ExecuteScalar());
    }
    catch
    {
      sqlInt32_1 = (SqlInt32) (-1);
    }
    if (sqlInt32_1 == (SqlInt32) (-1))
    {
      SqlContext.Pipe.Send(Class2.smethod_0("ᩯ᪖᪓ ᪢᪠᪔\x1AAFᪧ\x1A9E᪶ᚈ᪪ᚊ᪥᪪᪡᪬᪫ᚐᚓ") + ActionSystemCode.ToString() + Class2.smethod_0("ᙿᙾ\x1A9C᪕ᚁ᪠᪔᪡᪕᪦᪪\x1A9E\x1A9Eᪧᚋ\x1A9Eᚍ\x1AAF\x1AAE᪰᪡᪤᪱᪻᪲\x1AAE᪱᪭ᚚ"));
      connection.Close();
      Action_id = SqlInt32.Null;
    }
    else
    {
      sqlCommand.CommandText = Class2.smethod_0("ᚰᚣᚫᚥᚤᚶᚃᛇᛓᛚᚇᚥᚉᛍᛚᛡᛛᛢᚗᚚᚚᚒᚹᛆᛄᛃᚗᛁᛇᛀᛊᛎᛊᚿᛓᛉᛐᛐᛢᛗᛈᛎᛌᛕᛊᚸᛟᛍᛏᛚᛔᛣᚱᛩᛛᛙᛧᛛᚷ᛬ᛚᛜᛧᛡ\x16FC᛬ᛠ᛭ᛦᛂᛠᛄᛌᜓ\x1716\x171Eᜈ᛫ᜎᜠ\x1716\x171D\x171Dᜣᜐ") + sqlInt32_1.ToString() + Class2.smethod_0("ᚄ");
      SqlInt32 int32 = (SqlInt32) Convert.ToInt32(sqlCommand.ExecuteScalar());
      ArrayList arrayList = new ArrayList();
      sqlCommand.CommandText = Class2.smethod_0("ᛐᛃᛋᛅᛄᛖᚃᚿᚨᛕᛓᛝᛖᛘᚯᛑᛓᛗᛝᛙᛥᛛᛢᛢᛒᚖᚴᚘᚠᛕᚾ᛫ᛩᛳ᛬ᛮᛠᚩᚣᚯᚥᛩᛨ\x16FB\x16FDᚲ᛬ᚺᛝᛯᜁᛱ\x16FEᛷᜇ\x16F9ᜇᛪᜐᜈ\x16FE\x16F9ᜄᜀᚽ\x16FFᜒᛀ\x1717ᜃ\x1715ᜇ\x170Dᜇ\x1719ᛐᛛᛚᛔᛕᛍᛙᛏᛗᜎᛒᛚᛔᛠᛖ\x171B\x171Aᜨᛨᜂᜡᜱᜂᜠ᜴ᜢ\x1716\x173C᜴ᜪᛮᜪᛶ\x171Fᜳ\x173Eᝁᜮ\x173Aᜣᝉᝁ\x1737\x16FCᛴᜀᛶ\x16FEᛸᜧᜯᜧᜨᜄ\x16FEᝅᝒᝐᝏᜃᜥᝈ\x175Aᝐ\x1757\x1757\x173Eᝤ\x175Cᝒ\x173Eᝐᝢᝒ\x175F\x1758ᝨ\x175Aᝨᝪ\x1718\x175A\x176D\x171B\x175D\x171D\x1775ᝨ\x1774ᝩᜢᜫᝲ\x1774ᝲ\x1776ᝫ\x1774ᜳᜫ\x1778ᝲ\x1774ឃᜰ\x177Bខ\x177Cគ᜵\x175Fᝥ\x175Eᝨᝬᝨ\x175D\x1771ᝧᝮᝮក\x1775ᝦᝬᝪᝳᝨ\x1756ᝬ\x1779\x1777ខ\x177A\x177Cគᝐធឥᝓព\x1755ឭហឬឡ\x175Aᝣឪឬឪឮឣឬᝫᝣឳឳᝦᝮឋីាៀឹុឭ\x1776ᝰ\x177Cᝲា឵ៈ៊\x177Fឹជឪូ៎ើ់ោ។ំ។ិ៝៕់ំ៑៍ដ៌\x17DFឍ៤័២។៚។៦ឝឨឧឡអរីវ\x17DFឬែ៏៍ៗ័្៤។ៈ៕៎ឪ\x17EC\x17FA\x17F1ឮ\x17F1ើ៥៓៕០៚\x17F5៥៙៦\x17DFុ៙ួៅ᠌\x180F᠗᠁៤᠇᠙\x180F᠖᠖\x181C᠉") + sqlInt32_1.ToString() + Class2.smethod_0("ᚄᙾᛉᛏᛊᛐᚃ") + LEADEngine.smethod_7(13U, 14, 0U) + Class2.smethod_0("ᙽᚿᛒ ᛄᚂᛚᛍᛙᛎᚇᚐᛗᛙᛗᛛᛐᛙᚘᚐᛠᛠᚓᛕᚣᛆᛘᛪᛚᛧᛠᛰᛢᛰᛓ\x16F9ᛱᛧᛢ᛭ᛩᚦᛄᚨ᛬ᚸ\x16FFᛵᛱᚮᜆᛸᛶᜄᛸᚴᛶᛄᛘ\x16FB\x170Dᜃᜊᜊᛱ\x1717ᜏᜅᜀᜋᜇᛄᛢᛆ") + sqlInt32_1.ToString() + Class2.smethod_0("ᙽᚿᛍᛄᚁᛄᚑᚧᚴᚲᚼᚵᚷᛉᚹᚭᚺᚳᚏᛙᛤᚒᛁᛉᛁᛂ");
      SqlDataReader sqlDataReader1 = sqlCommand.ExecuteReader();
      string str1 = "";
      bool flag = false;
      while (sqlDataReader1.Read())
      {
        flag = true;
        string str2 = sqlDataReader1.GetString(0);
        if (int32 > (SqlInt32) 0)
        {
          str1 = str1 + Class2.smethod_0("\x169Eᚪᚳᚥᚳᚂᚷᚥᚧᚲᚬᚈᛄᛗᛚᛢᛌᚯᛒᛤᛚᛡᛡᛧᛔ") + sqlInt32_1.ToString() + Class2.smethod_0("ᚺᙾᚠᚤᚥᚂ") + str2 + Class2.smethod_0("ᚘᙾᚦᚯ᚜ᚂ");
        }
        else
        {
          if (str1.Length > 0)
            str1 += Class2.smethod_0("ᚉᙾ");
          str1 += str2;
        }
      }
      sqlDataReader1.Close();
      if (flag)
      {
        if (int32 == (SqlInt32) 0)
          str1 = Class2.smethod_0("ᚠᚰᚤᚡᚵᚧᚃᚸᚦᚨᚳᚭᚉᛅᛘᛛᛣᛍᚰᛓᛥᛛᛢᛢᛨᛕ") + sqlInt32_1.ToString() + Class2.smethod_0("ᚺᙾᚇᛔᛊᛆᚃᛍᛓᛚᚇᛖᛘᛞᚋᛚᛢᛚᛛᚐᛚᛖᛘᛢᛩᛟ᛫ᛱᚡᚪᚧᚭᚦ\x169Eᛯᛲᛪᛯᛤᛶ\x16FEᚦᛲ᛭ᜂᚶᚫᛟᜁᛯᜁᜄᛒᛵᜇ\x16FDᜄᜄᚷ\x16FC\x16FAᜎᜀᜐᜆᜋᜄᛀᜏᜑ\x1717ᛄᜓ\x171Bᜓ᜔ᛉᜎᜐᜒᜎᜣ\x171Bᜤᛑ\x1719\x1718ᜨ\x1719\x1717ᜫ\x171Dᛡᛣᛧᛜᜂᜬᜣᜁᜤ᜶ᜬᜳᜳᛦᜫᜩ\x173Dᜯ\x173F᜵\x173Aᜳᛯ\x173Eᝆ\x173E\x173Fᜀᛵ\x1717\x173Aᝌᝂᝉᝉᜣᜲᜧᜣᜀ\x1756ᝐᝌ\x1755\x175Aᝋᝐᝌᝎ\x1758\x175F\x1755ᝓ\x1757\x1754ᝢ\x171Dᜒᝃᝠ\x1756ᝤᝥ\x175D\x175D\x173Fᝩᝠᝁ\x175Fᝳᝥᜡᝦᝤ\x1778ᝪ\x177Aᝰ\x1775ᝮᜪ\x1779ខ\x1779\x177A\x173Bᜰ\x1755ᝳជ\x1779\x1759\x177F\x177D\x177E\x1739\x177Cងឃឆឌនᝀតភតថᝑᝆ") + str1 + Class2.smethod_0("ᚆ");
        sqlCommand.CommandText = str1;
        sqlCommand.ExecuteNonQuery();
      }
      XmlDocument xmlDocument = new XmlDocument();
      xmlDocument.LoadXml(ActionParameters.Value);
      string upper = Guid.NewGuid().ToString().ToUpper();
      string str3 = Class2.smethod_0("ᚸ\x169Fᛂᛔᛊᛑᛑᚫᚺᚯᚫᛅᚕᚊᛆᚿᛡᛏᛡᛤᚲᛕᛧᛝᛤᛤᛔᚤᚙᛕᛋᛨᛞ᛬᛭ᛥᛥᛇᛱᛨᛉᛧ\x16FB᛭ᛦ");
      string str4 = Class2.smethod_0("ᚄ") + upper + Class2.smethod_0("ᚄᚊᙿᛇᛆᛖᛇᛅᛙᛋᚏᚑᚕᚊᚑᚒᚽᚺᚰᚾᚿᚷᚷᚚ᚛");
      string str5 = "";
      foreach (XmlElement xmlElement in (XmlNode) xmlDocument[Class2.smethod_0("ᚬᛀᛉᛅᛄᛖᚶᛇᛍᛋᛔᛉ")])
      {
        if (xmlElement.Name == Class2.smethod_0("ᚬᛀᛉᛅᛄᛖ"))
        {
          SqlInt32 sqlInt32_2 = (SqlInt32) (-1);
          if (xmlElement.Attributes[Class2.smethod_0("ᚭᚿᛑᛁᛎᛇᛗᛉᛗᚴᛈᛕᛎ")] != null)
          {
            string str2 = xmlElement.Attributes[Class2.smethod_0("ᚭᚿᛑᛁᛎᛇᛗᛉᛗᚴᛈᛕᛎ")].Value.Replace(Class2.smethod_0("ᚄ"), "");
            sqlCommand.CommandText = Class2.smethod_0("ᚰᚣᚫᚥᚤᚶᚃᚸᚴᚶᚇᚙᚉᚺᛌᛞᛎᛛᛔᛤᛖᛤᛇ᛭ᛥᛛᛖᛡᛝᚚᛁᛎᛌᛋ\x169Fᛁᛤᛶ᛬ᛳᛳᛚᜀᛸᛮᛚ᛬\x16FEᛮ\x16FBᛴᜄᛶᜄᜆᚴᜌ\x16FFᜋᜀᚹᛂᜉᜋᜉ\x170Dᜂᜋᛊᛂ\x16FA᛬ᛪᛸ᛬ᛈᛪ\x170D\x171F\x1715\x171C\x171Cᜃᜩᜡ\x1717ᜒ\x171D\x1719ᛖᛴᛘ") + sqlInt32_1.ToString() + Class2.smethod_0("ᙽ\x169Fᚭᚤᚁᚥᛘᛗᛙᛕᛔᚶᛊᛗᛐᚌᚪᚎᚖ") + str2 + Class2.smethod_0("ᚄ");
            try
            {
              sqlInt32_2 = (SqlInt32) Convert.ToInt32(sqlCommand.ExecuteScalar());
            }
            catch
            {
            }
          }
          else if (xmlElement.Attributes[Class2.smethod_0("ᚯᛃᛅᚴᛚᛒᛈ")] != null)
          {
            string str2 = xmlElement.Attributes[Class2.smethod_0("ᚯᛃᛅᚴᛚᛒᛈ")].Value.Replace(Class2.smethod_0("ᚄ"), "");
            sqlCommand.CommandText = Class2.smethod_0("ᚰᚣᚫᚥᚤᚶᚃᚸᚴᚶᚇᚙᚉᛞᛔᛐᚍᚴᛁᚿᚾᚒ") + LEADEngine.smethod_7(13U, 14, 0U) + Class2.smethod_0("ᙽᛕᛈᛔᛉᚂᚋᛒᛔᛒᛖᛋᛔᚓᚋᛃᚵᚳᛁᚵᚑᛄᛘᛚᛉᛯᛧᛝᚙᚷ᚛ᚣ") + str2 + Class2.smethod_0("ᚄ");
            try
            {
              sqlInt32_2 = (SqlInt32) Convert.ToInt32(sqlCommand.ExecuteScalar());
            }
            catch
            {
              sqlInt32_2 = (SqlInt32) (-1);
            }
          }
          else if (xmlElement.Attributes[Class2.smethod_0("ᚭᚿᛑᛁᛎᛇᛗᛉᛗᚺᛠᛘᛎᛉᛔᛐ")] != null)
          {
            try
            {
              sqlInt32_2 = (SqlInt32) Convert.ToInt32(xmlElement.Attributes[Class2.smethod_0("ᚭᚿᛑᛁᛎᛇᛗᛉᛗᚺᛠᛘᛎᛉᛔᛐ")].Value);
            }
            catch
            {
              sqlInt32_2 = (SqlInt32) (-1);
            }
          }
          if ((SqlBoolean) (xmlElement.Attributes[Class2.smethod_0("ᚳᚿᛋᛕᛆ")] != null) && sqlInt32_2 != (SqlInt32) (-1))
          {
            string str2 = xmlElement.Attributes[Class2.smethod_0("ᚳᚿᛋᛕᛆ")].Value.Replace(Class2.smethod_0("ᚄ"), "");
            if (str3.Length > 0)
              str3 += Class2.smethod_0("ᚉᙾ");
            str3 = str3 + Class2.smethod_0("ᚸᚡᛎᛌᛖᛏᛑᛃ") + sqlInt32_2.ToString() + Class2.smethod_0("ᚺ");
            if (str4.Length > 0)
              str4 += Class2.smethod_0("ᚉᙾ");
            str4 = str4 + Class2.smethod_0("ᚄ") + str2.Replace(Class2.smethod_0("ᚄ"), "") + Class2.smethod_0("ᚄ");
            if (str5.Length > 0)
              str5 += Class2.smethod_0("ᙽ\x169Fᚭᚤᚁ");
            str5 = str5 + Class2.smethod_0("ᚸᚡᛎᛌᛖᛏᛑᛃ") + sqlInt32_2.ToString() + Class2.smethod_0("ᚺᙾ᚜ ᚈ") + str2 + Class2.smethod_0("ᚄ");
          }
        }
      }
      int num1 = 0;
      int num2 = -1;
      if (str5.Length > 0)
      {
        string str2 = Class2.smethod_0("ᚰᚣᚫᚥᚤᚶᚃᚧᚴᚻᚵᚼᚑᚔᚔᚘᚍᚯᛅᚷᚙᛍᚷᛕᛩᛛᚻᛡᛟᛠᛘᚥ\x169Dᛄᛑᛏᛎᚢᛞᛱᛴ\x16FCᛦᛉ᛬\x16FEᛴ\x16FB\x16FBᜁᛮ") + sqlInt32_1.ToString() + Class2.smethod_0("ᚺᙾᛖᛉᛕᛊᚃᚌᛓᛕᛓᛗᛌᛕᚔᚌᛄᚶᚴᛂᚶᚒᛎᚸᛖᛪᛜᚼᛢᛠᛡᛙ\x169Dᛧᛲᚠᛏᛑᛗᚤᛓᛛᛓᛔᚩᛋᛙᛐᚭ") + str5;
        sqlCommand.CommandText = str2;
        SqlDataReader sqlDataReader2 = sqlCommand.ExecuteReader();
        while (sqlDataReader2.Read())
        {
          num1 = sqlDataReader2.GetInt32(0);
          if (!sqlDataReader2.IsDBNull(1))
            num2 = sqlDataReader2.GetInt32(1);
        }
        sqlDataReader2.Close();
      }
      if (num1 == 0)
        num2 = 0;
      string str6 = str4.Replace(Class2.smethod_0("ᚃᚄᚯᚬᚢᚰᚱᚩᚩᚌᚍ"), Class2.smethod_0("ᛁᚿᛓᛅᛂᛆᛇᚌᛘᛋᛊᛗᛗᛎᚗᚌ") + num2.ToString() + Class2.smethod_0("ᚉᙾᛆᛅᛕᛆᛄᛘᛊᚎᚐᚑ"));
      string message = Class2.smethod_0("ᚡᚣᚢᚬᚢᚴᚨᚄᚥᛚᛌᛕᛙᚊᚿᚭᚯᚺᚴᚐᚙᛦᛜᛘᚕᛟᛥ᛬ᚙᛨᛪᛰ\x169D᛬ᛴ᛬᛭ᚫᚾᚤᛎᛔᛚᛍᛛᛞᚫᛕᛛᛢᛞᚰ᛬\x16FFᜂᜊᛴᛗ\x16FAᜌᜂᜉᜉᜏ\x16FC") + sqlInt32_1.ToString() + Class2.smethod_0("ᚺᙾᚇ") + str3 + Class2.smethod_0("ᚆᙾᚮᚵᚵᚲᚸᚸᚅᛏᛕᛛᛎᛜᛟᛑᛑ᚜ᛣᛙᛕᚒᚼᛂᛉᛅᚗᚸ᛭ᛟᛨ᛬\x169Dᚦᛳᛩᛥᚫᚣᛚᛆᛒᛜᛍᛜᚪᚳ") + str6 + Class2.smethod_0("ᚆᚙᙿᚳᚦᚮᚨᚧᚹᚆᚻᚷᚹᚊ᚜ᚌᛡᛗᛓᚐᚷᛄᛂᛁᚕᚶ᛫ᛝᛦᛪᚶ");
      SqlContext.Pipe.Send(message);
      sqlCommand.CommandText = message;
      Action_id = (SqlInt32) Convert.ToInt32(sqlCommand.ExecuteScalar());
      connection.Close();
    }
  }

  [SqlProcedure]
  public static void XMLPostEvent(SqlString EventSystemCode, SqlXml EventParameters)
  {
    SqlConnection sqlConnection = new SqlConnection(LEADEngine.smethod_7(16U, 23, 0U));
    sqlConnection.Open();
    SqlCommand sqlCommand = new SqlCommand(Class2.smethod_0("ᚰᚣᚫᚥᚤᚶᚃᛘᛎᛊᚇᚮᚻᚹᚸᚌᚲᛤᛔᛞᛥᛆ᛬ᛤᛚᛩᚗᛯᛢᛮᛣ᚜ᚥ᛬ᛮ᛬ᛰᛥᛮᚭᚥᛝᛏᛍᛛᛏᚫᛟᜆᜁᜃᛵ\x16FEᛕᜂᛸ\x16FAᚶᛔᚸᛀ") + EventSystemCode.ToString() + Class2.smethod_0("ᚄ"), sqlConnection);
    SqlInt32 sqlInt32_1;
    try
    {
      sqlInt32_1 = (SqlInt32) Convert.ToInt32(sqlCommand.ExecuteScalar());
    }
    catch
    {
      sqlInt32_1 = (SqlInt32) (-1);
    }
    if (sqlInt32_1 == (SqlInt32)(-1))
    {
      SqlContext.Pipe.Send(Class2.smethod_0("ᩯ᪖᪓ ᪢᪠᪔\x1AAFᪧ\x1A9E᪶ᚈ᪪ᚊ᪥᪪᪡᪬᪫ᚐᚓ") + EventSystemCode.ToString() + Class2.smethod_0("ᙿᙾ\x1A9C᪕ᚁ᪠᪔᪡᪕᪦᪪\x1A9E\x1A9Eᪧᚋ\x1A9Eᚍ\x1AAF\x1AAE᪰᪡᪤᪱᪻᪲\x1AAE᪱᪭ᚚ"));
      sqlConnection.Close();
    }
    else
    {
      sqlCommand.CommandText = Class2.smethod_0("ᚰᚣᚫᚥᚤᚶᚃᛇᛓᛚᚇᚥᚉᛍᛚᛡᛛᛢᚗᚚᚚᚒᚹᛆᛄᛃᚗᛁᛇᛀᛊᛎᛊᚿᛓᛉᛐᛐᛢᛗᛈᛎᛌᛕᛊᚸᛟᛍᛏᛚᛔᛣᚱᛩᛛᛙᛧᛛᚷ᛬ᛚᛜᛧᛡ\x16FC᛬ᛠ᛭ᛦᛂᛠᛄᛌᜓ\x1716\x171Eᜈᛯᜡᜑ\x171Bᜢᜢᜏ") + sqlInt32_1.ToString() + Class2.smethod_0("ᚄ");
      SqlInt32 int32 = (SqlInt32) Convert.ToInt32(sqlCommand.ExecuteScalar());
      ArrayList arrayList1 = new ArrayList();
      sqlCommand.CommandText = Class2.smethod_0("ᛐᛃᛋᛅᛄᛖᚃᚿᚨᛕᛓᛝᛖᛘᚯᛑᛓᛗᛝᛙᛥᛛᛢᛢᛒᚖᚴᚘᚠᛕᚾ᛫ᛩᛳ᛬ᛮᛠᚩᚣᚯᚥᛩᛨ\x16FB\x16FDᚲ᛬ᚺᛝᛯᜁᛱ\x16FEᛷᜇ\x16F9ᜇᛪᜐᜈ\x16FE\x16F9ᜄᜀᚽ\x16FFᜒᛀ\x1717ᜃ\x1715ᜇ\x170Dᜇ\x1719ᛐᛛᛚᛔᛕᛍᛙᛏᛗᜎᛒᛚᛔᛠᛖ\x171B\x171Aᜨᛨᜂᜡᜱᜂᜠ᜴ᜢ\x1716\x173C᜴ᜪᛮᜪᛶ\x171Fᜳ\x173Eᝁᜮ\x173Aᜣᝉᝁ\x1737\x16FCᛴᜀᛶ\x16FEᛸᜧᜯᜧᜨᜄ\x16FEᝅᝒᝐᝏᜃᜩ\x175Bᝋ\x1755\x175C\x173Dᝣ\x175Bᝑ\x173Dᝏᝡᝑ\x175E\x1757ᝧ\x1759ᝧᝩ\x1717\x1759ᝬ\x171A\x175C\x171C\x1774ᝧᝳᝨᜡᜪ\x1771ᝳ\x1771\x1775ᝪᝳᜲᜪ\x1777\x1771ᝳគᜯ\x177Aក\x177Bខ᜴\x175Eᝤ\x175Dᝧᝫᝧ\x175Cᝰᝦ\x176D\x176D\x177F\x1774ᝥᝫᝩᝲᝧ\x1755ᝫ\x1778\x1776ក\x1779\x177Bខᝏទឤᝒផ\x1754ឬសឫហ\x1759ᝢឩឫឩឭអឫᝪᝢឲឲᝥ\x176Dដិ឵ឿីឺឬ\x1775ᝯ\x177B\x1771឵឴ះ៉\x177Eីឆឩុ៍ួ៊ៃ៓ៅ៓ាៜ។៊ៅ័៌ញ់\x17DEឌ៣៏១៓៙៓៥វឧឦហឡយិល\x17DEឫេ៎៌៖៏៑៣៓ះ។៍ឩ\x17EB\x17F9\x17F0ឭ\x17F0ួ៤្។\x17DF៙\x17F4៤៘៥\x17DEឺ៘ូោ᠋\x180E᠖᠀៧᠙᠉᠓\x181A\x181A᠇") + sqlInt32_1.ToString() + Class2.smethod_0("ᚄᙾᛉᛏᛊᛐᚃ") + LEADEngine.smethod_7(13U, 14, 0U) + Class2.smethod_0("ᙽᚿᛒ ᛄᚂᛚᛍᛙᛎᚇᚐᛗᛙᛗᛛᛐᛙᚘᚐᛠᛠᚓᛕᚣᛆᛘᛪᛚᛧᛠᛰᛢᛰᛓ\x16F9ᛱᛧᛢ᛭ᛩᚦᛄᚨ᛬ᚸ\x16FFᛵᛱᚮᜆᛸᛶᜄᛸᚴᛶᛄᛜᜎ\x16FEᜈᜏᛰ\x1716ᜎᜄ\x16FFᜊᜆᛃᛡᛅ") + sqlInt32_1.ToString() + Class2.smethod_0("ᙽᚿᛍᛄᚁᛄᚑᚧᚴᚲᚼᚵᚷᛉᚹᚭᚺᚳᚏᛙᛤᚒᛁᛉᛁᛂ");
      string message1 = "";
      bool flag = false;
      using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
      {
        while (sqlDataReader.Read())
        {
          flag = true;
          string str = sqlDataReader.GetString(0);
          if (int32 > (SqlInt32) 0)
          {
            message1 = message1 + Class2.smethod_0("\x169Eᚪᚳᚥᚳᚂᚷᚥᚧᚲᚬᚈᛄᛗᛚᛢᛌᚳᛥᛕᛟᛦᛦᛓ") + sqlInt32_1.ToString() + Class2.smethod_0("ᚺᙾᚠᚤᚥᚂ") + str + Class2.smethod_0("ᚘᙾᚦᚯ᚜ᚂ");
          }
          else
          {
            if (message1.Length > 0)
              message1 += Class2.smethod_0("ᚉᙾ");
            message1 += str;
          }
        }
        sqlDataReader.Close();
      }
      if (flag)
      {
        if (int32 == (SqlInt32) 0)
          message1 = Class2.smethod_0("ᚠᚰᚤᚡᚵᚧᚃᚸᚦᚨᚳᚭᚉᛅᛘᛛᛣᛍᚴᛦᛖᛠᛧᛧᛔ") + sqlInt32_1.ToString() + Class2.smethod_0("ᚺᙾᚇᛔᛊᛆᚃᛍᛓᛚᚇᛖᛘᛞᚋᛚᛢᛚᛛᚐᛚᛖᛘᛢᛩᛟ᛫ᛱᚡᚪᚧᚭᚦ\x169Eᛯᛲᛪᛯᛤᛶ\x16FEᚦᛲ᛭ᜂᚶᚫᛑᜃᛳ\x16FDᜄᛕᛳᜇ\x16F9ᚵ\x16FAᛸᜌ\x16FEᜎᜄᜉᜂᚾ\x170Dᜏ\x1715ᛂᜑ\x1719ᜑᜒᛇᜌᜎᜐᜌᜡ\x1719ᜢᛏ\x1717\x1716ᜦ\x1717\x1715ᜩ\x171Bᛟᛡᛥᛚᜀᜲᜢᜬᜳᜇ\x1716ᜋᜇᛤ\x173A᜴ᜰ\x1739\x173Eᜯ᜴ᜰᜲ\x173Cᝃ\x1739\x1737\x173B\x1738ᝆᜁᛶ") + message1 + Class2.smethod_0("ᚆ");
        SqlContext.Pipe.Send(message1);
        sqlCommand.CommandText = message1;
        sqlCommand.ExecuteNonQuery();
      }
      ArrayList arrayList2 = new ArrayList();
      sqlCommand.CommandText = Class2.smethod_0("ᚰᚣᚫᚥᚤᚶᚃᛅᚓᚩᛖᛖᛝᛋᛎᛠᚴᛠᛞᛥᛡᛑᛜᛘᚡᚖᛙᚦᛉ᛬ᛪᛟᛢᛢᛴᛲᛦᛐᛤᛱᛪᚲᚧᛩᚷᛙ᛭\x16FF\x16FC\x16FAᛴᜄᛶᛟ\x16FCᜂᜊᜊ\x16FCᜋᛅᚺ\x16FEᛊᛱᜃᜌᜐ\x170Dᜃ\x1717ᜉᛳᜇ᜔\x170Dᛕᛊᜎᛚᜈ\x16FB᜔ᜣᜤᜓ\x171A\x1719ᜒᛢᛗ\x171Bᛧᜮᜤᜠᛝᜄᜑᜏᜎᛢᜈ\x173Aᜪ᜴\x173B\x1716\x1738\x173E᜴ᜲ᜶ᜱᜰᝄ\x173Aᝁᝁᝇ\x171Cᝈᝆᝍᝉᝍ\x16FB\x173Dᝐ\x16FEᝀᜀ\x1758ᝋ\x1757ᝌᜅᜎ\x1755\x1757\x1755\x1759ᝎ\x1757\x1716ᜎ\x1739\x173F\x173Aᝀᜓᝂᝤᝪᝠ\x175Eᝢ\x175D\x175Cᝰᝦ\x176D\x176D\x1754\x177Aᝲᝨ\x1777ᜥᝧ\x177Aᜨᝫᜪគ\x1775ខ\x1776ᜯ\x1738\x177Fខ\x177Fឃ\x1778ខᝀ\x1738ᝨᝨ\x173B\x177Dᝋᝬណបដឈឌជឆរថភភ\x177Eឤវធឍមបᝑᝯᝓពᝣឪហវ\x1759ងដចឋ\x175Eឍឯ឵ឫឩឭឨឧុឱីីសឱឺើុឱៅិំ\x1774ា៉\x1777ុ\x1779៑ោ័ៅ\x177Eជ៎័៎្ះ័តជិិដ៌រុ៝៣៙ៗ៛៖៕៩\x17DF៦៦៍\x17DF៨\x17EC៩\x17DF\x17F3៥០\x17EB៧ឤែឦ\x17EAា\x17FD\x17F3\x17EFឬ៤៖។២៖ឲ\x17F4ែ៚᠌\x17FC᠆᠍\x17EE᠔᠌᠂\x17FD᠈᠄េ\x17DFៃ") + sqlInt32_1.ToString() + Class2.smethod_0("ᙽᙾᚠᚮᚥᚂᛄᚒᚸᛚᛈᛚᛝᚮᛌᛠᛒᚎᚫᚭᚑᛙᛘᛨᛙᛗ᛫ᛝᚡᚣ᚛ᚽᛋᛂ\x169Fᛩᛴᛰᛸᛰᛱᚮᛨᚶᛎᛸᛯᛐᛮᜂᛴᚼᚱᚹᛅᛍᛎᛏᛄᛉᛋᛇᛎᛍᛄᛇᚿᛞᛞᛂᜊᜉ\x1719ᜊᜈ\x171Cᜎᛒᛔ");
      using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
      {
        while (sqlDataReader.Read())
          arrayList2.Add((object) new LEADEngine.Struct3()
          {
            int_0 = sqlDataReader.GetInt32(0),
            string_0 = sqlDataReader.GetString(1),
            int_1 = sqlDataReader.GetInt32(2),
            string_1 = sqlDataReader.GetString(3),
            int_2 = sqlDataReader.GetInt32(4),
            string_2 = sqlDataReader.GetString(4)
          });
        sqlDataReader.Close();
      }
      XmlDocument xmlDocument = new XmlDocument();
      xmlDocument.LoadXml(EventParameters.Value);
      string upper = Guid.NewGuid().ToString().ToUpper();
      string str1 = Class2.smethod_0("ᚸᚣᛕᛅᛏᛖᚪᚹᚮᚪᛄ");
      string str2 = Class2.smethod_0("ᚄ") + upper + Class2.smethod_0("ᚄ");
      foreach (XmlElement xmlElement in (XmlNode) xmlDocument[Class2.smethod_0("ᚬᛀᛉᛅᛄᛖᚶᛇᛍᛋᛔᛉ")])
      {
        if (xmlElement.Name == Class2.smethod_0("ᚬᛀᛉᛅᛄᛖ"))
        {
          SqlInt32 sqlInt32_2 = (SqlInt32)(-1);
          if (xmlElement.Attributes[Class2.smethod_0("ᚭᚿᛑᛁᛎᛇᛗᛉᛗᚴᛈᛕᛎ")] != null)
          {
            string str3 = xmlElement.Attributes[Class2.smethod_0("ᚭᚿᛑᛁᛎᛇᛗᛉᛗᚴᛈᛕᛎ")].Value.Replace(Class2.smethod_0("ᚄ"), "");
            sqlCommand.CommandText = Class2.smethod_0("ᚰᚣᚫᚥᚤᚶᚃᚸᚴᚶᚇᚙᚉᚺᛌᛞᛎᛛᛔᛤᛖᛤᛇ᛭ᛥᛛᛖᛡᛝᚚᛁᛎᛌᛋ\x169Fᛅᛷᛧᛱᛸᛙ\x16FFᛷ᛭ᛙ᛫\x16FD᛭\x16FAᛳᜃᛵᜃᜅᚳᜋ\x16FEᜊ\x16FFᚸᛁᜈᜊᜈᜌᜁᜊᛉᛁ\x16F9᛫ᛩᛷ᛫ᛇ᛭\x171Fᜏ\x1719ᜠᜁᜧ\x171F\x1715ᜐ\x171B\x1717ᛔᛲᛖ") + sqlInt32_1.ToString() + Class2.smethod_0("ᙽ\x169Fᚭᚤᚁᚥᛘᛗᛙᛕᛔᚶᛊᛗᛐᚌᚪᚎᚖ") + str3 + Class2.smethod_0("ᚄ");
            try
            {
              sqlInt32_2 = (SqlInt32) Convert.ToInt32(sqlCommand.ExecuteScalar());
            }
            catch
            {
              sqlInt32_2 = (SqlInt32)(-1);
            }
          }
          else if (xmlElement.Attributes[Class2.smethod_0("ᚯᛃᛅᚴᛚᛒᛈ")] != null)
          {
            string str3 = xmlElement.Attributes[Class2.smethod_0("ᚯᛃᛅᚴᛚᛒᛈ")].Value.Replace(Class2.smethod_0("ᚄ"), "");
            sqlCommand.CommandText = Class2.smethod_0("ᚰᚣᚫᚥᚤᚶᚃᚸᚴᚶᚇᚙᚉᛞᛔᛐᚍᚴᛁᚿᚾᚒ") + LEADEngine.smethod_7(13U, 14, 0U) + Class2.smethod_0("ᙽᛕᛈᛔᛉᚂᚋᛒᛔᛒᛖᛋᛔᚓᚋᛃᚵᚳᛁᚵᚑᛄᛘᛚᛉᛯᛧᛝᚙᚷ᚛ᚣ") + str3 + Class2.smethod_0("ᚄ");
            try
            {
              sqlInt32_2 = (SqlInt32) Convert.ToInt32(sqlCommand.ExecuteScalar());
            }
            catch
            {
              sqlInt32_2 = (SqlInt32)(-1);
            }
          }
          else if (xmlElement.Attributes[Class2.smethod_0("ᚭᚿᛑᛁᛎᛇᛗᛉᛗᚺᛠᛘᛎᛉᛔᛐ")] != null)
          {
            try
            {
              sqlInt32_2 = (SqlInt32) Convert.ToInt32(xmlElement.Attributes[Class2.smethod_0("ᚭᚿᛑᛁᛎᛇᛗᛉᛗᚺᛠᛘᛎᛉᛔᛐ")].Value);
            }
            catch
            {
              sqlInt32_2 = (SqlInt32)(-1);
            }
          }
          if ((SqlBoolean) (xmlElement.Attributes[Class2.smethod_0("ᚳᚿᛋᛕᛆ")] != null) && sqlInt32_2 != (SqlInt32) (-1))
          {
            string ParameterValue = xmlElement.Attributes[Class2.smethod_0("ᚳᚿᛋᛕᛆ")].Value;
            LEADEngine.ParameterShowValue(sqlConnection, sqlInt32_2.Value, ParameterValue);
            if (str1.Length > 0)
              str1 += Class2.smethod_0("ᚉᙾ");
            str1 = str1 + Class2.smethod_0("ᚸᚡᛎᛌᛖᛏᛑᛃ") + sqlInt32_2.ToString() + Class2.smethod_0("ᚺ");
            if (str2.Length > 0)
              str2 += Class2.smethod_0("ᚉᙾ");
            str2 = str2 + Class2.smethod_0("ᚄ") + ParameterValue.Replace(Class2.smethod_0("ᚄ"), "") + Class2.smethod_0("ᚄ");
          }
        }
      }
      string message2 = Class2.smethod_0("ᚦᚬᚲᚥᚳᚶᚃᚭᚳᚺᚶᚈᛄᛗᛚᛢᛌᚳᛥᛕᛟᛦᛦᛓ") + sqlInt32_1.ToString() + Class2.smethod_0("ᚺᙾᚇ") + str1 + Class2.smethod_0("ᚆᙾᚵᚡᚭᚷᚨᚷᚅᚎ") + str2 + Class2.smethod_0("ᚆ");
      SqlContext.Pipe.Send(message2);
      sqlCommand.CommandText = message2;
      sqlCommand.ExecuteNonQuery();
      string str4 = Class2.smethod_0("ᚦᚬᚲᚥᚳᚶᚃᚭᚳᚺᚶᚈᚼᛟᛍᛟᛐᛠᛘᛒᛖᛤᛁᛙ᛬ᛩᚗᚠᚾᛰᛠᛪᛱᛒᛸᛰᛦᛡ᛬ᛨᚱᚦᛌ\x16FEᛮᛸ\x16FFᛓᛢᛗᛓᚹᚱᛨᛔᛠᛪᛛᛪᚸᛁ") + sqlInt32_1.ToString() + Class2.smethod_0("ᚉᙾᚆ") + upper + Class2.smethod_0("ᚄᚇ");
      sqlCommand.CommandText = str4;
      sqlCommand.ExecuteNonQuery();
      sqlConnection.Close();
    }
  }

  [SqlProcedure]
  public static void FieldOfRecordset(SqlString SQL, SqlString FieldName)
  {
    if (LEADEngine.smethod_8(Class2.smethod_0("ᚣᛇᛄᛌᛅᚱᛉᚶᛊᛉᛖᛚᛍᛝᛐᛠ")) % 8 != 0)
      return;
    ArrayList array = LEADEngine.DivideToArray(FieldName.ToString(), Class2.smethod_0("ᚉ"));
    SqlConnection connection = new SqlConnection(LEADEngine.smethod_7(16U, 23, 0U));
    connection.Open();
    SqlMetaData[] sqlMetaDataArray = new SqlMetaData[array.Count];
    SqlDataRecord record = (SqlDataRecord) null;
    SqlDataReader sqlDataReader = new SqlCommand(SQL.ToString(), connection).ExecuteReader();
    bool flag = false;
    while (sqlDataReader.Read())
    {
      if (!flag)
      {
        DataTableReader dataReader = sqlDataReader.GetSchemaTable().CreateDataReader();
        while (dataReader.Read())
        {
          for (int index = 0; index < array.Count; ++index)
          {
            if (array[index].ToString().ToUpper() == dataReader[Class2.smethod_0("ᚠᛍᛋᛕᛎᛐᚱᛅᛒᛋ")].ToString().ToUpper())
              sqlMetaDataArray[index] = LEADEngine.smethod_9(dataReader);
            else if (array[index].ToString() == Class2.smethod_0("ᚫᚳᚫᚬ"))
              sqlMetaDataArray[index] = new SqlMetaData(Class2.smethod_0("ᚠᛍᛋᛕᛎᛐ") + index.ToString(), SqlDbType.VarChar, 200L, 1033L, SqlCompareOptions.None);
          }
        }
        record = new SqlDataRecord(sqlMetaDataArray);
        SqlContext.Pipe.SendResultsStart(record);
        flag = true;
      }
      if (flag)
      {
        for (int ordinal1 = 0; ordinal1 < sqlDataReader.FieldCount; ++ordinal1)
        {
          int ordinal2 = array.IndexOf((object) sqlDataReader.GetName(ordinal1).ToUpper());
          if (ordinal2 >= 0)
          {
            if (sqlDataReader.IsDBNull(ordinal1))
              record.SetDBNull(ordinal2);
            else
              record.SetValue(ordinal2, sqlDataReader.GetValue(ordinal1));
          }
        }
        SqlContext.Pipe.SendResultsRow(record);
      }
    }
    sqlDataReader.Close();
    if (flag)
      SqlContext.Pipe.SendResultsEnd();
    connection.Close();
  }

  [SqlProcedure]
  public static void ConstructMiningCondition(SqlInt32 UserQuery_id, SqlString SQLStatement, out SqlString SQLCondition)
  {
    SQLCondition = SqlString.Null;
    string str = "";
    SqlConnection connection = new SqlConnection(LEADEngine.smethod_7(16U, 23, 0U));
    connection.Open();
    ArrayList arrayList = new ArrayList();
    SqlCommand sqlCommand = new SqlCommand(Class2.smethod_0("ᚰᚣᚫᚥᚤᚶᚃᛆᚓᚵᛉᛒᛎᛍᛟᚺᛎᛛᛔᚐᚷᛄᛂᛁᚕᛋᛪᛝ᛫ᛋᛰᛡᛯᛷᛎᛢ᛫ᛧᛦᛸᛸᚦᛨ\x16FBᚩ᛫ᚫᜃᛶᜂᛷᚰᚹᜀᜂᜀᜄ\x16F9ᜂᛁᚹᛤᛪᛥ᛫ᚾᛱᜅᜑᜑ\x1715\x1718ᜊ\x1718ᛶᜊᜓᜏᜎᜠᜠᛎᜐᜣᛑ᜔ᛓᜫ\x171Eᜪ\x171Fᛘᛡᜨᜪᜨᜬᜡᜪᛩᛡᜑᜑᛤᜦᛴ\x1719ᜭ\x1739\x1739\x173Dᝀᜲᝀ\x171Eᜲ\x173B\x1737᜶ᝈ᜴\x173F\x173Bᛸ\x1716\x16FA\x173Dᜊᝑᝇᝃᜀᜫᜱᜬᜲᜅ\x1738ᝌ\x1758\x1758\x175C\x175Fᝑ\x175Fᜲ\x1758\x175D\x1756ᝠᝦ\x175Dᝤᝤᝪ\x1718\x175A\x176D\x171B\x175F\x171D\x1775ᝨ\x1774ᝩᜢᜫᝲ\x1774ᝲ\x1776ᝫ\x1774ᜳᜫ\x175B\x175Bᜮ\x1771\x173Eᝣ\x1777ឃឃជដ\x177Cដ\x175Dឃឈខឋទឈតតខឌឈᝅᝣᝇឋ\x1757ឞបថᝍច\x1777\x1775ឃ\x1777ᝓផᝣឋឪឝឫឋឰឡឯិឞឩឥᝢកᝤ") + UserQuery_id.ToString(), connection);
    SqlDataReader sqlDataReader1 = sqlCommand.ExecuteReader();
    while (sqlDataReader1.Read())
      arrayList.Add((object) sqlDataReader1.GetString(0));
    sqlDataReader1.Close();
    sqlCommand.CommandText = SQLStatement.ToString();
    SqlDataReader sqlDataReader2 = sqlCommand.ExecuteReader();
    while (sqlDataReader2.Read())
    {
      for (int index1 = 0; index1 < arrayList.Count; ++index1)
      {
        string index2 = arrayList[index1].ToString();
        if (str.Length > 0)
          str += Class2.smethod_0("ᙽ\x169Fᚭᚤᚁ");
        str = str + Class2.smethod_0("ᛘ") + index2 + Class2.smethod_0("ᛚᙾ᚜ ᚈ") + sqlDataReader2[index2].ToString() + Class2.smethod_0("ᚄ");
      }
    }
    sqlDataReader2.Close();
    SQLCondition = (SqlString) str;
    connection.Close();
  }

  [SqlProcedure]
  public static void RowAsRecordset(SqlString SQLStatement)
  {
    if (LEADEngine.smethod_8(Class2.smethod_0("ᚯᛍᛖᚡᛔᚴᛈᛇᛔᛘᛋᛛᛎᛞ")) % 7 != 0)
      return;
    SqlDataRecord record = new SqlDataRecord(new SqlMetaData[2]
    {
      new SqlMetaData(Class2.smethod_0("ᚣᛇᛄᛌᛅᚰᛄᛑᛊ"), SqlDbType.NVarChar, 200L, 1033L, SqlCompareOptions.None),
      new SqlMetaData(Class2.smethod_0("ᚣᛇᛄᛌᛅᚸᛄᛐᛚᛋ"), SqlDbType.NVarChar, 2000L, 1033L, SqlCompareOptions.None)
    });
    SqlContext.Pipe.SendResultsStart(record);
    using (SqlConnection connection = new SqlConnection(LEADEngine.smethod_7(16U, 23, 0U)))
    {
      connection.Open();
      using (SqlCommand sqlCommand = new SqlCommand(SQLStatement.ToString(), connection))
      {
        using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
        {
          if (sqlDataReader.Read())
          {
            for (int ordinal = 0; ordinal < sqlDataReader.FieldCount; ++ordinal)
            {
              record.SetString(0, sqlDataReader.GetName(ordinal));
              if (sqlDataReader.IsDBNull(ordinal))
                record.SetDBNull(1);
              else
                record.SetString(1, sqlDataReader[ordinal].ToString());
              SqlContext.Pipe.SendResultsRow(record);
            }
          }
        }
      }
    }
    SqlContext.Pipe.SendResultsEnd();
  }

  [SqlProcedure]
  public static void ReportingServicesRequest(SqlString URL, SqlString ReportName, SqlString PrinterName, SqlString ReportParams, SqlInt32 PageLimit, bool IsLandscape, out SqlBoolean IsFinished, SqlInt32 Copies)
  {
    IsFinished = (SqlBoolean) true;
    if (LEADEngine.smethod_8(Class2.smethod_0("ᚯᛃᛏᛏᛓᛖᛌᛒᛌᚹᛌᛚᛟᛓᛎᛑᛠᛀᛔᛡᛦᛗᛦᛨ")) % 10 == 4)
    {
      string str = !IsLandscape ? Class2.smethod_0("ᚍ") : Class2.smethod_0("ᚎ");
      string requestUriString = URL.ToString() + Class2.smethod_0("᚜ᚰᛄᛐᛐᛔᛗᚲᛆᛓᛌᚥ") + ReportName.ToString() + Class2.smethod_0("ᚃᚮᛑᛉᛏᛖᛈᛖᚳᛇᛔᛍᚦ") + PrinterName.ToString() + Class2.smethod_0("ᚃᚰᛄᛐᛐᛔᛗᚴᛆᛘᛈᛕᛜᚧ") + ReportParams.ToString() + Class2.smethod_0("ᚃᚧᛒᚬᛂᛐᛇᛗᛈᛇᛗᛍᚦ") + str + Class2.smethod_0("ᚃᚡᛎᛐᛊᛇᛖᚡ") + Copies.ToString();
      if (PageLimit != (SqlInt32)(-1))
        requestUriString = requestUriString + Class2.smethod_0("ᚃᚮᛀᛇᛆᚮᛌᛑᛎᛚᚤ") + PageLimit.ToString();
      try
      {
        WebRequest webRequest = WebRequest.Create(requestUriString);
        webRequest.Credentials = CredentialCache.DefaultCredentials;
        if (!(LEADEngine.smethod_7(5U, 8, 0U) == Class2.smethod_0("ᚶᚭᚴᚡᚳᚧᚶᚳ")))
          return;
        webRequest.GetResponse().Close();
      }
      catch (Exception ex)
      {
        SqlContext.Pipe.Send(ex.Message);
        IsFinished = (SqlBoolean) false;
      }
    }
    else
      IsFinished = (SqlBoolean) false;
  }

  [SqlProcedure]
  public static void GetUserQueryAdvanced(SqlInt32 UserQuery_id, SqlInt32 TopRows, SqlInt32 ReporterFilter_id, out SqlInt32 TotalRows, out SqlString SQLStatement, SqlString DirectWhere, SqlBoolean IsIgnoreGrouping)
  {
    TotalRows = SqlInt32.Null;
    SQLStatement = SqlString.Null;
    if (LEADEngine.smethod_8(Class2.smethod_0("ᚤᛃᛓᚵᛔᛇᛕᚵᛚᛋᛙᛡᚪᛎᛡᛍᛛᛑᛔᛔ")) % 9 != 2)
      return;
    SqlConnection sqlConnection = new SqlConnection(LEADEngine.smethod_7(16U, 23, 0U));
    sqlConnection.Open();
    SQLStatement = !(LEADEngine.smethod_7(9U, 8, 0U) == Class2.smethod_0("ᚡᚭᚭᚴᚭᚱᚲᚯ")) ? (SqlString) "" : (SqlString) new DataPathConstructor()
    {
      ThisConnection = sqlConnection,
      LimitRows = (int) TopRows
    }.ConstructSQLFromQuery((int) UserQuery_id, (int) ReporterFilter_id, DirectWhere.ToString(), IsIgnoreGrouping);
    sqlConnection.Close();
    TotalRows = (SqlInt32) LEADEngine.CountRowsForSQL_Inner(SQLStatement.ToString());
  }

  [SqlFunction]
  public static SqlString ReplicateValue(SqlString Value, SqlString Prefix, SqlInt32 Length)
  {
    string str1 = "";
    string str2 = Prefix.ToString();
    string str3 = !Value.IsNull ? Value.ToString() : "";
    int length1 = str3.Length;
    int length2 = (int) Length;
    if (length1 > length2)
      str1 = str3.Substring(0, length2);
    else if (length1 == length2)
      str1 = str3;
    else if (length1 < length2)
    {
      string str4 = "";
      for (int index = 0; index < length2 - length1; ++index)
        str4 += str2;
      str1 = str4 + str3;
    }
    return (SqlString) str1;
  }

  [SqlFunction]
  public static SqlBoolean CompareFirstWords(SqlString FirstString, SqlString SecondString)
  {
    bool flag;
    if (LEADEngine.smethod_8(Class2.smethod_0("ᚠᛍᛌᛐᛂᛔᛈᚪᛎᛘᛚᛜᛀᛙᛝᛐᛠ")) % 8 == 1)
    {
      string str1 = FirstString.ToString();
      if (str1.IndexOf(Class2.smethod_0("ᙽ")) > -1)
        str1 = str1.Substring(0, str1.IndexOf(Class2.smethod_0("ᙽ")));
      string str2 = SecondString.ToString();
      if (str2.IndexOf(Class2.smethod_0("ᙽ")) > -1)
        str2 = str2.Substring(0, str2.IndexOf(Class2.smethod_0("ᙽ")));
      flag = str1 == str2;
    }
    else
      flag = false;
    return new SqlBoolean(flag);
  }

  public static byte[] Encrypt(byte[] clearData, byte[] Key, byte[] IV)
  {
    MemoryStream memoryStream = new MemoryStream();
    Rijndael rijndael = Rijndael.Create();
    rijndael.Key = Key;
    rijndael.IV = IV;
    CryptoStream cryptoStream = new CryptoStream((Stream) memoryStream, rijndael.CreateEncryptor(), CryptoStreamMode.Write);
    cryptoStream.Write(clearData, 0, clearData.Length);
    cryptoStream.Close();
    return memoryStream.ToArray();
  }

  public static string Encrypt(string clearText, string Password)
  {
    byte[] bytes = Encoding.Unicode.GetBytes(clearText);
    PasswordDeriveBytes passwordDeriveBytes = new PasswordDeriveBytes(Password, new byte[13]
    {
      (byte) 73,
      (byte) 118,
      (byte) 97,
      (byte) 110,
      (byte) 32,
      (byte) 77,
      (byte) 101,
      (byte) 100,
      (byte) 118,
      (byte) 101,
      (byte) 100,
      (byte) 101,
      (byte) 118
    });
    return Convert.ToBase64String(LEADEngine.Encrypt(bytes, passwordDeriveBytes.GetBytes(32), passwordDeriveBytes.GetBytes(16)));
  }

  public static byte[] Encrypt(byte[] clearData, string Password)
  {
    PasswordDeriveBytes passwordDeriveBytes = new PasswordDeriveBytes(Password, new byte[13]
    {
      (byte) 73,
      (byte) 118,
      (byte) 97,
      (byte) 110,
      (byte) 32,
      (byte) 77,
      (byte) 101,
      (byte) 100,
      (byte) 118,
      (byte) 101,
      (byte) 100,
      (byte) 101,
      (byte) 118
    });
    return LEADEngine.Encrypt(clearData, passwordDeriveBytes.GetBytes(32), passwordDeriveBytes.GetBytes(16));
  }

  public static byte[] Decrypt(byte[] cipherData, byte[] Key, byte[] IV)
  {
    MemoryStream memoryStream = new MemoryStream();
    Rijndael rijndael = Rijndael.Create();
    rijndael.Key = Key;
    rijndael.IV = IV;
    CryptoStream cryptoStream = new CryptoStream((Stream) memoryStream, rijndael.CreateDecryptor(), CryptoStreamMode.Write);
    cryptoStream.Write(cipherData, 0, cipherData.Length);
    cryptoStream.Close();
    return memoryStream.ToArray();
  }

  public static string Decrypt(string cipherText, string Password)
  {
    byte[] cipherData = Convert.FromBase64String(cipherText);
    PasswordDeriveBytes passwordDeriveBytes = new PasswordDeriveBytes(Password, new byte[13]
    {
      (byte) 73,
      (byte) 118,
      (byte) 97,
      (byte) 110,
      (byte) 32,
      (byte) 77,
      (byte) 101,
      (byte) 100,
      (byte) 118,
      (byte) 101,
      (byte) 100,
      (byte) 101,
      (byte) 118
    });
    return Encoding.Unicode.GetString(LEADEngine.Decrypt(cipherData, passwordDeriveBytes.GetBytes(32), passwordDeriveBytes.GetBytes(16)));
  }

  public static byte[] Decrypt(byte[] cipherData, string Password)
  {
    PasswordDeriveBytes passwordDeriveBytes = new PasswordDeriveBytes(Password, new byte[13]
    {
      (byte) 73,
      (byte) 118,
      (byte) 97,
      (byte) 110,
      (byte) 32,
      (byte) 77,
      (byte) 101,
      (byte) 100,
      (byte) 118,
      (byte) 101,
      (byte) 100,
      (byte) 101,
      (byte) 118
    });
    return LEADEngine.Decrypt(cipherData, passwordDeriveBytes.GetBytes(32), passwordDeriveBytes.GetBytes(16));
  }

  private struct Struct2
  {
    public SqlString sqlString_0;
    public SqlInt32 sqlInt32_0;
    public SqlString sqlString_1;
    public SqlString sqlString_2;
    public SqlInt32 sqlInt32_1;
    public bool bool_0;
    public bool bool_1;
  }

  private struct Struct3
  {
    public int int_0;
    public int int_1;
    public int int_2;
    public string string_0;
    public string string_1;
    public string string_2;
  }

  private struct Struct4
  {
    public string string_0;
    public int int_0;
  }

  private struct Struct5
  {
    public string string_0;
    public bool bool_0;
  }
}
