﻿// Decompiled with JetBrains decompiler
// Type: ns1.Class1
// Assembly: LEADEngine, Version=3.5.4571.20772, Culture=neutral, PublicKeyToken=null
// MVID: 827379C1-423B-4109-A89B-ED084C02FF99
// Assembly location: C:\2\leadengine-cleaned.dll

using System.Collections;
using System.Xml;

namespace ns1
{
  internal class Class1
  {
    public XmlDocument xmlDocument_0;

    public void method_0(string XML)
    {
      this.xmlDocument_0 = new XmlDocument();
      this.xmlDocument_0.LoadXml(XML);
    }

    public ArrayList method_1()
    {
      ArrayList arrayList = new ArrayList();
      string empty = string.Empty;
      foreach (XmlElement xmlElement in (XmlNode) this.xmlDocument_0[Class2.smethod_0("ᚮᛓᛄᛒᛚ")])
      {
        if (xmlElement.Name == Class2.smethod_0("ᚬᛀᛉᛅᛄᛖ") && xmlElement.Attributes[Class2.smethod_0("ᚫᚿᛌᛅ")] != null)
        {
          string str = xmlElement.Attributes[Class2.smethod_0("ᚫᚿᛌᛅ")].Value;
          arrayList.Add((object) str);
        }
      }
      return arrayList;
    }
  }
}
