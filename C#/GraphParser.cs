﻿using System.Collections;

public class GraphParser
{
  public ArrayList ResultGraph;

  public GraphParser()
  {
    this.ResultGraph = new ArrayList();
  }

  public void AddToUniqueArray(ArrayList Array, object Value)
  {
    if (Array.IndexOf(Value) != -1)
      return;
    Array.Add(Value);
  }

  public ArrayList CalculateRoute(ArrayList GraphsCollection, ArrayList ObligatoryPoints)
  {
    this.ResultGraph.Clear();
    for (int index = 0; index < ObligatoryPoints.Count; ++index)
      this.ResultGraph.Add((object) this.Scout(ObligatoryPoints, GraphsCollection, (int) ObligatoryPoints[0], 0));
    int index1 = -1;
    int num = -1;
    for (int index2 = 0; index2 < this.ResultGraph.Count; ++index2)
    {
      GraphParser.ScoutData scoutData = (GraphParser.ScoutData) this.ResultGraph[index2];
      if (index1 == -1)
      {
        index1 = index2;
        num = scoutData.ScoutWay.Count;
      }
      else if (scoutData.ScoutWay.Count < num)
      {
        index1 = index2;
        num = scoutData.ScoutWay.Count;
      }
    }
    return index1 == -1 ? new ArrayList() : ((GraphParser.ScoutData) this.ResultGraph[index1]).ScoutWay;
  }

  private ArrayList method_0(ArrayList FromArray)
  {
    ArrayList arrayList = new ArrayList();
    for (int index = 0; index < FromArray.Count; ++index)
      arrayList.Add(FromArray[index]);
    return arrayList;
  }

  public GraphParser.ScoutData Scout(ArrayList SourceCheckPoints, ArrayList SourceRoutes, int CurrentPoint_id, int ScoutDepth)
  {
    ArrayList SourceCheckPoints1 = this.method_0(SourceCheckPoints);
    ArrayList arrayList1 = this.method_0(SourceRoutes);
    ArrayList SourceRoutes1 = this.method_0(SourceRoutes);
    ArrayList arrayList2 = new ArrayList();
    ArrayList arrayList3 = new ArrayList();
    ArrayList arrayList4 = new ArrayList();
    if (SourceCheckPoints1.IndexOf((object) CurrentPoint_id) >= 0)
    {
      SourceCheckPoints1.Remove((object) CurrentPoint_id);
      arrayList3.Add((object) CurrentPoint_id);
    }
    GraphParser.ScoutData scoutData1;
    for (int index1 = 0; index1 < arrayList1.Count; ++index1)
    {
      GraphParser.GraphLine graphLine = (GraphParser.GraphLine) arrayList1[index1];
      int CurrentPoint_id1 = graphLine.StartPoint_id != CurrentPoint_id ? (graphLine.EndPoint_id != CurrentPoint_id ? -1 : graphLine.StartPoint_id) : graphLine.EndPoint_id;
      if (CurrentPoint_id1 != -1)
      {
        SourceRoutes1.Remove((object) graphLine);
        scoutData1 = this.Scout(SourceCheckPoints1, SourceRoutes1, CurrentPoint_id1, ScoutDepth + 1);
        int count = scoutData1.ScoutPoints.Count;
        if (count > 0)
        {
          scoutData1.ScoutWay.Add((object) graphLine);
          arrayList4.Add((object) scoutData1);
          for (int index2 = 0; index2 < count; ++index2)
            SourceCheckPoints1.Remove(scoutData1.ScoutPoints[index2]);
        }
      }
    }
    for (int index1 = 0; index1 < arrayList4.Count; ++index1)
    {
      scoutData1 = (GraphParser.ScoutData) arrayList4[index1];
      for (int index2 = 0; index2 < scoutData1.ScoutWay.Count; ++index2)
        arrayList2.Add(scoutData1.ScoutWay[index2]);
      for (int index2 = 0; index2 < scoutData1.ScoutPoints.Count; ++index2)
      {
        arrayList3.Add(scoutData1.ScoutPoints[index2]);
        SourceCheckPoints1.Remove(scoutData1.ScoutPoints[index2]);
      }
    }
    GraphParser.ScoutData scoutData2 = new GraphParser.ScoutData();
    if ((ScoutDepth != 0 || SourceCheckPoints1.Count != 0 ? (ScoutDepth == 0 ? 1 : 0) : 0) == 0)
    {
      scoutData2.ScoutWay = arrayList2;
      scoutData2.ScoutPoints = arrayList3;
    }
    else
    {
      scoutData2.ScoutWay = new ArrayList();
      scoutData2.ScoutPoints = new ArrayList();
    }
    return scoutData2;
  }

  public struct GraphLine
  {
    public int Graph_id;
    public int StartPoint_id;
    public int EndPoint_id;
    public int LineWeight;
  }

  public struct ScoutData
  {
    public ArrayList ScoutWay;
    public ArrayList ScoutPoints;
  }
}
