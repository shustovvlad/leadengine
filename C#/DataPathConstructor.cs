﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Xml;

public class DataPathConstructor
{
  public int LimitRows = 0;
  public SqlConnection ThisConnection;

  public void CreateQueryObject(SqlString TableGUID, SqlString ObjectName, SqlString SortType, SqlInt32 SortPriority, SqlInt32 ReporterLayer_id)
  {
    SqlCommand sqlCommand = new SqlCommand();
    sqlCommand.Connection = this.ThisConnection;
    sqlCommand.CommandText = "SELECT cnt = count(*) FROM sysobjects with (nolock) WHERE [name] = 'temp_ObjectQuery_" + TableGUID.ToString() + "'";
    SqlDataReader sqlDataReader1 = sqlCommand.ExecuteReader();
    int num1 = 0;
    while (sqlDataReader1.Read())
      num1 = sqlDataReader1.GetInt32(0);
    sqlDataReader1.Close();
    if (num1 == 0)
    {
      sqlCommand.CommandText = "CREATE TABLE [temp_ObjectQuery_" + TableGUID.ToString() + "] (tid int not null identity(0,1), ReporterObject_id int not null, SortType varchar(10) null, Priority int null)";
      sqlCommand.ExecuteNonQuery();
    }
    sqlCommand.CommandText = "SELECT TOP 1 tid FROM ReporterObjects with (nolock) WHERE ObjectName = '" + ObjectName.ToString() + "' AND ReporterLayer_id = " + ReporterLayer_id.ToString();
    SqlInt32 sqlInt32 = (SqlInt32) (-1);
    SqlDataReader sqlDataReader2 = sqlCommand.ExecuteReader();
    while (sqlDataReader2.Read())
      sqlInt32 = (SqlInt32) sqlDataReader2.GetInt32(0);
    sqlDataReader2.Close();
    if (sqlInt32 != (SqlInt32) (-1))
    {
      SortType = !SortType.IsNull ? (SqlString) ("'" + SortType.ToString().Replace("'", "") + "'") : (SqlString)"NULL";
      string str = !SortPriority.IsNull ? SortPriority.ToString() : "NULL";
      sqlCommand.CommandText = "SELECT cnt = count(*) FROM [temp_ObjectQuery_" + TableGUID.ToString() + "] WHERE ReporterObject_id = " + sqlInt32.ToString();
      SqlDataReader sqlDataReader3 = sqlCommand.ExecuteReader();
      int num2 = 0;
      while (sqlDataReader3.Read())
        num2 = sqlDataReader3.GetInt32(0);
      sqlDataReader3.Close();
      if (num2 > 0)
        sqlCommand.CommandText = "UPDATE [temp_ObjectQuery_" + 
                                TableGUID.ToString() +
                                "] SET SortType = "
                                + SortType.ToString() +
                                ", Priority = " + 
                                str + 
                                " WHERE ReporterObject_id = " 
                                + sqlInt32.ToString();
      else if (num2 == 0)
        sqlCommand.CommandText =    "INSERT INTO [temp_ObjectQuery_" + 
                                    TableGUID.ToString() +
                                    "] (ReporterObject_id, SortType, Priority) VALUES (" + 
                                    sqlInt32.ToString() +
                                    ", " + 
                                    SortType.ToString() +
                                    ", " + 
                                    str +
                                    ")";
      sqlCommand.ExecuteReader();
    }
  }

  public ArrayList CopyArrayList(ArrayList SourceList)
  {
    ArrayList arrayList = new ArrayList();
    for (int index = 0; index < SourceList.Count; ++index)
      arrayList.Add(SourceList[index]);
    return arrayList;
  }

  public string ConstructSQLFromData(string ObjectsTable, string Condition, string GroupType, string DirectWhere, out string ErrorMessage, out bool IsNormal, bool IsIgnoreGrouping, int ReporterLayer_id)
  {
    ErrorMessage = "";
    IsNormal = true;
    ArrayList ArgArray = new ArrayList();
    ArrayList OrderArray = new ArrayList();
    SqlCommand sqlCommand = new SqlCommand();
    sqlCommand.Connection = this.ThisConnection;
    sqlCommand.CommandText = "SELECT ReporterObject_id, SortType, Priority FROM [" + 
            ObjectsTable +
            "] with (nolock)";
    SqlDataReader sqlDataReader;
    string str;
    try
    {
      sqlDataReader = sqlCommand.ExecuteReader();
    }
    catch (Exception ex)
    {
      IsNormal = false;
      ErrorMessage = ex.Message;
      str = "";
      goto label_8;
    }
    while (sqlDataReader.Read())
    {
      if ((sqlDataReader.IsDBNull(1) ? 1 : (sqlDataReader.IsDBNull(2) ? 1 : 0)) == 0)
        OrderArray.Add((object) new DataPathConstructor.OrderRow()
        {
          Object_id = sqlDataReader.GetInt32(0),
          Method = sqlDataReader.GetString(1),
          Queue = sqlDataReader.GetInt32(2)
        });
      ArgArray.Add((object) sqlDataReader.GetInt32(0));
    }
    sqlDataReader.Close();
    str = this.GetDataCube(ArgArray, Condition, this.LimitRows, OrderArray, GroupType, DirectWhere, IsIgnoreGrouping, (ArrayList) null, ReporterLayer_id, -1, (ArrayList) null);
label_8:
    return str;
  }

  public string ConstructSQLFromXML(SqlXml ObjectXML, SqlInt32 ReporterFilter_id, string DirectWhere, SqlBoolean IsIgnoreGrouping, SqlInt32 ReporterLayer_id)
  {
    XmlDocument xmlDocument = new XmlDocument();
    xmlDocument.LoadXml(ObjectXML.Value);
    ArrayList ArgArray = new ArrayList();
    ArrayList OrderArray = new ArrayList();
    ArrayList ConcatenateArray = new ArrayList();
    int num1 = -1;
    int num2 = -1;
    string ArgCondition = "";
    if (xmlDocument["Query"].Attributes["FilterCondition"] != null)
      ArgCondition = xmlDocument["Query"].Attributes["FilterCondition"].Value;
    foreach (XmlElement xmlElement in (XmlNode) xmlDocument["Query"])
    {
      if (xmlElement.Name == "Object")
      {
        num1 = -1;
        if (xmlElement.Attributes["Id"] != null)
        {
          int int32 = Convert.ToInt32(xmlElement.Attributes["Id"].Value);
          if ((xmlElement.Attributes["SortMethod"] == null ? 1 : (xmlElement.Attributes["SortQueue"] == null ? 1 : 0)) == 0)
          {
            DataPathConstructor.OrderRow orderRow = new DataPathConstructor.OrderRow();
            orderRow.Object_id = int32;
            if (xmlElement.Attributes["SortMethod"] != null)
              orderRow.Method = xmlElement.Attributes["SortMethod"].Value;
            if (xmlElement.Attributes["SortQueue"] != null)
              orderRow.Queue = Convert.ToInt32(xmlElement.Attributes["SortQueue"].Value);
            OrderArray.Add((object) orderRow);
          }
          ArgArray.Add((object) int32);
        }
      }
      else if (xmlElement.Name == "ConcatenationRule")
      {
        num2 = -1;
        if (xmlElement.Attributes["Object_id"] != null)
        {
          int int32 = Convert.ToInt32(xmlElement.Attributes["Object_id"].Value);
          ConcatenateArray.Add((object) new DataPathConstructor.ConcatenateStruct()
          {
            ReporterObject_id = int32,
            Prefix = (xmlElement.Attributes["Prefix"] == null ? "0" : xmlElement.Attributes["Prefix"].Value),
            CodeLength = (xmlElement.Attributes["Length"] == null ? 20 : 
            Convert.ToInt32(xmlElement.Attributes["Length"].Value))
          });
          ArgArray.Add((object) int32);
        }
      }
    }
    return ConcatenateArray.Count <= 0 ? this.GetDataCube(ArgArray, ArgCondition, this.LimitRows, OrderArray, "", DirectWhere, false, (ArrayList) null, (int) ReporterLayer_id, -1, (ArrayList) null) : this.GetDataCube(ArgArray, ArgCondition, this.LimitRows, OrderArray, "", DirectWhere, false, ConcatenateArray, (int) ReporterLayer_id, -1, (ArrayList) null);
  }

  public string ConstructSQLFromQuery(int UserQuery_id, int ReporterFilter_id, string DirectWhere, SqlBoolean IsIgnoreGrouping)
  {
    ArrayList ArgArray = new ArrayList();
    ArrayList OrderArray = new ArrayList();
    SqlCommand sqlCommand = new SqlCommand();
    sqlCommand.Connection = this.ThisConnection;
    sqlCommand.CommandText = "SELECT ReporterLayer_id FROM UserQuery with (nolock) WHERE tid = " + UserQuery_id.ToString();
    int ReporterLayer_id = -1;
    SqlDataReader sqlDataReader1 = sqlCommand.ExecuteReader();
    while (sqlDataReader1.Read())
      ReporterLayer_id = sqlDataReader1.GetInt32(0);
    sqlDataReader1.Close();
    sqlCommand.CommandText = "SELECT a.ReporterObject_id, b.SortType, c.Priority FROM UserQueryObjects as a with (nolock) LEFT JOIN SortTypes as b with (nolock) ON a.SortType_id = b.tid LEFT JOIN PriorityTypes as c with (nolock) ON a.PriorityType_id = c.tid WHERE a.UserQuery_id = " + 
            UserQuery_id.ToString();
    SqlDataReader sqlDataReader2 = sqlCommand.ExecuteReader();
    while (sqlDataReader2.Read())
    {
      if ((sqlDataReader2.IsDBNull(1) ? 1 : (sqlDataReader2.IsDBNull(2) ? 1 : 0)) == 0)
        OrderArray.Add((object) new DataPathConstructor.OrderRow()
        {
          Object_id = sqlDataReader2.GetInt32(0),
          Method = sqlDataReader2.GetString(1),
          Queue = sqlDataReader2.GetInt32(2)
        });
      ArgArray.Add((object) sqlDataReader2.GetInt32(0));
    }
    sqlDataReader2.Close();
    string str = "";
    if (ReporterFilter_id != -1)
    {
      sqlCommand.CommandText = "SELECT Condition FROM ReporterFilters with (nolock) WHERE tid = " + 
                ReporterFilter_id.ToString() +
          " AND UserQuery_id = " + UserQuery_id.ToString();
      SqlDataReader sqlDataReader3 = sqlCommand.ExecuteReader();
      while (sqlDataReader3.Read())
      {
        if (!sqlDataReader3.IsDBNull(0))
          str = sqlDataReader3.GetString(0);
      }
      sqlDataReader3.Close();
    }
    sqlCommand.CommandText = "SELECT a.Condition, b.GroupType FROM UserQuery as a with (nolock) LEFT JOIN GroupTypes as b with (nolock) ON a.GroupType_id = b.tid WHERE a.tid =" + 
            UserQuery_id.ToString();
    SqlDataReader sqlDataReader4 = sqlCommand.ExecuteReader();
    string ArgCondition = "";
    string GroupType = "";
    while (sqlDataReader4.Read())
    {
      ArgCondition = str.Length != 0 ? str : (!sqlDataReader4.IsDBNull(0) ? sqlDataReader4.GetString(0) : "");
      GroupType = !sqlDataReader4.IsDBNull(1) ? sqlDataReader4.GetString(1) : "";
    }
    sqlDataReader4.Close();
    return this.GetDataCube(ArgArray, ArgCondition, this.LimitRows, OrderArray, GroupType, DirectWhere, false, (ArrayList) null, ReporterLayer_id, UserQuery_id, (ArrayList) null);
  }

  public ArrayList LinkTables(ArrayList TableList, int ReporterLayer_id)
  {
    ArrayList arrayList1 = new ArrayList();
    GraphParser graphParser = new GraphParser();
    SqlCommand sqlCommand = new SqlCommand();
    sqlCommand.Connection = this.ThisConnection;
    sqlCommand.CommandType = CommandType.Text;
    sqlCommand.CommandText =
            "SELECT tid, SourceTable_id, TargetTable_id FROM ReporterTableJoins with (nolock) WHERE ReporterLayer_id = " + 
            ReporterLayer_id.ToString();
    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
    ArrayList GraphsCollection = new ArrayList();
    GraphParser.GraphLine graphLine;
    while (sqlDataReader.Read())
    {
      graphLine = new GraphParser.GraphLine();
      graphLine.Graph_id = sqlDataReader.GetInt32(0);
      graphLine.StartPoint_id = sqlDataReader.GetInt32(1);
      graphLine.EndPoint_id = sqlDataReader.GetInt32(2);
      GraphsCollection.Add((object) graphLine);
    }
    sqlDataReader.Close();
    ArrayList route = graphParser.CalculateRoute(GraphsCollection, TableList);
    ArrayList arrayList2 = new ArrayList();
    for (int index = 0; index < route.Count; ++index)
    {
      graphLine = (GraphParser.GraphLine) route[index];
      if (arrayList2.IndexOf((object) graphLine.Graph_id) == -1)
        arrayList2.Add((object) graphLine.Graph_id);
    }
    return arrayList2;
  }

  public string GetDataCube(ArrayList ArgArray, string ArgCondition, int LimitRows, ArrayList OrderArray, string GroupType, string DirectWhereClause, bool IsIgnoreGrouping, ArrayList ConcatenateArray, int ReporterLayer_id, int UserQuery_id, ArrayList NameChangeList)
  {
    SqlCommand sqlCommand = new SqlCommand();
    sqlCommand.Connection = this.ThisConnection;
    int count1 = ArgArray.Count;
    string str1;
    if (count1 == 0)
    {
      str1 = "";
    }
    else
    {
      string str2 = "";
      if (ArgCondition.Length > 0)
      {
        bool flag = false;
        for (int startIndex = 0; startIndex < ArgCondition.Length; ++startIndex)
        {
          if (ArgCondition.Substring(startIndex, 1) == "}")
          {
            flag = false;
            if (str2.Length > 0)
            {
              int num = -1;
              sqlCommand.CommandText =
                "SELECT TOP 1 tid FROM ReporterObjects with (nolock) WHERE ObjectName = '" + 
                str2 +
                "' AND ReporterLayer_id = " + 
                            ReporterLayer_id.ToString();
              SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
              while (sqlDataReader.Read())
                num = sqlDataReader.GetInt32(0);
              sqlDataReader.Close();
              if ((num == -1 ? 1 : (ArgArray.IndexOf((object) num) >= 0 ? 1 : 0)) == 0)
                ArgArray.Add((object) num);
            }
          }
          if (flag)
            str2 += ArgCondition.Substring(startIndex, 1);
          if (ArgCondition.Substring(startIndex, 1) == "{")
          {
            flag = true;
            str2 = "";
          }
        }
      }
      ArrayList arrayList1 = new ArrayList();
      ArrayList arrayList2 = new ArrayList();
      if (UserQuery_id != -1)
      {
        sqlCommand.CommandText =
                    " SELECT ReporterObject_id, ColumnTitle, IsHidden = isnull(IsHidden, 0) FROM UserQueryObjects with (nolock) WHERE UserQuery_id =  "
                    + UserQuery_id.ToString();
        SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
        while (sqlDataReader.Read())
        {
          if (!sqlDataReader.IsDBNull(1))
            arrayList1.Add((object) new DataPathConstructor.Struct1()
            {
              int_0 = sqlDataReader.GetInt32(0),
              string_0 = sqlDataReader.GetString(1)
            });
          if (!sqlDataReader.IsDBNull(2) && sqlDataReader.GetInt32(2) == 1)
            arrayList2.Add((object) sqlDataReader.GetInt32(0));
        }
        sqlDataReader.Close();
      }
      for (int index = 0; index < count1; ++index)
      {
        int num1 = -1;
        int num2 = -1;
        int num3 = -1;
        int num4 = -1;
        int num5 = (int) ArgArray[index];
        sqlCommand.CommandText = "SELECT ReporterDetail_id, ReporterClass_id FROM ReporterObjects with (nolock) WHERE tid = " + 
                    num5.ToString() +
            " AND ReporterLayer_id = " + ReporterLayer_id.ToString();
        SqlDataReader sqlDataReader1 = sqlCommand.ExecuteReader();
        while (sqlDataReader1.Read())
        {
          num1 = !sqlDataReader1.IsDBNull(0) ? sqlDataReader1.GetInt32(0) : -1;
          num2 = !sqlDataReader1.IsDBNull(1) ? sqlDataReader1.GetInt32(1) : -1;
        }
        sqlDataReader1.Close();
        if (num1 > -1)
        {
          sqlCommand.CommandText = "SELECT ReporterDimension_id FROM ReporterDetails with (nolock) WHERE tid = " + 
                        num1.ToString();
          SqlDataReader sqlDataReader2 = sqlCommand.ExecuteReader();
          while (sqlDataReader2.Read())
            num3 = !sqlDataReader2.IsDBNull(0) ? sqlDataReader2.GetInt32(0) : -1;
          sqlDataReader2.Close();
          sqlCommand.CommandText = "SELECT TOP 1 tid FROM ReporterObjects with (nolock) WHERE ReporterDimension_id = " + 
              num3.ToString() +
              " AND ReporterClass_id = " + 
              num2.ToString() +
              " AND ReporterLayer_id = " + ReporterLayer_id.ToString();
          SqlDataReader sqlDataReader3 = sqlCommand.ExecuteReader();
          while (sqlDataReader3.Read())
            num4 = !sqlDataReader3.IsDBNull(0) ? sqlDataReader3.GetInt32(0) : -1;
          sqlDataReader3.Close();
          if (ArgArray.IndexOf((object) num4) == -1)
            ArgArray.Add((object) num4);
        }
      }
      int count2 = ArgArray.Count;
      ArrayList ThisArray1 = new ArrayList();
      ArrayList ThisArray2 = new ArrayList();
      ArrayList TableList = new ArrayList();
      ArrayList arrayList3 = new ArrayList();
      ArrayList arrayList4 = new ArrayList();
      ArrayList arrayList5 = new ArrayList();
      string str3 = "";
      IComparer comparer = (IComparer) new DataPathConstructor.OrderRowComparer();
      OrderArray.Sort(comparer);
      DataPathConstructor.OrderRow orderRow = new DataPathConstructor.OrderRow();
      for (int index = 0; index < OrderArray.Count; ++index)
      {
        DataPathConstructor.OrderRow order = (DataPathConstructor.OrderRow) OrderArray[index];
        if (str3.Length > 0)
          str3 += ", ";
        str3 = str3 + "{{" + order.Object_id.ToString() + "}} " + 
                    order.Method;
      }
      ArrayList arrayList6 = new ArrayList();
      ArrayList ThisArray3 = new ArrayList();
      string str4 = "";
      bool flag1 = false;
      for (int index1 = 0; index1 < count2; ++index1)
      {
        int num1 = -1;
        int num2 = -1;
        int num3 = -1;
        string str5 = "";
        string newValue = "";
        string str6 = "";
        int num4 = 0;
        int num5 = (int) ArgArray[index1];
        sqlCommand.CommandText = "SELECT ReporterDimension_id, ReporterMeasure_id, ReporterDetail_id, ObjectName, IsCheckLinks = isnull(IsCheckLinks, 0) FROM ReporterObjects with (nolock) WHERE tid = " + 
            num5.ToString() +
            " AND ReporterLayer_id = " + ReporterLayer_id.ToString();
        SqlDataReader sqlDataReader1 = sqlCommand.ExecuteReader();
        while (sqlDataReader1.Read())
        {
          num1 = !sqlDataReader1.IsDBNull(0) ? sqlDataReader1.GetInt32(0) : -1;
          num2 = !sqlDataReader1.IsDBNull(1) ? sqlDataReader1.GetInt32(1) : -1;
          num3 = !sqlDataReader1.IsDBNull(2) ? sqlDataReader1.GetInt32(2) : -1;
          str5 = !sqlDataReader1.IsDBNull(3) ? sqlDataReader1.GetString(3) : "";
          num4 = !sqlDataReader1.IsDBNull(4) ? sqlDataReader1.GetInt32(4) : 0;
        }
        sqlDataReader1.Close();
        DataPathConstructor.Struct1 struct1_1;
        if ((num4 != 1 ? 1 : (flag1 ? 1 : 0)) == 0)
        {
          sqlCommand.CommandText = "SELECT DISTINCT Table_id = COALESCE(c.tid, e.tid, h.tid), TableName = COALESCE(c.TableName, e.TableName, h.TableName), TableAlias = COALESCE(c.TableAlias, e.TableAlias, h.TableAlias) FROM ReporterObjects as a with (nolock) LEFT JOIN ReporterDimensions as b with (nolock) on a.ReporterDimension_id = b.tid LEFT JOIN ReporterTables as c with (nolock) on b.ReporterTable_id = c.tid LEFT JOIN ReporterMeasures as d with (nolock) on a.ReporterMeasure_id = d.tid LEFT JOIN ReporterTables as e with (nolock) on d.ReporterTable_id = e.tid LEFT JOIN ReporterDetails as f with (nolock) on a.ReporterDetail_id = f.tid LEFT JOIN ReporterDimensions as g with (nolock) on f.ReporterDimension_id = g.tid LEFT JOIN ReporterTables as h with (nolock) on g.ReporterTable_id = h.tid WHERE a.ReporterLayer_id = " + 
                        ReporterLayer_id.ToString();
          SqlDataReader sqlDataReader2 = sqlCommand.ExecuteReader();
          while (sqlDataReader2.Read())
          {
            struct1_1 = new DataPathConstructor.Struct1();
            struct1_1.int_0 = sqlDataReader2.GetInt32(0);
            struct1_1.string_0 = sqlDataReader2.GetString(1);
            struct1_1.string_1 = sqlDataReader2.GetString(2);
            arrayList6.Add((object) struct1_1);
          }
          sqlDataReader2.Close();
        }
        DataPathConstructor.Struct1 struct1_2;
        if (num1 > -1)
        {
          int Table_id = -1;
          string str7 = "";
          string str8 = "";
          sqlCommand.CommandText = "SELECT ReporterTable_id, AssociatedColumn, SelectStatement, WhereStatement FROM ReporterDimensions with (nolock) WHERE tid = " + 
                        num1.ToString();
          SqlDataReader sqlDataReader2 = sqlCommand.ExecuteReader();
          while (sqlDataReader2.Read())
          {
            Table_id = !sqlDataReader2.IsDBNull(0) ? sqlDataReader2.GetInt32(0) : -1;
            str6 = !sqlDataReader2.IsDBNull(1) ? sqlDataReader2.GetString(1) : "";
            str7 = !sqlDataReader2.IsDBNull(2) ? sqlDataReader2.GetString(2) : "";
            str8 = !sqlDataReader2.IsDBNull(3) ? sqlDataReader2.GetString(3) : "";
          }
          sqlDataReader2.Close();
          DataPathConstructor.TableInfo tableInfo = this.GetTableInfo(Table_id);
          string tableName = tableInfo.TableName;
          string tableAlias = tableInfo.TableAlias;
          if (TableList.IndexOf((object) Table_id) == -1)
          {
            TableList.Add((object) Table_id);
            arrayList3.Add((object) tableName);
            arrayList4.Add((object) tableAlias);
          }
          if (num4 == 1)
          {
            for (int index2 = 0; index2 < arrayList6.Count; ++index2)
            {
              struct1_1 = (DataPathConstructor.Struct1) arrayList6[index2];
              int int0 = struct1_1.int_0;
              string string0 = struct1_1.string_0;
              string string1 = struct1_1.string_1;
              if ((str7.ToUpper().IndexOf(string1.ToUpper() + ".") == -1 || string.IsNullOrEmpty(str7) ? (str8.ToUpper().IndexOf(string1.ToUpper() + 
                  ".") == -1 ? 1 : (string.IsNullOrEmpty(str8) ? 1 : 0)) : 0) == 0 && TableList.IndexOf((object) int0) == -1)
              {
                TableList.Add((object) int0);
                arrayList3.Add((object) string0);
                arrayList4.Add((object) string1);
              }
            }
          }
          newValue = !(str7 == str6) ? str7 : tableAlias + 
                        "." + str7;
          bool flag2 = false;
          if (arrayList2.IndexOf((object) num5) == -1)
          {
            if (arrayList1.Count > 0)
            {
              for (int index2 = 0; index2 < arrayList1.Count; ++index2)
              {
                struct1_2 = (DataPathConstructor.Struct1) arrayList1[index2];
                if (struct1_2.int_0 == num5)
                {
                  ThisArray3.Add((object) ("[" + struct1_2.string_0 + 
                      "] = " + 
                      newValue));
                  flag2 = true;
                  break;
                }
              }
            }
            if (!flag2)
              ThisArray3.Add((object) ("[" + str5 + "] = " + newValue));
          }
          ArgCondition = ArgCondition.ToString().Replace("{" + str5 + "}", newValue);
          str3 = str3.ToString().Replace("{{" + num5.ToString() + "}}", newValue);
          ThisArray1.Add((object) (tableAlias + "." + str6));
          if (str8.Trim().Length > 0)
            ThisArray2.Add((object) str8);
        }
        else if (num2 > -1)
        {
          int Table_id = -1;
          int num6 = -1;
          string str7 = "";
          string str8 = "";
          int num7 = 0;
          string str9 = "";
          sqlCommand.CommandText =
                        "SELECT ReporterTable_id, ReporterAggregate_id, AssociatedColumn, SelectStatement, WhereStatement FROM ReporterMeasures with (nolock) WHERE tid = " + num2.ToString();
          SqlDataReader sqlDataReader2 = sqlCommand.ExecuteReader();
          while (sqlDataReader2.Read())
          {
            Table_id = !sqlDataReader2.IsDBNull(0) ? sqlDataReader2.GetInt32(0) : -1;
            num6 = !sqlDataReader2.IsDBNull(1) ? sqlDataReader2.GetInt32(1) : -1;
            str6 = !sqlDataReader2.IsDBNull(2) ? sqlDataReader2.GetString(2) : "";
            str7 = !sqlDataReader2.IsDBNull(3) ? sqlDataReader2.GetString(3) : "";
            str8 = !sqlDataReader2.IsDBNull(4) ? sqlDataReader2.GetString(4) : "";
          }
          sqlDataReader2.Close();
          sqlCommand.CommandText = "SELECT UseAggregate, AggregateFunction FROM ReporterAggregates with (nolock) WHERE tid = " + num6.ToString();
          SqlDataReader sqlDataReader3 = sqlCommand.ExecuteReader();
          while (sqlDataReader3.Read())
          {
            num7 = !sqlDataReader3.IsDBNull(0) ? sqlDataReader3.GetInt32(0) : 0;
            str9 = !sqlDataReader3.IsDBNull(1) ? sqlDataReader3.GetString(1) : "";
          }
          sqlDataReader3.Close();
          DataPathConstructor.TableInfo tableInfo = this.GetTableInfo(Table_id);
          string tableName = tableInfo.TableName;
          string tableAlias = tableInfo.TableAlias;
          if (TableList.IndexOf((object) Table_id) == -1)
          {
            TableList.Add((object) Table_id);
            arrayList3.Add((object) tableName);
            arrayList4.Add((object) tableAlias);
          }
          if (num4 == 1)
          {
            for (int index2 = 0; index2 < arrayList6.Count; ++index2)
            {
              struct1_1 = (DataPathConstructor.Struct1) arrayList6[index2];
              int int0 = struct1_1.int_0;
              string string0 = struct1_1.string_0;
              string string1 = struct1_1.string_1;
              if ((str7.ToUpper().IndexOf(string1.ToUpper() + ".") == -1 || string.IsNullOrEmpty(str7) ? (str8.ToUpper().IndexOf(string1.ToUpper() + ".") == -1 ? 1 : (string.IsNullOrEmpty(str8) ? 1 : 0)) : 0) == 0 && TableList.IndexOf((object) int0) == -1)
              {
                TableList.Add((object) int0);
                arrayList3.Add((object) string0);
                arrayList4.Add((object) string1);
              }
            }
          }
          if (IsIgnoreGrouping)
            num7 = 0;
          if (num7 == 1)
          {
            if (str7 == str6)
              newValue = str9 + "(" + tableAlias + "." + str7 + ")";
            else
              newValue = str9 + "(" + str7 + ")";
          }
          else
          {
            newValue = !(str7 == str6) ? str7 : tableAlias + "." + str7;
            ThisArray1.Add((object) (tableAlias + "." + str6));
          }
          bool flag2 = false;
          if (arrayList2.IndexOf((object) num5) == -1)
          {
            if (arrayList1.Count > 0)
            {
              for (int index2 = 0; index2 < arrayList1.Count; ++index2)
              {
                struct1_2 = (DataPathConstructor.Struct1) arrayList1[index2];
                if (struct1_2.int_0 == num5)
                {
                  ThisArray3.Add((object) ("[" + struct1_2.string_0 + "] = " + newValue));
                  flag2 = true;
                  break;
                }
              }
            }
            if (!flag2)
              ThisArray3.Add((object) ("[" + str5 + "] = " + newValue));
          }
          ArgCondition = ArgCondition.ToString().Replace("{" + str5 + "}", newValue);
          str3 = str3.ToString().Replace("{{" + num5.ToString() + "}}", newValue);
          if (str8.Trim().Length > 0)
            ThisArray2.Add((object) str8);
        }
        else if (num3 > -1)
        {
          int Table_id = -1;
          string str7 = "";
          string str8 = "";
          sqlCommand.CommandText =
                        "SELECT b.ReporterTable_id, a.AssociatedColumn, a.SelectStatement, a.WhereStatement FROM ReporterDetails as a with (nolock) JOIN ReporterDimensions as b with (nolock) ON a.ReporterDimension_id = b.tid WHERE a.tid = " + num3.ToString();
          SqlDataReader sqlDataReader2 = sqlCommand.ExecuteReader();
          while (sqlDataReader2.Read())
          {
            Table_id = !sqlDataReader2.IsDBNull(0) ? sqlDataReader2.GetInt32(0) : -1;
            str6 = !sqlDataReader2.IsDBNull(1) ? sqlDataReader2.GetString(1) : "";
            str7 = !sqlDataReader2.IsDBNull(2) ? sqlDataReader2.GetString(2) : "";
            str8 = !sqlDataReader2.IsDBNull(3) ? sqlDataReader2.GetString(3) : "";
          }
          sqlDataReader2.Close();
          DataPathConstructor.TableInfo tableInfo = this.GetTableInfo(Table_id);
          string tableName = tableInfo.TableName;
          string tableAlias = tableInfo.TableAlias;
          if (TableList.IndexOf((object) Table_id) == -1)
          {
            TableList.Add((object) Table_id);
            arrayList3.Add((object) tableName);
            arrayList4.Add((object) tableAlias);
          }
          if (num4 == 1)
          {
            for (int index2 = 0; index2 < arrayList6.Count; ++index2)
            {
              struct1_1 = (DataPathConstructor.Struct1) arrayList6[index2];
              int int0 = struct1_1.int_0;
              string string0 = struct1_1.string_0;
              string string1 = struct1_1.string_1;
              if ((str7.ToUpper().IndexOf(string1.ToUpper() + ".") == -1 || string.IsNullOrEmpty(str7) ? (str8.ToUpper().IndexOf(string1.ToUpper() + ".") == -1 ? 1 : (string.IsNullOrEmpty(str8) ? 1 : 0)) : 0) == 0 && TableList.IndexOf((object) int0) == -1)
              {
                TableList.Add((object) int0);
                arrayList3.Add((object) string0);
                arrayList4.Add((object) string1);
              }
            }
          }
          newValue = !(str7 == str6) ? str7 : tableAlias + "." + str7;
          bool flag2 = false;
          if (arrayList2.IndexOf((object) num5) == -1)
          {
            if (arrayList1.Count > 0)
            {
              for (int index2 = 0; index2 < arrayList1.Count; ++index2)
              {
                struct1_2 = (DataPathConstructor.Struct1) arrayList1[index2];
                if (struct1_2.int_0 == num5)
                {
                  ThisArray3.Add((object) ("[" + struct1_2.string_0 + "] = " + newValue));
                  flag2 = true;
                  break;
                }
              }
            }
            if (!flag2)
              ThisArray3.Add((object) ("[" + str5 + "] = " + newValue));
          }
          ArgCondition = ArgCondition.ToString().Replace("{" + str5 + "}", newValue);
          str3 = str3.ToString().Replace("{{" + num5.ToString() + "}}", newValue);
          ThisArray1.Add((object) (tableAlias + "." + str6));
          if (str8.Trim().Length > 0)
            ThisArray2.Add((object) str8);
        }
        if (ConcatenateArray != null)
        {
          for (int index2 = 0; index2 < ConcatenateArray.Count; ++index2)
          {
            DataPathConstructor.ConcatenateStruct concatenate = (DataPathConstructor.ConcatenateStruct) ConcatenateArray[index2];
            if (concatenate.ReporterObject_id == num5)
            {
              if (str4.Length > 0)
                str4 += " + ";
              str4 = str4 + "dbo.ReplicateValue(cast(" + 
                                newValue +
                                " as varchar(200)), " + 
                                concatenate.Prefix + ", " + 
                                concatenate.CodeLength.ToString() + ")";
            }
          }
        }
      }
      string str10 = "";
      ArrayList arrayList7 = new ArrayList();
      ArrayList arrayList8 = new ArrayList();
      ArrayList ThisArray4 = new ArrayList();
      if (TableList.Count > 1)
      {
        ArrayList arrayList9 = this.LinkTables(TableList, ReporterLayer_id);
        while (arrayList9.Count > 0)
        {
          arrayList8.Clear();
          for (int index1 = 0; index1 < arrayList9.Count; ++index1)
          {
            string str5 = "";
            string str6 = "";
            string str7 = "";
            string str8 = "";
            string str9 = "";
            string str11 = "";
            SqlInt32 sqlInt32_1 = (SqlInt32) (-1);
            SqlInt32 sqlInt32_2 = (SqlInt32) (-1);
            bool flag2 = false;
            string list = this.ArrayToList(ThisArray4, ", ", "", "");
            string str12;
            if (list.Length > 0)
              str12 = " AND (a.SourceTable_id IN (" + 
                                list + ") OR a.TargetTable_id IN ("
                                + list + "))";
            else
              str12 = "";
            sqlCommand.CommandText =
                            "SELECT c.TableName as STN, c.TableAlias as STA, d.TableName as DTN, d.TableAlias as DTA, a.ConditionStatement, b.JoinFunction, a.SourceTable_id, a.TargetTable_id FROM ReporterTableJoins as a with (nolock) JOIN ReporterJoins as b with (nolock) ON a.ReporterJoin_id = b.tid JOIN ReporterTables as c with (nolock) ON a.SourceTable_id = c.tid JOIN ReporterTables as d with (nolock) ON a.TargetTable_id = d.tid WHERE a.tid = "
                            + arrayList9[index1].ToString() +
                            " AND a.ReporterLayer_id = " + 
                            ReporterLayer_id.ToString() + " " + str12;
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
            while (sqlDataReader.Read())
            {
              flag2 = true;
              str5 = !sqlDataReader.IsDBNull(0) ? sqlDataReader.GetString(0) : "";
              str6 = !sqlDataReader.IsDBNull(1) ? sqlDataReader.GetString(1) : "";
              str7 = !sqlDataReader.IsDBNull(2) ? sqlDataReader.GetString(2) : "";
              str8 = !sqlDataReader.IsDBNull(3) ? sqlDataReader.GetString(3) : "";
              str9 = !sqlDataReader.IsDBNull(4) ? sqlDataReader.GetString(4) : "";
              str11 = !sqlDataReader.IsDBNull(5) ? sqlDataReader.GetString(5) : "";
              sqlInt32_1 = (SqlInt32) sqlDataReader.GetInt32(6);
              sqlInt32_2 = (SqlInt32) sqlDataReader.GetInt32(7);
            }
            sqlDataReader.Close();
            if (flag2)
            {
              if (str10.Length == 0)
              {
                arrayList7.Add((object) str5);
                ThisArray4.Add((object) sqlInt32_1);
                str10 = str10 + str5 +
                                    " as " + 
                                    str6 +
                                    " with (nolock) ";
              }
              string str13;
              string str14;
              if (arrayList7.IndexOf((object) str5) != -1)
              {
                str13 = str7;
                str14 = str8;
                ThisArray4.Add((object) sqlInt32_2);
              }
              else
              {
                str13 = str5;
                str14 = str6;
                ThisArray4.Add((object) sqlInt32_1);
              }
              if (NameChangeList != null)
              {
                for (int index2 = 0; index2 < NameChangeList.Count; ++index2)
                {
                  DataPathConstructor.TableInfo nameChange = (DataPathConstructor.TableInfo) NameChangeList[index2];
                  if (nameChange.TableName == str13)
                  {
                    str13 = nameChange.TableAlias;
                    break;
                  }
                }
              }
              str10 = str10 + " " + str11 + " " + str13 + " as " + 
                                str14 +
                                " with (nolock) ON (" + str9 + ")";
              arrayList7.Add((object) str13);
              arrayList8.Add(arrayList9[index1]);
            }
          }
          for (int index = 0; index < arrayList8.Count; ++index)
            arrayList9.Remove(arrayList8[index]);
        }
      }
      else if (TableList.Count == 1)
      {
        string tableAlias = arrayList3[0].ToString();
        string str5 = arrayList4[0].ToString();
        if (NameChangeList != null)
        {
          for (int index = 0; index < NameChangeList.Count; ++index)
          {
            DataPathConstructor.TableInfo nameChange = (DataPathConstructor.TableInfo) NameChangeList[index];
            if (nameChange.TableName == tableAlias)
            {
              tableAlias = nameChange.TableAlias;
              break;
            }
          }
        }
        str10 = tableAlias + " as " + str5 + " with (nolock) ";
      }
      string list1 = this.ArrayToList(ThisArray1, ", ", "", "");
      string str15;
      if ((list1.Length <= 0 ? 1 : (IsIgnoreGrouping ? 1 : 0)) == 0)
      {
        str15 = " GROUP BY " + list1;
        if (GroupType.Length > 0)
          str15 = str15 + " WITH " + GroupType;
      }
      else
        str15 = "";
      string str16 = this.ArrayToList(ThisArray3, ", ", "", "");
      if (str4.Length > 0)
      {
        if (str16.Length > 0)
          str16 += ", ";
        str16 = str16 + "[SYS_CONCATCODE] = " + str4;
      }
      string str17 = this.ArrayToList(ThisArray2, " AND ", "(", ")");
      if (str17.Length > 0)
        str17 = " WHERE " + str17;
      if (DirectWhereClause.Length > 0)
        str17 = str17.Length <= 0 ?" WHERE " + DirectWhereClause : str17 + " AND " + DirectWhereClause;
      string str18 = "";
      if (!IsIgnoreGrouping)
      {
        if (ArgCondition.Length > 0)
          str18 = " HAVING " + ArgCondition;
      }
      else if (ArgCondition.Length > 0)
        str17 = (str17.Length <= 0 ?" WHERE " : str17 + " AND ") + 
                    ArgCondition;
      if (str3.Length > 0)
        str3 = " ORDER BY " + str3;
      string str19 = "";
      if (str10.Length > 0)
        str19 = "SELECT " + 
                    (LimitRows == 0 ? "" : " TOP (" + 
                    LimitRows.ToString() + ") ") + " " + 
                    str16 +
                    " FROM " + str10 + " " + str17 + " " + str15 + " " + 
                    str18 + " " + str3;
      str1 = str19;
    }
    return str1;
  }

  public ArrayList UniqueArray(ArrayList SourceArray)
  {
    ArrayList arrayList = new ArrayList();
    for (int index = 0; index < SourceArray.Count; ++index)
    {
      if (arrayList.IndexOf(SourceArray[index]) == -1)
        arrayList.Add(SourceArray[index]);
    }
    return arrayList;
  }

  public string ArrayToList(ArrayList ThisArray, string Divider, string StartFrom, string EndWith)
  {
    string str = "";
    for (int index = 0; index < ThisArray.Count; ++index)
    {
      if (str.Length > 0)
        str += Divider;
      str = str + StartFrom + ThisArray[index].ToString() + EndWith;
    }
    return str;
  }

  public DataPathConstructor.TableInfo GetTableInfo(int Table_id)
  {
    DataPathConstructor.TableInfo tableInfo = new DataPathConstructor.TableInfo();
    SqlDataReader sqlDataReader = new SqlCommand(
        "SELECT TableName, TableAlias FROM ReporterTables with (nolock) WHERE tid = " + 
        Table_id.ToString(), 
        this.ThisConnection).ExecuteReader();
    while (sqlDataReader.Read())
    {
      tableInfo.TableName = !sqlDataReader.IsDBNull(0) ? sqlDataReader.GetString(0) : "";
      tableInfo.TableAlias = !sqlDataReader.IsDBNull(1) ? sqlDataReader.GetString(1) : "";
    }
    sqlDataReader.Close();
    return tableInfo;
  }

  private struct Struct1
  {
    public int int_0;
    public string string_0;
    public string string_1;
  }

  public struct ConcatenateStruct
  {
    public string Prefix;
    public int ReporterObject_id;
    public int CodeLength;
  }

  public struct DataTables
  {
    public string TableIdent;
    public string TableName;
    public string TableAlias;
  }

  public struct JoinProcedureInfo
  {
    public int SourceTable_id;
    public int TargetTable_id;
    public bool IsSourceProcedure;
    public bool IsTargetProcedure;
  }

  public struct JoinRow
  {
    public int Join_id;
    public int Table_id;
    public int TableType;
  }

  public struct Joins
  {
    public int Join_id;
    public int SourceTable_id;
    public int TargetTable_id;
  }

  public struct OrderRow
  {
    public int Queue;
    public int Object_id;
    public string Method;
  }

  public class OrderRowComparer : IComparer
  {
    int IComparer.Compare(object x, object y)
    {
      return new CaseInsensitiveComparer().Compare((object) ((DataPathConstructor.OrderRow) y).Queue, (object) ((DataPathConstructor.OrderRow) x).Queue);
    }
  }

  public struct ProcedureParameter
  {
    public string QueryObjectName;
    public string ParameterName;
  }

  public struct TableDependRating
  {
    public int ReporterTable_id;
    public int Counter;
  }

  public struct TableInfo
  {
    public string TableName;
    public string TableAlias;
  }

  public struct TablePathStruct
  {
    public ArrayList JoinsList;
    public bool IsFound;
  }
}
