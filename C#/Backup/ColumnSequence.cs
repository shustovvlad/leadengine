﻿// Decompiled with JetBrains decompiler
// Type: ColumnSequence
// Assembly: LEADEngine, Version=3.5.4571.20772, Culture=neutral, PublicKeyToken=null
// MVID: 827379C1-423B-4109-A89B-ED084C02FF99
// Assembly location: C:\2\leadengine-cleaned.dll

using Microsoft.SqlServer.Server;
using System;
using System.Data.SqlTypes;
using System.IO;
using System.Text;

[SqlUserDefinedAggregate(Format.UserDefined, IsInvariantToDuplicates = false, IsInvariantToNulls = true, IsInvariantToOrder = false, MaxByteSize = 8000)]
[Serializable]
public class ColumnSequence : IBinarySerialize
{
  private StringBuilder stringBuilder_0;

  public void Init()
  {
    this.stringBuilder_0 = new StringBuilder();
  }

  public void Accumulate(SqlString value)
  {
    if (value.IsNull || this.stringBuilder_0.ToString().Length > 3000)
      return;
    this.stringBuilder_0.Append(Class2.smethod_0("ᚸ") + value.Value + Class2.smethod_0("ᚺᚊ"));
  }

  public void Merge(ColumnSequence other)
  {
    if ((this.stringBuilder_0.ToString() + other.stringBuilder_0.ToString()).Length > 3000)
      return;
    this.stringBuilder_0.Append((object) other.stringBuilder_0);
  }

  public SqlString Terminate()
  {
    string empty = string.Empty;
    if ((this.stringBuilder_0 == null ? 1 : (this.stringBuilder_0.Length <= 0 ? 1 : 0)) == 0)
      empty = this.stringBuilder_0.ToString(0, this.stringBuilder_0.Length - 1);
    return new SqlString(empty);
  }

  public void Read(BinaryReader r)
  {
    this.stringBuilder_0 = new StringBuilder(r.ReadString());
  }

  public void Write(BinaryWriter w)
  {
    string str = this.stringBuilder_0.ToString();
    if (str.Length > 7000)
      str = str.Substring(0, 7000);
    w.Write(str);
  }
}
