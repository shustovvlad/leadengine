﻿// Decompiled with JetBrains decompiler
// Type: DataPathConstructor
// Assembly: LEADEngine, Version=3.5.4571.20772, Culture=neutral, PublicKeyToken=null
// MVID: 827379C1-423B-4109-A89B-ED084C02FF99
// Assembly location: C:\2\leadengine-cleaned.dll

using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Xml;

public class DataPathConstructor
{
  public int LimitRows = 0;
  public SqlConnection ThisConnection;

  public void CreateQueryObject(SqlString TableGUID, SqlString ObjectName, SqlString SortType, SqlInt32 SortPriority, SqlInt32 ReporterLayer_id)
  {
    SqlCommand sqlCommand = new SqlCommand();
    sqlCommand.Connection = this.ThisConnection;
    sqlCommand.CommandText = Class2.smethod_0("ᚰᚣᚫᚥᚤᚶᚃᛇᛓᛚᚇᚥᚉᛍᛚᛡᛛᛢᚗᚚᚚᚒᚹᛆᛄᛃᚗ᛫ᛲ᛭ᛪᛞᛧᛣᛢᛴᛴᚢ\x16FA᛭\x16F9ᛮᚧᚰᛷ\x16F9ᛷ\x16FBᛰ\x16F9ᚸᚰᛨᛚᛘᛦᛚᚶᛲᜆ\x16FAᜇᜀ\x16F9ᚽᛛᚿᛇ\x1715ᜇᜐ᜔ᜄᛵᜉᜒᜎ\x170D\x171F\x16FDᜢᜓᜡᜩᜐ") + TableGUID.ToString() + Class2.smethod_0("ᚄ");
    SqlDataReader sqlDataReader1 = sqlCommand.ExecuteReader();
    int num1 = 0;
    while (sqlDataReader1.Read())
      num1 = sqlDataReader1.GetInt32(0);
    sqlDataReader1.Close();
    if (num1 == 0)
    {
      sqlCommand.CommandText = Class2.smethod_0("ᚠᚰᚤᚡᚵᚧᚃᚸᚦᚨᚳᚭᚉᛅᛟᛑᛚᛞᛎᚿᛓᛜᛘᛗᛩᛇ᛬ᛝ᛫ᛳᛚ") + TableGUID.ToString() + Class2.smethod_0("ᚺᙾᚇᛔᛊᛆᚃᛍᛓᛚᚇᛖᛘᛞᚋᛚᛢᛚᛛᚐᛚᛖᛘᛢᛩᛟ᛫ᛱᚡᚪᚧᚭᚦᚪ\x169Fᛒᛦᛲᛲᛶ\x16F9᛫\x16F9ᛗ᛫ᛴᛰᛯᜁ᛭ᛸᛴᚱ\x16FBᜁᜈᚵᜄᜆᜌᚹᜈᜐᜈᜉᛊᚿᛳᜐ᜔\x1717ᛸ\x171E\x1716ᜌᛈ\x171Fᜋ\x171Dᜏ\x1715ᜏᜡᛘᛢᛢᛜᛔᜣᜫᜣᜤᛥᛚᜋᜮᜦᜭᜱᜩ᜵\x173Bᛣᜭᜳ\x173Aᛧ᜶\x173E᜶\x1737ᛵ");
      sqlCommand.ExecuteNonQuery();
    }
    sqlCommand.CommandText = Class2.smethod_0("ᚰᚣᚫᚥᚤᚶᚃᚸᚴᚶᚇᚙᚉᛞᛔᛐᚍᚴᛁᚿᚾᚒᛅᛙᛥᛥᛩ᛬ᛞ᛬ᛊᛞᛧᛣᛢᛴᛴᚢ\x16FA᛭\x16F9ᛮᚧᚰᛷ\x16F9ᛷ\x16FBᛰ\x16F9ᚸᚰᛨᛚᛘᛦᛚᚶᛦ\x16FAᜃ\x16FF\x16FEᜐ᛫\x16FFᜌᜅᛁᛟᛃᛋ") + ObjectName.ToString() + Class2.smethod_0("ᚄᙾᚠᚮᚥᚂᚵᛉᛕᛕᛙᛜᛎᛜᚷᛍᛦᛓᛡᛏᛚᛖᚓᚱᚕ") + ReporterLayer_id.ToString();
    SqlInt32 sqlInt32 = (SqlInt32) -1;
    SqlDataReader sqlDataReader2 = sqlCommand.ExecuteReader();
    while (sqlDataReader2.Read())
      sqlInt32 = (SqlInt32) sqlDataReader2.GetInt32(0);
    sqlDataReader2.Close();
    if (sqlInt32 != (SqlInt32) -1)
    {
      SortType = !SortType.IsNull ? (SqlString) (Class2.smethod_0("ᚄ") + SortType.ToString().Replace(Class2.smethod_0("ᚄ"), "") + Class2.smethod_0("ᚄ")) : (SqlString) Class2.smethod_0("ᚫᚳᚫᚬ");
      string str = !SortPriority.IsNull ? SortPriority.ToString() : Class2.smethod_0("ᚫᚳᚫᚬ");
      sqlCommand.CommandText = Class2.smethod_0("ᚰᚣᚫᚥᚤᚶᚃᛇᛓᛚᚇᚥᚉᛍᛚᛡᛛᛢᚗᚚᚚᚒᚹᛆᛄᛃᚗᛓ᛭ᛟᛨ᛬ᛜᛍᛡᛪᛦᛥᛷᛕ\x16FA᛫\x16F9ᜁᛨ") + TableGUID.ToString() + Class2.smethod_0("ᚺᙾᚶᚨᚦᚴᚨᚄᚷᛋᛗᛗᛛᛞᛐᛞᚼᛐᛙᛕᛔᛦᛒᛝᛙᚖᚴᚘ") + sqlInt32.ToString();
      SqlDataReader sqlDataReader3 = sqlCommand.ExecuteReader();
      int num2 = 0;
      while (sqlDataReader3.Read())
        num2 = sqlDataReader3.GetInt32(0);
      sqlDataReader3.Close();
      if (num2 > 0)
        sqlCommand.CommandText = Class2.smethod_0("ᚲᚮᚣᚡᚵᚧᚃᚿᛙᛋᛔᛘᛈᚹᛍᛖᛒᛑᛣᛁᛦᛗᛥ᛭ᛔ") + TableGUID.ToString() + Class2.smethod_0("ᚺᙾᚲᚥᚵᚂᚶᛓᛗᛚᚻᛡᛙᛏᚋᚩᚍ") + SortType.ToString() + Class2.smethod_0("ᚉᙾᚯᛒᛊᛑᛕᛍᛙᛟᚇᚥᚉ") + str + Class2.smethod_0("ᙽᚵᚧᚥᚳᚧᚃᚶᛊᛖᛖᛚᛝᛏᛝᚻᛏᛘᛔᛓᛥᛑᛜᛘᚕᚳᚗ") + sqlInt32.ToString();
      else if (num2 == 0)
        sqlCommand.CommandText = Class2.smethod_0("ᚦᚬᚲᚥᚳᚶᚃᚭᚳᚺᚶᚈᛄᛞᛐᛙᛝᛍᚾᛒᛛᛗᛖᛨᛆ᛫ᛜᛪᛲᛙ") + TableGUID.ToString() + Class2.smethod_0("ᚺᙾᚇᚲᛆᛒᛒᛖᛙᛋᛙᚷᛋᛔᛐᛏᛡᛍᛘᛔ\x169Dᚒᛆᛣᛧᛪᛋᛱᛩᛟᚧ᚜ᛍᛰᛨᛯᛳ᛫ᛷ\x16FDᚮᚦᛝᛉᛕᛟᛐᛟᚭᚶ") + sqlInt32.ToString() + Class2.smethod_0("ᚉᙾ") + SortType.ToString() + Class2.smethod_0("ᚉᙾ") + str + Class2.smethod_0("ᚆ");
      sqlCommand.ExecuteReader();
    }
  }

  public ArrayList CopyArrayList(ArrayList SourceList)
  {
    ArrayList arrayList = new ArrayList();
    for (int index = 0; index < SourceList.Count; ++index)
      arrayList.Add(SourceList[index]);
    return arrayList;
  }

  public string ConstructSQLFromData(string ObjectsTable, string Condition, string GroupType, string DirectWhere, out string ErrorMessage, out bool IsNormal, bool IsIgnoreGrouping, int ReporterLayer_id)
  {
    ErrorMessage = "";
    IsNormal = true;
    ArrayList ArgArray = new ArrayList();
    ArrayList OrderArray = new ArrayList();
    SqlCommand sqlCommand = new SqlCommand();
    sqlCommand.Connection = this.ThisConnection;
    sqlCommand.CommandText = Class2.smethod_0("ᚰᚣᚫᚥᚤᚶᚃᚶᛊᛖᛖᛚᛝᛏᛝᚻᛏᛘᛔᛓᛥᛑᛜᛘᚡᚖᛊᛧ᛫ᛮᛏᛵ᛭ᛣᚫᚠᛑᛴ᛬ᛳᛷᛯ\x16FBᜁᚩᛐᛝᛛᛚᚮᛪ") + ObjectsTable + Class2.smethod_0("ᚺᙾᛖᛉᛕᛊᚃᚌᛓᛕᛓᛗᛌᛕᚔ");
    SqlDataReader sqlDataReader;
    string str;
    try
    {
      sqlDataReader = sqlCommand.ExecuteReader();
    }
    catch (Exception ex)
    {
      IsNormal = false;
      ErrorMessage = ex.Message;
      str = "";
      goto label_8;
    }
    while (sqlDataReader.Read())
    {
      if ((sqlDataReader.IsDBNull(1) ? 1 : (sqlDataReader.IsDBNull(2) ? 1 : 0)) == 0)
        OrderArray.Add((object) new DataPathConstructor.OrderRow()
        {
          Object_id = sqlDataReader.GetInt32(0),
          Method = sqlDataReader.GetString(1),
          Queue = sqlDataReader.GetInt32(2)
        });
      ArgArray.Add((object) sqlDataReader.GetInt32(0));
    }
    sqlDataReader.Close();
    str = this.GetDataCube(ArgArray, Condition, this.LimitRows, OrderArray, GroupType, DirectWhere, IsIgnoreGrouping, (ArrayList) null, ReporterLayer_id, -1, (ArrayList) null);
label_8:
    return str;
  }

  public string ConstructSQLFromXML(SqlXml ObjectXML, SqlInt32 ReporterFilter_id, string DirectWhere, SqlBoolean IsIgnoreGrouping, SqlInt32 ReporterLayer_id)
  {
    XmlDocument xmlDocument = new XmlDocument();
    xmlDocument.LoadXml(ObjectXML.Value);
    ArrayList ArgArray = new ArrayList();
    ArrayList OrderArray = new ArrayList();
    ArrayList ConcatenateArray = new ArrayList();
    int num1 = -1;
    int num2 = -1;
    string ArgCondition = "";
    if (xmlDocument[Class2.smethod_0("ᚮᛓᛄᛒᛚ")].Attributes[Class2.smethod_0("ᚣᛇᛋᛔᛆᛔᚦᛓᛓᛊᛐᛜᛒᛙᛙ")] != null)
      ArgCondition = xmlDocument[Class2.smethod_0("ᚮᛓᛄᛒᛚ")].Attributes[Class2.smethod_0("ᚣᛇᛋᛔᛆᛔᚦᛓᛓᛊᛐᛜᛒᛙᛙ")].Value;
    foreach (XmlElement xmlElement in (XmlNode) xmlDocument[Class2.smethod_0("ᚮᛓᛄᛒᛚ")])
    {
      if (xmlElement.Name == Class2.smethod_0("ᚬᛀᛉᛅᛄᛖ"))
      {
        num1 = -1;
        if (xmlElement.Attributes[Class2.smethod_0("ᚦᛂ")] != null)
        {
          int int32 = Convert.ToInt32(xmlElement.Attributes[Class2.smethod_0("ᚦᛂ")].Value);
          if ((xmlElement.Attributes[Class2.smethod_0("ᚰᛍᛑᛔᚮᛇᛗᛌᛔᛊ")] == null ? 1 : (xmlElement.Attributes[Class2.smethod_0("ᚰᛍᛑᛔᚲᛗᛈᛙᛊ")] == null ? 1 : 0)) == 0)
          {
            DataPathConstructor.OrderRow orderRow = new DataPathConstructor.OrderRow();
            orderRow.Object_id = int32;
            if (xmlElement.Attributes[Class2.smethod_0("ᚰᛍᛑᛔᚮᛇᛗᛌᛔᛊ")] != null)
              orderRow.Method = xmlElement.Attributes[Class2.smethod_0("ᚰᛍᛑᛔᚮᛇᛗᛌᛔᛊ")].Value;
            if (xmlElement.Attributes[Class2.smethod_0("ᚰᛍᛑᛔᚲᛗᛈᛙᛊ")] != null)
              orderRow.Queue = Convert.ToInt32(xmlElement.Attributes[Class2.smethod_0("ᚰᛍᛑᛔᚲᛗᛈᛙᛊ")].Value);
            OrderArray.Add((object) orderRow);
          }
          ArgArray.Add((object) int32);
        }
      }
      else if (xmlElement.Name == Class2.smethod_0("ᚠᛍᛍᛃᛂᛖᛈᛒᛆᛚᛐᛗᛗᚼᛠᛘᛒ"))
      {
        num2 = -1;
        if (xmlElement.Attributes[Class2.smethod_0("ᚬᛀᛉᛅᛄᛖᛂᛍᛉ")] != null)
        {
          int int32 = Convert.ToInt32(xmlElement.Attributes[Class2.smethod_0("ᚬᛀᛉᛅᛄᛖᛂᛍᛉ")].Value);
          ConcatenateArray.Add((object) new DataPathConstructor.ConcatenateStruct()
          {
            ReporterObject_id = int32,
            Prefix = (xmlElement.Attributes[Class2.smethod_0("ᚭᛐᛄᛆᛊᛚ")] == null ? Class2.smethod_0("ᚍ") : xmlElement.Attributes[Class2.smethod_0("ᚭᛐᛄᛆᛊᛚ")].Value),
            CodeLength = (xmlElement.Attributes[Class2.smethod_0("ᚩᛃᛍᛇᛕᛊ")] == null ? 20 : Convert.ToInt32(xmlElement.Attributes[Class2.smethod_0("ᚩᛃᛍᛇᛕᛊ")].Value))
          });
          ArgArray.Add((object) int32);
        }
      }
    }
    return ConcatenateArray.Count <= 0 ? this.GetDataCube(ArgArray, ArgCondition, this.LimitRows, OrderArray, "", DirectWhere, false, (ArrayList) null, (int) ReporterLayer_id, -1, (ArrayList) null) : this.GetDataCube(ArgArray, ArgCondition, this.LimitRows, OrderArray, "", DirectWhere, false, ConcatenateArray, (int) ReporterLayer_id, -1, (ArrayList) null);
  }

  public string ConstructSQLFromQuery(int UserQuery_id, int ReporterFilter_id, string DirectWhere, SqlBoolean IsIgnoreGrouping)
  {
    ArrayList ArgArray = new ArrayList();
    ArrayList OrderArray = new ArrayList();
    SqlCommand sqlCommand = new SqlCommand();
    sqlCommand.Connection = this.ThisConnection;
    sqlCommand.CommandText = Class2.smethod_0("ᚰᚣᚫᚥᚤᚶᚃᚶᛊᛖᛖᛚᛝᛏᛝᚸᛎᛧᛔᛢᛐᛛᛗᚔᚻᛈᛆᛅᚙᛏᛮᛡᛯᛏᛴᛥᛳ\x16FBᚣ\x16FBᛮ\x16FAᛯᚨᚱᛸ\x16FAᛸ\x16FCᛱ\x16FAᚹᚱᛩᛛᛙᛧᛛᚷᜌᜂ\x16FEᚻᛙᚽ") + UserQuery_id.ToString();
    int ReporterLayer_id = -1;
    SqlDataReader sqlDataReader1 = sqlCommand.ExecuteReader();
    while (sqlDataReader1.Read())
      ReporterLayer_id = sqlDataReader1.GetInt32(0);
    sqlDataReader1.Close();
    sqlCommand.CommandText = Class2.smethod_0("ᚰᚣᚫᚥᚤᚶᚃᛅᚓᚸᛌᛘᛘᛜᛟᛑᛟᚽᛑᛚᛖᛕᛧᛓᛞᛚᚣᚘᛛᚨᛎ᛫ᛯᛲᛓ\x16F9ᛱᛧᚯᚤᛨᚴᛗ\x16FAᛲ\x16F9\x16FDᛵᜁᜇᚯᛖᛣᛡᛠᚴᛪᜉ\x16FCᜊᛪᜏᜀᜎ\x1716᛭ᜁᜊᜆᜅ\x1717\x1717ᛅᜇ\x171Aᛈᜊᛊᜢ\x1715ᜡ\x1716ᛏᛘ\x171Fᜡ\x171Fᜣ\x1718ᜡᛠᛘᜅ\x16FFᜁᜐᛝᜈᜎᜉᜏᛢ\x1716ᜳ\x1737\x173A\x171Bᝁ\x1739ᜯ\x173E᛬ᜮᝁᛯᜲᛱᝉ\x173Cᝈ\x173Dᛶ\x16FFᝆᝈᝆᝊ\x173Fᝈᜇ\x16FFᜯᜯᜂᝄᜒ\x1738\x1755\x1759\x175C\x173Dᝣ\x175Bᝑᝌ\x1757ᝓᜐᜮᜒ\x1755ᜢᝩ\x175F\x175B\x1718ᝅ\x173Fᝁᝐ\x171Dᝈᝎᝉᝏᜢᝓ\x1776ᝮ\x1775\x1779\x1771\x177Dឃ\x175Fច\x177Dᝳគᜰᝲចᜳ\x1777᜵ឍកឌខ\x173Aᝃដឌដណឃឌᝋᝃᝳᝳᝆឈ\x1756\x1779វបលសភឣឩចឫឣយបសល\x1758\x1776\x175Aឞᝪឱឧឣᝠមដឈពដᝦឨ\x1776ឞួឰើឞៃ឴ែ៊ឱូី\x1775ន\x1777") + UserQuery_id.ToString();
    SqlDataReader sqlDataReader2 = sqlCommand.ExecuteReader();
    while (sqlDataReader2.Read())
    {
      if ((sqlDataReader2.IsDBNull(1) ? 1 : (sqlDataReader2.IsDBNull(2) ? 1 : 0)) == 0)
        OrderArray.Add((object) new DataPathConstructor.OrderRow()
        {
          Object_id = sqlDataReader2.GetInt32(0),
          Method = sqlDataReader2.GetString(1),
          Queue = sqlDataReader2.GetInt32(2)
        });
      ArgArray.Add((object) sqlDataReader2.GetInt32(0));
    }
    sqlDataReader2.Close();
    string str = "";
    if (ReporterFilter_id != -1)
    {
      sqlCommand.CommandText = Class2.smethod_0("ᚰᚣᚫᚥᚤᚶᚃᚧᛔᛔᛋᛑᛝᛓᛚᛚᚍᚴᛁᚿᚾᚒᛅᛙᛥᛥᛩ᛬ᛞ᛬ᛁᛥᛩᛲᛤᛲᛴᚢ\x16FA᛭\x16F9ᛮᚧᚰᛷ\x16F9ᛷ\x16FBᛰ\x16F9ᚸᚰᛨᛚᛘᛦᛚᚶᜋᜁ\x16FDᚺᛘᚼ") + ReporterFilter_id.ToString() + Class2.smethod_0("ᙽ\x169Fᚭᚤᚁᚷᛖᛉᛗᚷᛜᛍᛛᛣᛊᛕᛑᚎᚬᚐ") + UserQuery_id.ToString();
      SqlDataReader sqlDataReader3 = sqlCommand.ExecuteReader();
      while (sqlDataReader3.Read())
      {
        if (!sqlDataReader3.IsDBNull(0))
          str = sqlDataReader3.GetString(0);
      }
      sqlDataReader3.Close();
    }
    sqlCommand.CommandText = Class2.smethod_0("ᚰᚣᚫᚥᚤᚶᚃᛅᚓᚩᛖᛖᛍᛓᛟᛕᛜᛜ᚛ᚐᛓᚠᚺᛦᛤ᛫ᛧᛌᛲᛪᛠ᚜ᛃᛐᛎᛍᚡᛗᛶᛩᛷᛗ\x16FC᛭\x16FBᜃᚫ᛭ᜀᚮᛰᚰᜈ\x16FBᜇ\x16FCᚵᚾᜅᜇᜅᜉ\x16FEᜇᛆᚾ᛫ᛥᛧᛶᛃᛮᛴᛯᛵᛈᛰ\x171C\x171Aᜡ\x171Dᜂᜨᜠ\x1716ᜥᛓ\x1715ᜨᛖ\x1719ᛘᜰᜣᜯᜤᛝᛦᜭᜯᜭᜱᜦᜯᛮᛦ\x1716\x1716ᛩᜫ\x16F9ᜓ\x173F\x173Dᝄᝀᜥᝋᝃ\x1739᜴\x173F\x173Bᛸ\x1716\x16FA\x173Dᜊᝑᝇᝃᜀ\x1738ᜪᜨ᜶ᜪᜆᝈ\x1716\x175Dᝓᝏᜌᜪᜎ") + UserQuery_id.ToString();
    SqlDataReader sqlDataReader4 = sqlCommand.ExecuteReader();
    string ArgCondition = "";
    string GroupType = "";
    while (sqlDataReader4.Read())
    {
      ArgCondition = str.Length != 0 ? str : (!sqlDataReader4.IsDBNull(0) ? sqlDataReader4.GetString(0) : "");
      GroupType = !sqlDataReader4.IsDBNull(1) ? sqlDataReader4.GetString(1) : "";
    }
    sqlDataReader4.Close();
    return this.GetDataCube(ArgArray, ArgCondition, this.LimitRows, OrderArray, GroupType, DirectWhere, false, (ArrayList) null, ReporterLayer_id, UserQuery_id, (ArrayList) null);
  }

  public ArrayList LinkTables(ArrayList TableList, int ReporterLayer_id)
  {
    ArrayList arrayList1 = new ArrayList();
    GraphParser graphParser = new GraphParser();
    SqlCommand sqlCommand = new SqlCommand();
    sqlCommand.Connection = this.ThisConnection;
    sqlCommand.CommandType = CommandType.Text;
    sqlCommand.CommandText = Class2.smethod_0("ᚰᚣᚫᚥᚤᚶᚃᛘᛎᛊᚓᚈᚼᛙᛠᛞᛐᛓᛃᛑᛓᛞᛘᛓᛞᛚᚣᚘᛍᛛ᛭ᛣᛢᛲᛓᛡᛣᛮᛨᛣᛮᛪᚧᛎᛛᛙᛘᚬᛟᛳ\x16FF\x16FFᜃᜆᛸᜆᛩᛷ\x16F9ᜄ\x16FEᛤᜊᜅᜋᜑᚿ\x1717ᜊ\x1716ᜋᛄᛍ᜔\x1716᜔\x1718\x170D\x1716ᛕᛍᜅᛷᛵᜃᛷᛓᜆ\x171Aᜦᜦᜪᜭ\x171Fᜭᜈ\x171E\x1737ᜤᜲᜠᜫᜧᛤᜂᛦ") + ReporterLayer_id.ToString();
    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
    ArrayList GraphsCollection = new ArrayList();
    GraphParser.GraphLine graphLine;
    while (sqlDataReader.Read())
    {
      graphLine = new GraphParser.GraphLine();
      graphLine.Graph_id = sqlDataReader.GetInt32(0);
      graphLine.StartPoint_id = sqlDataReader.GetInt32(1);
      graphLine.EndPoint_id = sqlDataReader.GetInt32(2);
      GraphsCollection.Add((object) graphLine);
    }
    sqlDataReader.Close();
    ArrayList route = graphParser.CalculateRoute(GraphsCollection, TableList);
    ArrayList arrayList2 = new ArrayList();
    for (int index = 0; index < route.Count; ++index)
    {
      graphLine = (GraphParser.GraphLine) route[index];
      if (arrayList2.IndexOf((object) graphLine.Graph_id) == -1)
        arrayList2.Add((object) graphLine.Graph_id);
    }
    return arrayList2;
  }

  public string GetDataCube(ArrayList ArgArray, string ArgCondition, int LimitRows, ArrayList OrderArray, string GroupType, string DirectWhereClause, bool IsIgnoreGrouping, ArrayList ConcatenateArray, int ReporterLayer_id, int UserQuery_id, ArrayList NameChangeList)
  {
    SqlCommand sqlCommand = new SqlCommand();
    sqlCommand.Connection = this.ThisConnection;
    int count1 = ArgArray.Count;
    string str1;
    if (count1 == 0)
    {
      str1 = "";
    }
    else
    {
      string str2 = "";
      if (ArgCondition.Length > 0)
      {
        bool flag = false;
        for (int startIndex = 0; startIndex < ArgCondition.Length; ++startIndex)
        {
          if (ArgCondition.Substring(startIndex, 1) == Class2.smethod_0("ᛚ"))
          {
            flag = false;
            if (str2.Length > 0)
            {
              int num = -1;
              sqlCommand.CommandText = Class2.smethod_0("ᚰᚣᚫᚥᚤᚶᚃᚸᚴᚶᚇᚙᚉᛞᛔᛐᚍᚴᛁᚿᚾᚒᛅᛙᛥᛥᛩ᛬ᛞ᛬ᛊᛞᛧᛣᛢᛴᛴᚢ\x16FA᛭\x16F9ᛮᚧᚰᛷ\x16F9ᛷ\x16FBᛰ\x16F9ᚸᚰᛨᛚᛘᛦᛚᚶᛦ\x16FAᜃ\x16FF\x16FEᜐ᛫\x16FFᜌᜅᛁᛟᛃᛋ") + str2 + Class2.smethod_0("ᚄᙾᚠᚮᚥᚂᚵᛉᛕᛕᛙᛜᛎᛜᚷᛍᛦᛓᛡᛏᛚᛖᚓᚱᚕ") + ReporterLayer_id.ToString();
              SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
              while (sqlDataReader.Read())
                num = sqlDataReader.GetInt32(0);
              sqlDataReader.Close();
              if ((num == -1 ? 1 : (ArgArray.IndexOf((object) num) >= 0 ? 1 : 0)) == 0)
                ArgArray.Add((object) num);
            }
          }
          if (flag)
            str2 += ArgCondition.Substring(startIndex, 1);
          if (ArgCondition.Substring(startIndex, 1) == Class2.smethod_0("ᛘ"))
          {
            flag = true;
            str2 = "";
          }
        }
      }
      ArrayList arrayList1 = new ArrayList();
      ArrayList arrayList2 = new ArrayList();
      if (UserQuery_id != -1)
      {
        sqlCommand.CommandText = Class2.smethod_0("ᚰᚣᚫᚥᚤᚶᚃᚶᛊᛖᛖᛚᛝᛏᛝᚻᛏᛘᛔᛓᛥᛑᛜᛘᚡᚖᚺᛧᛥᛯᛨᛪᛑᛧᛳ᛬ᛦᚮᚣᛍᛸᛎᛰ᛬᛭ᛯ\x16F9ᚬᛊᚮᛸᜃ\x16FFᜇ\x16FFᜀᚽᛟᜊᛠᜂ\x16FE\x16FFᜁᜋᛊᚿᛐᛊᛂᛩᛶᛴᛳᛇ\x16FD\x171Cᜏ\x171D\x16FDᜢᜓᜡᜩᜀ᜔\x171D\x1719\x1718ᜪᜪᛘᜰᜣᜯᜤᛝᛦᜭᜯᜭᜱᜦᜯᛮᛦ\x171Eᜐᜎ\x171Cᜐ᛬ᜢᝁ᜴ᝂᜢᝇ\x1738ᝆᝎ᜵ᝀ\x173C\x16F9\x1717\x16FB") + UserQuery_id.ToString();
        SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
        while (sqlDataReader.Read())
        {
          if (!sqlDataReader.IsDBNull(1))
            arrayList1.Add((object) new DataPathConstructor.Struct1()
            {
              int_0 = sqlDataReader.GetInt32(0),
              string_0 = sqlDataReader.GetString(1)
            });
          if (!sqlDataReader.IsDBNull(2) && sqlDataReader.GetInt32(2) == 1)
            arrayList2.Add((object) sqlDataReader.GetInt32(0));
        }
        sqlDataReader.Close();
      }
      for (int index = 0; index < count1; ++index)
      {
        int num1 = -1;
        int num2 = -1;
        int num3 = -1;
        int num4 = -1;
        int num5 = (int) ArgArray[index];
        sqlCommand.CommandText = Class2.smethod_0("ᚰᚣᚫᚥᚤᚶᚃᚶᛊᛖᛖᛚᛝᛏᛝᚰᛒᛢᛐᛙᛝᛑᛜᛘᚡᚖᛉᛝᛩᛩ᛭ᛰᛢᛰᛂ᛬ᛢᛵᛶᛣᛮᛪᚧᛎᛛᛙᛘᚬᛟᛳ\x16FF\x16FFᜃᜆᛸᜆᛤᛸᜁ\x16FD\x16FCᜎᜎᚼ᜔ᜇᜓᜈᛁᛊᜑᜓᜑ\x1715ᜊᜓᛒᛊᜂᛴᛲᜀᛴᛐᜥ\x171B\x1717ᛔᛲᛖ") + num5.ToString() + Class2.smethod_0("ᙽ\x169Fᚭᚤᚁᚴᛈᛔᛔᛘᛛᛍᛛᚶᛌᛥᛒᛠᛎᛙᛕᚒᚰᚔ") + ReporterLayer_id.ToString();
        SqlDataReader sqlDataReader1 = sqlCommand.ExecuteReader();
        while (sqlDataReader1.Read())
        {
          num1 = !sqlDataReader1.IsDBNull(0) ? sqlDataReader1.GetInt32(0) : -1;
          num2 = !sqlDataReader1.IsDBNull(1) ? sqlDataReader1.GetInt32(1) : -1;
        }
        sqlDataReader1.Close();
        if (num1 > -1)
        {
          sqlCommand.CommandText = Class2.smethod_0("ᚰᚣᚫᚥᚤᚶᚃᚶᛊᛖᛖᛚᛝᛏᛝᚰᛖᛛᛔᛞᛤᛛᛢᛢᛔᛟᛛᚘᚿᛌᛊᛉ\x169Dᛐᛤᛰᛰᛴᛷᛩᛷᛊ᛬\x16FCᛪᛳᛷ\x16FFᚭᜅᛸᜄ\x16F9ᚲᚻᜂᜄᜂᜆ\x16FBᜄᛃᚻᛳᛥᛣᛱᛥᛁ\x1716ᜌᜈᛅᛣᛇ") + num1.ToString();
          SqlDataReader sqlDataReader2 = sqlCommand.ExecuteReader();
          while (sqlDataReader2.Read())
            num3 = !sqlDataReader2.IsDBNull(0) ? sqlDataReader2.GetInt32(0) : -1;
          sqlDataReader2.Close();
          sqlCommand.CommandText = Class2.smethod_0("ᚰᚣᚫᚥᚤᚶᚃᚸᚴᚶᚇᚙᚉᛞᛔᛐᚍᚴᛁᚿᚾᚒᛅᛙᛥᛥᛩ᛬ᛞ᛬ᛊᛞᛧᛣᛢᛴᛴᚢ\x16FA᛭\x16F9ᛮᚧᚰᛷ\x16F9ᛷ\x16FBᛰ\x16F9ᚸᚰᛨᛚᛘᛦᛚᚶᛩ\x16FDᜉᜉ\x170Dᜐᜂᜐᛣᜉᜎᜇᜑ\x1717ᜎ\x1715\x1715ᜇᜒᜎᛋᛩᛍ") + num3.ToString() + Class2.smethod_0("ᙽ\x169Fᚭᚤᚁᚴᛈᛔᛔᛘᛛᛍᛛᚭᛗᛍᛠᛡᛎᛙᛕᚒᚰᚔ") + num2.ToString() + Class2.smethod_0("ᙽ\x169Fᚭᚤᚁᚴᛈᛔᛔᛘᛛᛍᛛᚶᛌᛥᛒᛠᛎᛙᛕᚒᚰᚔ") + ReporterLayer_id.ToString();
          SqlDataReader sqlDataReader3 = sqlCommand.ExecuteReader();
          while (sqlDataReader3.Read())
            num4 = !sqlDataReader3.IsDBNull(0) ? sqlDataReader3.GetInt32(0) : -1;
          sqlDataReader3.Close();
          if (ArgArray.IndexOf((object) num4) == -1)
            ArgArray.Add((object) num4);
        }
      }
      int count2 = ArgArray.Count;
      ArrayList ThisArray1 = new ArrayList();
      ArrayList ThisArray2 = new ArrayList();
      ArrayList TableList = new ArrayList();
      ArrayList arrayList3 = new ArrayList();
      ArrayList arrayList4 = new ArrayList();
      ArrayList arrayList5 = new ArrayList();
      string str3 = "";
      IComparer comparer = (IComparer) new DataPathConstructor.OrderRowComparer();
      OrderArray.Sort(comparer);
      DataPathConstructor.OrderRow orderRow = new DataPathConstructor.OrderRow();
      for (int index = 0; index < OrderArray.Count; ++index)
      {
        DataPathConstructor.OrderRow order = (DataPathConstructor.OrderRow) OrderArray[index];
        if (str3.Length > 0)
          str3 += Class2.smethod_0("ᚉᙾ");
        str3 = str3 + Class2.smethod_0("ᛘᛙ") + order.Object_id.ToString() + Class2.smethod_0("ᛚᛛᙿ") + order.Method;
      }
      ArrayList arrayList6 = new ArrayList();
      ArrayList ThisArray3 = new ArrayList();
      string str4 = "";
      bool flag1 = false;
      for (int index1 = 0; index1 < count2; ++index1)
      {
        int num1 = -1;
        int num2 = -1;
        int num3 = -1;
        string str5 = "";
        string newValue = "";
        string str6 = "";
        int num4 = 0;
        int num5 = (int) ArgArray[index1];
        sqlCommand.CommandText = Class2.smethod_0("ᚰᚣᚫᚥᚤᚶᚃᚶᛊᛖᛖᛚᛝᛏᛝᚰᛖᛛᛔᛞᛤᛛᛢᛢᛔᛟᛛᚤᚙᛌᛠ᛬᛬ᛰᛳᛥᛳᛏᛨᛥᛸ\x16FB\x16F9᛭ᛨᛳᛯᚸᚭᛠᛴᜀᜀᜄᜇ\x16F9ᜇᛚ\x16FCᜌ\x16FAᜃᜇ\x16FBᜆᜂᛋᛀᛰᜄ\x170Dᜉᜈ\x171Aᛵᜉ\x1716ᜏᛗᛌᛶᜡᛲ\x1718\x1716\x1715\x171Eᜀ\x171Eᜤᜢᜫᛙᛷᛛᜥᜰᜬ᜴ᜬᜭᛪᜌ\x1737ᜈᜮᜬᜫ᜴\x1716᜴\x173A\x1738ᝁ\x16FBᛰᜁ\x16FBᛳ\x171Aᜧᜥᜤᛸᜫ\x173Fᝋᝋᝏᝒᝄᝒᜰᝄᝍᝉᝈ\x175A\x175Aᜈᝠᝓ\x175F\x1754\x170D\x1716\x175D\x175F\x175Dᝡ\x1756\x175F\x171E\x1716ᝎᝀ\x173Eᝌᝀ\x171C\x1771ᝧᝣᜠ\x173Eᜢ") + num5.ToString() + Class2.smethod_0("ᙽ\x169Fᚭᚤᚁᚴᛈᛔᛔᛘᛛᛍᛛᚶᛌᛥᛒᛠᛎᛙᛕᚒᚰᚔ") + ReporterLayer_id.ToString();
        SqlDataReader sqlDataReader1 = sqlCommand.ExecuteReader();
        while (sqlDataReader1.Read())
        {
          num1 = !sqlDataReader1.IsDBNull(0) ? sqlDataReader1.GetInt32(0) : -1;
          num2 = !sqlDataReader1.IsDBNull(1) ? sqlDataReader1.GetInt32(1) : -1;
          num3 = !sqlDataReader1.IsDBNull(2) ? sqlDataReader1.GetInt32(2) : -1;
          str5 = !sqlDataReader1.IsDBNull(3) ? sqlDataReader1.GetString(3) : "";
          num4 = !sqlDataReader1.IsDBNull(4) ? sqlDataReader1.GetInt32(4) : 0;
        }
        sqlDataReader1.Close();
        DataPathConstructor.Struct1 struct1_1;
        if ((num4 != 1 ? 1 : (flag1 ? 1 : 0)) == 0)
        {
          sqlCommand.CommandText = Class2.smethod_0("ᚰᚣᚫᚥᚤᚶᚃᚨᚮᚹᚻᚱᚷᚭᚿᚌᛁᛏᛑᛜᛖᛑᛜᛘᚕᚳᚗᚻᛈᚻᛇᛁᛐᛁᛄᚨᛤᚰᛷ᛭ᛩᚲᚧ᛭ᚷ\x16FEᛴᛰᚹᚮᛷᚾᜅ\x16FBᛷᚽᛁᚶ᛫\x16F9\x16FBᜆᜀᛪ\x16FEᜋᜄᛀᛞᛂᛦᛳᛦᛲ᛬\x16FB᛬ᛯᛓᜏᛛᜂᜐᜒ\x171D\x1717ᜁ\x1715ᜢ\x171Bᛣᛘ\x171Eᛨᜏ\x171D\x171Fᜪᜤᜎᜢᜯᜨᛰᛥᜮᛵ\x171Cᜪᜬ\x1737ᜱ\x171Bᜯ\x173C᜵\x16FA\x16FEᛳᜨ᜶\x1738ᝃ\x173D\x171Aᝆᝄ\x173Dᝐ\x16FE\x171Cᜀᜤᜱᜤᜰᜪ\x1739ᜪᜭᜑᝍ\x1719ᝀᝎᝐ\x175B\x1755ᜲ\x175E\x175C\x1755ᝨᜢ\x1717\x175Dᜧᝎ\x175C\x175Eᝩᝣᝀᝬᝪᝣ\x1776ᜰᜥᝮ᜵\x175Cᝪᝬ\x1777\x1771ᝎ\x177A\x1778\x1771ង\x173Bᜳ\x175Aᝧᝥᝤ\x1738ᝫ\x177Fឋឋតធងធᝰងឍញឈររᝈដឝᝋឍᝍឥមឤយᝒ\x175Bអឤអឦលឤᝣ\x175Bឈគងនᝠឋទឌធᝥមឬីីូឿឱឿធីួាៀំួោោ៊\x1778ឺ៍\x177Bើ\x177D៕ៈ។៉គឋ្។្៖់។នឋ៛៛ណ័ឞៃៗ៣៣៧\x17EAៜ\x17EAួ៣៨១\x17EB\x17F1៨\x17EF\x17EF១\x17EC៨ឥៃឧ\x17EAិ\x17FE\x17F4\x17F0ឭ៚។៖៥ឲ៝៣\x17DE៤ិ\x17EA\x17FE᠊᠊\x180E᠑᠃᠑\x17F4᠂᠄\x180F᠉᠘ំ᠈\x181B៉᠍់ᠣ᠖ᠢ᠗័៙ᠠᠢᠠᠤ᠙ᠢ១៙ᠩᠩៜ\x181F\x17EC᠑ᠥᠱᠱᠵᠸᠪᠸ\x181Bᠩᠫᠶᠰᠫᠶᠲ\x17EF᠍\x17F1ᠵ᠁ᡈᠾᠺ\x17F7ᠤ\x181Eᠠᠯ\x17FCᠧᠭᠨᠮ᠁ᠴᡈᡔᡔᡘᡛᡍᡛᠷᡐᡍᡠᡣᡡᡕᡤ᠒ᡔᡧ᠕ᡚ᠗ᡯᡢᡮᡣ\x181Cᠥᡬᡮᡬᡰᡥᡮᠭᠥᡵᡵᠨᡪᠸᡝᡱ\x187D\x187Dᢁᢄᡶᢄᡠ\x1879ᡶᢉᢌᢊ\x187E\x1879ᢄᢀᠽᡛᠿᢄᡏᢖᢌᢈᡅᡲᡬᡮ\x187Dᡊᡵ\x187Bᡶ\x187Cᡏᢂᢖᢢᢢᢦᢩᢛᢩᢌᢚᢜᢧᢡᢰᡞᢠᢳᡡᢧᡣᢻ\x18AEᢺ\x18AFᡨᡱᢸᢺᢸᢼᢱᢺ\x1879ᡱᣁᣁᡴᢹᢄᢩᢽᣉᣉᣍᣐᣂᣐᢳᣁᣃᣎᣈᣃᣎᣊᢇᢥᢉᣏᢙᣠᣖᣒᢏᢼᢶᢸᣇᢔᢿᣅᣀᣆᢙᣌᣠᣬᣬᣰᣳᣥᣳᣆᣨ\x18F8ᣦᣯᣳ\x18FBᢩᣫ\x18FE\x18ACᣳ\x18AEᤆ\x18F9ᤅ\x18FAᢳᢼᤃᤅᤃᤇ\x18FCᤅᣄᢼᤌᤌᢿᤁᣏᣴᤈᤔᤔᤘᤛᤍᤛᣮᤐᤠᤎᤗᤛᤏᤚᤖᣓᣱᣕᤜᣥ\x192Cᤢᤞᣛᤈᤂᤄᤓᣠᤋᤑᤌᤒᣥᤘ\x192Cᤸᤸ\x193C\x193Fᤱ\x193Fᤒᤸ\x193Dᤶ᥀᥆\x193D᥄᥄᥊\x18F8᤺᥍\x18FB\x1943\x18FDᥕ᥈ᥔ᥉ᤂᤋᥒᥔᥒᥖ᥋ᥔᤓᤋᥛᥛᤎᥕᤞ\x1943ᥗᥣᥣᥧᥪᥜᥪ\x193Dᥣᥨᥡᥫᥱᥨ\x196F\x196Fᥡᥬᥨᤥ\x1943ᤧ\x196Fᤷ\x197Eᥴᥰ\x192Dᥚᥔᥖᥥᤲᥝᥣᥞᥤᤷᥪ\x197Eᦊᦊᦎᦑᦃᦑᥴᦂᦄᦏᦉᦘ᥆ᦈᦛ᥉ᦒ᥋ᦣᦖᦢᦗᥐᥙᦠᦢᦠᦤᦙᦢᥡᥙᦩᦩᥜᦤᥬᦑᦥᦱᦱᦵᦸᦪᦸᦛᦩᦫᦶᦰᦫᦶᦲ\x196Fᦍᥱᦺᦁᧈᦾᦺ\x1977\x19AFᦡᦟ\x19ADᦡ\x197Dᦿᦍᦲᧆ᧒᧒᧖᧙\x19CB᧙ᦴ\x19CA᧣᧐᧞\x19CC᧗᧓ᦐ\x19AEᦒ") + ReporterLayer_id.ToString();
          SqlDataReader sqlDataReader2 = sqlCommand.ExecuteReader();
          while (sqlDataReader2.Read())
          {
            struct1_1 = new DataPathConstructor.Struct1();
            struct1_1.int_0 = sqlDataReader2.GetInt32(0);
            struct1_1.string_0 = sqlDataReader2.GetString(1);
            struct1_1.string_1 = sqlDataReader2.GetString(2);
            arrayList6.Add((object) struct1_1);
          }
          sqlDataReader2.Close();
        }
        DataPathConstructor.Struct1 struct1_2;
        if (num1 > -1)
        {
          int Table_id = -1;
          string str7 = "";
          string str8 = "";
          sqlCommand.CommandText = Class2.smethod_0("ᚰᚣᚫᚥᚤᚶᚃᚶᛊᛖᛖᛚᛝᛏᛝᛀᛎᛐᛛᛕᛐᛛᛗᚠᚕᚷᛪ᛫ᛨᛝᛤᛝᛱᛣᛣᛃᛰᛮᛸᛱᛳᚲᚧᛛᛮᛶᛰᛯᜁᛡᜃᛱᜅᛷᜀ\x16F9ᜃᜊᛃᚸᛰᜂᜀᜎᜂᛱᜓᜁ\x1715ᜇᜐᜉᜓ\x171Aᛇᛮ\x16FB\x16F9ᛸᛌ\x16FFᜓ\x171F\x171Fᜣᜦ\x1718ᜦ\x16F9\x171Fᜤ\x171Dᜧᜭᜤᜫᜫᜱᛟ\x1737ᜪ᜶ᜫᛤ᛭᜴᜶᜴\x1738ᜭ᜶ᛵ᛭ᜥ\x1717\x1715ᜣ\x1717ᛳᝈ\x173E\x173Aᛷ\x1715\x16F9") + num1.ToString();
          SqlDataReader sqlDataReader2 = sqlCommand.ExecuteReader();
          while (sqlDataReader2.Read())
          {
            Table_id = !sqlDataReader2.IsDBNull(0) ? sqlDataReader2.GetInt32(0) : -1;
            str6 = !sqlDataReader2.IsDBNull(1) ? sqlDataReader2.GetString(1) : "";
            str7 = !sqlDataReader2.IsDBNull(2) ? sqlDataReader2.GetString(2) : "";
            str8 = !sqlDataReader2.IsDBNull(3) ? sqlDataReader2.GetString(3) : "";
          }
          sqlDataReader2.Close();
          DataPathConstructor.TableInfo tableInfo = this.GetTableInfo(Table_id);
          string tableName = tableInfo.TableName;
          string tableAlias = tableInfo.TableAlias;
          if (TableList.IndexOf((object) Table_id) == -1)
          {
            TableList.Add((object) Table_id);
            arrayList3.Add((object) tableName);
            arrayList4.Add((object) tableAlias);
          }
          if (num4 == 1)
          {
            for (int index2 = 0; index2 < arrayList6.Count; ++index2)
            {
              struct1_1 = (DataPathConstructor.Struct1) arrayList6[index2];
              int int0 = struct1_1.int_0;
              string string0 = struct1_1.string_0;
              string string1 = struct1_1.string_1;
              if ((str7.ToUpper().IndexOf(string1.ToUpper() + Class2.smethod_0("ᚋ")) == -1 || string.IsNullOrEmpty(str7) ? (str8.ToUpper().IndexOf(string1.ToUpper() + Class2.smethod_0("ᚋ")) == -1 ? 1 : (string.IsNullOrEmpty(str8) ? 1 : 0)) : 0) == 0 && TableList.IndexOf((object) int0) == -1)
              {
                TableList.Add((object) int0);
                arrayList3.Add((object) string0);
                arrayList4.Add((object) string1);
              }
            }
          }
          newValue = !(str7 == str6) ? str7 : tableAlias + Class2.smethod_0("ᚋ") + str7;
          bool flag2 = false;
          if (arrayList2.IndexOf((object) num5) == -1)
          {
            if (arrayList1.Count > 0)
            {
              for (int index2 = 0; index2 < arrayList1.Count; ++index2)
              {
                struct1_2 = (DataPathConstructor.Struct1) arrayList1[index2];
                if (struct1_2.int_0 == num5)
                {
                  ThisArray3.Add((object) (Class2.smethod_0("ᚸ") + struct1_2.string_0 + Class2.smethod_0("ᚺᙾ᚜ ") + newValue));
                  flag2 = true;
                  break;
                }
              }
            }
            if (!flag2)
              ThisArray3.Add((object) (Class2.smethod_0("ᚸ") + str5 + Class2.smethod_0("ᚺᙾ᚜ ") + newValue));
          }
          ArgCondition = ArgCondition.ToString().Replace(Class2.smethod_0("ᛘ") + str5 + Class2.smethod_0("ᛚ"), newValue);
          str3 = str3.ToString().Replace(Class2.smethod_0("ᛘᛙ") + num5.ToString() + Class2.smethod_0("ᛚᛛ"), newValue);
          ThisArray1.Add((object) (tableAlias + Class2.smethod_0("ᚋ") + str6));
          if (str8.Trim().Length > 0)
            ThisArray2.Add((object) str8);
        }
        else if (num2 > -1)
        {
          int Table_id = -1;
          int num6 = -1;
          string str7 = "";
          string str8 = "";
          int num7 = 0;
          string str9 = "";
          sqlCommand.CommandText = Class2.smethod_0("ᚰᚣᚫᚥᚤᚶᚃᚶᛊᛖᛖᛚᛝᛏᛝᛀᛎᛐᛛᛕᛐᛛᛗᚠᚕᛈᛜᛨᛨ᛬ᛯᛡᛯᚿᛦᛧᛳᛧᛪᛥ\x16F9᛫ᛦᛱ᛭ᚶᚫᛍᜀᜁ\x16FEᛳ\x16FAᛳᜇ\x16F9\x16F9ᛙᜆᜄᜎᜇᜉᛈᚽᛱᜄᜌᜆᜅ\x1717ᛷ\x1719ᜇ\x171B\x170D\x1716ᜏ\x1719ᜠᛙᛎᜆ\x1718\x1716ᜤ\x1718ᜇᜩ\x1717ᜫ\x171Dᜦ\x171Fᜩᜰᛝᜄᜑᜏᜎᛢ\x1715ᜩ᜵᜵\x1739\x173Cᜮ\x173C\x1718ᜱᜮᝁᝄᝂ᜶ᝅᛳᝋ\x173Eᝊ\x173Fᛸᜁᝈᝊᝈᝌᝁᝊᜉᜁ\x1739ᜫᜩ\x1737ᜫᜇ\x175Cᝒᝎᜋᜩ\x170D") + num2.ToString();
          SqlDataReader sqlDataReader2 = sqlCommand.ExecuteReader();
          while (sqlDataReader2.Read())
          {
            Table_id = !sqlDataReader2.IsDBNull(0) ? sqlDataReader2.GetInt32(0) : -1;
            num6 = !sqlDataReader2.IsDBNull(1) ? sqlDataReader2.GetInt32(1) : -1;
            str6 = !sqlDataReader2.IsDBNull(2) ? sqlDataReader2.GetString(2) : "";
            str7 = !sqlDataReader2.IsDBNull(3) ? sqlDataReader2.GetString(3) : "";
            str8 = !sqlDataReader2.IsDBNull(4) ? sqlDataReader2.GetString(4) : "";
          }
          sqlDataReader2.Close();
          sqlCommand.CommandText = Class2.smethod_0("ᚰᚣᚫᚥᚤᚶᚃᚹᛘᛋᚨᛏᛐᛜᛐᛓᛎᛢᛔ᚜ᚑᚳᛚᛛᛧᛛᛞᛙ᛭ᛟᛁᛱ᛫ᛡᛳᛩᛰᛰᚣᛊᛗᛕᛔᚨᛛᛯ\x16FB\x16FB\x16FFᜂᛴᜂᛒ\x16F9\x16FAᜆ\x16FA\x16FDᛸᜌ\x16FE\x170Dᚻᜓᜆᜒᜇᛀᛉᜐᜒᜐ᜔ᜉᜒᛑᛉᜁᛳᛱ\x16FFᛳᛏᜤ\x171A\x1716ᛓᛱᛕ") + num6.ToString();
          SqlDataReader sqlDataReader3 = sqlCommand.ExecuteReader();
          while (sqlDataReader3.Read())
          {
            num7 = !sqlDataReader3.IsDBNull(0) ? sqlDataReader3.GetInt32(0) : 0;
            str9 = !sqlDataReader3.IsDBNull(1) ? sqlDataReader3.GetString(1) : "";
          }
          sqlDataReader3.Close();
          DataPathConstructor.TableInfo tableInfo = this.GetTableInfo(Table_id);
          string tableName = tableInfo.TableName;
          string tableAlias = tableInfo.TableAlias;
          if (TableList.IndexOf((object) Table_id) == -1)
          {
            TableList.Add((object) Table_id);
            arrayList3.Add((object) tableName);
            arrayList4.Add((object) tableAlias);
          }
          if (num4 == 1)
          {
            for (int index2 = 0; index2 < arrayList6.Count; ++index2)
            {
              struct1_1 = (DataPathConstructor.Struct1) arrayList6[index2];
              int int0 = struct1_1.int_0;
              string string0 = struct1_1.string_0;
              string string1 = struct1_1.string_1;
              if ((str7.ToUpper().IndexOf(string1.ToUpper() + Class2.smethod_0("ᚋ")) == -1 || string.IsNullOrEmpty(str7) ? (str8.ToUpper().IndexOf(string1.ToUpper() + Class2.smethod_0("ᚋ")) == -1 ? 1 : (string.IsNullOrEmpty(str8) ? 1 : 0)) : 0) == 0 && TableList.IndexOf((object) int0) == -1)
              {
                TableList.Add((object) int0);
                arrayList3.Add((object) string0);
                arrayList4.Add((object) string1);
              }
            }
          }
          if (IsIgnoreGrouping)
            num7 = 0;
          if (num7 == 1)
          {
            if (str7 == str6)
              newValue = str9 + Class2.smethod_0("ᚅ") + tableAlias + Class2.smethod_0("ᚋ") + str7 + Class2.smethod_0("ᚆ");
            else
              newValue = str9 + Class2.smethod_0("ᚅ") + str7 + Class2.smethod_0("ᚆ");
          }
          else
          {
            newValue = !(str7 == str6) ? str7 : tableAlias + Class2.smethod_0("ᚋ") + str7;
            ThisArray1.Add((object) (tableAlias + Class2.smethod_0("ᚋ") + str6));
          }
          bool flag2 = false;
          if (arrayList2.IndexOf((object) num5) == -1)
          {
            if (arrayList1.Count > 0)
            {
              for (int index2 = 0; index2 < arrayList1.Count; ++index2)
              {
                struct1_2 = (DataPathConstructor.Struct1) arrayList1[index2];
                if (struct1_2.int_0 == num5)
                {
                  ThisArray3.Add((object) (Class2.smethod_0("ᚸ") + struct1_2.string_0 + Class2.smethod_0("ᚺᙾ᚜ ") + newValue));
                  flag2 = true;
                  break;
                }
              }
            }
            if (!flag2)
              ThisArray3.Add((object) (Class2.smethod_0("ᚸ") + str5 + Class2.smethod_0("ᚺᙾ᚜ ") + newValue));
          }
          ArgCondition = ArgCondition.ToString().Replace(Class2.smethod_0("ᛘ") + str5 + Class2.smethod_0("ᛚ"), newValue);
          str3 = str3.ToString().Replace(Class2.smethod_0("ᛘᛙ") + num5.ToString() + Class2.smethod_0("ᛚᛛ"), newValue);
          if (str8.Trim().Length > 0)
            ThisArray2.Add((object) str8);
        }
        else if (num3 > -1)
        {
          int Table_id = -1;
          string str7 = "";
          string str8 = "";
          sqlCommand.CommandText = Class2.smethod_0("ᚰᚣᚫᚥᚤᚶᚃᛆᚓᚸᛌᛘᛘᛜᛟᛑᛟᛂᛐᛒᛝᛗᛒᛝᛙᚢᚗᛙᚧᚻᛮᛯ᛬ᛡᛨᛡᛵᛧᛧᛇᛴᛲ\x16FCᛵᛷᚶᚫ᛭ᚻᛡᛴ\x16FCᛶᛵᜇᛧᜉᛷᜋ\x16FDᜆ\x16FFᜉᜐᛉᚾᜀᛎᛸᜊᜈ\x1716ᜊ\x16F9\x171Bᜉ\x171Dᜏ\x1718ᜑ\x171Bᜢᛏᛶᜃᜁᜀᛔᜇ\x171Bᜧᜧᜫᜮᜠᜮᜁᜣᜳᜡᜪᜮ᜶ᛤᜦ\x1739ᛧᜩᛩᝁ᜴ᝀ᜵ᛮᛷ\x173Eᝀ\x173Eᝂ\x1737ᝀ\x16FFᛷᜢᜨᜣᜩ\x16FCᜯᝃᝏᝏᝓ\x1756ᝈ\x1756ᜩᝏ\x1754ᝍ\x1757\x175D\x1754\x175B\x175Bᝡᜏᝑᝤᜒ\x1755᜔ᝬ\x175Fᝫᝠ\x1719ᜢᝩᝫᝩ\x176Dᝢᝫᜪᜢᝒᝒᜥᝧ᜵\x175Aᝮ\x177A\x177A\x177Eខᝳខ\x1754\x177A\x177F\x1778គឈ\x177Fឆឆ\x1778ឃ\x177F\x173C\x175A\x173Eខᝎផឋជᝄ\x177Cᝮᝬ\x177Aᝮᝊឌ\x175Aឡភនᝐᝮᝒ") + num3.ToString();
          SqlDataReader sqlDataReader2 = sqlCommand.ExecuteReader();
          while (sqlDataReader2.Read())
          {
            Table_id = !sqlDataReader2.IsDBNull(0) ? sqlDataReader2.GetInt32(0) : -1;
            str6 = !sqlDataReader2.IsDBNull(1) ? sqlDataReader2.GetString(1) : "";
            str7 = !sqlDataReader2.IsDBNull(2) ? sqlDataReader2.GetString(2) : "";
            str8 = !sqlDataReader2.IsDBNull(3) ? sqlDataReader2.GetString(3) : "";
          }
          sqlDataReader2.Close();
          DataPathConstructor.TableInfo tableInfo = this.GetTableInfo(Table_id);
          string tableName = tableInfo.TableName;
          string tableAlias = tableInfo.TableAlias;
          if (TableList.IndexOf((object) Table_id) == -1)
          {
            TableList.Add((object) Table_id);
            arrayList3.Add((object) tableName);
            arrayList4.Add((object) tableAlias);
          }
          if (num4 == 1)
          {
            for (int index2 = 0; index2 < arrayList6.Count; ++index2)
            {
              struct1_1 = (DataPathConstructor.Struct1) arrayList6[index2];
              int int0 = struct1_1.int_0;
              string string0 = struct1_1.string_0;
              string string1 = struct1_1.string_1;
              if ((str7.ToUpper().IndexOf(string1.ToUpper() + Class2.smethod_0("ᚋ")) == -1 || string.IsNullOrEmpty(str7) ? (str8.ToUpper().IndexOf(string1.ToUpper() + Class2.smethod_0("ᚋ")) == -1 ? 1 : (string.IsNullOrEmpty(str8) ? 1 : 0)) : 0) == 0 && TableList.IndexOf((object) int0) == -1)
              {
                TableList.Add((object) int0);
                arrayList3.Add((object) string0);
                arrayList4.Add((object) string1);
              }
            }
          }
          newValue = !(str7 == str6) ? str7 : tableAlias + Class2.smethod_0("ᚋ") + str7;
          bool flag2 = false;
          if (arrayList2.IndexOf((object) num5) == -1)
          {
            if (arrayList1.Count > 0)
            {
              for (int index2 = 0; index2 < arrayList1.Count; ++index2)
              {
                struct1_2 = (DataPathConstructor.Struct1) arrayList1[index2];
                if (struct1_2.int_0 == num5)
                {
                  ThisArray3.Add((object) (Class2.smethod_0("ᚸ") + struct1_2.string_0 + Class2.smethod_0("ᚺᙾ᚜ ") + newValue));
                  flag2 = true;
                  break;
                }
              }
            }
            if (!flag2)
              ThisArray3.Add((object) (Class2.smethod_0("ᚸ") + str5 + Class2.smethod_0("ᚺᙾ᚜ ") + newValue));
          }
          ArgCondition = ArgCondition.ToString().Replace(Class2.smethod_0("ᛘ") + str5 + Class2.smethod_0("ᛚ"), newValue);
          str3 = str3.ToString().Replace(Class2.smethod_0("ᛘᛙ") + num5.ToString() + Class2.smethod_0("ᛚᛛ"), newValue);
          ThisArray1.Add((object) (tableAlias + Class2.smethod_0("ᚋ") + str6));
          if (str8.Trim().Length > 0)
            ThisArray2.Add((object) str8);
        }
        if (ConcatenateArray != null)
        {
          for (int index2 = 0; index2 < ConcatenateArray.Count; ++index2)
          {
            DataPathConstructor.ConcatenateStruct concatenate = (DataPathConstructor.ConcatenateStruct) ConcatenateArray[index2];
            if (concatenate.ReporterObject_id == num5)
            {
              if (str4.Length > 0)
                str4 += Class2.smethod_0("ᙽᚉᙿ");
              str4 = str4 + Class2.smethod_0("ᛁᛀᛎᚎᚳᛇᛓᛐᛎᛉᛈᛜᛎᛀᛌᛘᛢᛓᚗᛓᛒᛥᛧ᚜") + newValue + Class2.smethod_0("ᙽᚿᛒ ᛗᛃᛕᛇᛍᛇᛙᚐ᚛ᚚ᚛ᚕᚖᚚᚏ") + concatenate.Prefix + Class2.smethod_0("ᚉᙾ") + concatenate.CodeLength.ToString() + Class2.smethod_0("ᚆ");
            }
          }
        }
      }
      string str10 = "";
      ArrayList arrayList7 = new ArrayList();
      ArrayList arrayList8 = new ArrayList();
      ArrayList ThisArray4 = new ArrayList();
      if (TableList.Count > 1)
      {
        ArrayList arrayList9 = this.LinkTables(TableList, ReporterLayer_id);
        while (arrayList9.Count > 0)
        {
          arrayList8.Clear();
          for (int index1 = 0; index1 < arrayList9.Count; ++index1)
          {
            string str5 = "";
            string str6 = "";
            string str7 = "";
            string str8 = "";
            string str9 = "";
            string str11 = "";
            SqlInt32 sqlInt32_1 = (SqlInt32) -1;
            SqlInt32 sqlInt32_2 = (SqlInt32) -1;
            bool flag2 = false;
            string list = this.ArrayToList(ThisArray4, Class2.smethod_0("ᚉᙾ"), "", "");
            string str12;
            if (list.Length > 0)
              str12 = Class2.smethod_0("ᙽ\x169Fᚭᚤᚁᚊᛄᚒᚸᛕᛜᛚᛌᛏᚿᛍᛏᛚᛔᛏᛚᛖᚓᚽᛃᚖ\x169F") + list + Class2.smethod_0("ᚆᙾᚮᚲᚁᛃᚑᚸᛆᛘᛎᛍᛝᚾᛌᛎᛙᛓᛎᛙᛕᚒᚼᛂᚕ\x169E") + list + Class2.smethod_0("ᚆᚇ");
            else
              str12 = "";
            sqlCommand.CommandText = Class2.smethod_0("ᚰᚣᚫᚥᚤᚶᚃᛇᚓᚺᛈᛊᛕᛏᚹᛍᛚᛓᚏᛑᛤᚒᛆᛈᛃᚢᚗᛛᚧᛎᛜᛞᛩᛣᛀ᛬ᛪᛣᛶᚤᛦ\x16F9ᚧᛛᛝᛋᚷᚬᛱᚼᛣᛱᛳ\x16FEᛸᛢᛶᜃ\x16FCᚸ\x16FA\x170Dᚻᛠᛱ᛬ᛋᛀᜅᛐᛷᜅᜇᜒᜌᛩ\x1715ᜓᜌ\x171Fᛍᜏᜢᛐᛵᜆᛴᛠᛕ\x1717ᛥ\x16FBᜨᜨ\x171Fᜥᜱᜧᜮᜮ᜔᜶ᜤ\x1738ᜪᜳᜬ᜶\x173Dᛶ᛫ᜮ\x16FB\x1718\x173E\x1739\x173F\x1718ᝈᝂ\x1738ᝊᝀᝇᝇᜆ\x16FB\x173Dᜋᜱᝎ\x1755ᝓᝅᝈ\x1738ᝆᝈᝓᝍᝈᝓᝏ\x1718\x170Dᝏ\x171Dᝄᝒᝤ\x175A\x1759ᝩᝊ\x1758\x175Aᝥ\x175F\x175Aᝥᝡ\x171Eᝅᝒᝐᝏᜣ\x1756ᝪ\x1776\x1776\x177A\x177Dᝯ\x177Dᝠᝮᝰ\x177B\x1775\x175Bខ\x177Cគឈ᜶\x1778ឋ\x1739\x177B\x173Bនឆធជᝀᝉថធថបញធᝑᝉ\x1774\x177A\x1775\x177Bᝎខផឡឡឥឨរឨខឧអឨឮ\x175Cឞឱ\x175Fអᝡឹឬីឭᝦᝯាីាឺឯី\x1777ᝯសសᝲ឴គឧុះះ់៎ៀ៎ឧ៍ៈ៎ៀ់ះងអឆ៉ព៝៓៏ឌិួីើទោ៘៤៤៨\x17EB៝\x17EB៎ៜ\x17DE៩៣\x17F2ហ២\x17F5ឣ៧ឥ\x17FD\x17F0\x17FC\x17F1ឪឳ\x17FA\x17FC\x17FA\x17FE\x17F3\x17FCុឳ៣៣ា\x17F8ំ\x17EC᠉᠐\x180E᠀᠃\x17F3᠁᠃\x180E᠈᠃\x180E᠊ះ៥៉᠍៙ᠠ᠖᠒៏\x17FA᠀\x17FB᠁។᠇\x181Bᠧᠧᠫᠮᠠᠮ᠑\x181Fᠡᠬᠦᠵ៣ᠥᠸ៦ᠫ៨ᡀᠳᠿᠴ\x17ED\x17F6ᠽᠿᠽᡁᠶᠿ\x17FE\x17F6ᠦᠦ\x17F9ᠻ᠉ᠰᠾᡐᡆᡅᡕᠶᡄᡆᡑᡋᡆᡑᡍ᠊ᠨ᠌ᡑ\x181Cᡣᡙᡕ᠒ᡊᠼᠺᡈᠼ᠘ᡚᠨᡯᡥᡡ\x181Eᠼᠠ") + arrayList9[index1].ToString() + Class2.smethod_0("ᙽ\x169Fᚭᚤᚁᛃᚑᚶᛊᛖᛖᛚᛝᛏᛝᚸᛎᛧᛔᛢᛐᛛᛗᚔᚲᚖ") + ReporterLayer_id.ToString() + Class2.smethod_0("ᙽ") + str12;
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
            while (sqlDataReader.Read())
            {
              flag2 = true;
              str5 = !sqlDataReader.IsDBNull(0) ? sqlDataReader.GetString(0) : "";
              str6 = !sqlDataReader.IsDBNull(1) ? sqlDataReader.GetString(1) : "";
              str7 = !sqlDataReader.IsDBNull(2) ? sqlDataReader.GetString(2) : "";
              str8 = !sqlDataReader.IsDBNull(3) ? sqlDataReader.GetString(3) : "";
              str9 = !sqlDataReader.IsDBNull(4) ? sqlDataReader.GetString(4) : "";
              str11 = !sqlDataReader.IsDBNull(5) ? sqlDataReader.GetString(5) : "";
              sqlInt32_1 = (SqlInt32) sqlDataReader.GetInt32(6);
              sqlInt32_2 = (SqlInt32) sqlDataReader.GetInt32(7);
            }
            sqlDataReader.Close();
            if (flag2)
            {
              if (str10.Length == 0)
              {
                arrayList7.Add((object) str5);
                ThisArray4.Add((object) sqlInt32_1);
                str10 = str10 + str5 + Class2.smethod_0("ᙽᚿᛒ ") + str6 + Class2.smethod_0("ᙽᛕᛈᛔᛉᚂᚋᛒᛔᛒᛖᛋᛔᚓᚋ");
              }
              string str13;
              string str14;
              if (arrayList7.IndexOf((object) str5) != -1)
              {
                str13 = str7;
                str14 = str8;
                ThisArray4.Add((object) sqlInt32_2);
              }
              else
              {
                str13 = str5;
                str14 = str6;
                ThisArray4.Add((object) sqlInt32_1);
              }
              if (NameChangeList != null)
              {
                for (int index2 = 0; index2 < NameChangeList.Count; ++index2)
                {
                  DataPathConstructor.TableInfo nameChange = (DataPathConstructor.TableInfo) NameChangeList[index2];
                  if (nameChange.TableName == str13)
                  {
                    str13 = nameChange.TableAlias;
                    break;
                  }
                }
              }
              str10 = str10 + Class2.smethod_0("ᙽ") + str11 + Class2.smethod_0("ᙽ") + str13 + Class2.smethod_0("ᙽᚿᛒ ") + str14 + Class2.smethod_0("ᙽᛕᛈᛔᛉᚂᚋᛒᛔᛒᛖᛋᛔᚓᚋᚻᚻᚎᚗ") + str9 + Class2.smethod_0("ᚆ");
              arrayList7.Add((object) str13);
              arrayList8.Add(arrayList9[index1]);
            }
          }
          for (int index = 0; index < arrayList8.Count; ++index)
            arrayList9.Remove(arrayList8[index]);
        }
      }
      else if (TableList.Count == 1)
      {
        string tableAlias = arrayList3[0].ToString();
        string str5 = arrayList4[0].ToString();
        if (NameChangeList != null)
        {
          for (int index = 0; index < NameChangeList.Count; ++index)
          {
            DataPathConstructor.TableInfo nameChange = (DataPathConstructor.TableInfo) NameChangeList[index];
            if (nameChange.TableName == tableAlias)
            {
              tableAlias = nameChange.TableAlias;
              break;
            }
          }
        }
        str10 = tableAlias + Class2.smethod_0("ᙽᚿᛒ ") + str5 + Class2.smethod_0("ᙽᛕᛈᛔᛉᚂᚋᛒᛔᛒᛖᛋᛔᚓᚋ");
      }
      string list1 = this.ArrayToList(ThisArray1, Class2.smethod_0("ᚉᙾ"), "", "");
      string str15;
      if ((list1.Length <= 0 ? 1 : (IsIgnoreGrouping ? 1 : 0)) == 0)
      {
        str15 = Class2.smethod_0("ᙽᚥᚱᚯᚶᚲᚃᚦᚾᚆ") + list1;
        if (GroupType.Length > 0)
          str15 = str15 + Class2.smethod_0("ᙽᚵᚨᚴᚩᚂ") + GroupType;
      }
      else
        str15 = "";
      string str16 = this.ArrayToList(ThisArray3, Class2.smethod_0("ᚉᙾ"), "", "");
      if (str4.Length > 0)
      {
        if (str16.Length > 0)
          str16 += Class2.smethod_0("ᚉᙾ");
        str16 = str16 + Class2.smethod_0("ᚸᚱᚸᚳᛀᚥᚲᚲᚨᚧᚻᚫᚸᚮᚰᛉᚍᚫᚏ") + str4;
      }
      string str17 = this.ArrayToList(ThisArray2, Class2.smethod_0("ᙽ\x169Fᚭᚤᚁ"), Class2.smethod_0("ᚅ"), Class2.smethod_0("ᚆ"));
      if (str17.Length > 0)
        str17 = Class2.smethod_0("ᙽᚵᚧᚥᚳᚧᚃ") + str17;
      if (DirectWhereClause.Length > 0)
        str17 = str17.Length <= 0 ? Class2.smethod_0("ᙽᚵᚧᚥᚳᚧᚃ") + DirectWhereClause : str17 + Class2.smethod_0("ᙽ\x169Fᚭᚤᚁ") + DirectWhereClause;
      string str18 = "";
      if (!IsIgnoreGrouping)
      {
        if (ArgCondition.Length > 0)
          str18 = Class2.smethod_0("ᙽᚦᚠᚶᚪᚰᚪᚄ") + ArgCondition;
      }
      else if (ArgCondition.Length > 0)
        str17 = (str17.Length <= 0 ? Class2.smethod_0("ᙽᚵᚧᚥᚳᚧᚃ") : str17 + Class2.smethod_0("ᙽ\x169Fᚭᚤᚁ")) + ArgCondition;
      if (str3.Length > 0)
        str3 = Class2.smethod_0("ᙽᚭᚱᚤᚦᚴᚃᚦᚾᚆ") + str3;
      string str19 = "";
      if (str10.Length > 0)
        str19 = Class2.smethod_0("ᚰᚣᚫᚥᚤᚶᚃ") + (LimitRows == 0 ? "" : Class2.smethod_0("ᙽᚲᚮᚰᚁᚊ") + LimitRows.ToString() + Class2.smethod_0("ᚆᙾ")) + Class2.smethod_0("ᙽ") + str16 + Class2.smethod_0("ᙽᚤᚱᚯᚮᚂ") + str10 + Class2.smethod_0("ᙽ") + str17 + Class2.smethod_0("ᙽ") + str15 + Class2.smethod_0("ᙽ") + str18 + Class2.smethod_0("ᙽ") + str3;
      str1 = str19;
    }
    return str1;
  }

  public ArrayList UniqueArray(ArrayList SourceArray)
  {
    ArrayList arrayList = new ArrayList();
    for (int index = 0; index < SourceArray.Count; ++index)
    {
      if (arrayList.IndexOf(SourceArray[index]) == -1)
        arrayList.Add(SourceArray[index]);
    }
    return arrayList;
  }

  public string ArrayToList(ArrayList ThisArray, string Divider, string StartFrom, string EndWith)
  {
    string str = "";
    for (int index = 0; index < ThisArray.Count; ++index)
    {
      if (str.Length > 0)
        str += Divider;
      str = str + StartFrom + ThisArray[index].ToString() + EndWith;
    }
    return str;
  }

  public DataPathConstructor.TableInfo GetTableInfo(int Table_id)
  {
    DataPathConstructor.TableInfo tableInfo = new DataPathConstructor.TableInfo();
    SqlDataReader sqlDataReader = new SqlCommand(Class2.smethod_0("ᚰᚣᚫᚥᚤᚶᚃᚸᛆᛈᛓᛍᚷᛋᛘᛑᚙᚎᛃᛑᛓᛞᛘᚵᛡᛟᛘ᛫ᚙᛀᛍᛋᛊ\x169Eᛑᛥᛱᛱᛵᛸᛪᛸᛛᛩ᛫ᛶᛰ\x16FFᚭᜅᛸᜄ\x16F9ᚲᚻᜂᜄᜂᜆ\x16FBᜄᛃᚻᛳᛥᛣᛱᛥᛁ\x1716ᜌᜈᛅᛣᛇ") + Table_id.ToString(), this.ThisConnection).ExecuteReader();
    while (sqlDataReader.Read())
    {
      tableInfo.TableName = !sqlDataReader.IsDBNull(0) ? sqlDataReader.GetString(0) : "";
      tableInfo.TableAlias = !sqlDataReader.IsDBNull(1) ? sqlDataReader.GetString(1) : "";
    }
    sqlDataReader.Close();
    return tableInfo;
  }

  private struct Struct1
  {
    public int int_0;
    public string string_0;
    public string string_1;
  }

  public struct ConcatenateStruct
  {
    public string Prefix;
    public int ReporterObject_id;
    public int CodeLength;
  }

  public struct DataTables
  {
    public string TableIdent;
    public string TableName;
    public string TableAlias;
  }

  public struct JoinProcedureInfo
  {
    public int SourceTable_id;
    public int TargetTable_id;
    public bool IsSourceProcedure;
    public bool IsTargetProcedure;
  }

  public struct JoinRow
  {
    public int Join_id;
    public int Table_id;
    public int TableType;
  }

  public struct Joins
  {
    public int Join_id;
    public int SourceTable_id;
    public int TargetTable_id;
  }

  public struct OrderRow
  {
    public int Queue;
    public int Object_id;
    public string Method;
  }

  public class OrderRowComparer : IComparer
  {
    int IComparer.Compare(object x, object y)
    {
      return new CaseInsensitiveComparer().Compare((object) ((DataPathConstructor.OrderRow) y).Queue, (object) ((DataPathConstructor.OrderRow) x).Queue);
    }
  }

  public struct ProcedureParameter
  {
    public string QueryObjectName;
    public string ParameterName;
  }

  public struct TableDependRating
  {
    public int ReporterTable_id;
    public int Counter;
  }

  public struct TableInfo
  {
    public string TableName;
    public string TableAlias;
  }

  public struct TablePathStruct
  {
    public ArrayList JoinsList;
    public bool IsFound;
  }
}
