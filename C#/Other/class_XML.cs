﻿using System.Collections;
using System.Xml;

namespace Other
{
  internal class class_XML
  {
    public XmlDocument xmlDocument_0;

    public void method_0(string XML)
    {
      this.xmlDocument_0 = new XmlDocument();
      this.xmlDocument_0.LoadXml(XML);
    }

    public ArrayList method_1()
    {
      ArrayList arrayList = new ArrayList();
      string empty = string.Empty;
      foreach (XmlElement xmlElement in (XmlNode) this.xmlDocument_0["Query"])
      {
        if (xmlElement.Name == "Object" && xmlElement.Attributes["Name"] != null)
        {
          string str = xmlElement.Attributes["Name"].Value;
          arrayList.Add((object) str);
        }
      }
      return arrayList;
    }
  }
}
