﻿using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.Permissions;

[assembly: AssemblyTitle("LEAD WMS Engine")]
[assembly: AssemblyCompany("AMANAT")]
[assembly: ComVisible(false)]
[assembly: AssemblyDescription("Core assembly of LEAD WMS")]
[assembly: AssemblyCopyright("swd")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyTrademark("LEAD WMS")]
[assembly: AssemblyProduct("LEAD WMS")]
[assembly: AssemblyVersion("4.0.0.0")]
[assembly: PermissionSet(SecurityAction.RequestMinimum, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\r\nversion=\"1\">\r\n<IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\r\nversion=\"1\"\r\nFlags=\"SkipVerification\"/>\r\n</PermissionSet>\r\n")]
