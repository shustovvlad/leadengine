﻿using Microsoft.SqlServer.Server;
using Other;
using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Xml;

public partial  class LEADEngine
{

    private static bool bool_0 = false;
    private static Guid guid_0 = Guid.NewGuid();
    private object object_0 = new object();

    private void smethod_0(string MessageText)
    {
        EventLog eventLog = new EventLog();
        eventLog.Source = "LEAD WMS";
        eventLog.WriteEntry(MessageText, EventLogEntryType.Error);
        eventLog.Close();
    }

    internal string decode(string str)
    {
        if (str == null || str.Length == 0)
            return str;
        char[] charArray = str.ToCharArray();
        int num2 = 15;
        for (int index = 0; index < charArray.Length; ++index)
            charArray[index] = (char)((uint)charArray[index] - (uint)(ushort)(5725 + index));
        if (num2 + 7 == 5)
        {
            for (int index = 0; index < charArray.Length; ++index)
                charArray[index] = (char)((uint)charArray[index] - (uint)(ushort)(345 - index));
        }
        return new string(charArray);
    }

    public static int CountRowsForSQL_Inner(string SQLStatement)
    {
        SqlConnection connection = new SqlConnection("context connection = true");
        connection.Open();
        SqlCommand sqlCommand = new SqlCommand(SQLStatement.ToString(), connection);
        int num = 0;
        try
        {
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
            while (sqlDataReader.Read())
                ++num;
            sqlDataReader.Close();
        }
        catch (Exception ex)
        {
            num = -1;
            if (bool_0)
            {
                SqlContext.Pipe.Send("---------- Ошибка при выполнении CountRowsForSQL_Inner (начало) ----------");
                SqlContext.Pipe.Send(ex.Message);
                SqlContext.Pipe.Send("---------- Ошибка при выполнении CountRowsForSQL_Inner (конец) ----------");
            }
        }
        connection.Close();
        return num;
    }

    [Microsoft.SqlServer.Server.SqlProcedure]
    public static void ShowSessionGUID()
    {
        SqlContext.Pipe.Send(guid_0.ToString());
    }

    [SqlProcedure]
    public static void SetDebug(SqlBoolean IsDebug)
    {
        bool_0 = IsDebug.Value;
    }

    [SqlProcedure]
    public static void CountRowsForSQL(SqlString SQLStatement, out SqlInt32 RowCount)
    {
        RowCount = (SqlInt32)CountRowsForSQL_Inner(SQLStatement.ToString());
    }

    [SqlProcedure]
    public static void GetXMLCube(SqlXml ObjectXML, SqlInt32 ReporterFilter_id, SqlString DirectWhere, SqlBoolean IsIgnoreGrouping, out SqlString SQL, SqlInt32 ReporterLayer_id)
    {
        SQL = SqlString.Null;

        using (SqlConnection sqlConnection = new SqlConnection("context connection=true"))
        {
            sqlConnection.Open();
            DataPathConstructor dataPathConstructor = new DataPathConstructor();
            dataPathConstructor.ThisConnection = sqlConnection;
            
            SQL = (SqlString)dataPathConstructor.ConstructSQLFromXML(ObjectXML, ReporterFilter_id, DirectWhere.ToString(), IsIgnoreGrouping, ReporterLayer_id);
            
        }
    }

    [SqlProcedure]
    public static void CLRGetVariable(SqlString GUID, SqlInt32 ParameterType_id, out SqlString Value, out SqlBoolean IsExist, out SqlString FilterCondition, out SqlBoolean IsApplication, out SqlInt32 Priority)
    {
        Value = SqlString.Null;
        IsExist = SqlBoolean.False;
        FilterCondition = SqlString.Null;
        IsApplication = SqlBoolean.Null;
        Priority = SqlInt32.Null;
        SqlConnection connection = new SqlConnection("context connection = true");
        connection.Open();
        SqlCommand sqlCommand = new SqlCommand("SELECT" +
            " TOP 1 [Value], [FilterCondition], [IsApplication] = isnull([IsApplication], 0), [Priority] = isnull([Priority], 0) FROM [" + "temp_FunctionVariables_" +
            GUID.ToString() +
            "] with (nolock) WHERE ParameterType_id = " +
            ParameterType_id.ToString(), connection);
        try
        {
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
            while (sqlDataReader.Read())
            {
                Value = (SqlString)sqlDataReader.GetValue(0).ToString();
                IsExist = SqlBoolean.True;
                FilterCondition = !sqlDataReader.IsDBNull(1) ? (SqlString)sqlDataReader.GetValue(1).ToString() : (SqlString)"";
                IsApplication = !(sqlDataReader.GetValue(2).ToString() == "1") ? SqlBoolean.False : SqlBoolean.True;
                Priority = (SqlInt32)Convert.ToInt32(sqlDataReader.GetValue(3));
            }
            sqlDataReader.Close();
        }
        catch (Exception ex)
        {
            Value = SqlString.Null;
            SqlContext.Pipe.Send(ex.Message);
        }
    }

    [SqlProcedure]
    public static void CountRecords(SqlString TableName, out SqlInt32 RowCount)
    {
        RowCount = (SqlInt32)(-1);
        RowCount = (SqlInt32)0;
        SqlConnection connection = new SqlConnection("context connection = true");
        connection.Open();
        SqlCommand sqlCommand = new SqlCommand("SELECT" + " count(*) FROM [" + TableName.ToString() + "] with (nolock)", connection);
        try
        {
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
            while (sqlDataReader.Read())
                RowCount = (SqlInt32)sqlDataReader.GetInt32(0);
            sqlDataReader.Close();
        }
        catch
        {
            RowCount = (SqlInt32)(-1);
        }
        connection.Close();
    }

    [SqlProcedure]
    public static void GetTableColumn(SqlString TableName, SqlString KeyColumn, SqlString GetColumn, SqlString KeyValue, out SqlString Value)
    {
        Value = SqlString.Null;
        SqlConnection connection = new SqlConnection("context connection=true");
        connection.Open();
        string cmdText;
        //if ("YOUARESO" == "YOUARESO")
            cmdText = "SELECT [" + GetColumn.ToString() +
                        "] FROM [" +
                        TableName.ToString() +
                        "] with (nolock) WHERE [" +
                        KeyColumn.ToString() +
                        "] = '" +
                        KeyValue.ToString() + "'";
        //else
        //    cmdText = "SELECT [" +
        //                TableName.ToString() +
        //                "] FROM [" +
        //                GetColumn.ToString() +
        //                "] with (nolock) WHERE [" +
        //                KeyColumn.ToString() +
        //                "] = '" +
        //                KeyValue.ToString() +
        //                "'";
        SqlCommand sqlCommand = new SqlCommand(cmdText, connection);
        try
        {
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
            while (sqlDataReader.Read())
                Value = (SqlString)sqlDataReader.GetValue(0).ToString();
            sqlDataReader.Close();
        }
        catch
        {
            Value = SqlString.Null;
        }

    }


  [SqlProcedure]
  public static void ApplyFunction(SqlString FunctionName, SqlString InputValue, out SqlString OutputValue)
  {
    OutputValue = SqlString.Null;
    string str = !(InputValue == SqlString.Null ? true : false) ? "'" + 
            InputValue.ToString().Replace("'", 
            "''") + 
            "'" : "NULL";
    SqlConnection connection = new SqlConnection("context connection=true");
    connection.Open();
    SqlCommand sqlCommand = new SqlCommand("SELECT  [" +
        "RESULT" + "] = " + 
        FunctionName.ToString() + 
        "(" + str + 
        ")", connection);
    try
    {
      OutputValue = (SqlString) sqlCommand.ExecuteScalar().ToString();
    }
    catch
    {
      OutputValue = SqlString.Null;
    }
  }

  [SqlProcedure]
  public static void ExecuteTransaction(SqlGuid GUID, out SqlBoolean IsNormal, out SqlInt32 ErrorNumber, out SqlString ErrorMessage)
  {
    IsNormal = SqlBoolean.True;
    ErrorNumber = SqlInt32.Null;
    ErrorMessage = SqlString.Null;

    SqlConnection connection = new SqlConnection("context connection=true");
    connection.Open();
    SqlString sqlString = (SqlString) ("temp_Transaction_" + GUID.ToString());
    string str1 = "temp_TranProperties_" + GUID.ToString();
    SqlCommand sqlCommand = new SqlCommand(
        "SELECT a.SQL FROM [" + 
        sqlString.ToString() +
        "] as a with (nolock) LEFT JOIN Registers as b with (nolock) ON a.Register_id = b.tid ORDER BY b.ExecutionPriority ASC, a.tid ASC"
        , connection);
    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
        string str2 = "declare @IsNormal bit, @Retry int, @AsyncGUID varchar(50), @XMessage varchar(500), @XNormal bit; set transaction isolation level read committed; set deadlock_priority high; set @Retry = 5; set @IsNormal = 1; while (@Retry > 0) begin     begin try        begin transaction; ";
            
    while (sqlDataReader.Read())
      str2 = str2 + sqlDataReader.GetString(0) + " ";
    sqlDataReader.Close();
    string str3 = str2 +
            "       commit transaction;        exec GetTransactionProperty                        @TransactionGUID = '" + 
            GUID.ToString() +
            "',                        @PropertyName = 'AsyncGUID',                        @PropertyValue = @AsyncGUID output        if @AsyncGUID is NOT NULL and len(@AsyncGUID) > 0 begin            exec RunTransaction                        @GUID = @AsyncGUID,                        @ErrorMessage = @XMessage output,                        @IsNormal = @XNormal output            if isnull(@XNormal, 0) = 0 begin                insert into AsyncErrors (ErrorMessage, ProcedureName, RecordDate, DynamicSQL) values (@XMessage, 'Transaction', getdate(), @AsyncGUID)            end        end        set @Retry = 0;        goto finished " +
            "rollback_all:        rollback transaction        select [ERROR_NUMBER] = -1, [ERROR_MESSAGE] = 'Транзакция не может быть проведена!', [IS_NORMAL] = 0        set @IsNormal = 0" + 
            "finished:        set @Retry = 0;    end try    begin catch        if (ERROR_NUMBER() = 1205) begin            waitfor delay '00:00:01';            set @Retry = @Retry - 1;        end else begin            set @Retry = 0;        end;        if XACT_STATE() <> 0 rollback transaction;        if @Retry = 0 begin            select [ERROR_NUMBER] = ERROR_NUMBER(), [ERROR_MESSAGE] = ERROR_MESSAGE(), [IS_NORMAL] = 0           set @IsNormal = 0        end;    end catch; end;" +
            "if @IsNormal = 1 and object_id('" + 
            sqlString.ToString() +
            "') is NOT NULL exec StartJob @DynamicSQL = 'drop table [" +
            sqlString.ToString() +
            "]', @JobType = 'SYSTEM' if @IsNormal = 1 and object_id('" + 
            str1 +
            "') is NOT NULL exec StartJob @DynamicSQL = 'drop table [" + 
            str1 +
            "]', @JobType = 'SYSTEM' ";
    sqlCommand.CommandText = str3;
    try
    {
        sqlDataReader = sqlCommand.ExecuteReader();
    }
    catch (Exception ex)
    {
      IsNormal = SqlBoolean.False;
      ErrorMessage = (SqlString) ("Пакет транзакций содержит ошибки и не может быть проведен: " + ex.Message.ToString());
      ErrorNumber = (SqlInt32) (-1);
    }
    if (IsNormal == SqlBoolean.True)
    {
      while (sqlDataReader.Read())
      {
        for (int ordinal = 0; ordinal < sqlDataReader.FieldCount; ++ordinal)
        {
          if (sqlDataReader.GetName(ordinal) == "ERROR_NUMBER")
            ErrorNumber = (SqlInt32) Convert.ToInt32(sqlDataReader.GetValue(ordinal));
          if (sqlDataReader.GetName(ordinal) == "ERROR_MESSAGE")
            ErrorMessage = (SqlString) sqlDataReader.GetValue(ordinal).ToString();
          if (sqlDataReader.GetName(ordinal) == "IS_NORMAL")
            IsNormal = !(sqlDataReader.GetValue(ordinal).ToString() == "0") ? SqlBoolean.True : SqlBoolean.False;
        }
      }
      sqlDataReader.Close();
    }
  }

  [SqlProcedure]
  public static void RunTransaction(SqlGuid GUID, out SqlString ErrorMessage, out SqlBoolean IsNormal)
  {
    IsNormal = SqlBoolean.True;
    ErrorMessage = SqlString.Null;
    SqlConnection connection = new SqlConnection("context connection=true");
    connection.Open();
    SqlCommand sqlCommand = new SqlCommand("SELECT" + " a.SQL FROM [" +  ("temp_Transaction_" + GUID.ToString()).ToString() + "] as a with (nolock) LEFT JOIN Registers as b with (nolock) ON a.Register_id = b.tid ORDER BY b.ExecutionPriority ASC, a.tid ASC", connection);
    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
    string str = "";
    while (sqlDataReader.Read())
      str = str + sqlDataReader.GetString(0) + " ";
    sqlDataReader.Close();
    if (str.Length > 0)
    {
      sqlCommand.CommandText = str;
      try
      {
        sqlCommand.ExecuteNonQuery();
      }
      catch (Exception ex)
      {
        IsNormal = SqlBoolean.False;
        ErrorMessage = (SqlString) ("RunTransaction (" + GUID.ToString() + "). Пакет транзакций содержит ошибки: " + ex.Message.ToString());
      }
    }
    else
      IsNormal = SqlBoolean.True;
  }

  [SqlProcedure]
  public static void CLR_SetVariable(SqlGuid GUID, SqlInt32 ParameterType_id, SqlString Value, out SqlBoolean IsError, SqlString FilterCondition, SqlInt32 Priority, SqlInt32 IsApplication)
  {
    IsError = SqlBoolean.Null;
    SqlConnection connection = new SqlConnection("context connection=true");
    connection.Open();
    SqlCommand sqlCommand = new SqlCommand("SELECT RefTableName, ColumnName = isnull(RefColumnMemoName, RefColumnShowName), PutFunction, FormType FROM " +
        "ParameterTypes" +
        " WHERE tid = " + ParameterType_id.ToString(), connection);
    SqlDataReader sqlDataReader1 = sqlCommand.ExecuteReader();
    SqlString sqlString1 = SqlString.Null;
    SqlString sqlString2 = SqlString.Null;
    SqlString sqlString3 = SqlString.Null;
    while (sqlDataReader1.Read())
    {
      sqlString1 = (SqlString) sqlDataReader1.GetValue(0).ToString();
      sqlString3 = (SqlString) sqlDataReader1.GetValue(1).ToString();
      sqlString2 = (SqlString) sqlDataReader1.GetValue(2).ToString();
      SqlString sqlString4 = (SqlString) sqlDataReader1.GetValue(3).ToString();
    }
    sqlDataReader1.Close();
    SqlInt32 sqlInt32_1 = (SqlInt32) 0;
    sqlCommand.CommandText = "SELECT cnt = count(*) FROM [" + 
            sqlString1.ToString() +
            "] with (nolock) WHERE [" + 
            sqlString3.ToString() +
            "] = '" + Value.ToString().Replace("'", "''") + "'";
    SqlDataReader sqlDataReader2 = sqlCommand.ExecuteReader();
    while (sqlDataReader2.Read())
      sqlInt32_1 = (SqlInt32) sqlDataReader2.GetInt32(0);
    sqlDataReader2.Close();
    if (sqlInt32_1 == (SqlInt32) 0)
    {
      IsError = SqlBoolean.True;
    }
    else
    {
      IsError = SqlBoolean.False;
      if (sqlString2.ToString().Length > 0)
      {
        sqlCommand.CommandText = "SELECT ThisResult = " + 
                    sqlString2.ToString() + 
                    "('" + 
                    Value.ToString().Replace("'", "''") + 
                    "')";
        SqlDataReader sqlDataReader3 = sqlCommand.ExecuteReader();
        while (sqlDataReader3.Read())
          Value = (SqlString) sqlDataReader3.GetString(0);
        sqlDataReader3.Close();
      }
      SqlInt32 sqlInt32_2 = (SqlInt32) 0;
      string str1 = "temp_FunctionVariables_" + GUID.ToString();
      sqlCommand.CommandText = "SELECT cnt = count(*) FROM sysobjects with (nolock) WHERE [name] = '"
                 + str1 + "'";
      SqlDataReader sqlDataReader4 = sqlCommand.ExecuteReader();
      while (sqlDataReader4.Read())
        sqlInt32_2 = (SqlInt32) sqlDataReader4.GetInt32(0);
      sqlDataReader4.Close();
      if (sqlInt32_2 == (SqlInt32) 0)
      {
        sqlCommand.CommandText = "CREATE TABLE [" + str1 + "] ([ParameterType_id] int not null, [Value] varchar(1000) null, [FilterCondition] varchar(10) null, [Priority] int null, [IsApplication] int null)";
        sqlCommand.ExecuteNonQuery();
      }
      Value = !(Value == SqlString.Null ? true : false) ? (SqlString) ("'" + Value.ToString().Replace("'", "''") + "'") : (SqlString) "NULL";
      FilterCondition = !(FilterCondition == SqlString.Null ? true : false) ? (SqlString) ("'" + FilterCondition.ToString() + "'") : (SqlString) "NULL";
      string str2 = !(IsApplication == SqlInt32.Null ? true : false) ? IsApplication.ToString() : "NULL";
      string str3 = !(Priority == SqlInt32.Null ? true : false) ? Priority.ToString() : "NULL";
      sqlCommand.CommandText = "begin transaction update [" + 
                str1.ToString() +
                "] set [Value] = " + 
                Value.ToString() +
                "] set [Value] = " + 
                FilterCondition.ToString() +
                ", [Priority] = " + str3 +
                ", [IsApplication] = " + str2 +
                " where ParameterType_id = " + 
                ParameterType_id.ToString() +
                " if @@rowcount = 0 insert into [ " +
                str1.ToString() +
                " ] (ParameterType_id, [Value], FilterCondition, Priority, IsApplication) VALUES ( " + 
                ParameterType_id.ToString() + 
                ", " + 
                Value.ToString() + 
                ", " + 
                FilterCondition.ToString() + 
                ", " + str3 + ", " + str2 +
                " ); commit transaction ";
      sqlCommand.ExecuteNonQuery();
    }
  }

  [SqlProcedure]
  public static void SetTransactionProperty(SqlGuid TransactionGUID, SqlString PropertyName, SqlString PropertyValue)
  {

    SqlConnection connection = new SqlConnection("context connection=true");
    connection.Open();
    PropertyValue = !PropertyValue.IsNull ? (SqlString) ("'" + PropertyValue.ToString().Replace("'", "''") + "'") : (SqlString) "NULL";
    SqlCommand sqlCommand = new SqlCommand("begin transaction update [temp_TranProperties_" + 
                                            TransactionGUID.ToString() +
                                            "] with (tablockx) set PropertyValue = " + 
                                            PropertyValue.ToString() +
                                            " where PropertyName = '" + 
                                            PropertyName.ToString() +
                                            "' if @@rowcount = 0 begin insert into [temp_TranProperties_" + 
                                            TransactionGUID.ToString() +
                                            "] (PropertyName, PropertyValue) VALUES ('" + 
                                            PropertyName.ToString() + 
                                            "', " + 
                                            PropertyValue.ToString() +
                                            ") end commit transaction "
                                            , connection);
    try
    {
      sqlCommand.ExecuteNonQuery();
    }
    catch
    {
      SqlContext.Pipe.Send("Error on transaction property set");
    }
  }

  [SqlProcedure]
  public static void GetWaveCodeOnce(SqlInt32 WaveType_id, SqlString SQLStatement, out SqlString WaveCode)
  {
    WaveCode = SqlString.Null;
    SqlConnection connection = new SqlConnection("context connection=true");
    connection.Open();
    SqlCommand sqlCommand = 
            new SqlCommand(
                "SELECT b.ObjectName, ValueLength = isnull(a.ValueLength, 200) " +
                " FROM WaveTypeParameters as a with (nolock) JOIN ReporterObjects as b with (nolock) ON a.ReporterObject_id = b.tid " +
                " WHERE a.WaveType_id = "
                + WaveType_id.ToString(), connection);
    SqlDataReader sqlDataReader1 = sqlCommand.ExecuteReader();
    ArrayList arrayList = new ArrayList();
    while (sqlDataReader1.Read())
    {
      string str = sqlDataReader1.GetValue(0).ToString();
      int int32 = sqlDataReader1.GetInt32(1);
      arrayList.Add((object) new LEADEngine.Struct4()
      {
        string_0 = str,
        int_0 = int32
      });
    }
    sqlDataReader1.Close();
    sqlCommand.CommandText = SQLStatement.ToString();
    SqlDataReader sqlDataReader2 = sqlCommand.ExecuteReader();
    if (sqlDataReader2.Read())
    {
      string str1 = "";
      for (int index1 = 0; index1 < arrayList.Count; ++index1)
      {
        LEADEngine.Struct4 struct4 = (LEADEngine.Struct4) arrayList[index1];
        string str2 = sqlDataReader2[struct4.string_0].ToString();
        int int0 = struct4.int_0;
        if (str2.Length > int0)
          str2 = str2.Substring(0, int0);
        else if (str2.Length < int0)
        {
          string str3 = "";
          for (int index2 = 0; index2 < int0 - str2.Length; ++index2)
            str3 += "0";
          str2 = str3 + str2;
        }
        str1 += str2;
      }
      WaveCode = !("NOTALONE" == "NOTALONE") ? (SqlString) str1.Length.ToString() : (SqlString) str1;
    }
    sqlDataReader2.Close();
    connection.Close();
  }

  [SqlProcedure]
  public static void GetWaveCode(SqlInt32 WaveType_id, SqlString SQLStatement)
  {
    SqlConnection connection = new SqlConnection("context connection=true");
    connection.Open();
    string str1 = connection.Database;
    if (str1.Length < 3)
      str1 = "context";
    SqlCommand sqlCommand = new SqlCommand("SELECT b.ObjectName, ValueLength = isnull(a.ValueLength, 200) " 
        + " FROM WaveTypeParameters as a with (nolock) JOIN ReporterObjects as b with (nolock) ON a.ReporterObject_id = b.tid " + 
        " WHERE a.WaveType_id = " + 
        WaveType_id.ToString(), connection);
    bool flag1 = true;
    flag1 = str1[2] != 'D';
    SqlDataReader sqlDataReader1 = sqlCommand.ExecuteReader();
    flag1 = str1[0] != 'C';
    ArrayList arrayList = new ArrayList();
    while (sqlDataReader1.Read())
    {
      string str2 = sqlDataReader1.GetValue(0).ToString();
      int int32 = sqlDataReader1.GetInt32(1);
      arrayList.Add((object) new LEADEngine.Struct4()
      {
        string_0 = str2,
        int_0 = int32
      });
    }
    sqlDataReader1.Close();
    bool flag2 = str1[1] != 'P';
    SqlDataRecord record = new SqlDataRecord(new SqlMetaData[2]
    {
      new SqlMetaData("GroupCode", SqlDbType.NVarChar, 3000L, 1033L, SqlCompareOptions.None),
      new SqlMetaData("GroupCondition", SqlDbType.NVarChar, 3000L, 1033L, SqlCompareOptions.None)
    });
    SqlContext.Pipe.SendResultsStart(record);
    sqlCommand.CommandText = SQLStatement.ToString();
    SqlDataReader sqlDataReader2 = sqlCommand.ExecuteReader();
    while (sqlDataReader2.Read())
    {
      string str2 = "";
      string str3 = "";
      for (int index1 = 0; index1 < arrayList.Count; ++index1)
      {
        LEADEngine.Struct4 struct4 = (LEADEngine.Struct4) arrayList[index1];
        string str4 = sqlDataReader2[struct4.string_0].ToString();
        int int0 = struct4.int_0;
        if (str4.Length > int0)
          str4 = str4.Substring(0, int0);
        else if (str4.Length < int0)
        {
          string str5 = "";
          for (int index2 = 0; index2 < int0 - str4.Length; ++index2)
            str5 += "0";
          str4 = str5 + str4;
        }
        str2 += str4;
      }
      for (int ordinal = 0; ordinal < sqlDataReader2.FieldCount; ++ordinal)
      {
        if (str3.Length > 0)
          str3 += " AND ";
        str3 = str3 + 
                    "{" + 
                    sqlDataReader2.GetName(ordinal) + 
                    "} = '" + 
                    sqlDataReader2.GetValue(ordinal).ToString() + 
                    "'";
      }
      record.SetString(0, str2);
      record.SetString(1, str3);
      SqlContext.Pipe.SendResultsRow(record);
    }
    sqlDataReader2.Close();
    if (!flag2)
      SqlContext.Pipe.SendResultsEnd();
    connection.Close();
  }

  [SqlProcedure]
  public static void ForceInsert(SqlString LocalCommand, SqlString ConnectionString, SqlString TargetTable, SqlString TargetPrepare, out SqlString LastError, SqlString FinishCommand)
  {
    LastError = SqlString.Null;
    bool flag1 = true;
    ArrayList arrayList = new ArrayList();
    SqlConnection sqlConnection = new SqlConnection("context connection=true");
    sqlConnection.ConnectionString = ConnectionString.ToString();
    SqlConnection connection = new SqlConnection("context connection=true");
    try
    {
      connection.Open();
      sqlConnection.Open();
    }
    catch (Exception ex)
    {
      LastError = (SqlString) ex.Message;
      flag1 = false;
    }
    string str1 = connection.Database;
    if (str1.Length < 3)
      str1 = connection.ConnectionString;
    bool flag2 = false;
    string str2 = "";
    flag2 = str1[1] == 'P';
    bool flag3 = true;
    if (!flag1)
      return;
    flag2 = str1[0] == 'C';
    SqlCommand sqlCommand1 = new SqlCommand(LocalCommand.ToString(), connection);
    SqlCommand sqlCommand2 = new SqlCommand();
    bool flag4 = str1[2] == 'D';
    try
    {
      SqlDataReader sqlDataReader = sqlCommand1.ExecuteReader();
      while (sqlDataReader.Read())
      {
        string str3 = "";
        for (int ordinal = 0; ordinal < sqlDataReader.FieldCount; ++ordinal)
        {
          if (flag3)
          {
            if (str2.Length > 0)
              str2 += ", ";
            str2 = str2 + "[" + sqlDataReader.GetName(ordinal) + "]";
          }
          if (str3.Length > 0)
            str3 += ", ";
          str3 = !sqlDataReader.IsDBNull(ordinal) ? str3 + "'" + sqlDataReader.GetValue(ordinal).ToString().Replace("'", "") + "'" : str3 + "NULL";
        }
        string str4;
        if (!flag4)
          str4 = "INSERT INTO " + TargetTable.ToString() + " (" + str2 + ") VALUES (" + str3 + ")";
        else
          str4 = "DECLARE @a DATETIME; SET @a = getdate(); PRINT @a";
        arrayList.Add((object) str4);
        flag3 = false;
      }
      sqlDataReader.Close();
      sqlCommand2.Connection = sqlConnection;
      for (int index = 0; index < arrayList.Count; ++index)
      {
        if (TargetPrepare.IsNull)
          sqlCommand2.CommandText = arrayList[index].ToString();
        else
          sqlCommand2.CommandText = 
                        TargetPrepare.ToString() + 
                        " " + 
                        arrayList[index].ToString();
        
          sqlCommand2.ExecuteNonQuery();
      }
    }
    catch (Exception ex)
    {
      LastError = (SqlString) ex.Message;
      flag1 = false;
    }
    if (flag1 && !FinishCommand.IsNull)
    {
      sqlCommand1.Connection = connection;
      sqlCommand1.CommandText = FinishCommand.ToString();
      try
      {
        sqlCommand1.ExecuteNonQuery();
      }
      catch (Exception ex)
      {
        LastError = (SqlString) ex.Message;
      }
    }
    sqlConnection.Close();
    connection.Close();
  }

  [SqlProcedure]
  public static void ForceInsertBatch(SqlString LocalCommand, SqlString ConnectionString, SqlString TargetTable, SqlString TargetPrepare, out SqlString LastError, SqlString FinishCommand)
  {
    LastError = SqlString.Null;
    bool flag1 = true;
    ArrayList arrayList = new ArrayList();
    SqlConnection sqlConnection = new SqlConnection();
    sqlConnection.ConnectionString = ConnectionString.ToString();
    SqlTransaction sqlTransaction = (SqlTransaction) null;
    SqlConnection connection = new SqlConnection("context connection=true");
    try
    {
      connection.Open();
      sqlConnection.Open();
    }
    catch (Exception ex)
    {
      LastError = (SqlString) ex.Message;
      flag1 = false;
    }
    string str1 = connection.Database;
    if (str1.Length < 3)
      str1 = "Connection";
    bool flag2 = false;
    string str2 = "";
    bool flag3 = true;
    flag2 = str1[2] == 'D';
    if (!flag1)
      return;
    SqlCommand sqlCommand1 = new SqlCommand(LocalCommand.ToString(), connection);
    SqlCommand sqlCommand2 = new SqlCommand();
    try
    {
      flag2 = str1[0] == 'C';
      SqlDataReader sqlDataReader = sqlCommand1.ExecuteReader();
      while (sqlDataReader.Read())
      {
        string str3 = "";
        for (int ordinal = 0; ordinal < sqlDataReader.FieldCount; ++ordinal)
        {
          if (flag3)
          {
            if (str2.Length > 0)
              str2 += ", ";
            str2 = str2 + "[" + sqlDataReader.GetName(ordinal) + "]";
          }
          if (str3.Length > 0)
            str3 += ", ";
          str3 = !sqlDataReader.IsDBNull(ordinal) ? str3 + "'" + sqlDataReader.GetValue(ordinal).ToString().Replace("'", "") + "'" : str3 + "NULL";
        }
        string str4 = "INSERT INTO" + 
                    " " + 
                    TargetTable.ToString() + 
                    " (" + str2 +
                    ") VALUES (" + str3 + ")";
        arrayList.Add((object) str4);
        flag3 = false;
      }
      sqlDataReader.Close();
      bool flag4 = str1[1] == 'P';
      sqlCommand2.Connection = sqlConnection;
      sqlTransaction = sqlConnection.BeginTransaction();
      for (int index = 0; index < arrayList.Count; ++index)
      {
        sqlCommand2.Transaction = sqlTransaction;
        if (TargetPrepare.IsNull)
          sqlCommand2.CommandText = arrayList[index].ToString();
        else
          sqlCommand2.CommandText = TargetPrepare.ToString() + " " + arrayList[index].ToString();
        if (!flag4)
          sqlCommand2.ExecuteNonQuery();
      }
      
      sqlTransaction.Commit();
    }
    catch (Exception ex)
    {
      if (sqlTransaction != null)
        sqlTransaction.Rollback();
      LastError = (SqlString) ex.Message;
      flag1 = false;
    }
    if (flag1 && !FinishCommand.IsNull)
    {
      sqlCommand1.Connection = connection;
      sqlCommand1.CommandText = FinishCommand.ToString();
      try
      {
        sqlCommand1.ExecuteNonQuery();
      }
      catch (Exception ex)
      {
        LastError = (SqlString) ex.Message;
      }
    }
    sqlConnection.Close();
    connection.Close();
  }

  [SqlProcedure]
  public static void ExecCommand(SqlString ConnectionString, SqlString CommandString, SqlBoolean ReturnResult, out SqlString LastError)
  {
    LastError = SqlString.Null;
    bool flag = true;
    SqlConnection connection = new SqlConnection();
    try
    {
      connection.ConnectionString = ConnectionString.ToString();
      connection.Open();
    }
    catch (Exception ex)
    {
      LastError = (SqlString) ex.Message;
      flag = false;
    }
    if (!flag)
      return;
    SqlCommand sqlCommand = new SqlCommand(CommandString.ToString(), connection);
    try
    {
      if (!ReturnResult)
      {
        sqlCommand.ExecuteNonQuery();
      }
      else
      {
        SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
        do
        {
          SqlMetaData[] sqlMetaDataArray = new SqlMetaData[sqlDataReader.FieldCount];
          for (int ordinal = 0; ordinal < sqlDataReader.FieldCount; ++ordinal)
            sqlMetaDataArray[ordinal] = new SqlMetaData(sqlDataReader.GetName(ordinal), SqlDbType.NVarChar, 4000L, 1033L, SqlCompareOptions.None);
          SqlDataRecord record = new SqlDataRecord(sqlMetaDataArray);
          SqlContext.Pipe.SendResultsStart(record);
          while (sqlDataReader.Read())
          {
            for (int ordinal = 0; ordinal < sqlDataReader.FieldCount; ++ordinal)
            {
              if (!sqlDataReader.IsDBNull(ordinal))
                record.SetString(ordinal, sqlDataReader.GetValue(ordinal).ToString());
              else
                record.SetDBNull(ordinal);
            }
            SqlContext.Pipe.SendResultsRow(record);
          }
          SqlContext.Pipe.SendResultsEnd();
        }
        while (sqlDataReader.NextResult());
        sqlDataReader.Close();
      }
    }
    catch (Exception ex)
    {
      LastError = (SqlString) ex.Message;
    }
    connection.Close();
  }

  [SqlProcedure]
  public static void ResultToFile(SqlString FilePath, SqlString FileName, SqlString ColumnDivider, SqlString RowDivider, SqlString RecordsetDivider, SqlBoolean IsIncludeNames, SqlString SQL)
  {
    string str = Guid.NewGuid().ToString() + ".lead";
    FileStream fileStream = System.IO.File.Create(FilePath.ToString() + 
        "\\" + str);
    SqlConnection connection = new SqlConnection("context connection=true");
    connection.Open();
    SqlDataReader sqlDataReader = new SqlCommand(SQL.ToString(), connection).ExecuteReader();
    byte[] bytes1 = new UTF8Encoding(true).GetBytes(RowDivider.ToString());
    byte[] bytes2 = new UTF8Encoding(true).GetBytes(RecordsetDivider.ToString());
    int num = 0;
    do
    {
      bool flag = true;
      if (num > 0)
        goto label_19;
label_16:
      while (sqlDataReader.Read())
      {
        if ((SqlBoolean) flag && IsIncludeNames)
        {
          for (int ordinal = 0; ordinal < sqlDataReader.FieldCount; ++ordinal)
          {
            string s = sqlDataReader.GetName(ordinal).Replace(ColumnDivider.ToString(), "").Replace(RowDivider.ToString(), "").Replace(RecordsetDivider.ToString(), "");
            if (ordinal > 0)
              s = ColumnDivider.ToString() + s;
            byte[] bytes3 = new UTF8Encoding(true).GetBytes(s);
            fileStream.Write(bytes3, 0, bytes3.Length);
          }
          fileStream.Write(bytes1, 0, bytes1.Length);
          flag = false;
        }
        for (int ordinal = 0; ordinal < sqlDataReader.FieldCount; ++ordinal)
        {
          string s = !sqlDataReader.IsDBNull(ordinal) ? sqlDataReader.GetValue(ordinal).ToString().Replace(ColumnDivider.ToString(), "").Replace(RowDivider.ToString(), "").Replace(RecordsetDivider.ToString(), "") : "";
          if (ordinal > 0)
            s = ColumnDivider.ToString() + s;
          byte[] bytes3 = new UTF8Encoding(true).GetBytes(s);
          fileStream.Write(bytes3, 0, bytes3.Length);
        }

        fileStream.Write(bytes1, 0, bytes1.Length);
      }
      ++num;
      continue;
label_19:
      fileStream.Write(bytes2, 0, bytes2.Length);
      goto label_16;
    }
    while (sqlDataReader.NextResult());
    fileStream.Flush();
    fileStream.Close();
    connection.Close();
    //if (!(LEADEngine.smethod_7(1U, 8, 0U) == LEADEngine.smethod_0("ᚱᚣᚱᚭᚰᚵᚷᚥ")))
    //  return;
    if (System.IO.File.Exists(FilePath.ToString() + "\\" + FileName.ToString()))
      System.IO.File.Delete(FilePath.ToString() + "\\" + FileName.ToString());
    System.IO.File.Move(FilePath.ToString() + "\\" + str, FilePath.ToString() + "\\" + FileName.ToString());
  }

  private static SqlMetaData smethod_9(DataTableReader MetaReader)
  {
    SqlMetaData sqlMetaData = (SqlMetaData) null;
    string upper = ((SqlDbType) MetaReader["ProviderType"]).ToString().ToUpper();
    if (upper == "BIGINT")
      sqlMetaData = new SqlMetaData(MetaReader["ColumnName"].ToString(), SqlDbType.BigInt);
    else if (upper == "VARCHAR")
      sqlMetaData = new SqlMetaData(MetaReader["ColumnName"].ToString(), SqlDbType.VarChar, 
          (long) Convert.ToInt32(
              MetaReader["ColumnSize"]), 1033L, SqlCompareOptions.None);
    else if (upper == "BIT")
      sqlMetaData = new SqlMetaData(MetaReader["ColumnName"].ToString(), SqlDbType.Bit);
    else if (upper == "CHAR")
      sqlMetaData = new SqlMetaData(MetaReader["ColumnName"].ToString(), SqlDbType.Char, 
          (long) Convert.ToInt32(MetaReader["ColumnSize"]), 1033L, SqlCompareOptions.None);
    else if (upper == "DATE")
      sqlMetaData = new SqlMetaData(MetaReader["ColumnName"].ToString(), SqlDbType.Date);
    else if (upper == "DATETIME")
      sqlMetaData = new SqlMetaData(MetaReader["ColumnName"].ToString(), SqlDbType.DateTime);
    else if (upper == "DATETIME2")
      sqlMetaData = new SqlMetaData(MetaReader["ColumnName"].ToString(), SqlDbType.DateTime2);
    else if (upper == "DECIMAL")
      sqlMetaData = new SqlMetaData(MetaReader["ColumnName"].ToString(), SqlDbType.Decimal, Convert.ToByte(MetaReader["NumericPrecision"]), Convert.ToByte(MetaReader["NumericScale"]));
    else if (upper == "FLOAT")
      sqlMetaData = new SqlMetaData(MetaReader["ColumnName"].ToString(), SqlDbType.Float, Convert.ToByte(MetaReader["NumericPrecision"]), Convert.ToByte(MetaReader["NumericScale"]));
    else if (upper == "INT")
      sqlMetaData = new SqlMetaData(MetaReader["ColumnName"].ToString(), SqlDbType.Int);
    else if (upper == "MONEY")
      sqlMetaData = new SqlMetaData(MetaReader["ColumnName"].ToString(), SqlDbType.Money);
    else if (upper == "NCHAR")
      sqlMetaData = new SqlMetaData(MetaReader["ColumnName"].ToString(), SqlDbType.NChar, (long) Convert.ToInt32(MetaReader["ColumnSize"]), 1033L, SqlCompareOptions.None);
    else if (upper == "NUMERIC")
      sqlMetaData = new SqlMetaData(MetaReader["ColumnName"].ToString(), SqlDbType.Decimal, Convert.ToByte(MetaReader["NumericPrecision"]), Convert.ToByte(MetaReader["NumericScale"]));
    else if (upper == "NVARCHAR")
      sqlMetaData = new SqlMetaData(MetaReader["ColumnName"].ToString(), SqlDbType.NVarChar, (long) Convert.ToInt32(MetaReader["ColumnSize"]), 1033L, SqlCompareOptions.None);
    else if (upper == "REAL")
      sqlMetaData = new SqlMetaData(MetaReader["ColumnName"].ToString(), SqlDbType.Real, Convert.ToByte(MetaReader["NumericPrecision"]), Convert.ToByte(MetaReader["NumericScale"]));
    else if (upper == "SMALLINT")
      sqlMetaData = new SqlMetaData(MetaReader["ColumnName"].ToString(), SqlDbType.SmallInt);
    else if (upper == "SMALLMONEY")
      sqlMetaData = new SqlMetaData(MetaReader["ColumnName"].ToString(), SqlDbType.SmallMoney);
    else if (upper == "TINYINT")
      sqlMetaData = new SqlMetaData(MetaReader["ColumnName"].ToString(), SqlDbType.TinyInt);
    else if (upper == "UNIQUEIDENTIFIER")
      sqlMetaData = new SqlMetaData(MetaReader["ColumnName"].ToString(), SqlDbType.UniqueIdentifier);
    return sqlMetaData;
  }

  [SqlProcedure]
  public static void RetrieveRecordset(SqlString SQLStatement, SqlString ObjectGUID)
  {
    SqlConnection connection = new SqlConnection("context connection=true");
    connection.Open();
    SqlCommand sqlCommand = new SqlCommand(
        "SELECT b.ObjectName FROM [temp_ObjectQuery_" 
        + ObjectGUID.ToString() + 
        "] as a with (nolock) " +
        " JOIN ReporterObjects as b with (nolock) ON a.ReporterObject_id = b.tid ORDER BY a.tid ASC ", connection);
    SqlDataReader sqlDataReader1 = sqlCommand.ExecuteReader();
    ArrayList arrayList = new ArrayList();
    while (sqlDataReader1.Read())
      arrayList.Add((object) sqlDataReader1.GetValue(0).ToString());
    sqlDataReader1.Close();
    string database = connection.Database;
    bool flag1 = false;
    SqlMetaData[] sqlMetaDataArray = new SqlMetaData[arrayList.Count];
    SqlDataRecord record = (SqlDataRecord) null;
    int num = -1;
    bool flag2 = false;
    sqlCommand.CommandText = SQLStatement.ToString();
    SqlDataReader sqlDataReader2 = sqlCommand.ExecuteReader();
    while (sqlDataReader2.Read())
    {
      if (!flag2)
      {
        DataTableReader dataReader = sqlDataReader2.GetSchemaTable().CreateDataReader();
        while (dataReader.Read())
        {
          int index = arrayList.IndexOf(dataReader["ColumnName"]);
          if (index >= 0)
            sqlMetaDataArray[index] = LEADEngine.smethod_9(dataReader);
        }
        record = new SqlDataRecord(sqlMetaDataArray);
        SqlContext.Pipe.SendResultsStart(record);
        flag2 = true;
      }
      if (flag2)
      {
        for (int ordinal1 = 0; ordinal1 < sqlDataReader2.FieldCount; ++ordinal1)
        {
          num = -1;
          int ordinal2 = arrayList.IndexOf((object) sqlDataReader2.GetName(ordinal1));
          if ((ordinal2 <= -1 ? 1 : (sqlDataReader2.IsDBNull(ordinal1) ? 1 : 0)) == 0)
          {
            if (!flag1)
              record.SetValue(ordinal2, sqlDataReader2.GetValue(ordinal1));
            else
              record.SetDBNull(ordinal2);
          }
          else if ((ordinal2 <= -1 ? 1 : (!sqlDataReader2.IsDBNull(ordinal1) ? 1 : 0)) == 0)
            record.SetDBNull(ordinal2);
        }
        SqlContext.Pipe.SendResultsRow(record);
      }
    }
    if (flag2)
      SqlContext.Pipe.SendResultsEnd();
    sqlDataReader2.Close();
    connection.Close();
  }

  [SqlProcedure]
  public static void XMLRetrieveRecordset(SqlString SQLStatement, SqlXml ObjectXML)
  {
    SqlConnection connection = new SqlConnection("context connection=true");
    connection.Open();
    class_XML class1 = new class_XML();
    class1.method_0(ObjectXML.ToString());
    ArrayList arrayList = class1.method_1();
    SqlMetaData[] sqlMetaDataArray = new SqlMetaData[arrayList.Count];
    SqlDataRecord record = (SqlDataRecord) null;
    int num = -1;
    bool flag = false;
    SqlDataReader sqlDataReader = new SqlCommand(SQLStatement.ToString(), connection).ExecuteReader();
    while (sqlDataReader.Read())
    {
      if (!flag)
      {
        DataTableReader dataReader = sqlDataReader.GetSchemaTable().CreateDataReader();
        while (dataReader.Read())
        {
          int index = arrayList.IndexOf(dataReader["ColumnName"]);
          if (index >= 0)
            sqlMetaDataArray[index] = LEADEngine.smethod_9(dataReader);
        }
        record = new SqlDataRecord(sqlMetaDataArray);
        SqlContext.Pipe.SendResultsStart(record);
        flag = true;
      }
      if (flag)
      {
        for (int ordinal1 = 0; ordinal1 < sqlDataReader.FieldCount; ++ordinal1)
        {
          num = -1;
          int ordinal2 = arrayList.IndexOf((object) sqlDataReader.GetName(ordinal1));
          if ((ordinal2 <= -1 ? 1 : (sqlDataReader.IsDBNull(ordinal1) ? 1 : 0)) == 0)
            record.SetValue(ordinal2, sqlDataReader.GetValue(ordinal1));
          else if ((ordinal2 <= -1 ? 1 : (!sqlDataReader.IsDBNull(ordinal1) ? 1 : 0)) == 0)
            record.SetDBNull(ordinal2);
        }
        SqlContext.Pipe.SendResultsRow(record);
      }
    }
    if (flag)
      SqlContext.Pipe.SendResultsEnd();
    sqlDataReader.Close();
    connection.Close();
  }

  [SqlProcedure]
  public static void GetTransactionProperty(SqlGuid TransactionGUID, SqlString PropertyName, out SqlString PropertyValue)
  {
    PropertyValue = SqlString.Null;
    SqlConnection connection = new SqlConnection("context connection=true");
    connection.Open();
    SqlCommand sqlCommand = new SqlCommand(
        "SELECT PropertyValue FROM [temp_TranProperties_" + 
        TransactionGUID.ToString() +
        "] with (nolock) WHERE PropertyName = '" + 
        PropertyName.ToString() + "'", 
        connection);
    try
    {
      SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
      while (sqlDataReader.Read())
        PropertyValue = (SqlString) sqlDataReader.GetString(0);
      sqlDataReader.Close();
    }
    catch
    {
      PropertyValue = SqlString.Null;
    }
  }

  [SqlProcedure]
  public static void CreateQueryObject(SqlString TableGUID, SqlString ObjectName, SqlString SortType, SqlInt32 SortPriority, SqlInt32 ReporterLayer_id)
  {
    SqlConnection sqlConnection = new SqlConnection("context connection=true");
    sqlConnection.Open();
    string database = sqlConnection.Database;
    new DataPathConstructor()
    {
      ThisConnection = sqlConnection
    }.CreateQueryObject(TableGUID, ObjectName, SortType, SortPriority, ReporterLayer_id);
    sqlConnection.Close();
  }

  [SqlProcedure]
  public static void GetObjectsSQL(SqlString ObjectsTable, SqlString Condition, SqlString GroupType, SqlString DirectWhere, out SqlString ErrorMessage, out SqlBoolean IsNormal, out SqlString SQLStatement, SqlBoolean IsIgnoreGrouping, SqlInt32 ReporterLayer_id)
  {
    ErrorMessage = SqlString.Null;
    IsNormal = SqlBoolean.Null;
    SQLStatement = SqlString.Null;
    SqlConnection sqlConnection = new SqlConnection("context connection=true");
    sqlConnection.Open();
    DataPathConstructor dataPathConstructor = new DataPathConstructor();
    dataPathConstructor.ThisConnection = sqlConnection;

      string ErrorMessage1;
      bool IsNormal1;
      SQLStatement = (SqlString) dataPathConstructor.
                ConstructSQLFromData("temp_ObjectQuery_" + 
                ObjectsTable.ToString(), 
                Condition.ToString(), 
                GroupType.ToString(), 
                DirectWhere.ToString(), 
                out ErrorMessage1, 
                out IsNormal1, 
                (bool) IsIgnoreGrouping, 
                (int) ReporterLayer_id);
      ErrorMessage = (SqlString) ErrorMessage1;
      IsNormal = (SqlBoolean) IsNormal1;
    
    sqlConnection.Close();
  }

  [SqlProcedure]
  public static void DropQuerySchema(SqlString SchemaGUID)
  {
    SqlConnection connection = new SqlConnection("context connection=true");
    SqlCommand sqlCommand = new SqlCommand("DROP TABLE [temp_ObjectQuery_" + SchemaGUID.ToString() + "]", connection);
    try
    {
      sqlCommand.ExecuteNonQuery();
    }
    catch
    {
    }
    connection.Close();
  }

  [SqlProcedure]
  public static void GetUserQuerySQL(SqlInt32 UserQuery_id, out SqlString SQLStatement, SqlInt32 ReporterLayer_id)
  {
    SQLStatement = SqlString.Null;
      SqlConnection sqlConnection = new SqlConnection("context connection=true");
      sqlConnection.Open();
      SQLStatement = (SqlString) new DataPathConstructor()
      {
        ThisConnection = sqlConnection
      }.ConstructSQLFromQuery((int) UserQuery_id, -1, "", (SqlBoolean) false);
      sqlConnection.Close();
  }

  public static ArrayList DivideToArray(string SourceString, string Divider)
  {
    ArrayList arrayList = new ArrayList();
    string str1 = "";
    for (int index = 0; index < SourceString.Length; ++index)
    {
      char ch = SourceString[index];
      if (ch.ToString() != Divider)
      {
        string str2 = str1;
        ch = SourceString[index];
        string str3 = ch.ToString();
        str1 = str2 + str3;
      }
      else
      {
        arrayList.Add((object) str1.Trim().ToUpper());
        str1 = "";
      }
    }
    if (str1.Trim().Length > 0)
      arrayList.Add((object) str1.Trim().ToUpper());
    return arrayList;
  }

  [SqlProcedure]
  public static void CreateTransactionBySchema(SqlInt32 TransactionType_id, SqlInt32 User_id, SqlInt32 ParentTransaction_id, SqlInt32 ParentTable_id, SqlInt32 ParentRow, SqlXml ObjectSchema, out SqlString LastError, out SqlInt32 Transaction_id)
  {
    LastError = SqlString.Null;
    Transaction_id = SqlInt32.Null;
    SqlConnection sqlConnection = new SqlConnection("context connection=true");
    sqlConnection.Open();
    SqlCommand sqlCommand = new SqlCommand();
    sqlCommand.Connection = sqlConnection;
    sqlCommand.CommandType = CommandType.Text;
    ArrayList arrayList = new ArrayList();
    sqlCommand.CommandText =
            "SELECT TOP 1 ParametersTable FROM TransactionTypes with (nolock) WHERE tid = " + 
            TransactionType_id.ToString();
    string str1;
    try
    {
      str1 = sqlCommand.ExecuteScalar().ToString();
    }
    catch (Exception ex)
    {
      LastError = (SqlString) (
                "Некорректно указан тип транзакции ID = " + 
                TransactionType_id.ToString() +
                " (" + ex.Message + ")");
      return;
    }
    sqlCommand.CommandText =
            "SELECT a.ParameterName, a.ParameterType_id, a.ParameterColumn, a.StoreColumn, a.IsNullable, b.IS_NULLABLE FROM TransactionTypeParameters as a with (nolock) JOIN INFORMATION_SCHEMA.COLUMNS as b with (nolock) ON a.ParameterColumn = b.COLUMN_NAME AND b.TABLE_NAME = '"
            + str1 +
            "' WHERE a.TransactionType_id = " + TransactionType_id.ToString();
    LEADEngine.Struct2 struct2_1;
    try
    {
      SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
      while (sqlDataReader.Read())
      {
        struct2_1 = new LEADEngine.Struct2();
        struct2_1.sqlString_0 = (SqlString) sqlDataReader.GetString(0);
        struct2_1.sqlInt32_0 = (SqlInt32) sqlDataReader.GetInt32(1);
        struct2_1.sqlString_1 = (SqlString) sqlDataReader.GetString(2);
        if (!sqlDataReader.IsDBNull(3))
          struct2_1.sqlString_2 = (SqlString) sqlDataReader.GetString(3);
        struct2_1.sqlInt32_1 = sqlDataReader.IsDBNull(4) ? (SqlInt32) 0 : (SqlInt32) sqlDataReader.GetInt32(4);
        struct2_1.bool_1 = 
                    (sqlDataReader.GetString(5) == "YES");
        arrayList.Add((object) struct2_1);
      }
      sqlDataReader.Close();
    }
    catch (Exception ex)
    {
      LastError = (SqlString) (
               "Некорректно указан тип транзакции ID = " + TransactionType_id.ToString() + " (" + ex.Message + ")");
      return;
    }
    XmlDocument xmlDocument = new XmlDocument();
    try
    {
      xmlDocument.LoadXml(ObjectSchema.Value);
    }
    catch (Exception ex)
    {
      LastError = (SqlString) ("Некорректный XML на входе (" + ex.Message + ")");
      return;
    }
    SqlInt32 sqlInt32_1 = SqlInt32.Null;
    SqlString sqlString1 = SqlString.Null;
    SqlString sqlString2 = SqlString.Null;
    string str2 = "";
    string str3 = "";
    foreach (XmlElement xmlElement in (XmlNode) xmlDocument["ObjectSchema"])
    {
      if (xmlElement.Name == "Object")
      {
        sqlInt32_1 = SqlInt32.Null;
        SqlString sqlString3 = SqlString.Null;
        if (xmlElement.Attributes["ParameterType_id"] != null)
        {
          SqlInt32 sqlInt32_2;
          try
          {
            sqlInt32_2 = SqlInt32.Parse(
                xmlElement.Attributes["ParameterType_id"].Value);
          }
          catch
          {
            sqlInt32_1 = SqlInt32.Null;
            LastError = (SqlString) ("Некорректное значение атрибута ParameterType_id = "
                             + xmlElement.Attributes["ParameterType_id"].Value.ToString());
            return;
          }
          if (xmlElement.Attributes["Value"] != null)
            sqlString3 = (SqlString) xmlElement.Attributes["Value"].Value;
          sqlCommand.CommandType = CommandType.Text;
          sqlCommand.CommandText = "SELECT RefTableName, RefColumnShowName, RefColumnMemoName, VisualType, NumberGenerator_id FROM " 
                        + "ParameterTypes" +
                        " with (nolock) WHERE tid = " + sqlInt32_2.ToString();
          SqlDataReader sqlDataReader1 = sqlCommand.ExecuteReader();
          string str4 = "";
          string str5 = "";
          string str6 = "";
          string str7 = "";
          string str8 = "";
          while (sqlDataReader1.Read())
          {
            if (!sqlDataReader1.IsDBNull(0))
              str4 = sqlDataReader1.GetValue(0).ToString();
            if (!sqlDataReader1.IsDBNull(1))
              str5 = sqlDataReader1.GetValue(1).ToString();
            if (!sqlDataReader1.IsDBNull(2))
              str6 = sqlDataReader1.GetValue(2).ToString();
            if (!sqlDataReader1.IsDBNull(3))
              str7 = sqlDataReader1.GetValue(3).ToString();
            if (!sqlDataReader1.IsDBNull(4))
              str8 = sqlDataReader1.GetValue(4).ToString();
          }
          sqlDataReader1.Close();
          SqlString sqlString4 = SqlString.Null;
          if (str7 == "vtReference")
          {
            sqlCommand.CommandType = CommandType.Text;
            sqlCommand.CommandText = "SELECT TOP 1 " + str5 +
                            " FROM " + str4 + " with (nolock) WHERE " + 
                            str6 +
                            " = '" + 
                            sqlString3.ToString().Replace("'", "''") + "'";
            try
            {
              SqlDataReader sqlDataReader2 = sqlCommand.ExecuteReader();
              while (sqlDataReader2.Read())
              {
                if (!sqlDataReader2.IsDBNull(0))
                  sqlString4 = (SqlString) sqlDataReader2.GetValue(0).ToString();
              }
              sqlDataReader2.Close();
            }
            catch
            {
              LastError = (SqlString) ("Определение типа представления данных " + 
                                sqlInt32_2.ToString() +
                                " выполнено с ошибками. Исправьте референсные параметры!");
              return;
            }
          }
          else if ((!(str7 == "vtString") || str8.Length <= 0 ? 1 : (!sqlString3.IsNull ? 1 : 0)) == 0)
          {
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandText = "GenerateNumber";
            SqlParameter sqlParameter1 = new SqlParameter();
            sqlParameter1.ParameterName = "@NumberGenerator_id";
            sqlParameter1.SqlDbType = SqlDbType.Int;
            sqlParameter1.Direction = ParameterDirection.Input;
            sqlParameter1.Value = (object) Convert.ToInt32(str8);
            sqlCommand.Parameters.Add(sqlParameter1);
            SqlParameter sqlParameter2 = new SqlParameter();
            sqlParameter2.ParameterName = "@GeneratorValue";
            sqlParameter2.SqlDbType = SqlDbType.VarChar;
            sqlParameter2.Size = 4000;
            sqlParameter2.Direction = ParameterDirection.Output;
            sqlCommand.Parameters.Add(sqlParameter2);
            try
            {
              sqlCommand.ExecuteNonQuery();
            }
            catch
            {
              LastError = (SqlString) (
                                "Выявлена некорректная работа генератора номеров ID = " + str8);
              return;
            }
            sqlString3 = (SqlString) sqlCommand.Parameters["@GeneratorValue"].Value.ToString();
            sqlCommand.Parameters.Clear();
          }
          for (int index = 0; index < arrayList.Count; ++index)
          {
            struct2_1 = (LEADEngine.Struct2) arrayList[index];
            if (struct2_1.sqlInt32_0 == sqlInt32_2)
            {
              if (str2.Length > 0)
                str2 += ", ";
              if (str3.Length > 0)
                str3 += ", ";
              str2 = str2 + "[" + struct2_1.sqlString_1.ToString() + "]";
              str3 = !sqlString3.IsNull ? str3 + "'" + sqlString3.ToString().Replace("'", "''") + "'" : str3 + "NULL";
              if (!struct2_1.sqlString_2.IsNull)
              {
                str2 = str2 + ", [" + struct2_1.sqlString_2.ToString() + "]";
                str3 = !sqlString4.IsNull ? str3 + ", '" + sqlString4.ToString().Replace("'", "''") + "'" : str3 + ", NULL";
              }
              struct2_1.bool_0 = true;
              arrayList[index] = (object) struct2_1;
            }
          }
        }
      }
    }
    LastError = (SqlString) "";
    for (int index = 0; index < arrayList.Count; ++index)
    {
      LEADEngine.Struct2 struct2_2 = (LEADEngine.Struct2) arrayList[index];
      if ((!struct2_2.bool_1 || struct2_2.sqlInt32_1.ToString() == "0" ? (struct2_2.bool_0 ? 1 : 0) : 1) == 0)
      {
        if (LastError.ToString().Length > 0)
          LastError += (SqlString) ", ";
        LastError += (SqlString) ("\"" + struct2_2.sqlString_0.ToString() + "\"");
      }
    }
    if (LastError.ToString().Length > 0)
    {
      LastError = (SqlString) (
                "Не заполнены обязательные поля: " + LastError.ToString());
    }
    else
    {
      sqlCommand.CommandType = CommandType.Text;
      string str4 = !ParentTransaction_id.IsNull ? ParentTransaction_id.ToString() : "NULL";
      string str5 = !ParentRow.IsNull ? ParentRow.ToString() : "NULL";
      string str6 = !ParentTable_id.IsNull ? ParentTable_id.ToString() : "NULL";
      sqlCommand.CommandText =
                "DECLARE @temp TABLE (Row_id INT NOT NULL) INSERT INTO Transactions (TransactionType_id, TransactionGUID, TransactionStatus, [User_id], ParentTransaction_id, ParentTable_id, ParentRow, TransactionCaption, CreationDate) OUTPUT inserted.tid INTO @temp (Row_id) VALUES (" + 
                TransactionType_id.ToString() +
                " , NewId(), 0,  " + 
                User_id.ToString() + ", " + 
                str4 + ", " + 
                str6 + ", " + 
                str5 +
                ", NULL, GetDate()) SELECT TOP 1 Row_id FROM @temp"
                ;
      try
      {
        Transaction_id = (SqlInt32) Convert.ToInt32(sqlCommand.ExecuteScalar());
      }
      catch (Exception ex)
      {
        LastError = (SqlString) ("Ошибка создания транзакции (" + ex.Message + ")");
        return;
      }
      sqlCommand.CommandText = "INSERT INTO " + str1 + " (Transaction_id, " + str2 + ") VALUES (" + Transaction_id.ToString() + ", " + str3 + ")";
      try
      {
        sqlCommand.ExecuteNonQuery();
      }
      catch (Exception ex)
      {
        LastError = (SqlString) ("Ошибка заполнения таблицы заголовков транзакции (" + ex.Message + ")");
        return;
      }
      sqlConnection.Close();
    }
  }

  public static string ParameterShowValue(SqlConnection ThisConnection, int ParameterType_id, string ParameterValue)
  {
    string str1 = ParameterValue;
    string str2 = "";
    string str3 = "";
    string str4 = "";
    string str5 = "";
    string str6 = "";
    SqlCommand sqlCommand = (SqlCommand) null;
    try
    {
      sqlCommand = new SqlCommand("SELECT RefTableName, RefColumnMemoName, RefColumnShowName, VisualType, GetFunction FROM " +
          " ParameterTypes " +
          " with (nolock) WHERE tid = " + ParameterType_id.ToString(), ThisConnection);
      SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
      while (sqlDataReader.Read())
      {
        str2 = sqlDataReader.GetString(0);
        str3 = sqlDataReader.GetString(1);
        str4 = sqlDataReader.GetString(2);
        str5 = sqlDataReader.GetString(3);
        str6 = sqlDataReader.GetString(4);
      }
      sqlDataReader.Close();
    }
    catch
    {
    }
    if (str5 == "vtReference")
    {
      try
      {
        sqlCommand.CommandText = 
                    "SELECT TOP 1 [" 
                    + str4 + 
                    "] FROM [" 
                    + str2 + 
                    "] WHERE [" + 
                    str3 + "] = '" + ParameterValue + "'";
        str1 = sqlCommand.ExecuteScalar().ToString();
      }
      catch
      {
      }
    }
    if (str6.Length > 0)
    {
      try
      {
        sqlCommand.CommandText = "SELECT [RESULT] = " + str6 + "('" + str1 + "')";
        str1 = sqlCommand.ExecuteScalar().ToString();
      }
      catch
      {
      }
    }
    return str1;
  }

  [SqlProcedure]
  public static void XMLFinishAction(SqlString ActionSystemCode, SqlInt32 Action_id)
  {
    SqlConnection connection = new SqlConnection("context connection=true");
    connection.Open();
    SqlCommand sqlCommand = new SqlCommand(
        "SELECT tid FROM ActionTypes with (nolock) WHERE SystemCode = '" + ActionSystemCode.ToString() + "'", connection);
    SqlInt32 sqlInt32;
    try
    {
      sqlInt32 = (SqlInt32) Convert.ToInt32(sqlCommand.ExecuteScalar());
    }
    catch
    {
      sqlInt32 = (SqlInt32) (-1);
    }
    if (sqlInt32 == (SqlInt32) (-1))
    {
      SqlContext.Pipe.Send(
          "Вид события с кодом \"" + 
          ActionSystemCode.ToString() +
          "\" не обнаружен в справочнике!");
      connection.Close();
    }
    else
    {
      sqlCommand.CommandText = "SELECT cnt = count(*) FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'mov_Actions_" + 
                sqlInt32.ToString() + "'";
      if (Convert.ToInt32(sqlCommand.ExecuteScalar()) == 0)
      {
        SqlContext.Pipe.Send("Событие с идентификатором " + 
            Action_id.ToString() +
            ", относящееся к коду \"" + 
            ActionSystemCode.ToString() +
            "\", не существует");
        connection.Close();
      }
      else
      {
        sqlCommand.CommandText = "UPDATE [mov_Actions_" + sqlInt32.ToString() +
                    "] SET EndAction = getdate() WHERE tid = " + Action_id.ToString();
        sqlCommand.ExecuteNonQuery();
        connection.Close();
      }
    }
  }

  [SqlProcedure]
  public static void XMLStartAction(SqlString ActionSystemCode, SqlXml ActionParameters, out SqlInt32 Action_id)
  {
    SqlConnection connection = new SqlConnection("context connection=true");
    connection.Open();
    SqlCommand sqlCommand = new SqlCommand(
        "SELECT tid FROM ActionTypes with (nolock) WHERE SystemCode = '" + ActionSystemCode.ToString() + "'", connection);
    SqlInt32 sqlInt32_1;
    try
    {
      sqlInt32_1 = (SqlInt32) Convert.ToInt32(sqlCommand.ExecuteScalar());
    }
    catch
    {
      sqlInt32_1 = (SqlInt32) (-1);
    }
    if (sqlInt32_1 == (SqlInt32) (-1))
    {
      SqlContext.Pipe.Send(
          "Вид события с кодом \"" 
          + ActionSystemCode.ToString() +
          "\" не обнаружен в справочнике!");
      connection.Close();
      Action_id = SqlInt32.Null;
    }
    else
    {
      sqlCommand.CommandText = "SELECT cnt = count(*) FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'mov_Actions_" + 
                sqlInt32_1.ToString() + "'";
      SqlInt32 int32 = (SqlInt32) Convert.ToInt32(sqlCommand.ExecuteScalar());
      ArrayList arrayList = new ArrayList();
      sqlCommand.CommandText =
                "select [ColumnDefinition] = '[Column_' + cast(a.ParameterType_id as varchar(20)) + '] ' + dbo.GetDataType(c.VisualType) + ' NULL' from ActionTypeParameters as a with (nolock) left join INFORMATION_SCHEMA.COLUMNS as b with (nolock) on 'Column_' + cast(a.ParameterType_id as varchar(20)) = b.COLUMN_NAME and b.TABLE_NAME = 'mov_Actions_" + 
                sqlInt32_1.ToString() +
                "' join " +
                " ParameterTypes " +
                " as c with (nolock) on a.ParameterType_id = c.tid where a.ActionType_id = " + 
                sqlInt32_1.ToString() +
                " and b.COLUMN_NAME is NULL";
      SqlDataReader sqlDataReader1 = sqlCommand.ExecuteReader();
      string str1 = "";
      bool flag = false;
      while (sqlDataReader1.Read())
      {
        flag = true;
        string str2 = sqlDataReader1.GetString(0);
        if (int32 > (SqlInt32) 0)
        {
          str1 = str1 + "ALTER TABLE [mov_Actions_" + 
              sqlInt32_1.ToString() + "] ADD " + str2 +
                       "; GO; ";
        }
        else
        {
          if (str1.Length > 0)
            str1 += ", ";
          str1 += str2;
        }
      }
      sqlDataReader1.Close();
      if (flag)
      {
        if (int32 == (SqlInt32) 0)
          str1 = "CREATE TABLE [mov_Actions_" + sqlInt32_1.ToString() +
                        "] (tid int not null identity(0,1) primary key, StartAction datetime not null default getdate(), EndAction datetime null, ActionGUID uniqueidentifier, PlannedEndDate datetime null, DateDiff bigint null, " + str1 + ")";
        sqlCommand.CommandText = str1;
        sqlCommand.ExecuteNonQuery();
      }
      XmlDocument xmlDocument = new XmlDocument();
      xmlDocument.LoadXml(ActionParameters.Value);
      string upper = Guid.NewGuid().ToString().ToUpper();
      string str3 = "[ActionGUID], [StartAction], [PlannedEndDate]";
      string str4 = "'" + upper + "', getdate(), &&PLANNED&&";
      string str5 = "";
      foreach (XmlElement xmlElement in (XmlNode) xmlDocument["ObjectSchema"])
      {
        if (xmlElement.Name == "Object")
        {
          SqlInt32 sqlInt32_2 = (SqlInt32) (-1);
          if (xmlElement.Attributes["ParameterName"] != null)
          {
            string str2 = xmlElement.Attributes["ParameterName"].Value.Replace("'", "");
            sqlCommand.CommandText =
                            "SELECT TOP 1 ParameterType_id FROM ActionTypeParameters with (nolock) WHERE ActionType_id = " + 
                            sqlInt32_1.ToString() +
                            " AND CustomName = '" + str2 + "'";
            try
            {
              sqlInt32_2 = (SqlInt32) Convert.ToInt32(sqlCommand.ExecuteScalar());
            }
            catch
            {
            }
          }
          else if (xmlElement.Attributes["RefType"] != null)
          {
            string str2 = xmlElement.Attributes["RefType"].Value.Replace("'", "");
            sqlCommand.CommandText = "SELECT TOP 1 tid FROM " +
                            " ParameterTypes " + 
                            " with (nolock) WHERE RefType = '" + str2 + "'";
            try
            {
              sqlInt32_2 = (SqlInt32) Convert.ToInt32(sqlCommand.ExecuteScalar());
            }
            catch
            {
              sqlInt32_2 = (SqlInt32) (-1);
            }
          }
          else if (xmlElement.Attributes["ParameterType_id"] != null)
          {
            try
            {
              sqlInt32_2 = (SqlInt32) Convert.ToInt32(xmlElement.Attributes["ParameterType_id"].Value);
            }
            catch
            {
              sqlInt32_2 = (SqlInt32) (-1);
            }
          }
          if ((SqlBoolean) (xmlElement.Attributes["Value"] != null) && sqlInt32_2 != (SqlInt32) (-1))
          {
            string str2 = xmlElement.Attributes["Value"].Value.Replace("'", "");
            if (str3.Length > 0)
              str3 += ", ";
            str3 = str3 + "[Column_" + sqlInt32_2.ToString() + "]";
            if (str4.Length > 0)
              str4 += ", ";
            str4 = str4 + "'" + str2.Replace("'", "") + "'";
            if (str5.Length > 0)
              str5 += " AND ";
            str5 = str5 + "[Column_" + sqlInt32_2.ToString() +
                            "] = '" + str2 + "'";
          }
        }
      }
      int num1 = 0;
      int num2 = -1;
      if (str5.Length > 0)
      {
        string str2 = "SELECT COUNT(*), AVG([DateDiff]) FROM [mov_Actions_"
                    + sqlInt32_1.ToString() +
                    "] with (nolock) WHERE [DateDiff] is NOT NULL AND" + str5;
        sqlCommand.CommandText = str2;
        SqlDataReader sqlDataReader2 = sqlCommand.ExecuteReader();
        while (sqlDataReader2.Read())
        {
          num1 = sqlDataReader2.GetInt32(0);
          if (!sqlDataReader2.IsDBNull(1))
            num2 = sqlDataReader2.GetInt32(1);
        }
        sqlDataReader2.Close();
      }
      if (num1 == 0)
        num2 = 0;
      string str6 = str4.Replace("&&PLANNED&&", "dateadd(second, ") + num2.ToString() + ", getdate())";
      string message =
                "DECLARE @temp TABLE (tid int not null); INSERT INTO [mov_Actions_" + 
                sqlInt32_1.ToString() + "] (" + 
                str3 +
                ") OUTPUT inserted.tid INTO @temp (tid) VALUES (" + str6 + "); SELECT TOP 1 tid FROM @temp;";
      SqlContext.Pipe.Send(message);
      sqlCommand.CommandText = message;
      Action_id = (SqlInt32) Convert.ToInt32(sqlCommand.ExecuteScalar());
      connection.Close();
    }
  }

  [SqlProcedure]
  public static void XMLPostEvent(SqlString EventSystemCode, SqlXml EventParameters)
  {
    SqlConnection sqlConnection = new SqlConnection("context connection=true");
    sqlConnection.Open();
    SqlCommand sqlCommand = new SqlCommand("SELECT tid FROM EventTypes with (nolock) WHERE SystemCode = '" 
        + EventSystemCode.ToString() + 
        "'", sqlConnection);
    SqlInt32 sqlInt32_1;
    try
    {
      sqlInt32_1 = (SqlInt32) Convert.ToInt32(sqlCommand.ExecuteScalar());
    }
    catch
    {
      sqlInt32_1 = (SqlInt32) (-1);
    }
    if (sqlInt32_1 == (SqlInt32) (-1))
    {
      SqlContext.Pipe.Send("SELECT tid FROM EventTypes with (nolock) WHERE SystemCode = '" + EventSystemCode.ToString() + "\" не обнаружен в справочнике!");
      sqlConnection.Close();
    }
    else
    {
      sqlCommand.CommandText = "SELECT cnt = count(*) FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'mov_Events_" + 
                sqlInt32_1.ToString() + 
                "'";
      SqlInt32 int32 = (SqlInt32) Convert.ToInt32(sqlCommand.ExecuteScalar());
      ArrayList arrayList1 = new ArrayList();
      sqlCommand.CommandText = "select [ColumnDefinition] = '[Column_' + cast(a.ParameterType_id as varchar(20)) + '] ' + dbo.GetDataType(c.VisualType) + ' NULL' from EventTypeParameters as a with (nolock) left join INFORMATION_SCHEMA.COLUMNS as b with (nolock) on 'Column_' + cast(a.ParameterType_id as varchar(20)) = b.COLUMN_NAME and b.TABLE_NAME = 'mov_Events_" + 
                sqlInt32_1.ToString() +
                "' join " +
                " ParameterTypes " +
                " as c with (nolock) on a.ParameterType_id = c.tid where a.EventType_id = " + 
                sqlInt32_1.ToString() +
                " and b.COLUMN_NAME is NULL";
      string message1 = "";
      bool flag = false;
      using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
      {
        while (sqlDataReader.Read())
        {
          flag = true;
          string str = sqlDataReader.GetString(0);
          if (int32 > (SqlInt32) 0)
          {
            message1 = message1 +
                            "ALTER TABLE [mov_Events_" + 
                            sqlInt32_1.ToString() +
                            "] ADD " + 
                            str +
                            "; GO; ";
          }
          else
          {
            if (message1.Length > 0)
              message1 += ", ";
            message1 += str;
          }
        }
        sqlDataReader.Close();
      }
      if (flag)
      {
        if (int32 == (SqlInt32) 0)
          message1 = "CREATE TABLE [mov_Events_" + sqlInt32_1.ToString() + "] (tid int not null identity(0,1) primary key, EventDate datetime not null default getdate(), EventGUID uniqueidentifier, " + message1 + ")";
        SqlContext.Pipe.Send(message1);
        sqlCommand.CommandText = message1;
        sqlCommand.ExecuteNonQuery();
      }
      ArrayList arrayList2 = new ArrayList();
      sqlCommand.CommandText =
                "SELECT a.ContactGroup_id, b.ProcedureName, a.ObsoleteMinutes, c.TemplateName, c.[Message], c.tid FROM EventNotificationsGroups as a with (nolock) JOIN NotificationTypes as b with (nolock) ON a.NotificationType_id = b.tid JOIN NotificationTemplates as c with (nolock) ON a.NotificationTemplate_id = c.tid WHERE a.EventType_id = " + 
                sqlInt32_1.ToString() +
                "  AND a.StartDate <= getdate() AND isnull(a.EndDate, '2999-12-31') >= getdate()";
      using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
      {
        while (sqlDataReader.Read())
          arrayList2.Add((object) new LEADEngine.Struct3()
          {
            int_0 = sqlDataReader.GetInt32(0),
            string_0 = sqlDataReader.GetString(1),
            int_1 = sqlDataReader.GetInt32(2),
            string_1 = sqlDataReader.GetString(3),
            int_2 = sqlDataReader.GetInt32(4),
            string_2 = sqlDataReader.GetString(4)
          });
        sqlDataReader.Close();
      }
      XmlDocument xmlDocument = new XmlDocument();
      xmlDocument.LoadXml(EventParameters.Value);
      string upper = Guid.NewGuid().ToString().ToUpper();
      string str1 = "[EventGUID]";
      string str2 = "'" + upper + "'";
      foreach (XmlElement xmlElement in (XmlNode) xmlDocument["ObjectSchema"])
      {
        if (xmlElement.Name == "Object")
        {
          SqlInt32 sqlInt32_2 = (SqlInt32) (-1);
          if (xmlElement.Attributes["ParameterName"] != null)
          {
            string str3 = xmlElement.Attributes["ParameterName"].Value.Replace("'", "");
            sqlCommand.CommandText =
                            "SELECT TOP 1 ParameterType_id FROM EventTypeParameters with (nolock) WHERE EventType_id = " + sqlInt32_1.ToString() +
                                " AND CustomName = '" + str3 + "'";
            try
            {
              sqlInt32_2 = (SqlInt32) Convert.ToInt32(sqlCommand.ExecuteScalar());
            }
            catch
            {
              sqlInt32_2 = (SqlInt32) (-1);
            }
          }
          else if (xmlElement.Attributes["RefType"] != null)
          {
            string str3 = xmlElement.Attributes["RefType"].Value.Replace("'", "");
            sqlCommand.CommandText = "SELECT TOP 1 tid FROM " +
                            " ParameterTypes " +
                            " with (nolock) WHERE RefType = '" + str3 + "'";
            try
            {
              sqlInt32_2 = (SqlInt32) Convert.ToInt32(sqlCommand.ExecuteScalar());
            }
            catch
            {
              sqlInt32_2 = (SqlInt32) (-1);
            }
          }
          else if (xmlElement.Attributes["ParameterType_id"] != null)
          {
            try
            {
              sqlInt32_2 = (SqlInt32) Convert.ToInt32(xmlElement.Attributes["ParameterType_id"].Value);
            }
            catch
            {
              sqlInt32_2 = (SqlInt32) (-1);
            }
          }
          if ((SqlBoolean) (xmlElement.Attributes["Value"] != null) && sqlInt32_2 != (SqlInt32) (-1))
          {
            string ParameterValue = xmlElement.Attributes["Value"].Value;
            LEADEngine.ParameterShowValue(sqlConnection, sqlInt32_2.Value, ParameterValue);
            if (str1.Length > 0)
              str1 += ", ";
            str1 = str1 + "[Column_" + sqlInt32_2.ToString() + "]";
            if (str2.Length > 0)
              str2 += ", ";
            str2 = str2 + "'" + ParameterValue.Replace("'", "") + "'";
          }
        }
      }
      string message2 = "INSERT INTO [mov_Events_" + sqlInt32_1.ToString() + 
                "] (" + str1 + ") VALUES (" + str2 + ")";
      SqlContext.Pipe.Send(message2);
      sqlCommand.CommandText = message2;
      sqlCommand.ExecuteNonQuery();
      string str4 =
                "INSERT INTO SubscriberNews (EventType_id, EventGUID) VALUES (" + sqlInt32_1.ToString() + ", '" + upper + "')";
      sqlCommand.CommandText = str4;
      sqlCommand.ExecuteNonQuery();
      sqlConnection.Close();
    }
  }

    [SqlProcedure]
    public static void FieldOfRecordset(SqlString SQL, SqlString FieldName)
    {
        ArrayList array = LEADEngine.DivideToArray(FieldName.ToString(),
            ",");

        SqlConnection connection = new SqlConnection("context connection=true");
        connection.Open();
        SqlMetaData[] sqlMetaDataArray = new SqlMetaData[array.Count];
        SqlDataRecord record = (SqlDataRecord)null;
        SqlDataReader sqlDataReader = new SqlCommand(SQL.ToString(), connection).ExecuteReader();


        bool flag = false;
        while (sqlDataReader.Read())
        {
            if (!flag)
            {
                DataTableReader dataReader = sqlDataReader.GetSchemaTable().CreateDataReader();
                while (dataReader.Read())
                {
                    for (int index = 0; index < array.Count; ++index)
                    {
                        if (array[index].ToString().ToUpper() == dataReader["ColumnName"].ToString().ToUpper())
                            sqlMetaDataArray[index] = LEADEngine.smethod_9(dataReader);
                        else if (array[index].ToString() == "NULL")
                            sqlMetaDataArray[index] = new SqlMetaData("Column" + index.ToString(), SqlDbType.VarChar, 200L, 1033L, SqlCompareOptions.None);
                    }
                }
                record = new SqlDataRecord(sqlMetaDataArray);
                SqlContext.Pipe.SendResultsStart(record);
                flag = true;
            }

            if (flag)
            {
                for (int ordinal1 = 0; ordinal1 < sqlDataReader.FieldCount; ++ordinal1)
                {
                    int ordinal2 = array.IndexOf((object)sqlDataReader.GetName(ordinal1).ToUpper());
                    if (ordinal2 >= 0)
                    {
                        if (sqlDataReader.IsDBNull(ordinal1))
                            record.SetDBNull(ordinal2);
                        else
                            record.SetValue(ordinal2, sqlDataReader.GetValue(ordinal1));
                    }
                }
                SqlContext.Pipe.SendResultsRow(record);
            }
        }
        sqlDataReader.Close();
        if (flag)
            SqlContext.Pipe.SendResultsEnd();
        connection.Close();
    }

  [SqlProcedure]
  public static void ConstructMiningCondition(SqlInt32 UserQuery_id, SqlString SQLStatement, out SqlString SQLCondition)
  {
    SQLCondition = SqlString.Null;
    string str = "";
    SqlConnection connection = new SqlConnection("context connection=true");
    connection.Open();
    ArrayList arrayList = new ArrayList();
    SqlCommand sqlCommand = new SqlCommand(
        "SELECT b.ObjectName FROM UserQueryObjects as a with (nolock) JOIN ReporterObjects as b with (nolock) ON a.ReporterObject_id = b.tid JOIN ReporterDimensions as c with (nolock) ON b.ReporterDimension_id = c.tid WHERE a.UserQuery_id = " + 
        UserQuery_id.ToString(), connection);
    SqlDataReader sqlDataReader1 = sqlCommand.ExecuteReader();
    while (sqlDataReader1.Read())
      arrayList.Add((object) sqlDataReader1.GetString(0));
    sqlDataReader1.Close();
    sqlCommand.CommandText = SQLStatement.ToString();
    SqlDataReader sqlDataReader2 = sqlCommand.ExecuteReader();
    while (sqlDataReader2.Read())
    {
      for (int index1 = 0; index1 < arrayList.Count; ++index1)
      {
        string index2 = arrayList[index1].ToString();
        if (str.Length > 0)
          str += " AND ";
                str = str + "{" + index2 + "} = '" + sqlDataReader2[index2].ToString() + "'";
      }
    }
    sqlDataReader2.Close();
    SQLCondition = (SqlString) str;
    connection.Close();
  }

  [SqlProcedure]
  public static void RowAsRecordset(SqlString SQLStatement)
  {
    SqlDataRecord record = new SqlDataRecord(new SqlMetaData[2]
    {
      new SqlMetaData("FieldName", SqlDbType.NVarChar, 200L, 1033L, SqlCompareOptions.None),
      new SqlMetaData("FieldValue", SqlDbType.NVarChar, 2000L, 1033L, SqlCompareOptions.None)
    });
    SqlContext.Pipe.SendResultsStart(record);
    using (SqlConnection connection = new SqlConnection("context connection=true"))
    {
      connection.Open();
      using (SqlCommand sqlCommand = new SqlCommand(SQLStatement.ToString(), connection))
      {
        using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
        {
          if (sqlDataReader.Read())
          {
            for (int ordinal = 0; ordinal < sqlDataReader.FieldCount; ++ordinal)
            {
              record.SetString(0, sqlDataReader.GetName(ordinal));
              if (sqlDataReader.IsDBNull(ordinal))
                record.SetDBNull(1);
              else
                record.SetString(1, sqlDataReader[ordinal].ToString());
              SqlContext.Pipe.SendResultsRow(record);
            }
          }
        }
      }
    }
    SqlContext.Pipe.SendResultsEnd();
  }

  [SqlProcedure]
  public static void ReportingServicesRequest(SqlString URL, SqlString ReportName, SqlString PrinterName, SqlString ReportParams, SqlInt32 PageLimit, bool IsLandscape, out SqlBoolean IsFinished, SqlInt32 Copies)
  {
    IsFinished = (SqlBoolean) true;
      string str = !IsLandscape ? "0" : "1";
      string requestUriString = URL.ToString() + "?ReportName=" + 
            ReportName.ToString() + "&PrinterName=" + 
            PrinterName.ToString() + "&ReportParams=" + 
            ReportParams.ToString() + "&IsLandscape=" + 
            str + "&Copies=" + Copies.ToString();
      if (PageLimit != (SqlInt32) (-1))
        requestUriString = requestUriString + "&PageLimit=" + PageLimit.ToString();
      try
      {
        WebRequest webRequest = WebRequest.Create(requestUriString);
        webRequest.Credentials = CredentialCache.DefaultCredentials;
        webRequest.GetResponse().Close();
      }
      catch (Exception ex)
      {
        SqlContext.Pipe.Send(ex.Message);
        IsFinished = (SqlBoolean) false;
      }
    
  }

  [SqlProcedure]
  public static void GetUserQueryAdvanced(SqlInt32 UserQuery_id, SqlInt32 TopRows, SqlInt32 ReporterFilter_id, out SqlInt32 TotalRows, out SqlString SQLStatement, SqlString DirectWhere, SqlBoolean IsIgnoreGrouping)
  {
    TotalRows = SqlInt32.Null;
    SQLStatement = SqlString.Null;
    SqlConnection sqlConnection = new SqlConnection("context connection=true");
    sqlConnection.Open();
    SQLStatement = !("DONTLOOK" == "DONTLOOK") ? (SqlString) "" : (SqlString) new DataPathConstructor()
    {
      ThisConnection = sqlConnection,
      LimitRows = (int) TopRows
    }.ConstructSQLFromQuery((int) UserQuery_id, (int) ReporterFilter_id, DirectWhere.ToString(), IsIgnoreGrouping);
    sqlConnection.Close();
    TotalRows = (SqlInt32)LEADEngine.CountRowsForSQL_Inner(SQLStatement.ToString());
  }

  [SqlFunction]
  public static SqlString ReplicateValue(SqlString Value, SqlString Prefix, SqlInt32 Length)
  {
    string str1 = "";
    string str2 = Prefix.ToString();
    string str3 = !Value.IsNull ? Value.ToString() : "";
    int length1 = str3.Length;
    int length2 = (int) Length;
    if (length1 > length2)
      str1 = str3.Substring(0, length2);
    else if (length1 == length2)
      str1 = str3;
    else if (length1 < length2)
    {
      string str4 = "";
      for (int index = 0; index < length2 - length1; ++index)
        str4 += str2;
      str1 = str4 + str3;
    }
    return (SqlString) str1;
  }

  [SqlFunction]
  public static SqlBoolean CompareFirstWords(SqlString FirstString, SqlString SecondString)
  {
    bool flag;

      string str1 = FirstString.ToString();
      if (str1.IndexOf(" ") > -1)
        str1 = str1.Substring(0, str1.IndexOf(" "));
      string str2 = SecondString.ToString();
      if (str2.IndexOf(" ") > -1)
        str2 = str2.Substring(0, str2.IndexOf(" "));
      flag = str1 == str2;

    return new SqlBoolean(flag);
  }

  public static byte[] Encrypt(byte[] clearData, byte[] Key, byte[] IV)
  {
    MemoryStream memoryStream = new MemoryStream();
    Rijndael rijndael = Rijndael.Create();
    rijndael.Key = Key;
    rijndael.IV = IV;
    CryptoStream cryptoStream = new CryptoStream((Stream) memoryStream, rijndael.CreateEncryptor(), CryptoStreamMode.Write);
    cryptoStream.Write(clearData, 0, clearData.Length);
    cryptoStream.Close();
    return memoryStream.ToArray();
  }

  public static string Encrypt(string clearText, string Password)
  {
    byte[] bytes = Encoding.Unicode.GetBytes(clearText);
    PasswordDeriveBytes passwordDeriveBytes = new PasswordDeriveBytes(Password, new byte[13]
    {
      (byte) 73,
      (byte) 118,
      (byte) 97,
      (byte) 110,
      (byte) 32,
      (byte) 77,
      (byte) 101,
      (byte) 100,
      (byte) 118,
      (byte) 101,
      (byte) 100,
      (byte) 101,
      (byte) 118
    });
    return Convert.ToBase64String(LEADEngine.Encrypt(bytes, passwordDeriveBytes.GetBytes(32), passwordDeriveBytes.GetBytes(16)));
  }

  public static byte[] Encrypt(byte[] clearData, string Password)
  {
    PasswordDeriveBytes passwordDeriveBytes = new PasswordDeriveBytes(Password, new byte[13]
    {
      (byte) 73,
      (byte) 118,
      (byte) 97,
      (byte) 110,
      (byte) 32,
      (byte) 77,
      (byte) 101,
      (byte) 100,
      (byte) 118,
      (byte) 101,
      (byte) 100,
      (byte) 101,
      (byte) 118
    });
    return LEADEngine.Encrypt(clearData, passwordDeriveBytes.GetBytes(32), passwordDeriveBytes.GetBytes(16));
  }

  public static byte[] Decrypt(byte[] cipherData, byte[] Key, byte[] IV)
  {
    MemoryStream memoryStream = new MemoryStream();
    Rijndael rijndael = Rijndael.Create();
    rijndael.Key = Key;
    rijndael.IV = IV;
    CryptoStream cryptoStream = new CryptoStream((Stream) memoryStream, rijndael.CreateDecryptor(), CryptoStreamMode.Write);
    cryptoStream.Write(cipherData, 0, cipherData.Length);
    cryptoStream.Close();
    return memoryStream.ToArray();
  }

  public static string Decrypt(string cipherText, string Password)
  {
    byte[] cipherData = Convert.FromBase64String(cipherText);
    PasswordDeriveBytes passwordDeriveBytes = new PasswordDeriveBytes(Password, new byte[13]
    {
      (byte) 73,
      (byte) 118,
      (byte) 97,
      (byte) 110,
      (byte) 32,
      (byte) 77,
      (byte) 101,
      (byte) 100,
      (byte) 118,
      (byte) 101,
      (byte) 100,
      (byte) 101,
      (byte) 118
    });
    return Encoding.Unicode.GetString(LEADEngine.Decrypt(cipherData, passwordDeriveBytes.GetBytes(32), passwordDeriveBytes.GetBytes(16)));
  }

  public static byte[] Decrypt(byte[] cipherData, string Password)
  {
    PasswordDeriveBytes passwordDeriveBytes = new PasswordDeriveBytes(Password, new byte[13]
    {
      (byte) 73,
      (byte) 118,
      (byte) 97,
      (byte) 110,
      (byte) 32,
      (byte) 77,
      (byte) 101,
      (byte) 100,
      (byte) 118,
      (byte) 101,
      (byte) 100,
      (byte) 101,
      (byte) 118
    });
    return LEADEngine.Decrypt(cipherData, passwordDeriveBytes.GetBytes(32), passwordDeriveBytes.GetBytes(16));
  }

  private struct Struct2
  {
    public SqlString sqlString_0;
    public SqlInt32 sqlInt32_0;
    public SqlString sqlString_1;
    public SqlString sqlString_2;
    public SqlInt32 sqlInt32_1;
    public bool bool_0;
    public bool bool_1;
  }

  private struct Struct3
  {
    public int int_0;
    public int int_1;
    public int int_2;
    public string string_0;
    public string string_1;
    public string string_2;
  }

  private struct Struct4
  {
    public string string_0;
    public int int_0;
  }

  //private struct Struct5
  //{
  //  public string string_0;
  //  public bool bool_0;
  //}
}

