﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Threading;
using System.Web;

namespace server
{
    public partial class Form1 : Form
    {
        static HttpListener _httpListener = new HttpListener();

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Console.WriteLine("Starting server...");
            //_httpListener.Prefixes.Add("http://localhost:5000/"); 
            _httpListener.Prefixes.Add("http://192.168.1.207:5000/"); 
            _httpListener.Start(); // start server (Run application as Administrator!)
            //Console.WriteLine("Server started.");
            Thread _responseThread = new Thread(ResponseThread);
            _responseThread.Start(); // start the response thread
        }

        static void ResponseThread()
        {
            while (true)
            {
                HttpListenerContext context = _httpListener.GetContext();
                HttpListenerRequest request = context.Request;

                String raw_url = request.RawUrl;
                Console.WriteLine(raw_url);

                var queryParams = HttpUtility.ParseQueryString(raw_url);
                //String PDTBridge_answer = PDTBridge.Execute(queryParams);

                //Console.WriteLine(PDTBridge_answer);

                byte[] _responseArray = Encoding.UTF8.GetBytes(raw_url); // get the bytes to response
                context.Response.OutputStream.Write(_responseArray, 0, _responseArray.Length); // write bytes to the output stream
                context.Response.KeepAlive = false; // set the KeepAlive bool to false
                context.Response.Close(); // close the connection
                Console.WriteLine("Respone given to a request.");
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
