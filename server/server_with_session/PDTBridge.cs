using System;
using System.Data;
using System.Data.SqlClient;
using System.Web;

public static class PDTBridge
{
    public static string Execute(Uri Query, String ConnectionString)
    {
        if (Query.Query.Length == 0)
        {
            return "swd c# wms dll 0.7";
        }

        string var_terminal = PDTBridge.Unquote(HttpUtility.ParseQueryString(Query.Query).Get("Terminal"));
        string var_marker   = PDTBridge.Unquote(HttpUtility.ParseQueryString(Query.Query).Get("Marker"));
        string var_method   = PDTBridge.Unquote(HttpUtility.ParseQueryString(Query.Query).Get("Method"));
        string var_name     = PDTBridge.Unquote(HttpUtility.ParseQueryString(Query.Query).Get("Name"));
        string var_value    = PDTBridge.Unquote(HttpUtility.ParseQueryString(Query.Query).Get("Value"));
        //string var_wait     = PDTBridge.Unquote(HttpUtility.ParseQueryString(Query.Query).Get("Wait"));

        //string var_terminal     = PDTBridge.Unquote(Query["Terminal"]);
        //string var_marker       = PDTBridge.Unquote(Query["Marker"]);
        //string var_method       = PDTBridge.Unquote(Query["Method"]);
        //string var_name         = PDTBridge.Unquote(Query["Name"]);
        //string var_value        = PDTBridge.Unquote(Query["Value"]);
        //string var_wait         = PDTBridge.Unquote(Query["Wait"]);

        if (var_method.Trim().Equals("")) {
            return string.Format("MARKER\t{0}\vERROR\t{1}\f{2}", var_marker, -1, string.Empty);
        }
        
        if (var_method == "SET")
        {
            if (var_name.Length == 0)
            {
                return string.Format("MARKER\t{0}\vERROR\t{1}\f{2}", var_marker, -1, string.Empty);
            }
        }

        SqlConnection sqlConnection = null;
        try
        {
            sqlConnection = new SqlConnection(ConnectionString);
            sqlConnection.Open();
        }
        catch
        {
            sqlConnection.Close();
            return string.Format("MARKER\t{0}\vERROR\t{1}\f{2}", var_marker, -2, string.Empty);
        }

        string arg = string.Empty;

        int num = 0;

        if (var_method == "SET")
        {
            PDTBridge.SetVariable(sqlConnection, var_terminal, var_name, var_value, out num);
            sqlConnection.Close();
            return string.Format("MARKER\t{0}\vERROR\t{1}\f{2}", var_marker, 0, arg);
        }

        if (var_method == "TIMER")
        {
            arg = PDTBridge.NextProcedure(sqlConnection, var_terminal, var_marker, true, out num);
            sqlConnection.Close();
            return string.Format("MARKER\t{0}\vERROR\t{1}\f{2}", var_marker, 0, arg);
        }

        if (var_method == "NEXT")
        {
            arg = PDTBridge.NextProcedure(sqlConnection, var_terminal, var_marker, false, out num);
            sqlConnection.Close();
            return string.Format("MARKER\t{0}\vERROR\t{1}\f{2}", var_marker, 0, arg);
        }

        //���� �� ���� �� ������� �� ���������
        num = -2;
        sqlConnection.Close();
        return string.Format("MARKER\t{0}\vERROR\t{1}\f{2}", var_marker, num, string.Empty);
    }


    private static string Unquote(string s)
    {
        if (s == null) return "";
        return s.Trim('"');
    }

    private static string NextProcedure(SqlConnection conn, string var_terminal, string var_marker, bool var_timer, out int ErrorCode)
    {
        ErrorCode = 0;

        SqlCommand sqlCommand = new SqlCommand("pdt_PDTClient_Check", conn);
        sqlCommand.CommandType = CommandType.StoredProcedure;

        sqlCommand.Parameters.Add("@TerminalAddress", SqlDbType.VarChar, 200).Value = var_terminal;
        sqlCommand.Parameters.Add("@Marker", SqlDbType.VarChar, 200).Value = var_marker;

        SqlParameter param_Session_id = sqlCommand.Parameters.Add("@Session_id", SqlDbType.Int);
        param_Session_id.Direction = ParameterDirection.Output;

        SqlParameter param_CurrProcedure = sqlCommand.Parameters.Add("@CurrProcedure", SqlDbType.VarChar, 200);
        param_CurrProcedure.Direction = ParameterDirection.Output;

        SqlParameter param_NextProcedure = sqlCommand.Parameters.Add("@NextProcedure", SqlDbType.VarChar, 200);
        param_NextProcedure.Direction = ParameterDirection.Output;

        SqlParameter param_TimerProcedure = sqlCommand.Parameters.Add("@TimerProcedure", SqlDbType.VarChar, 200);
        param_TimerProcedure.Direction = ParameterDirection.Output;

        SqlParameter param_Response = sqlCommand.Parameters.Add("@Response", SqlDbType.VarChar, -1);
        param_Response.Direction = ParameterDirection.Output;

        SqlParameter param_WatchdogCounter = sqlCommand.Parameters.Add("@WatchdogCounter", SqlDbType.Int);
        param_WatchdogCounter.Direction = ParameterDirection.Output;

        int     var_Session_id          = -1;
        string  var_CurrProcedure       = "";
        string  var_NextProcedure       = "pdt_Main";
        string  var_TimerProcedure      = "";
        int     var_WatchdogCounter     = 0;
        string  string_Response         = ""; 

        try
        {
            //sqlCommand.CommandTimeout = Timeout1;
            sqlCommand.ExecuteNonQuery();

            if (!Convert.IsDBNull(param_Session_id.Value))
            {
                var_Session_id = (int)param_Session_id.Value;
            }

            if (!Convert.IsDBNull(param_CurrProcedure.Value))
            {
                var_CurrProcedure = (string)param_CurrProcedure.Value;
            }

            if (!Convert.IsDBNull(param_NextProcedure.Value))
            {
                var_NextProcedure = (string)param_NextProcedure.Value;
            }

            if (!Convert.IsDBNull(param_TimerProcedure.Value))
            {
                var_TimerProcedure = (string)param_TimerProcedure.Value;
            }

            if (!Convert.IsDBNull(param_WatchdogCounter.Value))
            {
                var_WatchdogCounter = (int)param_WatchdogCounter.Value;
            }

            string_Response = string_Response + param_Response.Value;

            if (var_NextProcedure.Length == 0)
            {
                var_Session_id = -1;
                var_NextProcedure = "pdt_Main";
            }

            if (var_TimerProcedure.Length == 0)
            {
                var_TimerProcedure = var_NextProcedure;
            }
        }
        catch (SqlException ex)
        {
            ErrorCode = ((ex.Number == -2) ? -4 : -3);
            return "";
        }
        catch
        {
            ErrorCode = -3;
            return "";
        }

        //���� ������ ��������� �����
        if (string_Response.Length > 0)
        {
            return string_Response.ToString();
        }
        
        var_CurrProcedure = (var_timer ? var_TimerProcedure : var_NextProcedure);

        sqlCommand = new SqlCommand("pdt_PDTClient_Next", conn);
        sqlCommand.CommandType = CommandType.StoredProcedure;
        sqlCommand.Parameters.Add("@TerminalAddress", SqlDbType.VarChar, 200).Value = var_terminal;

        if (var_Session_id == -1)
        {
            sqlCommand.Parameters.Add("@Session_id", SqlDbType.Int).Value = DBNull.Value;
        }
        else
        {
            sqlCommand.Parameters.Add("@Session_id", SqlDbType.Int).Value = var_Session_id;
        }

        sqlCommand.Parameters.Add("@ProcedureName", SqlDbType.VarChar, 200).Value = var_CurrProcedure;

        SqlDataReader sqlDataReader = null;

        try
        {
            //sqlCommand.CommandTimeout = Timeout2;
            sqlDataReader = sqlCommand.ExecuteReader();

            var_NextProcedure = "pdt_Main";
            var_TimerProcedure = "";

            while (true)
            {
                if (!sqlDataReader.Read())
                {
                    if (!sqlDataReader.NextResult())
                    {
                        break;
                    }
                }
                else
                {
                    for (int i = 0; i < sqlDataReader.FieldCount; i++)
                    {
                        string str_key      = sqlDataReader.GetName(i).Replace("\t", " ").Replace("\v", "").Replace("\f", "");
                        string str_value    = sqlDataReader[i].ToString().Replace("\t", " ").Replace("\v", "").Replace("\f", "");

                        if (str_key.Length == 0 || str_key == null)
                        {
                            continue;
                        }

                        if (str_value.Length > 0 && sqlDataReader.GetFieldType(i).IsPrimitive)
                        {
                            str_value = str_value.Replace(',', '.');
                        }
                            
                        if (str_key == "ERRORCODE")
                        {
                            ErrorCode = (int)sqlDataReader[i];
                            continue;
                        }

                        if (str_key == "SESSION")
                        {
                            var_Session_id = -1;
                            if (str_value.Length > 0)
                            {
                                var_Session_id = (int)sqlDataReader[i];
                                continue;
                            }
                            continue;
                        }

                        if (str_key == "NEXTPROC")
                        {
                            if (str_value.Length > 0)
                            {
                                var_NextProcedure = str_value;
                                continue;
                            }
                            continue;
                        }

                        if (str_key == "TIMER")
                        {
                            if (str_value.Length > 0)
                            {
                                var_TimerProcedure = str_value;
                                continue;
                            }
                            continue;
                        }

                        if (str_key == "COMMAND")
                        {
                            if (str_value != null && str_value == "CLEARSESSION")
                            {
                                var_Session_id = -1;            
                                continue;
                            }
                            continue;
                        }

                        if (string_Response.Length > 0)
                        {
                                string_Response = string_Response + '\v';
                        }
                        string_Response = string_Response + String.Format("{0}\t{1}", str_key, str_value);
                        }
                        
                    }
                }
            }
            catch (SqlException ex2)
            {
                ErrorCode = ((ex2.Number == -2) ? -4 : -3);
                return "";
            }
            catch
            {
                ErrorCode = -3;
                return "";
            }
            finally
            {
                if (sqlDataReader != null)
                {
                    sqlDataReader.Close();
                }
            }
            if (string_Response.Length > 0)
            {
                var_WatchdogCounter = 0;
            }
            else
            {
                var_WatchdogCounter++;
            }
            if (var_WatchdogCounter > 100)
            {
                ErrorCode = -5;
                return "";
            }
            
            if (var_NextProcedure == "pdt_Main")
            {
                if (var_Session_id != -1)
                {
                    sqlCommand = new SqlCommand("lxa_CloseSession", conn);
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.Add("@Session_id", SqlDbType.Int).Value = var_Session_id;
                    try
                    {
                        //sqlCommand.CommandTimeout = Timeout1;
                        sqlCommand.ExecuteNonQuery();
                    }
                    catch
                    {
                    }
                }
            var_Session_id = -1;
            }

            sqlCommand = new SqlCommand("pdt_PDTClient_Adjust", conn);
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.Parameters.Add("@TerminalAddress", SqlDbType.VarChar, 200).Value = var_terminal;
            sqlCommand.Parameters.Add("@Marker", SqlDbType.VarChar, 200).Value = var_marker;

            if (var_CurrProcedure == "pdt_CheckPersonalBarcode" && var_Session_id == -1 && string_Response.Length == 0)
            {
                sqlCommand.Parameters.Add("@Session_id", SqlDbType.Int).Value = DBNull.Value;
                sqlCommand.Parameters.Add("@CurrProcedure", SqlDbType.VarChar, 200).Value = DBNull.Value;
                sqlCommand.Parameters.Add("@NextProcedure", SqlDbType.VarChar, 200).Value = DBNull.Value;
                sqlCommand.Parameters.Add("@TimerProcedure", SqlDbType.VarChar, 200).Value = DBNull.Value;
                sqlCommand.Parameters.Add("@Response", SqlDbType.VarChar, -1).Value = string.Empty;
                sqlCommand.Parameters.Add("@WatchdogCounter", SqlDbType.Int).Value = 0;
                string_Response = "COMMAND\tEXIT";
            }
            else
            {
                if (var_Session_id == -1)
                {
                    sqlCommand.Parameters.Add("@Session_id", SqlDbType.Int).Value = DBNull.Value;
                }
                else
                {
                    sqlCommand.Parameters.Add("@Session_id", SqlDbType.Int).Value = var_Session_id;
                }
                sqlCommand.Parameters.Add("@CurrProcedure", SqlDbType.VarChar, 200).Value = var_CurrProcedure;
                sqlCommand.Parameters.Add("@NextProcedure", SqlDbType.VarChar, 200).Value = var_NextProcedure;
                if (var_TimerProcedure.Length == 0)
                {
                    sqlCommand.Parameters.Add("@TimerProcedure", SqlDbType.VarChar, 200).Value = DBNull.Value;
                }
                else
                {
                    sqlCommand.Parameters.Add("@TimerProcedure", SqlDbType.VarChar, 200).Value = var_TimerProcedure;
                }
                sqlCommand.Parameters.Add("@Response", SqlDbType.VarChar, -1).Value = string_Response;
                sqlCommand.Parameters.Add("@WatchdogCounter", SqlDbType.Int).Value = var_WatchdogCounter;
            }
            try
            {
                //sqlCommand.CommandTimeout = Timeout1;
                sqlCommand.ExecuteNonQuery();
            }
            catch (SqlException ex3)
            {
                ErrorCode = ((ex3.Number == -2) ? -4 : -3);
                return "";
            }
            catch
            {
                ErrorCode = -3;
                return "";
            }
            
        return string_Response;
    }


    private static void SetVariable(SqlConnection conn, string var_terminal, string var_name, string var_value, out int var_errorCode)
    {
        var_errorCode = 0;
        SqlCommand sqlCommand = new SqlCommand("pdt_SetTerminalVariable", conn);
        sqlCommand.CommandType = CommandType.StoredProcedure;
        sqlCommand.Parameters.Add("@TerminalAddress", SqlDbType.VarChar, 200).Value = var_terminal;
        sqlCommand.Parameters.Add("@VariableName", SqlDbType.VarChar, 200).Value = var_name;
        if (var_value == "\0")
        {
            sqlCommand.Parameters.Add("@VariableValue", SqlDbType.VarChar, 200).Value = DBNull.Value;
        }
        else
        {
            sqlCommand.Parameters.Add("@VariableValue", SqlDbType.VarChar, 200).Value = var_value;
        }
        try
        {
            //sqlCommand.CommandTimeout = var_timeout1;
            sqlCommand.ExecuteNonQuery();
        }
        catch (SqlException ex)
        {
            var_errorCode = ((ex.Number == -2) ? -4 : -3);
        }
        catch
        {
            var_errorCode = -3;
        }
    }
    

}
