﻿using System;
using System.Text;
using System.Windows.Forms;
using System.Net;

namespace server
{
    public partial class Main : Form
    {
        static HttpListener _httpListener = new HttpListener();
        static String ConnectionString;
        //HttpListenerTimeoutManager manager;

        public Main()
        {
            InitializeComponent();
        }

        private static void OnContext(IAsyncResult ar)
        {
            var ctx = _httpListener.EndGetContext(ar);
            _httpListener.BeginGetContext(OnContext, null);
            HttpListenerRequest loc_request = ctx.Request;

            ctx.Response.ContentType = "text/plain; charset=utf-8";
            String raw_url = loc_request.Url.ToString();
            Uri myUri = new Uri(raw_url);
            try
            {
                String PDTBridge_answer = PDTBridge.Execute(myUri, ConnectionString);
                //String PDTBridge_answer = null;
                byte[] _responseArray = Encoding.UTF8.GetBytes(PDTBridge_answer);
                ctx.Response.OutputStream.Write(_responseArray, 0, _responseArray.Length);
            }
            catch (Exception e) { 
            
            }
            ctx.Response.OutputStream.Close();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            ConnectionString = String.Concat(
                "Data Source=" , tbServer.Text, ";",
                "Initial Catalog=", tbDatabase.Text , ";", 
                "User id=", tbUser.Text,";" ,
                "Password=",tbPassword.Text,";");

            if (!_httpListener.IsListening)
            {
                _httpListener.Prefixes.Add("http://*:" + tbPort.Value.ToString() + "/");
                _httpListener.Start(); 
                _httpListener.BeginGetContext(OnContext, null);
                MessageBox.Show("Сервер запущен", "WMS", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
