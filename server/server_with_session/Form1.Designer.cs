﻿namespace server
{
    partial class Main
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.caption_server = new System.Windows.Forms.Label();
            this.tbServer = new System.Windows.Forms.TextBox();
            this.tbPassword = new System.Windows.Forms.MaskedTextBox();
            this.tbUser = new System.Windows.Forms.TextBox();
            this.tbDatabase = new System.Windows.Forms.TextBox();
            this.caption_password = new System.Windows.Forms.Label();
            this.caption_user = new System.Windows.Forms.Label();
            this.caption_database = new System.Windows.Forms.Label();
            this.tbPort = new System.Windows.Forms.NumericUpDown();
            this.caption_port = new System.Windows.Forms.Label();
            this.btnStart = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbPort)).BeginInit();
            this.SuspendLayout();
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.55264F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 66.44736F));
            this.tableLayoutPanel1.Controls.Add(this.caption_server, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tbServer, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.tbPassword, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.tbUser, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.tbDatabase, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.caption_password, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.caption_user, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.caption_database, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.tbPort, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.caption_port, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.btnStart, 1, 5);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 6;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(304, 245);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // caption_server
            // 
            this.caption_server.AutoSize = true;
            this.caption_server.Dock = System.Windows.Forms.DockStyle.Fill;
            this.caption_server.Location = new System.Drawing.Point(3, 0);
            this.caption_server.Name = "caption_server";
            this.caption_server.Size = new System.Drawing.Size(96, 40);
            this.caption_server.TabIndex = 0;
            this.caption_server.Text = "IP сервера";
            // 
            // tbServer
            // 
            this.tbServer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbServer.Location = new System.Drawing.Point(105, 3);
            this.tbServer.Name = "tbServer";
            this.tbServer.Size = new System.Drawing.Size(196, 20);
            this.tbServer.TabIndex = 1;
            this.tbServer.Text = "192.168.4.11";
            // 
            // tbPassword
            // 
            this.tbPassword.Location = new System.Drawing.Point(105, 163);
            this.tbPassword.Name = "tbPassword";
            this.tbPassword.PasswordChar = '*';
            this.tbPassword.Size = new System.Drawing.Size(196, 20);
            this.tbPassword.TabIndex = 7;
            this.tbPassword.Text = "123456Qaz";
            // 
            // tbUser
            // 
            this.tbUser.Location = new System.Drawing.Point(105, 123);
            this.tbUser.Name = "tbUser";
            this.tbUser.Size = new System.Drawing.Size(196, 20);
            this.tbUser.TabIndex = 5;
            this.tbUser.Text = "swd";
            // 
            // tbDatabase
            // 
            this.tbDatabase.Location = new System.Drawing.Point(105, 83);
            this.tbDatabase.Name = "tbDatabase";
            this.tbDatabase.Size = new System.Drawing.Size(196, 20);
            this.tbDatabase.TabIndex = 3;
            this.tbDatabase.Text = "AMANAT";
            // 
            // caption_password
            // 
            this.caption_password.AutoSize = true;
            this.caption_password.Dock = System.Windows.Forms.DockStyle.Fill;
            this.caption_password.Location = new System.Drawing.Point(3, 160);
            this.caption_password.Name = "caption_password";
            this.caption_password.Size = new System.Drawing.Size(96, 40);
            this.caption_password.TabIndex = 6;
            this.caption_password.Text = "Пароль";
            // 
            // caption_user
            // 
            this.caption_user.AutoSize = true;
            this.caption_user.Dock = System.Windows.Forms.DockStyle.Fill;
            this.caption_user.Location = new System.Drawing.Point(3, 120);
            this.caption_user.Name = "caption_user";
            this.caption_user.Size = new System.Drawing.Size(96, 40);
            this.caption_user.TabIndex = 4;
            this.caption_user.Text = "Пользователь";
            // 
            // caption_database
            // 
            this.caption_database.AutoSize = true;
            this.caption_database.Dock = System.Windows.Forms.DockStyle.Fill;
            this.caption_database.Location = new System.Drawing.Point(3, 80);
            this.caption_database.Name = "caption_database";
            this.caption_database.Size = new System.Drawing.Size(96, 40);
            this.caption_database.TabIndex = 2;
            this.caption_database.Text = "Каталог базы";
            // 
            // tbPort
            // 
            this.tbPort.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbPort.Location = new System.Drawing.Point(105, 43);
            this.tbPort.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.tbPort.Name = "tbPort";
            this.tbPort.Size = new System.Drawing.Size(196, 20);
            this.tbPort.TabIndex = 8;
            this.tbPort.Value = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            // 
            // caption_port
            // 
            this.caption_port.AutoSize = true;
            this.caption_port.Dock = System.Windows.Forms.DockStyle.Fill;
            this.caption_port.Location = new System.Drawing.Point(3, 40);
            this.caption_port.Name = "caption_port";
            this.caption_port.Size = new System.Drawing.Size(96, 40);
            this.caption_port.TabIndex = 9;
            this.caption_port.Text = "Порт";
            // 
            // btnStart
            // 
            this.btnStart.BackColor = System.Drawing.Color.Lime;
            this.btnStart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnStart.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnStart.Location = new System.Drawing.Point(105, 203);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(196, 39);
            this.btnStart.TabIndex = 10;
            this.btnStart.Text = "Старт";
            this.btnStart.UseVisualStyleBackColor = false;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(304, 245);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Main";
            this.Text = "Терминальный сервер WMS";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbPort)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label caption_server;
        private System.Windows.Forms.TextBox tbServer;
        private System.Windows.Forms.TextBox tbDatabase;
        private System.Windows.Forms.Label caption_database;
        private System.Windows.Forms.TextBox tbUser;
        private System.Windows.Forms.Label caption_user;
        private System.Windows.Forms.Label caption_password;
        private System.Windows.Forms.MaskedTextBox tbPassword;
        private System.Windows.Forms.NumericUpDown tbPort;
        private System.Windows.Forms.Label caption_port;
        private System.Windows.Forms.Button btnStart;
    }
}

